<?php

/**
 * Description of MailAccounts
 *
 * @author kubrey
 */
class MailAccounts extends BaseModel
{

    public function getId()
    {
        return "id";
    }

    public function getTableName()
    {
        return "mail_accounts";
    }

    /**
     *
     * @return boolean|array
     */
    public function getDefaultAcc()
    {
        $default = "info";
        return $this->getAccountByName($default);
    }

    /**
     *
     * @param array $data
     * @return boolean
     */
    public function create(array $data)
    {
        $enc = new Encrypter;
        $data['password'] = (isset($data['password']) ? $enc->encrypt($data['password']) : "");
        return $this->insert($data);
    }

    /**
     *
     * @param string $name
     * @return boolean|array
     */
    public function getAccountByName($name)
    {
        $enc = new Encrypter;
        $acc = $this->findOne("name=?", ['*'], array($name));
        if (!$acc) {
            return false;
        }
        if ($acc['password']) {
            $acc['password'] = $enc->decrypt($acc['password']);
        }

        return $acc;
    }

    /**
     *
     * @param int $id
     * @return boolean|array
     */
    public function getAccountById($id)
    {
        $enc = new Encrypter;
        $acc = $this->findOne("id=?", array(), array($id));
        if (!$acc) {
            return false;
        }
        if ($acc['password']) {
            $acc['password'] = $enc->decrypt($acc['password']);
        }

        return $acc;
    }

}
