<?php

class TemplatesWidget extends WidgetCrud
{

    private $_table = "mailing_tpl";

    /**
     *
     */
    public function init()
    {
        $this->tableName="mailing_tpl";
        $this->primaryKey="idTpl";
    }

    /**
     * @return $this
     */
    public function GetContent()
    {
        $query = new QuerySimple();
        $query->Select(array($this->_table => array("*")));
        $query->AddFilter("isSystem = 1");

        $title = "System templates";

        $this->setProperty("title", $title)
            ->SetDefaultOrder("dateUpdate", false)
            ->Add($query)
            ->Add(new ActionCheckboxMultiple("Status", array("field" => "isActive", "Unfolded" => false), array(
                array("data" => "1", "equal" => "1", "title" => "not hidden"),
                array("data" => "0", "equal" => "0", "title" => "hidden")
            ), new RuleCheckboxEqual("isActive")))
            ->Add(new ButtonAppend(NULL, "Add new"));

        $table = new Table(
            new TableColumnLogicIcon("isSystem", "S", 22, "fa-lock"),
            new TableColumn("idTpl", "ID", 40, "center"), //
            new TableColumn("label", "Label"), //
            new TableColumn("hSender", "Sender"), //
            new TableColumn("hSubject", "Subject"), //

            new TableColumn("code", "TPL code"), //
            new TableColumnDateCreate(), //
            new TableColumnDateUpdate(), ///
            new ButtonClone(NULL, NULL), //
            new ButtonEdit(NULL, NULL), //
            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(), new ButtonRule("0", "isActive", "equally")),
            new ButtonDelete()
        );

        $table->SetPrimary("idTpl");
        $this->Add($table);

        return $this;
    }

    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormStatic("Letter template");
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $gr = new GrouperTab("Head", NULL, array("heightEnable" => false));
        $gr->Add(new InputText("Label", "label", array("placeholder" => "sample: My letter")));
        $gr->Add(new InputText("Template code", "code", array("placeholder" => "sample: {{option_test}}")));
        $gr->Add(new InputText("From", "hFrom", array("placeholder" => "sample: payment")));
        $gr->Add(new InputText("From name", "hFromName", array("placeholder" => "sample: Sitename.com")));
        $gr->Add(new InputText("Subject", "hSubject", array("placeholder" => "sample: Sitename.com - Notification")));
        $gr->Add(new InputText("Sender", "hSender", array("placeholder" => "sample: payment@Sitename.com")));
        $gr->Add(new InputText("Host", "hHost", array("placeholder" => "sample: Sitename.com")));
        $form->Add($gr);

        $gr = new GrouperTab("Body", NULL, array("heightEnable" => false, "onStart" => "tabOnShow[1] = scInitials;"));
        $sc = new InputSc("Content", "content", array("titleDisable" => true, "height" => 400, "init" => false));
        $gr->Add($sc);
        $form->Add($gr);

        $form->Add(new ButtonAdd(NULL, NULL, array("script" => "JsonAct")), new ButtonCancel(NULL, NULL, array("script" => "DataAct"))
        );
        echo "<script>function scInitials(){ " . $sc->domId . "initEditor();}</script>";
        $form->Draw();

        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $arr_data = $this->getPostDataJson();
        $arr_data['isSystem'] = 1;
        $arr_data['dateUpdate'] = time();
        $arr_data['dateCreate'] = time();

        SDbQuery::table($this->_table)
            ->enableMessages()
            ->insert($arr_data);

    }

    /**
     *
     */
    public function actionUpdateReady()
    {

        $tpl = SDbQuery::table($this->_table)
            ->where(["idTpl" => $this->getPostDataInt()])
            ->select();

        $form = new FormStatic("Letter template", $tpl);
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $gr = new GrouperTab("Head", NULL, array("heightEnable" => false));
        $gr->Add(new InputText("Label", "label", array("placeholder" => "sample: My letter")));
        $gr->Add(new InputText("Template code", "code", array("placeholder" => "sample: {{option_test}}")));
        $gr->Add(new InputText("From", "hFrom", array("placeholder" => "sample: payment")));
        $gr->Add(new InputText("From name", "hFromName", array("placeholder" => "sample: Sitename.com")));
        $gr->Add(new InputText("Subject", "hSubject", array("placeholder" => "sample: Sitename.com - Notification")));
        $gr->Add(new InputText("Sender", "hSender", array("placeholder" => "sample: payment@Sitename.com")));
        $gr->Add(new InputText("Host", "hHost", array("placeholder" => "sample: Sitename.com")));
        $form->Add($gr);

        $gr = new GrouperTab("Body", NULL, array("heightEnable" => false, "onStart" => "tabOnShow[1] = scInitials;"));
        $sc = new InputSc("Content", "content", array("titleDisable" => true, "height" => 400, "init" => false));
        $gr->Add($sc);
        $form->Add($gr);

        $form->Add(
            new InputVar("idTpl", "idTpl"), new ButtonSave(NULL, NULL, array("script" => "JsonAct")),
            new ButtonCancel(NULL, NULL, array("script" => "DataAct"))
        );

        echo "<script>function scInitials(){ " . $sc->domId . "initEditor();}</script>";
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {
        $arr_data = $this->getPostDataJson();
        $arr_data['dateUpdate'] = time();

        SDbQuery::table($this->_table)
            ->enableMessages()
            ->where(["idTpl" => $arr_data["idTpl"]])
            ->update($arr_data);


    }

    /**
     *
     */
    public function actionClone()
    {
        $original = SDbQuery::table($this->_table)
            ->where(["idTpl" => $this->getPostDataInt()])
            ->select();

        $original = current($original);

        unset($original["idTpl"]);
        $original["code"] .= "_clone";
        $original["label"] .= "_clone";

        SDbQuery::table($this->_table)
            ->enableMessages()
            ->insert($original);
    }

    /**
     *
     */
    public function actionPublished()
    {
        SDbQuery::table($this->_table)
            ->enableMessages()
            ->where(["idTpl" => $this->getPostDataInt()])
            ->update(array('isActive' => 1));


    }

    /**
     *
     */
    public function actionUnpublished()
    {
        SDbQuery::table($this->_table)
            ->enableMessages()
            ->where(["idTpl" => $this->getPostDataInt()])
            ->update(array('isActive' => 0));
    }

}
