<?php

/**
 * Class DepartmentsWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class AccountsWidget extends WidgetCrud
{
    protected $accModel;

    /**
     *
     */
    public function init()
    {
        $this->tableName = "mail_accounts";
        $this->primaryKey = "id";

        $this->accModel = new MailAccounts;
    }

    /**
     * @return $this
     */
    public function GetContent()
    {
        $query = new QuerySimple();
        $query->Select([
            $this->tableName => ["*"],

        ]);

        $this->setProperty("title", "Mail accounts list")
            ->SetDefaultOrder($this->primaryKey, false)
            ->Add($query)
            ->Add(new ButtonAppendModal())
            ->Add(new ButtonDeleteTable())
            ->Add(new ActionInputFilter("ID", array("field" => $this->primaryKey)))
            ->Add(new ActionInputFilter("Name", array("field" => "name")));


        $table = new TableMass(

            new TableColumn($this->primaryKey, "ID", 50, "center"),
            new TableColumn("name", "Name", 200),
            new TableColumn("password", "Password"),
            new TableColumnDateUpdate(),
            new TableColumnDateCreate(),
            new ButtonEditModal(),
            new ButtonDelete()
        );

        $table->SetPrimary($this->primaryKey);
        $this->Add($table);

        return $this;
    }

    /**
     * @param FormBase $form
     *
     * @return FromBase
     */
    public function getCrudFormComponents(FormBase $form)
    {
        $id = $this->getPostDataInt();
        if (is_numeric($id) && $id > 0)
            $form->dataArray = [$this->accModel->getAccountById($id)];

        $form->Add(
            new InputText("Name", "name", array("placeholder" => "name")),
            new InputText("Password", "password", array("placeholder" => "password"))
        );

        return $form;

    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function preProcessAdd(array $data = [])
    {
        $enc = new Encrypter();
        $data["password"] = $enc->encrypt($data['password']);
        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function preProcessUpdate(array $data = [])
    {
        $enc = new Encrypter();
        $data["password"] = $enc->encrypt($data['password']);
        return $data;
    }

    /**
     *
     */
    public function postProcessData()
    {
        $enc = new Encrypter();

        if (!empty($this->data))
            foreach ($this->data as &$val) {
                if ($val['password']) {
                    $val['password'] = mb_substr($enc->decrypt($val['password']), 0, -3, "UTF-8") . "***";
                }
            }
    } 

}