<?php

class QueueWidget extends Widget
{

    private $_table = "mailing_queue";

    /**
     *
     */
    public function init()
    {
    }


    /**
     * @return $this
     */
    public function GetContent()
    {

        $query = new QueryMulti();
        $query->Select(array(
            $this->_table => array("*"),
            "mailing_tpl" => array("code")
        ));

        $query->LeftOuterJoin("mailing_tpl", "mailing_tpl.idTpl = {$this->_table}.idTpl");



        $this->setProperty("title", "Mailing queue")
            ->SetDefaultOrder("idQueue", false)
            ->Add($query);

        $table = new Table(
            new TableColumnLogicIconOffOn("isProcessed", "P"), //
            new TableColumn("idQueue", "ID", 60, "center"), //
            new TableColumn("code", "Tpl code", 160, "center"), //
            new TableColumn("email", "E-mail", 160), //
            new TableColumnSpoiler("data", "Data"), //
            new TableColumnDateCreate()
        );

        $table->SetPrimary("idQueue");
        $this->Add(new ActionInputFilter("E-Mail", array("field" => "email"), NULL, new RuleLikeRough("email", $this->_table)));
        $this->Add($table);

        return $this;
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {
        foreach ($this->data as &$val) {
        }
        return $this;
    }


}
