<?php
/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 26.07.2017
 * Time: 12:18
 */

class mailingmodClean extends Clean
{
    public function run() {
        $this->setPack("mod");
        $this->setPackName("mailing");
        $this->setVersion("1.0.6");
        $this->setFiles([
            "mailingClean.php",
        ]);
        $this->setDirectory([
            'component'
        ]);
        $this->fileClean();
    }
}