<?php

/**
 * Class MailTemplate
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class MailTemplate extends BaseTemplate
{

    public $tplCode;
    public $tpl;
    public $lastError;

    protected $accModel;
    protected $isMailer = true;

    protected $bodyWrap;

    /**
     * MailTemplate constructor.
     *
     * @param string $tplCode
     */
    public function __construct($tplCode = "")
    {
        if ($tplCode) {
            $this->tplCode = $tplCode;
            $this->tpl = SDb::row("SELECT * FROM mailing_tpl WHERE code=?", $tplCode);
        }
    }

    /**
     * @param int $idTpl
     *
     * @return array|false|string
     */
    public function loadById($idTpl = 0)
    {

        $this->tpl = SDb::row("SELECT * FROM mailing_tpl WHERE idTpl=?", $idTpl);
        if (!$this->tpl) {
            return "Not template found!";
        }

        $this->tplCode = $this->tpl["code"];
        return $this->tpl;
    }


    /**
     *
     * @param string $to
     * @param boolean $innerRender
     *
     * @return boolean
     */
    public function Send($to, $innerRender = false)
    {

        if (!$this->tpl)
            return false;

        if (!$to) {
            return false;
        }

        if ($innerRender) {
            $msg = $this->ProcessContent();
            $subject = $this->ProcessSubject();
        } else {
            $msg = $this->tpl["content"];
            $subject = strtr($this->tpl["hSubject"], $this->data);
        }

        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->Host = $this->tpl["hHost"];
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = 1;
        $mail->From = $this->tpl["hFrom"];
        if (strpos($mail->From, '@') === false) {
            $mail->From = $this->tpl["hFrom"] . "@" . $mail->Host;
        }
        $acc = $this->getAccModel()->getAccountByName(strtolower($mail->From));

        if (!$acc) {
            $acc = $this->getAccModel()->getDefaultAcc();
        }
        if (!$acc) {
            return false;
        }

        $mail->Password = $acc['password'];
        $mail->Username = $mail->From;
        $mail->FromName = $this->tpl["hFromName"];
        $mail->Subject = $subject;
        $mail->Sender = $this->tpl["hSender"];
        $mail->SingleTo = true;

        $mail->AddAddress($to);

        $body = strtr($msg, $this->data);
        $mail->MsgHTML($body);

        $do = $mail->Send();

        if (!$do) {
            Debug::logMsg("Failed to send new order notification", "mailer.log");
            echo $mail->ErrorInfo;
        }
        return $do;
    }

    /**
     * @param bool $innerRender
     *
     * @return string
     */
    public function ProcessContent($innerRender = true)
    {
        $content = $this->tpl["content"];

        while (true) {
            if (substr_count($content, '{{') == 0) {
                break;
            }
            $content = preg_replace_callback('|{{(.*)}}|U', array($this, "TplDecode"), $content);
        }


        if ($innerRender) {
            ob_start();
            echo $content;
            $content = ob_get_contents();
            ob_end_clean();
        }

        return $content;
    }

    /**
     * @param bool $innerRender
     *
     * @return mixed|string
     */
    public function ProcessSubject($innerRender = true)
    {
        $content = $this->tpl["hSubject"];

        while (true) {
            if (substr_count($content, "{{") == 0) {
                break;
            }
            $content = preg_replace_callback('|{{(.*)}}|U', array($this, "TplDecode"), $content);
        }

        if ($innerRender) {
            ob_start();
            echo $content;
            $content = ob_get_contents();
            ob_end_clean();
        }

        return $content;
    }

    /**
     *
     * @return \MailAccounts
     */
    protected function getAccModel()
    {
        if (!$this->accModel instanceof MailAccounts) {
            $this->accModel = new MailAccounts();
        }
        return $this->accModel;
    }

    /**
     * @param $to
     * @param bool $innerRender
     *
     * @return bool|string
     */
    public function MessageTest($to, $innerRender = false)
    {
        if (!$to) {
            return false;
        }

        if ($innerRender) {
            $msg = $this->ProcessContent();
            $subject = $this->ProcessSubject();
        } else {
            $msg = $this->tpl["content"];
            $subject = $this->tpl["hSubject"];
        }

        $msg = $subject . " < hr>" . $msg;
        $body = strtr($msg, $this->data);


        return $body;
    }

}