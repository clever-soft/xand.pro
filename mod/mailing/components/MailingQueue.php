<?php

/**
 * Class MailingQueue
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class MailingQueue
{

    /**
     * @param MailTemplate $tpl
     * @param $email
     *
     * @return mixed
     */
    public static function add(MailTemplate $tpl, $email)
    {
        SDbQuery::table("mailing_queue")->insert([

            "idTpl" => $tpl->tpl["idTpl"],
            "data" => serialize($tpl->data),
            "email" => $email,
            "isProcessed" => 0,
            "dateCreate" => time()

        ]);

        $lastId = SDbBase::getLastInsertedId(); 

        return $lastId;

    }


    /**
     * @param bool $idQueue
     *
     * @return bool|void
     */
    public static function sendOne($idQueue = false)
    {
        if (!is_numeric($idQueue)) return false;

        $queueItem = SDb::row("SELECT * FROM mailing_queue WHERE idQueue=? AND isProcessed=0", $idQueue);
        if (!$queueItem) return;

        SDbQuery::table("mailing_queue")
            ->where(["idQueue" => $queueItem["idQueue"]])
            ->update(["isProcessed" => 1]);


        $tplMail = new MailTemplate();
        $tplMail->loadById($queueItem["idTpl"]);
        $tplMail->data = unserialize($queueItem["data"]);

        if ($tplMail->Send($queueItem["email"])) {
            return true;
        } else {
            return false;
        }
    }


}
