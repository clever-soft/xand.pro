<?php

/**
 * Description of WidgetsController
 *
 */
class WidgetsController extends BaseController
{

    public function defineAction($action = '') {
        return 'default';
    }

    //---	
    public function actionDefault() {

        $postNav = new Nav();
        $postNav->parse();

        if (!isset($postNav->path[2])) Message::I()->Error("Module not set");
        if (!isset($postNav->path[3])) Message::I()->Error("Widget not set");


        array_shift($postNav->path);
        array_shift($postNav->path);

        if (!AdminController::CheckAccess(implode(".", $postNav->path)))
            Message::I()->Error("Access Control System. You don't have access to this widget.")->Draw()->E();

        $path = $postNav->path;
        $fullPath = AdminController::GetWidgetPath($path);
        $widgetName = ucfirst(strtolower(array_pop($path)));

        if (!file_exists($fullPath)) {
            Message::I()->Error("Widget not found: " . $fullPath)->Draw()->E();
        } else {

            include_once($fullPath);

            $tpl = new TemplateCms("widgets", "tpl/tpl-admin/phtml/admin");
            $className = $widgetName . "Widget";
            $widget = new $className($widgetName,
                array(
                    "init" => count($this->get) > 1 ? true : false,
                    "url" => "admin/widgets/" . implode(DS, $postNav->path)
                )
            );

            if ($widget->getParams()) {
                $widget->setProperty('init', true);
            }

            if (method_exists($widget, "init")) {
                $widget->init();
            }

            $widget->triggerEvent($this->post['ACT']);

            if (isset($this->post["ACT"]) && method_exists($widget, "action" . $this->post["ACT"])) {
                $widget->{"action" . $this->post["ACT"]}();
            }

            $tpl->assign("widget", $widget);
            $tpl->Load();
        }


    }

}
