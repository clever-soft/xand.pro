<?php

class HelpersController extends BaseController
{
    //---
    public function actionDefault()
    {
        Message::I()->Error("No action")->Draw()->E();
    }

    /**
     * Getting geo info by ip address
     */
    public function actionGeo()
    {
        $ip = $this->getParam('ip');
        if (!$ip) {
            echo json_encode(['status' => true, 'message' => 'No ip set']);
            exit();
        }
        //options
        $options['ip'] = $ip;
        $options['countryName'] = "Unknown";
        $options['status'] = true;

        if (function_exists("geoip_country_name_by_name")) {
            try {
                @ $test = geoip_country_code_by_name($ip);
                if (!empty($test))
                    $options['countryName']  = $test;

            } catch (Exception $e) {
                $options['countryName'] = "Unknown";
            }
        }
 
        echo json_encode($options);
        exit();
    }

}
