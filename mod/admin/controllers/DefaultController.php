<?php

/**
 * Description of DefaultController
 *
 */
class DefaultController extends BaseController
{

    protected $usersModel;

    public function __construct()
    {
        parent::__construct();
        $this->usersModel = new UserModel();
    }

    private function AccessFilterTest()
    {

        $domainArr = explode(".", XAND_DOMAIN);
        if (XAND_DOMAIN == "localhost" || end($domainArr) == 'loc')
            return true;

        $conf = new CpConfig();
        $check = $conf->findField("attr='" . CpConfig::ATTR_ENABLE . "'", 'val');

        if ($check) {

            $realIp = $_SERVER["REMOTE_ADDR"];
            $country = "md";

            $listCheck = $conf->findField("attr='" . CpConfig::ATTR_LIST . "'", 'val');
            $list = explode("\n", $listCheck);

            if (in_array($country, $list)) {
                return true;
            }

            $ipListCheck = $conf->findField("attr='" . CpConfig::ATTR_IP . "'", 'val');
            $ipList = explode("\n", $ipListCheck);

            foreach ($ipList as $ip) {
                if (Helpers::isIpInSubnet($realIp, $ip) === true) {
                    return true;
                }
            }


        }
        return false;
    }


    /**
     * Страница входа
     */
    public function actionDefault()
    {

        /*
        if (!$this->AccessFilterTest()) {
            $this->httpError("home", "404", "Page not found", "Error 404");
            exit();
        }
        */


        if (UserModel::AuthCheck()) {
            $myName = $_SESSION[AUTH_KEY]['firstname'] . " " . $_SESSION[AUTH_KEY]['lastname'];
            $myId = (isset($_SESSION[AUTH_KEY]['idUser'])) ? $_SESSION[AUTH_KEY]['idUser'] : 0;
            $myRoles = (isset($_SESSION[AUTH_KEY]['roles'])) ? $_SESSION[AUTH_KEY]['roles'] : array();
            $myRolesIn = implode(",", $myRoles);

            $tpl = new TemplateCms("logged", "tpl/tpl-admin/phtml/admin");

            $tpl->assign("settings", UserModel::GetSettings($_SESSION[AUTH_KEY]['idUser']))
                ->assign("adminMenu", AdminController::GetAdminMenu())
                ->assign("myName", $myName)
                ->assign("version", XAND_VERSION);
        } else {
            $tpl = new TemplateCms("login", "tpl/tpl-admin/phtml/admin");

            $form = new FormStatic("Control panel", NULL, array("leftWidth" => 130));
            $form->Add(new Ajax(PATH_DS . "admin/action/auth", false, "html", "loginResponse"));
            $form->Add(new InputText("Login", "login", array("placeholder" => "Your login", "inputWidth" => 215, "titleWidth" => 120)));
            $form->Add(new InputPass("Password", "pass", array("placeholder" => "Your password", "inputWidth" => 215, "titleWidth" => 120)));
            $form->Add(new ControlCaptcha("Security code", "captcha", array("titleWidth" => 120)));


            $enter = new ButtonEnter(NULL, "Enter");
            $form->Add($enter);
            $tpl->assignForm("form", $form)
                ->assign("version", XAND_VERSION);
        }

        $tpl->Load();
    }

    /* * */

    public function actionSimulation()
    {
        if (!isset($_SESSION[AUTH_KEY]))
            Message::I()->Error("Access denied")->Draw()->E();

        if ($_SESSION[AUTH_KEY]['isSu'] == 1 || isset($_SESSION[AUTH_KEY]['viewAs'])) {
            $userModel = new UserModel();

            $postNav = new Nav();
            $postNav->parse($this->post['PATH']);
            $data = (int)Xand::$nav->path[3];

            $testAuth = $userModel->findById($data);
            if (!$testAuth) {
                return false;
            }
            if ($_SESSION[AUTH_KEY]['username'] != $testAuth['username']) {
                $_SESSION[AUTH_KEY]['viewAs'] = true;
            } else {
                unset($_SESSION[AUTH_KEY]['viewAs']);
            }

            $_SESSION[AUTH_KEY]['access'] = UserModel::GetAccessList($data);
            $_SESSION[AUTH_KEY]['settings'] = UserModel::GetSettings($testAuth['idUser']);

            $_SESSION[AUTH_KEY]['isSu'] = $testAuth['isSu'];
            $_SESSION[AUTH_KEY]['firstname'] = $testAuth['firstname'];
            $_SESSION[AUTH_KEY]['lastname'] = $testAuth['lastname'];
            $_SESSION[AUTH_KEY]['idUser'] = $testAuth['idUser'];
            $_SESSION[AUTH_KEY]['roles'] = CpUsersRolesModel::model()->findFieldAsList("idRole", "idUser=?", array($testAuth['idUser']));
            $postNav->path = array("cp");

        } else {
            Message::I()->Error("Access denied")->Draw()->E();
        }

    }


    /**
     *
     * @author kubrey
     */
    public function actionAuth()
    {

        $inputData = (array)json_decode($this->post["DATA"]);

        try {
            if ($inputData['login'] == "" || $inputData['pass'] == "") {
                throw new Exception("Fill in all fields");
            }
            $captcha = ControlCaptcha::GetCode();

            if ($inputData['captcha'] != $captcha) {
                ControlCaptcha::SetCode();
                throw new Exception("Wrong security code");
            }

            $login = UserModel::model()->where(["username" => $inputData['login']])->selectRow(['attemptsLogin', 'isActive', 'password']);

            if (empty($login)) {
                throw new Exception("Something went wrong. Try again or contact administration.");
            } else {
                if ($login['attemptsLogin'] > 4) {
                    $this->usersModel->updateByAttributes(array('username' => $inputData['login']), array('isActive' => 0, 'attemptsLogin' => 0));
                    throw new Exception('User is inactive, exceeded the maximum limit login attempts');
                }
                if (!$login['isActive']) {
                    throw new Exception("User is inactive, please contact to administrator");
                }

                $pass = $login['password'];
                $passRealArr = explode(":", $pass);
                if ($passRealArr[0] == md5($passRealArr[1] . $inputData['pass'])) {

                    if (!$this->initAuthedUser($inputData['login'])) {
                        throw new Exception('Failed to authorize');
                    }
                    Message::I()->Success("Signed in")->Draw()->Reload();
                    //echo $this->getStpl()->render(false, 'phtml/cms/helpers/reload');
                } else {
                    //UserModel::model()->where(["username" => $inputData['login']])->update(['attemptsLogin' => 'attemptsLogin + 1']);
                    //$this->usersModel->updateRawByRaw("username=?", "attemptsLogin=attemptsLogin+1", array($inputData['login']));
                    SDb::row("UPDATE users SET attemptsLogin+1 WHERE username = ?", $inputData['login']);

                    throw new Exception("Invalid username or password");
                }
            }
        } catch (Exception $ex) {
            Message::I()->Error($ex->getMessage())->Draw();
        }
    }

    /**
     *
     * @param string $login
     * @return bool
     */
    protected function initAuthedUser($login)
    {
        if (!$this->usersModel->updateByAttributes(array('username' => $login), array('attemptsLogin' => 1, 'dateLogin' => time()))) {
            return false;
        }
        $userUpdated = UserModel::model()->where(["username" => $login])->selectRow([]);
        if (!$userUpdated) {
            return false;
        }
        $_SESSION[AUTH_KEY] = $userUpdated;

        $idUser = (int)$_SESSION[AUTH_KEY]["idUser"];
        $_SESSION[AUTH_KEY]['access'] = UserModel::GetAccessList($idUser);
        $_SESSION[AUTH_KEY]['settings'] = UserModel::GetSettings($idUser);
        $_SESSION[AUTH_KEY]['roles'] = CpUsersRolesModel::model()->where(["idUser" => $idUser])->selectRow(["idRole"]);

        return true;
    }

}
