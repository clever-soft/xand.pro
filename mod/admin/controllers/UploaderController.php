<?php

/**
 * Class UploaderController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class UploaderController extends BaseController
{
    /**
     *
     */
    public function actionDefault()
    {
        Message::I()->Error("No action")->Draw();
    }


    /**
     *
     */
    public function actionGroups()
    {
        if (empty($_FILES)) {
            Message::I()->Error("No files")->Draw();
            return;
        }

        $sourceFile = $_FILES['file_upload']['tmp_name'];

        $targetPath = PATH_REAL . DS . PATH_PUBLIC . PATH_UPLOAD . DS . "groups";
        $uniqueName = File::GetUploadPath($targetPath);
        $demo_path = PATH_DS . 'uploads/groups';
        $targetFile = $targetPath . DS . $uniqueName . ".jpg";
        $targetFileMini = $targetPath . DS . $uniqueName . "_min.jpg";
        $imageInfo = getimagesize($sourceFile);
        $type = $imageInfo['mime'];
        $w = $imageInfo[0];
        $h = $imageInfo[1];

        if ($w < 640 || $h < 480) {
            Message::I()->Error("The image should not be less than width 640 px and height 480 px")->Draw();
            exit();
        }


        if ($type == "image/jpeg") {

            Image::CreateNewImage($sourceFile, $targetFile, $type);
            Image::CreateNewImage($targetFile, $targetFileMini, $type, 320, 240);

            echo '<img width="50px" src="' . $demo_path . DS . $uniqueName . '.jpg">';
            echo "<script>";
            echo "window.parent.document.getElementById('{$_POST['domId']}_value').value = '{$uniqueName}';";
            echo "</script>";

            Message::I()->Success("Uploaded successfully")->Draw();

        } else {
            Message::I()->Error("Allowed only upload jpg format")->Draw();
        }


    }


    /**
     *
     */
    public function actionShop()
    {
        if (empty($_FILES)) {
            Message::I()->Error("No files")->Draw();
            return;
        }

        $sourceFile = $_FILES['file_upload']['tmp_name'];

        $targetPath = PATH_REAL . DS . PATH_PUBLIC . PATH_UPLOAD . DS . "shop";
        $uniqueName = File::GetUploadPath($targetPath);

        $targetFile = $targetPath . DS . $uniqueName . ".jpg";
        $targetFileMini = $targetPath . DS . $uniqueName . "_min.jpg";
        $imageInfo = getimagesize($sourceFile);
        $type = $imageInfo['mime'];
        $w = $imageInfo[0];
        $h = $imageInfo[1];

        if ($w < 600 || $h < 600) {
            Message::I()->Error("The image should not be less than width 800 px and height 600px")->Draw();
            exit();
        }


        if ($type == "image/jpeg") {

            Image::CreateNewImage($sourceFile, $targetFile, $type);
            Image::CreateNewImage($targetFile, $targetFileMini, $type, 320, 240);

            echo "<script>";
            echo "window.parent.document.getElementById('{$_POST['domId']}_value_new').value = '{$uniqueName}';";
            echo "</script>";

            Message::I()->Success("Uploaded successfully")->Draw();

        } else {
            Message::I()->Error("Allowed only upload jpg format")->Draw();
        }


    }

    /**
     *
     */
    public function actionNews()
    {

        if (!empty($_FILES)) {

            $answer = array("error" => false, "data" => array(), "preview" => array());

            $sourceFile = $_FILES['file']['tmp_name'];
            $imageinfo = getimagesize($sourceFile);

            $type = $imageinfo['mime'];
            $w = $imageinfo[0];
            $h = $imageinfo[1];

            if ($type == "image/jpeg" || $type == "image/png") {

                $idAlbum = SDb::cell("SELECT idAlbum FROM gallery_albums WHERE url='news'");
                $idPhoto = SDb::emptyRow("gallery_photos", "idPhoto");

                $imageBasePath = Image::GetImageDir("news") . DS . $idPhoto;

                SDb::update("gallery_photos", [
                        "idAlbum" => $idAlbum,
                        "path" => $imageBasePath,
                        "dimensions" => $w . "x" . $h,
                        "name" => $_FILES['file']['name'],
                        "size" => $_FILES['file']['size'],
                        "dateCreate" => time(),
                        "dateUpdate" => time()], "idPhoto = ?", [$idPhoto]
                    );

                Image::CreateNewImage($sourceFile, PATH_REAL . DS . PATH_PUBLIC . $imageBasePath . ".jpg", $type, 800, 600, true, 95);

                $answer["preview"] = PATH_DS . $imageBasePath . ".jpg";
                $answer["data"] = $idPhoto;

            }
        } else {
            $answer["error"] = "Empty files";
        }

        echo json_encode($answer);
    }

    /**
     *
     */
    public function actionSlider()
    {

        if (empty($_FILES)) {
            Message::I()->Error("No files")->Draw();
            exit();
        }

        if (!isset(Xand::$nav->path[2])) {
            Message::I()->Error("No files")->Draw();
            exit();
        }

        $realPath = realpath('uploads/' . Xand::$nav->path[2]);
        $realPath = str_replace("\\", "/", $realPath);
        $sourceFile = $_FILES['file_upload']['tmp_name'];
        $targetFile = $realPath;
        $targetFileBig = $realPath;
        $demo_path = PATH_DS . 'uploads/' . Xand::$nav->path[2];
        $uniqueName = "i" . time();
        $tmbSuffix = "_50_30";

        $imageInfo = getimagesize($sourceFile);

        $type = $imageInfo['mime'];
        $w = $imageInfo[0];
        $h = $imageInfo[1];

        //--- For slider case
        if (Xand::$nav->path[2] == "slider") {

            if ($w != 1200 || $h != 400) {
                Message::I()->Error("Only 1200x400 px or biggest")->Draw();
                exit();
            }

            if ($type == "image/jpeg" || $type == "image/png") {

                Image::CreateNewImage($sourceFile, $targetFileBig . DS . $uniqueName . '.jpg', $type, 1200, 400, false, 95);
                Image::CreateNewImage($sourceFile, $targetFile . DS . $uniqueName . $tmbSuffix . '.jpg', $type, 50, 30, false, 95);

                echo '<img width="50px" src="' . $demo_path . DS . $uniqueName . $tmbSuffix . '.jpg">';
                echo "<script>";
                echo "$('#{$_POST['domId']}_value').val('{$uniqueName}');";
                echo "</script>";

            } else {
                Message::I()->Error("JPG or PNG only")->Draw();
                exit();
            }

        }

    }


}
