<?php

/**
 * Class ExportController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ExportController extends BaseController
{

    private $_widget;

    /**
     * @param string $action
     *
     * @return string
     */
    public function defineAction($action = '')
    {
        return 'default';
    }

    /**
     *
     */
    public function getExcelDocument()
    {

        $fname = Content::createUrl($this->_widget->getProperty("title"));

        include_once(PATH_REAL . '/tools/PHPExcel/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $active_sheet = $objPHPExcel->getActiveSheet();

        $active_sheet->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);

        $active_sheet->getPageSetup()
            ->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $active_sheet->getPageMargins()->setTop(1);
        $active_sheet->getPageMargins()->setRight(0.75);
        $active_sheet->getPageMargins()->setLeft(0.75);
        $active_sheet->setTitle("List");

        $resultRaw = $this->_widget->data;
        $result = array();
        $allowedColumns = array();

        foreach ($this->_widget->table->columns as $item)
            $allowedColumns[$item->key] = $item->name;


        if (isset($this->_widget->properties["postCorrectCallback"]))
            $result = $this->_widget->properties["postCorrectCallback"]($result);

        if (!empty($resultRaw)) {
            foreach ($resultRaw as $row) {
                $newRow = array();
                foreach ($row as $colKey => &$rowCol) {
                    if (!isset($allowedColumns[$colKey]))
                        continue;

                    $rowCol = strip_tags($rowCol);

                    if (is_numeric($rowCol) && strlen($rowCol) == 10)
                        $rowCol = date("Y-m-d H:i", $rowCol);

                    $newRow[$colKey] = $rowCol;


                }

                $result[] = $newRow;
            }

            $tableHead = array();
            foreach ($newRow as $key => $val)
                $tableHead[] = preg_replace("[^A-Za-z0-9_-]", "", $allowedColumns[$key]);

            array_unshift($result, $tableHead);
            $active_sheet->fromArray($result);

            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        }

        foreach (range('A', $active_sheet->getHighestDataColumn()) as $col)
            $active_sheet->getColumnDimension($col)->setAutoSize(true);


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fname . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }


    /**
     *
     */
    public function actionDefault()
    {

        $postNav = new Nav();
        $postNav->parse();

        if (!isset($postNav->path[2])) Message::I()->Error("Module not set");
        if (!isset($postNav->path[3])) Message::I()->Error("Widget not set");


        array_shift($postNav->path);
        array_shift($postNav->path);

        if (!AdminController::CheckAccess(implode(".", $postNav->path))) {
            Message::I()->Error("Access Control System. You don't have access to this widget.")->Draw()->E();
        }


        $path = $postNav->path;
        $fullPath = AdminController::GetWidgetPath($path);
        $widgetName = ucfirst(strtolower(array_pop($path)));

        if (!file_exists($fullPath)) {
            Message::I()->Error("Widget not found: " . $fullPath)->Draw()->E();
        } else {

            include_once($fullPath);
            $className = $widgetName . "Widget";
            $this->_widget = new $className($widgetName,
                array(
                    "init" => true,
                    "ajaxAllowed" => false,
                    "url" => "admin/widgets/" . implode(DS, $postNav->path)
                )
            );

            if (method_exists($this->_widget, "init")) {
                $this->_widget->init();
            }

            $this->_widget->stateRestore->Read();
            $this->_widget->getContent();
            $this->_widget->RenderBody();

            $this->getExcelDocument();

        }

    }

}
