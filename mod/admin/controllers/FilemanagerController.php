<?php

class FilemanagerController extends BaseController
{
    /**
     *
     */
    public function actionDefault()
    {
 
        if (!isset($_SESSION[AUTH_KEY])) exit("Access denied");


        $tpl = new TemplateCms("html", "tpl/tpl-admin/plugins/filemanager/phtml");
        $tpl->Assign("fm", new FileManager());
        $tpl->Load();

        return $this;
    }
}