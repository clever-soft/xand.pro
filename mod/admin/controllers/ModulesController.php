<?php

/**
 * Description of DefaultController
 *
 */
class ModulesController extends BaseController
{

    //----
    public function actionDefault() {

        $postNav = new Nav();
        $postNav->parse($this->post['PATH']);

        if (!isset($_SESSION[AUTH_KEY])) {
            Message::I()->Error("Your session is finished, login please again");
            UserModel::Logout();
        }

        // Debug::Dump($postNav);

        switch ($postNav->path[0]) {

            case "logout":
                UserModel::Logout();
                break;

            case "dashboard":
                $link = UserModel::$defaultSettings["navDefaultPage"];
                echo "<script>CAdminMenu.Go('{$link}');</script>";
                exit();

        }

        if (!AdminController::CheckAccess(implode(".", $postNav->path)))
            Message::I()->Error("Access Control System. You don't have access to this widget.")->Draw()->E();


        if (!isset($postNav->path[0]))
            $postNav->path = array("admin", "dashboard");

        $controller = PATH_REAL . DS . "mod" . DS . $postNav->path[0] . "/controllers/AdminController.php";

        if (file_exists($controller)) {
            include_once($controller);

        } else {

            $path = $postNav->path;
            AdminController::CheckAccess($path);

            $fullPath = AdminController::GetWidgetPath($path);
            $widgetName = ucfirst(strtolower(array_pop($path)));

            if (!file_exists($fullPath)) {
                Message::I()->Error("Widget not found: " . $fullPath)->Draw()->E();
            } else {

                include_once($fullPath);

                $tpl = new TemplateCms("widgets", "tpl/tpl-admin/phtml/admin");
                $className = $widgetName . "Widget";
                $widget = new $className($widgetName,
                    array(
                        "init" => true,
                        "url" => "admin/widgets/" . implode(DS, $postNav->path)
                    )
                );

                if (method_exists($widget, "init")) {
                    $widget->init();
                }


                $tpl->assign("widget", $widget);
                $tpl->Load();
            }


        }
    }

}
