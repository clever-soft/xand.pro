<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 25.12.15
 * Time: 11:18
 */
class RolesModel extends BaseModel
{
    public function getTableName() {
        return 'roles';
    }

    public function getId() {
        return 'idRole';
    }
}