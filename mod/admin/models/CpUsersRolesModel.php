<?php

/**
 * Description of CpUsersRolesModel
 *
 * @author kubrey
 */
class CpUsersRolesModel extends BaseModel{

    /**
     * @return SDbQuery
     */
    public static function model()
    {
        return SDbQuery::table("users_roles");
    }

    public function getTableName() {
        return "users_roles";
    }
    
    public function getId(){
        return "id";
    }
}
