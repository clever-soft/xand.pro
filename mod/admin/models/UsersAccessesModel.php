<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 25.12.15
 * Time: 11:11
 */
class UsersAccessesModel extends BaseModel
{
    const ACCESS_TYPE_WIDGET = 'widget';
    const ACCESS_TYPE_CONTROLLER = 'controller';

    public function getTableName() {
        return 'users_accesses';
    }

    public function getId() {
        return 'id';
    }
}