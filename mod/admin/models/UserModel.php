<?php

/**
 * Description of class
 *
 */
class UserModel extends BaseModel
{
    /**
     * @return SDbQuery
     */
    public static function model()
    {
        return SDbQuery::table("users");
    }

    public function getTableName() {
        return 'users';
    }

    public function getId() {
        return 'idUser';
    }

    public static $defaultSettings = array(
        "styleTextSize" => 12,
        "styleTextLineHeight" => 14,
        "styleMenuSize" => 17,
        "styleSubMenuSize" => 13,
        "styleInputSize" => 12,
        "styleTextBold" => "Anton.ttf",
        "styleTextNormal" => "NotoSans-Regular.ttf",
        "generalLang" => "en_US",
        "generalTooltips" => 1,
        "navDefaultPage" => "cms/management/textpages",
    );

    /**
     * @return array
     */
    public static function GetTemplatesList() {
        $mods = glob(PATH_REAL . DS . PATH_TPL . DS . PATH_THEME . DS . '*.phtml');
        $tpls = array();
        foreach ($mods as $mod) {
            $pathArr = explode("/", $mod);
            $path = substr(array_pop($pathArr), 0, -6);
            $tpls[] = array(
                'title' => $path . ".phtml",
                'equal' => $path,
                'data' => $path
            );

        }
        return $tpls;
    }

    public static function GetSettings($myId) {
        if (isset($_SESSION[AUTH_KEY]['settings']) && is_array($_SESSION[AUTH_KEY]['settings']) && $_SESSION[AUTH_KEY]['settings']) {
            $data = $_SESSION[AUTH_KEY]['settings'];
        } else {
            $me = new self();
            $dataRaw = $me->findField("idUser=?", "settings", array($myId));
            if (!$dataRaw) {
                return self::$defaultSettings;
            }
            $data = unserialize($dataRaw);

            foreach (self::$defaultSettings as $k => $v) {
                if (!isset($data[$k])) {
                    $data[$k] = $v;
                }
            }
        }


        return $data;
    }

    /**
     * @return bool
     */
    public static function isSU()
    {
        return (isset($_SESSION['auth']['isSu']) && $_SESSION['auth']['isSu']) ? true : false;
    }


    /**
     * Проверяет, есть ли доступ к опции у юзера
     * @param string $option
     * @return boolean
     */
    public static function hasOption($option) {
        if (isset($_SESSION[AUTH_KEY])) {
            if ($_SESSION[AUTH_KEY]['isSu'] == "1") {
                return true;
            }
            if (in_array($option, $_SESSION[AUTH_KEY]['access'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Выбирает права юзера
     * @param int $idUser
     * @param null|string $type
     * @return array
     */
    public static function GetAccessList($idUser, $type = null) {
        $andUa = ($type) ? " and users_accesses.type='$type' " : "";
        $rulesUser = SDb::rows(
            "SELECT users_accesses.path
             FROM users_accesses
             WHERE idUser =? $andUa
             GROUP BY path", array($idUser));

        $andUr = ($type) ? " and roles_accesses.type='$type' " : "";
        $rulesRole = SDb::rows(
            "(SELECT roles_accesses.path
			  FROM users_roles
			  JOIN roles_accesses ON roles_accesses.idRole = users_roles.idRole
              JOIN roles ON users_roles.idRole = roles.idRole
              WHERE users_roles.idUser =? $andUr
              GROUP BY path)", array($idUser));

        $resourcesRaw = array_merge($rulesUser, $rulesRole);
        $resources = array();

        if (!empty($resourcesRaw)) {
            foreach ($resourcesRaw as $r)
                $resources[] = $r["path"];

            $resources = array_unique($resources);
        }

        return $resources;
    }

    /**
     * @return bool|int
     */
    public static function getUserId() {
        return (isset($_SESSION[AUTH_KEY]['idUser']) ? (int)$_SESSION[AUTH_KEY]['idUser'] : false);
    }

    /**
     *
     */
    public static function Logout() {
        unset($_SESSION[AUTH_KEY]);
        if (isset($_COOKIE["PHPSESSID"])) {
            $sid = preg_replace("/[^A-Z0-9]+/ui", "", $_COOKIE["PHPSESSID"]);
            $cpSessions = new CustomersSessionsModel();
            $cpSessions->delete($sid);
        }
        echo "<script> AdminAjax.HashSet('dashboard');	setTimeout(function(){ window.location.reload(); }, 200); </script>";
        exit();
    }

    public static function AuthCheck() {
        return (isset($_SESSION[AUTH_KEY])) ? true : false;
    }


}
