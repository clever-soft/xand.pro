<?php

class AdminController
{
    //----
    public static function CheckAccess($path) {
        //Debug::Dump($path);

        if (!isset($_SESSION[AUTH_KEY]))
            return false;

        if (isset($_SESSION[AUTH_KEY]['isSu']) && $_SESSION[AUTH_KEY]['isSu'])
            return true;

        if (in_array($path, $_SESSION[AUTH_KEY]['access']))
            return true;

        return false;
    }


    /**
     * @return string json
     */
    public static function GetAdminMenu() {

        $mods = glob(PATH_REAL . '/mod/*', GLOB_ONLYDIR);
        $menuSort = array();

        $accessCurr = $_SESSION[AUTH_KEY]["access"];
        $accessCurr[] = "admin";

        foreach ($mods as $mod) {
            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }

            $xml = simplexml_load_file($fileXml);

            if ($xml->settings->active == 0) continue;
            $module = (array)$xml->settings;

            if ($xml->access) {
                foreach ($xml->access->group as $group) {

                    $groupName = (string)$group->name;
                    $groupUrl = (string)$group->url;
                    $widgets = array();

                    foreach ($group->widgets[0] as $widgetPath => $widgetName) {

                        $accessPath = $xml->settings->url . ".{$groupUrl}.{$widgetPath}";

                        if (self::CheckAccess($accessPath)) {
                            $widgets[$widgetPath] = (string)$widgetName;
                        }
                    }
                    if (!empty($widgets))
                        $module["access"][] = array($groupName, $groupUrl, $widgets);
                }

                if (isset($xml->access->widgets[0])) {
                    $widgets = array();

                    foreach ($xml->access->widgets[0] as $widgetPath => $widgetName) {

                        $accessPath = $xml->settings->url . ".{$widgetPath}";
                        if (self::CheckAccess($accessPath)) {
                            $widgets[$widgetPath] = (string)$widgetName;
                        }
                    }

                    if (!empty($widgets))
                        $module["access"]["_common"] = $widgets;
                }

            }

            //Debug::Dump($module);
            //echo "<hr>";

            if (!empty($module["access"]))
                $menuSort[(string)$xml->settings->sort] = $module;

        }

        return json_encode($menuSort);
    }

    //----
    public static function GetWidgetPath($filePath = array()) {

        $moduleName = array_shift($filePath);
        $widgetName = ucfirst(strtolower(array_pop($filePath)));
        array_push($filePath, $widgetName . "Widget");
        return PATH_REAL . "/mod/" . $moduleName . "/widgets/" . implode(DS, $filePath) . ".php";
    }


    //----
    public static function GetTemplatesList($subFolder = "") {
        $mods = glob(PATH_REAL . DS . PATH_PUBLIC . PATH_TPL . DS . PATH_THEME . DS . "phtml" . DS . $subFolder . DS . "*.phtml");
        $tpls = array();
        foreach ($mods as $mod) {
            $pathArr = explode("/", $mod);
            $path = substr(array_pop($pathArr), 0, -6);
            $tpls[] = array(
                'title' => $path . ".phtml",
                'equal' => $path,
                'data' => $path
            );

        }
        return $tpls;
    }


}
