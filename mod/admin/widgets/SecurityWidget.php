<?php include_once("../../../lib/class.Xand.php");

$nav   = new Nav();
$cst   = new Widget('security', new Ajax(PATH_DS."mod/admin/widgets/widget.security.php", false),$nav);
$query = new QueryMulti("cp_security[*]");

$cst->setProperty("title","Security")
	->SetDefaultOrder("dateCreate", true)
 	->Add($query);

	$table = new Table(
					  new TableColumn("isActive", "S", 25, "center"),		
				      new TableColumnDateCreate(),					 					 
					  new TableColumn("code", "C", 25, "center"), 					
					  new TableColumn("country", "Location", 160), 	
					  new TableColumn("ip", "IP", 150), 			     
					  new TableColumn("login", "Login", 150), 
					  new TableColumn("info", "Description")				


				  ); 
				 
$table->SetPrimary("id");

  
$cst->Add($table);
	
	if($cst->data != null)
	{	 
		 foreach($cst->data as &$val)
		 {	
			$val['code']  = "<img src='".PATH_DS.PATH_MODULES."/subscriptions/tpl/ico/".strtolower($val["code"]).
								  ".png' width='16px' height='11px' style='cursor:pointer'/>";	 
								  
			
			$ico = ($val["info"] == 1) ? "fam_bullet_success" : "error_msg_icon";	 
			$val['isActive']	= "<img src='".PATH_DS.PATH_MODULES."/crm/tpl/payments/{$ico}.gif' />";  
			
			$val['country']	= ($val['country'] == "") ? "-//-": $val['country'].", ".$val['city'];  
			 
			switch($val['info'])
			{
				case 1: $val['info'] = "Login successful"; break;
				case 2: $val['info'] = "Invalid username or password"; break;
				case 3: $val['info'] = "The wrong security code"; break;
			}		
			
			$val['info']	.= ($val['browser']!="") ? ", browser: ".$val['browser'] : "";			  
								   
		 }
	}
		

$cst->Draw();	
	

?>