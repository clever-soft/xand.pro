<?php include_once("../../../lib/class.Xand.php");


$cst = new Widget('informers', new Ajax(PATH_DS . "mod/admin/widgets/widget.informers.php", false));
$query = new QueryMulti("users_informers[*]", "cp_informers[name,link]");
$query->Join("cp_informers", "cp_informers.id = users_informers.idInformer");
$query->AddFilter("idUser=" . $_SESSION[AUTH_KEY]['idUser']);


$cst->setProperty("title", "My informers management")
    ->SetDefaultOrder("npp", false)
    ->Add($query)
    ->Add(new ButtonAppend(NULL, "New", array("script" => "ModalAct")));

$table = new Table(
    new TableColumn("npp", "№", 60, "center"),
    new TableColumn("idInformer", "ID", 60, "center"),
    new TableColumn("name", "Name", 160),
    new TableColumn("width", "Width, %", 100, "center"),
    new TableColumn("height", "Height, px", 100, "center"),
    new TableColumn("link", "Link"),
    new TableColumnDateCreate(),
    new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
    new ButtonDelete()
);

$table->SetPrimary("id");


switch ($this->getPostAction()) {
    case 'AddReady'  :
        $form = new FormModal("Adding a new informer");
        $form->assignParent($cst)->cloneAjax()->CloneNav();

        $form->Add(new ControlSelect("Informer", "idInformer", array("noChoose" => false), NULL,
            ControlSelect::DataFormat(Db::Select("cp_informers", NULL, "name"), array("name", "id"))));

        $form->Add(new InputNumeric("№", "npp", NULL, 1));


        $form->Add(new ControlSelect("Width, %", "width", array("noChoose" => false), NULL,
            array(
                array("title" => "20%", "equal" => "20", "data" => "20"),
                array("title" => "40%", "equal" => "40", "data" => "40"),
                array("title" => "60%", "equal" => "60", "data" => "60"),
                array("title" => "80%", "equal" => "80", "data" => "80"),
                array("title" => "100%", "equal" => "100", "data" => "100")
            )));

        $form->Add(new InputNumeric("Height", "height", NULL, 182));

        $form->Add(new ButtonAdd(NULL, "Add", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();


    case 'AddFinish' :
        $arr_data = $this->getPostDataJson();
        $arr_data["idUser"] = $_SESSION[AUTH_KEY]['idUser'];
        $arr_data["dateCreate"] = time();
        SDb::insert("users_informers", $arr_data);

        break;


    case 'UpdateReady' :
        $arr = SDb::select("users_informers", "id = ?", [$this->getPostDataInt()]);
        $form = new FormModal("Edit informer", $arr);
        $form->assignParent($cst)->cloneAjax()->CloneNav();

        $form->Add(new ControlSelect("Informer", "idInformer", array("noChoose" => false), NULL,
            ControlSelect::DataFormat(SDb::select("cp_informers", "name"), array("name", "id"))));

        $form->Add(new InputNumeric("№", "npp"));
        $form->Add(new InputVar("id", "id"));

        $form->Add(new ControlSelect("Width, %", "width", array("noChoose" => false), NULL,
            array(
                array("title" => "20%", "equal" => "20", "data" => "20"),
                array("title" => "40%", "equal" => "40", "data" => "40"),
                array("title" => "60%", "equal" => "60", "data" => "60"),
                array("title" => "80%", "equal" => "80", "data" => "80"),
                array("title" => "100%", "equal" => "100", "data" => "100")
            )));

        $form->Add(new InputNumeric("Height", "height"));

        $form->Add(new ButtonSave(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();


        exit();

    case 'UpdateFinish' :
        $arr_data = $this->getPostDataJson();
        SDbQuery::table("users_informers")
            ->where([
                "id" => $arr_data["id"]
            ])->update($arr_data);
        break;

    case 'Delete' :
        SDbQuery::table("users_informers")->enableMessages()->where(["id" => $this->getPostDataInt()]);
        break;


}

$cst->Add($table);

$cst->Draw();


?>