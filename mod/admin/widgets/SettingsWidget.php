<?php include_once("../../../lib/class.Xand.php");


$hash = md5($_SESSION[AUTH_KEY]["idUser"] . "cpStyles");

$default = array();
$default["styleTextSize"] = 12;
$default["styleTextLineHeight"] = 14;
$default["styleMenuSize"] = 17;
$default["styleSubMenuSize"] = 13;
$default["styleInputSize"] = 12;
$default["styleTextBold"] = "Anton.ttf";
$default["styleTextNormal"] = "NotoSans-Regular.ttf";
$default["generalLang"] = "en_US";
$default["generalTooltips"] = 1;

//------------------------------
switch ($this->getPostAction()) {
    case 'Reset' :
        SDb::execute("INSERT IGNORE INTO users_states (hash) VALUES ('{$hash}');");
        SDb::update("users_states", ["val" => serialize($default)],
            [
                "hash" => $hash
            ]);
        Message::I()->Success("Settings were updated successfully, please refresh your browser (F5)");
        break;

    case 'AddFinish' :
        SDb::execute("INSERT IGNORE INTO users_states (hash) VALUES ('{$hash}');");
        SDb::update("users_states", ["val" => serialize($this->getPostDataJson())], ["hash" => $hash]);
        Message::I()->Success("Settings were updated successfully, please refresh your browser (F5)");
        UserModel::SetSettings();
        break;

    case 'Tooltips' :
        $data = array();
        $dataRaw = SDb::cell("SELECT val FROM users_states WHERE hash = ?", [$hash]);
        $data = ($dataRaw) ? unserialize($dataRaw) : array();
        $data["generalTooltips"] = $this->getPostDataInt();
        SDb::execute("INSERT IGNORE INTO users_states (hash) VALUES ('{$hash}');");
        SDb::update("users_states", ["val" => serialize($data)], ["hash" => $hash]);
        UserModel::SetSettings();
        var_dump($this->getPostDataInt());
        exit();


}


$data = array();
$dataRaw = SDb::cell("SELECT val FROM users_states WHERE hash = ?", [$hash]);
$data = ($dataRaw) ? unserialize($dataRaw) : array();


$data["styleTextSize"] = (isset($data["styleTextSize"])) ? $data["styleTextSize"] : $default["styleTextSize"];
$data["styleTextLineHeight"] = (isset($data["styleTextLineHeight"])) ? $data["styleTextLineHeight"] : $default["styleTextLineHeight"];
$data["styleMenuSize"] = (isset($data["styleMenuSize"])) ? $data["styleMenuSize"] : $default["styleMenuSize"];
$data["styleSubMenuSize"] = (isset($data["styleSubMenuSize"])) ? $data["styleSubMenuSize"] : $default["styleSubMenuSize"];
$data["styleInputSize"] = (isset($data["styleInputSize"])) ? $data["styleInputSize"] : $default["styleInputSize"];
$data["generalLang"] = (isset($data["generalLang"])) ? $data["generalLang"] : $default["generalLang"];
$data["generalTooltips"] = (isset($data["generalTooltips"])) ? $data["generalTooltips"] : $default["generalTooltips"];


$files = scandir(PATH_REAL . DS . "mod/admin/tpl/themes/fonts");
$fontsData = array();
array_shift($files);
array_shift($files);

foreach ($files as $file) {
    $fontsData [] = array("equal" => $file, "title" => $file, "data" => $file);
}


$form = new FormStatic("My settings", array($data));
$form->Add(new Ajax(PATH_DS . "mod/admin/widgets/widget.settings.php", false, "html", "settings"));


$gr = new GrouperRow("General", NULL, array("show" => true));

$gr->Add(new InputCheckbox("Tooltips", "generalTooltips"));
$gr->Add(new ControlSelect("Language", "generalLang", array("noChoose" => false), NULL, array(

    array("title" => "Deutsch (Deutschland)", "equal" => "de_DE", "data" => "de_DE"),
    array("title" => "English (United Navs)", "equal" => "en_US", "data" => "en_US")

)));
$form->Add($gr);


$gr = new GrouperRow("Styles", NULL, array("show" => true));
$gr->Add(new ControlSelect("Bold text", "styleTextBold", array("noChoose" => true), NULL, $fontsData));
$gr->Add(new ControlSelect("Normal text", "styleTextNormal", array("noChoose" => true), NULL, $fontsData));

$gr->Add(new InputNumeric("Default font size", "styleTextSize", array("minValue" => 10, "maxValue" => 40, "width" => 50)));
$gr->Add(new InputNumeric("Default font line height", "styleTextLineHeight", array("minValue" => 10, "maxValue" => 40, "width" => 50)));
$gr->Add(new InputNumeric("Big menu font size", "styleMenuSize", array("minValue" => 10, "maxValue" => 40, "width" => 50)));
$gr->Add(new InputNumeric("Sub menu font size", "styleSubMenuSize", array("minValue" => 10, "maxValue" => 40, "width" => 50)));
$gr->Add(new InputNumeric("Inputs font size", "styleInputSize", array("minValue" => 10, "maxValue" => 40, "width" => 50)));
$form->Add($gr);


$form->Add(new ButtonCancel("Reset", "Reset"));
$form->Add(new ButtonAdd(NULL, "Set"));

echo "<div style='width:500px'>";
$form->Draw();
echo "</div>";
if (isset($_GET['_gait'])) {
    echo 'RTE';
}

Message::I()->Draw();

?>