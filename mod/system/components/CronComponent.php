<?php

/**
 * Description of CronComponent
 *
 * @author kubrey
 */
class CronComponent extends BaseComponent {

    public $key;
    protected $cliIds = array();
    protected $argv = null;
    protected $service;

    protected $basePath;

    public function __construct($key = false, $argv = null) {
        parent::__construct();

        $this->basePath = PATH_REAL.DIRECTORY_SEPARATOR."tools/bin/cli";
        $this->key = $key;
        $this->argv = $argv;
        $this->service = new CpServices();
        $this->handleCLI();
    }

    /**
     * <pre>
     * Ожидает строку ...script.php run 12,13,15[,...], где числа - id сервиса
     * Или ...script.php print Для вывода всех сервисов
     * </pre>
     * @return boolean
     */
    protected function handleCLI() {
        if (isset($this->argv) && isset($this->argv[1])) {
            switch ($this->argv[1]) {
                case 'run':
                    $ids = $this->argv[2];
                    if (!empty($ids)) {
                        if (is_numeric($ids)) {
                            $idsArr = array($ids);
                        } else {
                            $idsArr = explode(',', $ids);
                        }
                        if (!is_array($idsArr)) {
                            return false;
                        }
                        foreach ($idsArr as $id) {
                            if (is_numeric($id) && $id > 0) {
                                $this->cliIds[] = $id;
                            }
                        }
                    }
                    break;
                case 'print':
                    $this->printServices();
                    break;
                case 'custom':
                    $this->runCustomScript();
                    break;
                case 'custom_file':
                    $this->runScript($this->argv[2]);
                    break;
            }
        }
        return true;
    }

    private function runCustomScript() {
        //просто вставить сюда скрипт
        exec("php " . PATH_REAL . DS . "tools/cli/parse.php & echo $!");
        exit(0);
    }

    /**
     *
     */
    private function runScript($file) {
        $path = PATH_REAL . DS . $file;
        if (!is_file($path)) {
            echo "File " . $path . " doesn\t exist\n";
        } else {
            $options = '';
            for ($iter = 3; $iter < count($this->argv); $iter++) {
                if (trim($this->argv[$iter]) == '&') {
                    continue;
                }
                $options.=$this->argv[$iter] . ' ';
            }
            echo "Executing $path $options ...\n";
            exec('php ' . $path . " $options & echo $!");
        }
        exit(0);
    }

    public function printServices() {
        $serv = $this->service->findAll("isEnable=1");

        if (empty($serv)) {
            echo "Cannot output services\n";
            return false;
        }
        echo "Список доступных сервисов: \n";
        echo "--------------------------\n";
        foreach ($serv as $s) {
            if ($s['isActive'] == 1) {
                echo "\033[42m\033[30m";
            }
            echo $s['id'] . " " . $s['name'] . " " . $s['file'] . " " . $s['period'] . " " . $s['pid'] . " \033[0m\n";
        }
        echo "---------------------------\n";
    }

    /**
     *
     * @param int $id
     * @return boolean
     */
    public function killService($id) {
        $serv = $this->service->findById($id);
        if (!($serv)) {
            return false;
        }

        $pid = $serv['pid'];
        if (!$this->isProcessRunning($pid)) {
            return false;
        }
        exec('kill ' . $pid);
        $this->service->updateById($id, array('isActive' => 0));
        return true;
    }

    /**
     * Запущен ли процесс
     * @param int $pid
     * @return boolean
     */
    protected function isProcessRunning($pid) {
        if (!is_numeric($pid)) {
            return false;
        }
        if (is_dir('/proc/' . $pid)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param int $serviceId
     * @return boolean
     */
    public function isServiceRunning($serviceId) {
        $serv = $this->service->findById($serviceId);
        if (!($serv)) {
            return false;
        }
        return $this->isProcessRunning($serv['pid']);
    }

    /**
     * Запуск скрипта на выполнение отдельным процессом
     * @param string $file
     * @return boolean|int PID или false в случае неудачи
     */
    protected function runSeparate($file) {
        $arr = explode(DS, $file);
        $fl = end($arr);
        $module = $arr[count($arr)-3];
        $f = str_replace('php', 'log', $fl);
        $log = PATH_REAL . DS . 'system/crontab/' . strtolower($f);
        if (file_exists($file)) {
            $command = str_replace("Command.php", "", $fl);
            echo "start: {$file} \n";
            $pid = exec('php '.$this->basePath." " .$module.DS.$command . " >{$log} & echo $!");
            if (!is_numeric($pid)) {
                echo "Failed to run {$file}\n";
                return false;
            }
            echo "PID: " . $pid . "\n";
            return $pid;
        } else {
            echo "file does not exist! {$file} \n";
            return false;
        }
    }

    public function run() {
        if (empty($this->cliIds)) {
            return false;
        }

        foreach ($this->cliIds as $sid) {
            $s = $this->service->findOne("isEnable = 1 AND id = {$sid}");
            if ($this->isProcessRunning($s['pid']) === true) {
                $this->service->updateById($s['id'], array('isActive' => 1));
                continue;
            } else {
                if ($s['isActive'] == 1 && is_numeric($s['pid']) && !empty($s['pid'])) {
                    //реально процесс уже не запущен, убираем статус активности
                    $this->service->updateById($s['id'], array('isActive' => 0));
                    $s['isActive'] = 0;
                }
            }
            if ($s['isActive'] == 0) {
                $path = PATH_REAL.DS."mod".DS.$s['module'].DS."services".DS.$s['file'];
                if (is_file($path)) {
                    echo "start: {$path} \n";

                    $this->service->updateById($s['id'], array("isActive" => 1, "dateStart" => time(), "dateEnd" => 0, "pid" => getmypid()));
                    $this->runSeparate($path);
                } else {
                    echo "file does not exists! {$path} \n";
                }
            }
        }
    }

    /**
     *
     * @return boolean
     */
    public function process() {
        $services = $this->service->findAll("isEnable = 1 AND period = '{$this->key}'");
        if(!$services){
            return false;
        }

        foreach ($services as $s) {
            if ($this->isProcessRunning($s['pid']) === true) {
                //У сервиса неактивный статус, но реальный процесс работает -> ставим статус активный
                $this->service->updateById($s['id'], array('isActive' => 1));
                continue;
            } else {
                if (($s['isActive'] == 1 && is_numeric($s['pid'])) || !($s['pid'])) {
                    //реально процесс уже не запущен, но статус активный-> ставим статус в неактивный
                    $this->service->updateById($s['id'], array('isActive' => 0));
//                    continue;
//                    $s['isActive'] = 0;
                }
            }
            // double check


            $check = $this->service->findById($s['id']);
            $isActive = $check['isActive'];
            if ($isActive == 1) {
                continue;
            }
            $path = PATH_REAL . DS . "mod" . DS . $s["module"] . DS . "services/" . $s["file"];
            if (file_exists($path)) {
                echo "start: {$path} \n";
                if (isset($s['skipSec']) && is_numeric($s['skipSec'])) {
                    echo "sleep" . $s['skipSec'] . "\n";
                    sleep((int) $s['skipSec']);
                } else {
                    echo "Sleep not found\n";
                    sleep(mt_rand(1, 5));
                }
                $pid = $this->runSeparate($path);
                if ($pid === false) {
                    continue;
                }
                $this->service->updateById($s['id'], array("isActive" => 1, "dateStart" => date("U"), "dateEnd" => 0, "pid" => $pid));
            } else {
                echo "file not exists!!! {$path} \n";
            }
        }
    }

    public static function handleStart() {
        error_reporting(E_ALL);
//        sleep(mt_rand(1,30));
        ini_set("memory_limit", "150M");
        ini_set("pcre.backtrack_limit", 10000000);
        ini_set('max_execution_time', 0);
        ini_set('max_input_time', 0);
    }

}
