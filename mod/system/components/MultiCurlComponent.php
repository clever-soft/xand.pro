<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 06.01.16
 * Time: 14:45
 */
class MultiCurlComponent extends CurlComponent
{

    public $mc;
    protected $mcThreads = [];

    public function init($urls) {
        $this->mc = curl_multi_init();
        foreach ($urls as $id => $url) {
            var_dump($url);
            $this->mcThreads[$id] = curl_init();
            $this->setBaseOptions($url);
            curl_setopt_array($this->mcThreads[$id], $this->options);
            curl_multi_add_handle($this->mc, $this->mcThreads[$id]);
        }
    }

    public function callCurl() {
        $running = null;
        do {
            curl_multi_exec($this->mc, $running);
        } while ($running > 0);

        // get content and remove handles
        foreach ($this->mcThreads as $id => $c) {
            $this->curlData[$id] = curl_multi_getcontent($c);
            curl_multi_remove_handle($this->mc, $c);
        }
        $this->curlErr = curl_multi_info_read($this->mc);
        $this->curlInfo = curl_multi_info_read($this->mc);
        var_dump($this->curlData);

        curl_multi_close($this->mc);

        return $this->curlData;
    }
}