<?php

/**
 * Description of CurlComponent
 *
 * @author kubrey
 */
class CurlComponent extends BaseComponent
{

    protected $curlData, $curlErr, $curlInfo;
    public $info = array();
    public $res;

    protected $options = [];

    protected $isMulti = false;

    protected $threads = 10;

    /**
     *
     * @param string $url
     */
    public function init($url) {
        $this->res = curl_init();
        $this->setBaseOptions($url);

    }

    protected function setBaseOptions($url) {
        $this->options[CURLOPT_URL] = $url;
        $this->options[CURLOPT_USERAGENT] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36";
        $this->options[CURLOPT_TIMEOUT] = 60;

        $cookie = "cookie.txt";
        $this->options[CURLOPT_COOKIEJAR] = $cookie;
        $this->options[CURLOPT_COOKIE] = $cookie;
        $this->options[CURLOPT_FOLLOWLOCATION] = 1;
        $this->options[CURLOPT_RETURNTRANSFER] = 1;
    }


    /**
     *
     */
    public function callCurl() {
        curl_setopt_array($this->res, $this->options);
        $this->curlData = curl_exec($this->res);
        $this->curlErr = curl_error($this->res);
        $this->curlInfo = curl_getinfo($this->res);
        $this->info = $this->curlInfo;
        curl_close($this->res);
    }

    /**
     *
     * @return string
     */
    public function err() {
        return $this->curlErr;
    }

    /**
     *
     * @return array
     */
    public function info() {
        return $this->curlInfo;
    }

    /**
     *
     * @return array
     */
    public function data() {
        return $this->curlData;
    }

    public function useTor($torport = 9050) {
//        $torport = 8118;
        $this->options[CURLOPT_AUTOREFERER] = 1;
        $this->options[CURLOPT_RETURNTRANSFER] = 1;
        $this->options[CURLOPT_PROXY] = '127.0.0.1:' . $torport;
        $this->options[CURLOPT_PROXYTYPE] = 7;
        $this->options[CURLOPT_TIMEOUT] = 120;
        $this->options[CURLOPT_VERBOSE] = 0;
        $this->options[CURLOPT_HEADER] = 0;
    }

    public function useProxy($ip) {
        $this->options[CURLOPT_PROXY] = $ip;
    }

}
