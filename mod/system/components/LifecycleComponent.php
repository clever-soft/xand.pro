<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 26.11.15
 * Time: 9:09
 */
class LifecycleComponent extends BaseComponent
{

    protected $shutdownStack = [];

    public function __construct() {
        register_shutdown_function(array($this, 'shutdown'));
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function registerShutdown(Closure $callback) {
        $this->shutdownStack[] = $callback;
        return $this;
    }

    /**
     * shutdown фнкция
     */
    public function shutdown() {
        foreach ($this->shutdownStack as $cb) {
            $cb();
        }
    }

    /**
     * @return $this
     */
    public function start(){
        $s = new Sessions();
        session_start();
        Debug::init();

        return $this;
    }

}