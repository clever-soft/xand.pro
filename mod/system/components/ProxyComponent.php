<?php

/**
 * Description of ProxyComponent
 *
 * @author kubrey
 */
class ProxyComponent extends BaseComponent {

    protected $file;
    protected $model;
    public $db = true;
    protected $curl;

    public function __construct() {
        parent::__construct();
        $this->curl = new CurlComponent;
        $this->model = new ProxyList();
        if (!$this->db) {
            if (!is_dir(PATH_REAL . DS . "mod/system/etc/")) {
                mkdir(PATH_REAL . DS . "mod/system/etc/");
            }
            $this->file = PATH_REAL . DS . "mod/system/etc/proxy.txt";
        }
    }

    /**
     * 
     * @return array
     */
    public function getProxyList() {
        $list = array();
        if (!$this->db) {
            $f = fopen($this->file, "r");
            while ($row = fgets($f)) {
                if (!$row) {
                    continue;
                }
                $list[] = trim($row);
            }
        } else {
            $data = $this->model->findAll();
            if (!$data) {
                $this->setError($this->model->getLastError());
                return array();
            }
            foreach ($data as $row) {
                $list[] = $row['address'];
            }
        }
        return $list;
    }

    /**
     * 
     * @param string $addr ip:port
     * @param array $params [success=>bool,time=>float]
     * @return boolean
     */
    public function updateParams($addr, $params) {
        $curr = $this->model->findOne("address=?", array(), array($addr));
        if (!$curr) {
            $this->setError($this->model->getLastError());
            return false;
        }
        if ($params['success']) {
            $num = (int)$curr['okCounter'] + 1;
            $avgTime = ($curr['okCounter'] * $curr['avgTime'] + $params['time']) / $num;
            if (!$this->model->updateById($curr['id'], array('touchedAt' => time(), 'okCounter' => $num, 'avgTime' => $avgTime, 'isActive' => 1))) {
                $this->setError($this->model->getLastError());
                return false;
            }
        } else {
            echo $addr;
            if (!$this->model->updateById($curr['id'], array('touchedAt' => time(), 'isActive' => 0))) {
                $this->setError($this->model->getLastError());
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @return boolean|string
     */
    public function getRandomProxy() {
        if (!$this->db) {
            $list = $this->getProxyList();
            if (!$list) {
                return false;
            }
            $key = array_rand($list);
            return $list[$key];
        }
        $row = $this->model->selectSafe('select `address` from proxy_list where isActive=1 order by okCounter DESC limit 0,10');
        if ($row === false) {
            $this->setError($this->model->getLastError());
            return false;
        }
        if (!$row) {
            return false;
        }
        $rand = array_rand($row);
        return $row[$rand]['address'];
    }

    /**
     * 
     * @return boolean|array
     */
    public function search() {
        $ports = array("80", "8080", "3127", "3128", "1080", "8089");
        $port = $ports[rand(0, count($ports) - 1)];

        $uri = "http://hideme.ru/proxy-list/?ports=" . $port . "&anon=234";
        $this->curl->init($uri);

        $cookie = "jv_enter_ts_100415=1414234751683; jv_pc_100415=4; jv_gui_state_100415=WIDGET";

        curl_setopt($this->curl->res, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($this->curl->res, CURLOPT_HTTPHEADER, array('Host: hideme.ru'));
        curl_setopt($this->curl->res, CURLOPT_REFERER, "http://hideme.ru");
        curl_setopt($this->curl->res, CURLOPT_COOKIE, $cookie);
        
        $this->curl->callCurl();
        if ($this->curl->data() === false) {
            $this->setError($this->curl->err());
            return false;
        }
        $im = preg_match_all('/' . preg_quote('class=tdl>', '/') . '(.*)' . preg_quote('</td>', '/') . '/Us', $this->curl->data(), $match);
//        var_dump($match[1]);
        if (!isset($match[1]) || !$match[1]) {
            return array();
        }
        $list = array();
        foreach ($match[1] as $ind => $pr) {
            $list[] = $pr . ":" . $port;
        }
        return $list;
    }

    public function update($list) {
        if (!$this->db) {
            $current = $this->getProxyList();
            foreach ($list as $proxy) {
                if (!in_array($proxy, $current)) {
                    file_put_contents($this->file, $proxy . "\n", FILE_APPEND);
                }
            }
        } else {
            foreach ($list as $addr) {
                if ($this->model->findOne("address=?", array($this->model->getId()), array($addr))) {
                    continue;
                }
                $this->model->insert(array('touchedAt' => time(), 'address' => $addr,'isActive'=>1));
            }
        }
    }

}
