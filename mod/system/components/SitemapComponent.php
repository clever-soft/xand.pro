<?php

/**
 * Description of SitemapComponent
 *
 * @author kubrey
 */
class SitemapComponent extends BaseComponent {

    protected $map = array();

    /**
     * @outputBuffering enabled
     * @param string $lang
     * @param boolean $reload переписать кэш
     * @return array
     */
    public function build($lang = null, $reload = false) {
        $cacheKey = md5(__METHOD__ . $lang);
        $cache = MCache::Get($cacheKey);
        if (!$cache || $reload) {
            $modules = CpServices::getModulesList();
            foreach ($modules as $m) {
                $file = PATH_REAL . DS . "mod/" . strtolower($m['module']) . "/components/" . ucfirst(strtolower($m['module'])) . "MapComponent.php";
                if (!file_exists($file)) {
                    continue;
                }
                $class = ucfirst(strtolower($m['module'])) . "MapComponent";
                if (!class_exists($class)) {
                    $this->setError("No class $class");
                    continue;
                }
                $mapComponent = new $class();
                $map = $mapComponent->getMap($lang);
                if (!$map) {
                    continue;
                }
                foreach ($map as $cm) {
                    $this->map[] = $cm;
                }
            }
            $do = MCache::Set($cacheKey, $this->map, 3600);
        } else {
            $this->map = $cache;
        }

        return $this->map;
    }

}
