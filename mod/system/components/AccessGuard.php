<?php

/**
 * Description of AccessGuard
 *
 * @author kubrey
 */
class AccessGuard extends BaseComponent
{

    const LEVEL_GUEST = '?';
    const LEVEL_USER = '@';
    const LEVEL_ADM_PANEL = 'crm';

    protected $currModule, $currController;

    /**
     * @param $module
     * @param $controller
     * @return $this
     */
    public function setEnvironment($module, $controller) {
        $this->currModule = $module;
        $this->currController = $controller;
        return $this;
    }

    /**
     *
     * @param string $key имя экшена, * - все экшены
     * @param array $rule описание правила
     * @param string $currAction текущий экшен
     * @return array|boolean true или массив для редиректа
     */
    public function checkRule($key, $rule, $currAction) {
        $deny = array('/', 403, $rule['msg']);
        //проверяем на гостя
        switch ($rule['level']) {
            case self::LEVEL_GUEST:
                if (!$this->checkGuestAccess($key, $rule, $currAction)) {
                    return $deny;
                }
                break;
            case self::LEVEL_USER:
                if (!$this->checkUserAccess($key, $rule, $currAction)) {
                    return $deny;
                }
                break;
            case self::LEVEL_ADM_PANEL:
                if (!$this->checkCrmAccess($key, $rule, $currAction)) {
                    return $deny;
                }
                break;
        }
        return true;
    }

    /**
     *
     * Доступ только для пользователей админки
     * с учетом прав, установленных в ролях и у самого юзера
     * @param $key
     * @param $rule
     * @param $currAction
     * @return bool
     */
    protected function checkCrmAccess($key, $rule, $currAction) {
        if (!isset($_SESSION[AUTH_KEY])) {
            $this->callError($rule, $currAction);
            return false;
        }

        $userModel = new UserModel();
        $user = $userModel->findById($_SESSION[AUTH_KEY][$userModel->getId()]);
        if (!$user) {
            $this->callError($rule, $currAction);
            return false;
        }
        if ($user['isSu']) {
            $this->callSuccess($rule, $currAction);
            return true;
        }

        $accesses = UserModel::GetAccessList($_SESSION[AUTH_KEY][$userModel->getId()], UsersAccessesModel::ACCESS_TYPE_CONTROLLER);


        if ($accesses === false) {
            $this->callError($rule, $currAction);
            return false;
        }

        $actionMethod = "action" . ucfirst(strtolower($currAction));

        //checking access in user roles
        foreach ($accesses as $path) {
            list($module, $controller, $action) = explode('.', $path);

            if ($module == $this->currModule && $controller == $this->currController && $actionMethod == $action) {

                $this->callSuccess($rule, $currAction);
                return true;
            }
        }

        $this->callError($rule, $currAction);
        return false;
    }

    /**
     *
     * @param  $key
     * @param  $rule
     * @param  $currAction
     * @return boolean
     */
    protected function checkGuestAccess($key, $rule, $currAction) {
        if ($key == '*') {
            if (CustomersModel::isGuest()) {
                $this->callError($rule, $currAction);
                return false;
            }
        } elseif (strtolower($currAction) == $key) {
            if (CustomersModel::isGuest()) {
                $this->callError($rule, $currAction);
                return false;
            }
        }
        $this->callSuccess($rule, $currAction);
        return true;
    }

    /**
     *
     * @param string $key
     * @param array $rule
     * @param string $currAction
     * @return boolean
     */
    protected function checkUserAccess($key, $rule, $currAction) {
        $cust = new CustomersModel;
        $fields = $cust->getAttributes();
        $fieldRules = array();
        foreach ($fields as $f) {
            if (isset($rule['_' . $f])) {
                $fieldRules[$f] = $rule['_' . $f];
            }
        }
        //гостям запрещено 100%
        if (CustomersModel::isGuest()) {
            $this->callError($rule, $currAction);
            return false;
        }
        //текущий юзер
        $user = $cust->getUser();

        if ($key == '*') {
            foreach ($fieldRules as $key => $vals) {
                if (!in_array($user[$key], $vals)) {
                    $this->callError($rule, $currAction);
                    return false;
                }
            }
        } elseif (strtolower($currAction) == $key) {
            foreach ($fieldRules as $key => $vals) {
                if (!in_array($user[$key], $vals)) {
                    $this->callError($rule, $currAction);
                    return false;
                }
            }
        }
        $this->callSuccess($rule, $currAction);

        return true;
    }

    /**
     *
     * @param  $rule
     * @param  $currAction
     */
    protected function callError($rule, $currAction) {
        if (array_key_exists('call', $rule) && is_callable($rule['call'])) {
            call_user_func_array($rule['call'], array('input' => strtolower($currAction)));
        }
    }

    /**
     *
     * @param array $rule
     * @param string $currAction
     */
    protected function callSuccess($rule, $currAction) {
        if (array_key_exists('callSuccess', $rule) && is_callable($rule['callSuccess'])) {
            call_user_func_array($rule['callSuccess'], array('input' => strtolower($currAction)));
        }
    }

    /**
     * Сканирует контроллеры модулей
     * Только те контроллеры, у которых метод getAccessRules что-то возвращает(есть правила)
     * и только экшены
     * @param bool $reload
     * @return array
     */
    public static function reflectModules($reload = false) {
        $cacheKey = md5(__METHOD__);
        $cache = Mcache::get($cacheKey);
        if ($cache && !$reload) {
            return $cache;
        }
        $modules = ['admin'];

        $restrictedControllers = [];//контроллеры с ограничениями доступа
        foreach ($modules as $m) {
            $path = PATH_REAL . DS . PATH_MODULES . DS . $m;
            $controllers = glob($path . DS . "controllers" . DS . "*Controller.php");
            $restrictedControllers[$m] = [];
            foreach ($controllers as $file) {
                require_once $file;
                $className = str_replace('.php', '', basename($file));
                if (!class_exists($className)) {
                    continue;
                }
                $meths = [];
                $controllReflect = new ReflectionClass($className);
                if (!$controllReflect->hasMethod('getAccessRules')) {
                    continue;
                }
                try {
                    $contr = $controllReflect->newInstance();
                    if (!$contr->getAccessRules()) {
                        continue;
                    }
                    $methods = $controllReflect->getMethods(ReflectionMethod::IS_PUBLIC);
                    foreach ($methods as $meth) {
                        if ($meth->class == $controllReflect->getName() && strpos($meth->name, 'action') === 0) {
                            $phpDoc = $meth->getDocComment();
                            $docLines = explode("\n", $phpDoc);
                            $doc = '';
                            foreach ($docLines as $ln) {
                                $line = trim($ln, '/* ');
                                if (!$line) {
                                    continue;
                                }
                                if (strpos($line, '@') === 0) {
                                    continue;
                                }
                                $doc .= $line . " ";
                            }
                            $meths[$meth->name] = trim($doc);
                        }
                    }
                    $restrictedControllers[$m][$className] = $meths;
                } catch (ReflectionException $e) {
                    continue;
                }
            }
        }

        Mcache::set($cacheKey, 60);

        return $restrictedControllers;
    }

}
