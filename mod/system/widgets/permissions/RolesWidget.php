<?php

/**
 * Class RolesWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RolesWidget extends Widget
{

    /**
     *
     */
    public function init()
    {
    }

    /**
     * @return $this
     */
    public function GetContent()
    {
        $query = new QueryMulti();

        $query->Select(array(
            "roles" => array("*"),
            "users" => array("GROUP_CONCAT(users.username SEPARATOR ', ') AS users")
        ));

        $query->LeftOuterJoin("users_roles", "users_roles.idRole = roles.idRole");
        $query->LeftOuterJoin("users", "users.idUser = users_roles.idUser");
        $query->GroupBy("roles.idRole");

        $this->setProperty("title", "Roles list")
            ->Add($query)
            ->SetDefaultOrder("idRole");


        $table = new Table(
            new TableColumn("idRole", "ID", 80, "center"),
            new TableColumn("name", "Name", 200),
            new TableColumn("users", "Users"),
            new TableColumnDateUpdate(),
            new TableColumnDateCreate(),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonRoles(),
            new ButtonDelete()
        );

        $table->SetPrimary("idRole");

        $this->Add($table)
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")));


        return $this;
    }

    /**
     *
     */
    public function actionRolesReady()
    {

        $idRole = $this->getPostDataInt();
        $data = array();

        $accessDb = SDbQuery::table("roles_accesses")->where(["idRole" => $idRole])->select();

        foreach ($accessDb as $k => $v) {
            if ($v['type'] == 'controller') {
                $data[0]["controller_" . $v["path"]] = true;
            } else {
                $data[0][$v["path"]] = true;
            }
        }


        $role = SDbQuery::table("roles")->where(["idRole" => $idRole])->selectRow();

        $data[0]["idRole"] = $role["idRole"];
        $data[0]["name"] = $role["name"];

        $form = new ResourcesFormStatic("Permissions editor", $data, array("width" => 800));
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Prepare("RolesFinish");
        $form->Draw();

        Message::I()->Draw();

        exit();
    }

    /**
     *
     */
    public function actionRolesFinish()
    {
        $arr_data = $this->getPostDataJson();
        $idRole = $arr_data["idRole"];
        $name = $arr_data['name'];
        unset($arr_data["name"]);
        unset($arr_data["idRole"]);

        $widgets = $controls = [];
        foreach ($arr_data as $key => $val) {
            if (!$val) {
                continue;
            }
            if (strpos($key, "controller_") === 0) {
                $controls[str_replace("controller_", "", $key)] = $val;
            } else {
                $widgets[$key] = $val;
            }
        }


        SDbQuery::table("roles")
            ->where(["idRole" => $idRole])
            ->update(["name" => $name,
                "dateUpdate" => time()]);

        SDbQuery::table("roles_accesses")
            ->where(["idRole" => $idRole])
            ->delete();

        foreach ($widgets as $k => $v) {
            if (!$v || $k == "idRole") continue;

            SDbQuery::table("roles_accesses")
                ->insert(["idRole" => $idRole, "path" => $k, 'type' => 'widget']);

        }

        foreach ($controls as $k => $v) {
            if (!$v || $k == "idRole") continue;

            SDbQuery::table("roles_accesses")
                ->insert(["idRole" => $idRole, "path" => $k, 'type' => 'controller']);
        }

        Message::I()->Success("Role was updated successful")->Draw();
    }

    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormModal("Add new role");
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputCheckbox("Active", "isActive"));
        $form->Add(new InputText("Name", "name"));

        $users = SDbQuery::table("users")
            ->select(["idUser", "CONCAT(firstname,' ',lastname) AS name"]);

        $panel = new Panel("Users");
        foreach ($users as $d) {
            $panel->Add(new InputCheckbox($d["name"], "user_" . $d["idUser"]));
        }
        $form->Add($panel);

        $form->Add(new ButtonAdd(NULL, "Add", array('script' => 'JsonModalCloseAct')));
        $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $data = $this->getPostDataJson();

        SDbQuery::table("roles")
            ->insert([
                "name" => $data["name"],
                "isActive" => $data["isActive"],
                "dateCreate" => time(),
                "dateUpdate" => time()
            ]);

        $idRole = SDbBase::getLastInsertedId();

        SDbQuery::table("users_roles")
            ->where(["idRole" => $idRole])
            ->delete();

        Debug::Dump($data);

        foreach ($data as $k => $d) {

            if (strpos($k, "user_") === false)
                continue;

            if ($d == 0)
                continue;

            $k = str_replace("user_", "", $k);
            $k = (int)$k;

            if (is_numeric($k) && $k > 0) {

                SDbQuery::table("users_roles")
                    ->insert([
                        "idRole" => $idRole,
                        "idUser" => $k
                    ]);
            }

        }

        Message::I()->Success("Role was inserted successfully");
    }

    /**
     *
     */
    public function actionUpdateReady()
    {


        $role = SDbQuery::table("roles")->where(["idRole" => $this->getPostDataInt()])->selectRow();

        $form = new FormModal("Edit new role", $role);
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputCheckbox("Active", "isActive"));
        $form->Add(new InputText("Name", "name"));


        $users = SDbQuery::table("users")
            ->select(["idUser", "CONCAT(firstname,' ',lastname) AS name"]);

        $panel = new Panel("Users");
        foreach ($users as $d) {
            $access = SDbQuery::table("users_roles")
                ->where([
                    "idUser" => $d["idUser"],
                    "idRole" => $role['idRole']
                ])
                ->count();

            $panel->Add(new InputCheckbox($d["name"], "user_" . $d["idUser"], array("dataForce" => $access)));
        }
        $form->Add($panel);

        $form->Add(new InputVar("idRole", "idRole"));
        $form->Add(new ButtonSave(NULL, NULL, array('script' => 'JsonModalCloseAct')));
        $form->Add(new ButtonCancel(NULL, NULL, array("script" => "ModalClose")));
        $form->Draw();

        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $idRole = $data["idRole"];


        SDbQuery::table("roles")
            ->where(["idRole" => $idRole])
            ->update(["name" => $data["name"],
                "isActive" => $data["isActive"],
                "dateUpdate" => time()]);


        SDbQuery::table("users_roles")
            ->where(["idRole" => $idRole])
            ->delete();

        foreach ($data as $k => $d) {

            if (strpos($k, "user_") === false)
                continue;

            if ($d == 0)
                continue;

            $k = str_replace("user_", "", $k);
            $k = (int)$k;

            if (is_numeric($k) && $k > 0) {

                SDbQuery::table("users_roles")
                    ->insert([
                        "idRole" => $idRole,
                        "idUser" => $k
                    ]);
            }

        }

        Message::I()->Success("Role was updated successfully");
    }

    /**
     *
     */
    public function actionDelete()
    {

        $idRole = $this->getPostDataInt();
        SDbQuery::table("roles")
            ->where(["idRole" => $idRole])
            ->delete();

        SDbQuery::table("roles_accesses")
            ->enableMessages()
            ->where(["idRole" => $idRole])
            ->delete();
    }


    /**
     * @return $this
     */
    public function PostProcessData()
    {
        foreach ($this->data as &$val) {
        }
        return $this;
    }


}