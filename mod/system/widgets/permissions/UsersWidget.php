<?php

/**
 * Class UsersWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class UsersWidget extends Widget
{
    /**
     *
     */
    public function init()
    {
        $this->tableName = "users";
        $this->primaryKey = "idUser";
    }

    /**
     * @return $this
     */
    public function GetContent()
    {


        $query = new QueryMulti();
        $query->Select(array(

            $this->tableName => array("*"),
            "roles" => array("GROUP_CONCAT(roles.name SEPARATOR ', ') AS roles"),

        ));

        $query->LeftOuterJoin("users_roles", "users_roles.idUser = users.idUser");
        $query->LeftOuterJoin("roles", "users_roles.idRole = roles.idRole");
        $query->GroupBy("users.idUser");

        $this->setProperty("title", "Users list")
            ->SetDefaultOrder($this->primaryKey, false)
            ->Add($query);

        $table = new Table(
            new TableColumnLogicIconOffOn("isActive", "A", 30, "center"),
            new TableColumnLogicIconOffOn("isSu", "SU", 30, "center"),
            new TableColumn($this->primaryKey, "ID", 60, "center"),
            new TableColumn("username", "Login"),

            new TableColumn("firstname", "Firstname"),
            new TableColumn("lastname", "Lastname"),
            new TableColumn("email", "E-mail"),
            new TableColumn("roles", "Roles", NULL, NULL, false, false), //
            new TableColumn("attemptsLogin", "AL", 50, "center"),

            new TableColumnDate("dateLogin", "Login"),

            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(), new ButtonRule("0", "isActive", "equally")),
            new ButtonRoles(NULL, NULL, array("script" => "ModalAct")),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()

        );

        $table->SetPrimary("idUser");

        $this->Add($table)
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")));
 
        return $this;

    }

    /**
     *
     */
    public function actionRolesReady()
    {

        $data = SDb::rows("SELECT roles.*, users_roles.idUser
									FROM roles
									LEFT OUTER JOIN users_roles ON roles.idRole = users_roles.idRole
									AND users_roles.idUser = ?
                                    GROUP BY roles.idRole", $this->getPostDataInt());

        $form = new FormModal("Editing role user");
        $form->assignParent($this)->cloneAjax()->cloneState();
        foreach ($data as $d) {
            $ch = ($d["idUser"]) ? true : false;
            $form->Add(new InputCheckbox($d["name"], "role_" . $d["idRole"], NULL, $ch));
        }

        $form->Add(new InputVar("idUser", "idUser", NULL, $this->getPostDataInt()));
        $form->Add(new ButtonSave("RolesFinish", "Save", array('script' => 'JsonModalCloseAct')));
        $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionRolesFinish()
    {

        $data = $this->getPostDataJson();

        $idUser = $data["idUser"];
        unset($data["idUser"]);

        SDbQuery::table("users_roles")->where(["idUser" => $idUser])->delete();

        foreach ($data as $k => $d) {
            if ($d) {
                $k = str_replace("role_", "", $k);
                $k = (int)$k;

                if (is_numeric($k) && $k > 0) {
                    SDbQuery::table("users_roles")
                        ->enableMessages()
                        ->insert(["idRole" => $k, "idUser" => $idUser]);
                }
            }
        }


    }

    /**
     *
     */
    public function actionAddReady()
    {

        $form = new FormModal("Add new user");
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputText('Username (login)', 'username', array()));
        $form->Add(new InputPass('Password', 'password', array()));
        $form->Add(new InputText('Firstname', 'firstname', array()));
        $form->Add(new InputText('Lirstname', 'lastname', array()));
        $form->Add(new InputEmail('E-mail', 'email', array()));
        $form->Add(new InputCheckbox('Active', 'isActive'));
        $form->Add(new InputCheckbox('Super user', 'isSu'));

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $arr_data = $this->getPostDataJson();

        if ($arr_data["password"] != "") {
            $salt = substr(md5(rand(1, 100)), 0, 2);
            $arr_data["password"] = md5($salt . $arr_data["password"]) . ":" . $salt;
        } else {
            Message::I()->Error("Password is a required field");
            return;
        }

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->insert($arr_data);

    }

    /**
     *
     */
    public function actionUpdateReady()
    {

        $data = SDbQuery::table($this->tableName)
            ->where(["idUser" => $this->getPostDataInt()])
            ->select();

        $data[0]["password"] = "";

        $form = new FormModal("User edit form", $data);

        $form->assignParent($this)->cloneAjax()->cloneState();
        $form->Add(new InputVar('idUser', 'idUser', array('width' => 500), $this->getPostDataInt()));

        $form->Add(new InputText('Username (login)', 'username', array()));
        $form->Add(new InputPass('Password', 'password', array()));
        $form->Add(new InputText('Firstname', 'firstname', array()));
        $form->Add(new InputText('Lirstname', 'lastname', array()));
        $form->Add(new InputEmail('E-mail', 'email', array()));
        $form->Add(new InputCheckbox('Active', 'isActive'));
        $form->Add(new InputCheckbox('Super user', 'isSu'));

        $form->Add(new ButtonSave(NULL, NULL, array('script' => 'JsonModalCloseAct')));
        $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {

        $data = $this->getPostDataJson();

        if ($data["password"] != "") {
            $salt = substr(md5(rand(1, 100)), 0, 2);
            $data["password"] = md5($salt . $data["password"]) . ":" . $salt;
        } else {
            unset($data["password"]);
        }

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $data["idUser"]])
            ->update($data);
    }

    /**
     *
     */
    public function actionPublished()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->update(["isActive" => 1]);

    }

    /**
     *
     */
    public function actionUnpublished()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->update(["isActive" => 0]);

    }

    /**
     *
     */
    public function actionDelete()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->delete();
    }


}