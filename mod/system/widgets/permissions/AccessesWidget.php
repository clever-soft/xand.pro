<?php

/**
 * Class AccessesWidget
 * @property UserModel $user
 * @property UsersAccessesModel $userAccess
 */
class AccessesWidget extends Widget
{
    protected $user, $userAccess;

    public function init() {
        $this->user = new UserModel();
        $this->userAccess = new UsersAccessesModel();
    }

    public function DrawBody() {


        if (!$_SESSION[AUTH_KEY]['isSu'])
            Message::I()->Error("Only super users, has access to this tab")->E();

        $postPath = $this->GetPostPath();
        if (!is_numeric($postPath)) {
            $postPath = $this->user->findField("", "idUser");

//            $postPath = DbOnce::Field("users", "idUser");
        }
        $idUser = (int)$postPath;
        //----
        switch ($this->getPostAction()) {

            case 'UpdateFinish' :
                $ins = $this->getPostDataJson();

                $idUser = (int)$ins["idUser"];
                $widgets = $controls = [];
                foreach ($ins as $key => $val) {
                    if ($key == "idUser" || !$val) {
                        continue;
                    }
                    if (strpos($key, "controller_") === 0) {
                        $controls[str_replace("controller_", "", $key)] = $val;
                    } else {
                        $widgets[$key] = $val;
                    }
                }

                //$this->userAccess->deleteByAttributes("idUser=?", [$idUser]);
                SDbQuery::table("users_accesses")->where(["idUser" => $idUser])->delete();

                foreach ($controls as $k => $v) {
                    $this->userAccess->insert(["idUser" => $idUser,
                        "path" => $k, 'type' => 'controller']);
                }

                foreach ($widgets as $k => $v) {
                    $this->userAccess->insert(["idUser" => $idUser,
                        "path" => $k, 'type' => 'widget']);
                }

                Message::I()->Success("Permission added successful");
                break;

        }

        //----

        $dataRoles = array();
        $data = array();
        $data[0] = array();
        $data[0]["idUser"] = $idUser;

        $accessDb = $this->userAccess->findAll("idUser=?", [], [$idUser]);
        foreach ($accessDb as $k => $v) {
            if ($v['type'] == 'controller') {
                $data[0]["controller_" . $v["path"]] = true;
            } else {
                $data[0][$v["path"]] = true;
            }
        }

        $accessDb = $this->userAccess->selectSafe("SELECT GROUP_CONCAT(' ', roles.`name`) AS groups,
                                           roles_accesses.`path`,roles_accesses.type
                                    FROM users_roles
                                    JOIN `roles_accesses` ON roles_accesses.`idRole` = users_roles.`idRole`
                                    JOIN `roles` ON users_roles.`idRole` = roles.`idRole`
                                    WHERE users_roles.`idUser` = ?
                                    GROUP BY path", [$idUser]);


        if ($accessDb) {
            foreach ($accessDb as $k => $v) {
                if($v['type']=='controller'){
                    $v['path'] = 'controller_'.$v['path'];
                }
                $dataRoles[$v["path"]] = trim($v["groups"]);
            }
        }

//        Debug::Dump($dataRoles);


        //------
        $form = new ResourcesFormStatic("Permissions editor", $data, array("width" => 800));
        $form->Add(new Ajax(PATH_DS . "admin/widgets/system/permissions/accesses", false, "html", "ajaxAccesses"));

        $form->Prepare("UpdateFinish", $idUser, $dataRoles);
        $form->Draw();

        Message::I()->Draw();

        //---

        ?>
        <script>
            function ChangeUser(dom) {
                <?php echo $form->ajax->eid; ?>.
                Go($(dom).val())
            }
        </script>

        <?php

        return $this;

    }
}

 