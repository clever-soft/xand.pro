<?php

/**
 * Class MemcacheWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class MemcacheWidget extends Widget
{

    /**
     * @return $this
     */
    public function DrawBody()
    {

        $info = $this->GetInfo(MCache::GetStats());

        $data = array();

        $form = new FormStatic("Memcache managemant", $data, array("width" => 600));
        $form->Add(new Ajax(PATH_DS . "admin/widgets/system/cache/memcache", false, "html", "ajaxMemcache"));
        $form->Add(new ControlTable("Info", NULL, array("columnNames" => array("Option", "Value")), $info));

        $form->Add(new ButtonAdd("FlushCache", "Flush cache"));
        $form->Draw();

        Message::I()->Draw();

        return $this;

    }

    /**
     * @param $status
     *
     * @return array
     */
    private function GetInfo($status)
    {

        $result = array();

        $result[] = array("Memcache Server version: ", $status["version"]);
        $result[] = array("Process id of this server process ", $status["pid"]);
        $result[] = array("Number of seconds this server has been running ", $status["uptime"]);
        $result[] = array("Total number of items stored by this server ever since it started ", $status["total_items"]);
        $result[] = array("Number of open connections ", $status["curr_connections"]);
        $result[] = array("Total number of connections opened since the server started running ", $status["total_connections"]);
        $result[] = array("Number of connection structures allocated by the server ", $status["connection_structures"]);
        $result[] = array("Cumulative number of retrieval requests ", $status["cmd_get"]);
        $result[] = array(" Cumulative number of storage requests ", $status["cmd_set"]);

        if ($status["cmd_get"] > 0) {
            $percentCacheHit = round(((real)$status["get_hits"] / (real)$status["cmd_get"] * 100), 2);
            $percentCacheHit = round($percentCacheHit, 3);
            $percentCacheMiss = 100 - $percentCacheHit;
        }
        else
        {
            $percentCacheHit = "-//-";
            $percentCacheMiss = "-//-";
        }

        $result[] = array("Number of keys that have been requested and found present ", $status["get_hits"] . " ({$percentCacheHit}%) ");
        $result[] = array("Number of items that have been requested and not found ", $status["get_misses"] . " ($percentCacheMiss%)");

        $MBRead = round($status["bytes_read"] / (1024 * 1024), 2);
        $result[] = array("Total number of bytes read by this server from network", $MBRead . " MB");
        $MBWrite = round($status["bytes_written"] / (1024 * 1024), 2);

        $result[] = array("Total number of bytes sent by this server to network", $MBWrite . " MB");
        $MBSize = round($status["limit_maxbytes"] / (1024 * 1024), 2);
        $result[] = array("Number of bytes this server is allowed to use for storage.", $MBSize . " MB");
        $result[] = array("Number of valid items removed from cache to free memory for new items.", $status["evictions"]);

        return $result;
    }

    /**
     *
     */
    public function actionFlushCache()
    {
        MCache::Flush();
        Message::I()->Success("Memcache flushed");
    }
}
