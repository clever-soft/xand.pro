<?php

/**
 * Class ServicesWidget
 * @property CpServices $services
 */
class ServicesWidget extends Widget {

    protected $services;

    public function init() {
        $this->services = new CpServices();
    }

    public function getContent() {
        $query = new QuerySimple();
        $query->Select(array("system_services" => array("*")));

        $title = "Cron services";

        $this->setProperty("title", $title)
                ->SetDefaultOrder("id", false)
                ->Add($query)
                ->Add(new ActionCheckboxMultiple("Status", array("field" => "isActive", "Unfolded" => false), array(
                    array("data" => "1", "equal" => "1", "title" => "not hidden"),
                    array("data" => "0", "equal" => "0", "title" => "hidden")
                        ), new RuleCheckboxEqual("isActive")))
                ->Add(new ButtonAppend(NULL, "Add new", array('script' => "ModalAct")));

        $table = new Table(
                new TableColumn("id", "ID", 40, "center"), //
                new TableColumn("name", "Name"), //
                new TableColumn("pid", "PID"), //
                new TableColumnLightningIcon("isActive", "Active"), //
                new TableColumnLogic("isEnabled", "Enabled"), //
                new TableColumn("module", "Module"), //
                new TableColumn("descr", "Description"), //
                new TableColumnDate("dateStart", "Last start"), //
                new TableColumnDate("dateEnd", "Last end"), //
                new ButtonEdit(NULL, NULL, array('script' => "ModalAct")), //
                new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(), new ButtonRule("0", "isEnable", "equally")), //
                new ButtonDelete()
        );


        $table->SetPrimary("id");

        $this->Add($table);


        if ($this->data != null) {
            foreach ($this->data as &$val) {
                
            }
        }

        return $this;
    }

    public function actionAddReady() {
        $form = new FormModal("Adding a new service");
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputText("Name", "name"));
        $form->Add(new InputTextArea("Description", "descr"));

        $form->Add(new ControlSelect("File", "file", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getServicesList(), array("path", "file"))));

        $form->Add(new ControlSelect("Module", "module", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getModulesList(), array("module", "module"))));

        $form->Add(new ControlSelect("Period", "period", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getPeriodsList(), array("period", "period"))));
        $form->Add(new InputNumeric("Offset, sec", "skipSec"));
        $form->Add(new InputCheckbox("Enable", "isEnable"));

        $form->Add(new ButtonAdd(NULL, "Add", array("script" => "JsonModalCloseAct")), new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();

        exit();
    }

    public function actionAddFinish() {
        $data = $this->getPostDataJson();
        if ($this->services->insert($data)) {
            Message::I()->Info("Service successfully added");
        } else {
            Message::I()->Error("" . $this->services->getLastError());
        }
    }

    public function actionUpdateReady() {
        $data = $this->services->findById($this->getPostDataInt());
        $form = new FormModal("Edit service " . $data['id'], array($data));
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputText("Name", "name"));
        $form->Add(new InputVar("id", "id"));
        $form->Add(new InputTextArea("Description", "descr"));

        $form->Add(new ControlSelect("File", "file", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getServicesList(), array("path", "file"))));

        $form->Add(new ControlSelect("Module", "module", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getModulesList(), array("module", "module"))));

        $form->Add(new ControlSelect("Period", "period", array("noChoose" => false), NULL, ControlSelect::DataFormat(CpServices::getPeriodsList(), array("period", "period"))));
        $form->Add(new InputNumeric("Offset, sec", "skipSec"));
        $form->Add(new InputCheckbox("Enable", "isEnable"));

        $form->Add(new ButtonSave(NULL, "Update", array("script" => "JsonModalCloseAct")), new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();

        exit();
    }

    public function actionUpdateFinish() {
        $data = $this->getPostDataJson();
        $id = $data[$this->services->getId()];
        unset($data[$this->services->getId()]);
        if ($this->services->updateById($id, $data)) {
            Message::I()->Info("Service updated");
        } else {
            Message::I()->Error("" . $this->services->getLastError());
        }
    }

    public function actionDelete() {
        if ($this->services->delete($this->getPostDataInt())) {
            Message::I()->Info("Service removed");
        } else {
            Message::I()->Error("" . $this->services->getLastError());
        }
    }

    public function actionPublished() {
        if ($this->services->updateById($this->getPostDataInt(), array('isEnable' => 1))) {
            Message::I()->Info("Service enabled");
        } else {
            Message::I()->Error("" . $this->services->getLastError());
        }
    }

    public function actionUnpublished() {
        if ($this->services->updateById($this->getPostDataInt(), array('isEnable' => 0))) {
            Message::I()->Info("Service disabled");
        } else {
            Message::I()->Error("" . $this->services->getLastError());
        }
    }

}
