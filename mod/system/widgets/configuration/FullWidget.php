<?php

class FullWidget extends Widget
{

    //---
    public function init()
    {
    }

    //---
    public function getContent()
    {

        if ($this->checkFullVersion(false)) {
            $controller = new FullupdateComponent();
            $controller->actionUpdate();
        } else {
            Message::I()->Info("Your site didn't need be updated");
        }
        return $this;
    }
}