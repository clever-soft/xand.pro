<?php

class CoreWidget extends Widget
{

    public function DrawBody()
    {
        //----
        switch ($this->getPostAction()) {
            case 'AddFinish' :

                $data = $this->getPostDataJson();
                $dataNew = array();

                foreach ($data as $k => $d) {
                    $kArr = explode("_", $k);
                    if (count($kArr) < 2) continue;
                    $groupKey = array_shift($kArr);

                    $dataNew[$groupKey][implode("_", $kArr)] = $d;
                }


                if(Config::Save($dataNew)) {
                    Message::I()->Success("Config was updated successfully");
                } else {
                    Message::I()->Error("Config failed to be updated. Check file permissions");
                }
                break;
        }


        //---
        $data = array();
        $xml = simplexml_load_file(PATH_REAL . DS . "conf.xml");

        $form = new FormStatic("Site configuration", array($data), array("width" => 800));
        $form->Add(new Ajax(PATH_DS . "admin/widgets/system/configuration/core", false, "html", "ajaxCore"));

        foreach ($xml as $group) {

            $form->Add(new FormSeparator($group->name));

            foreach ($group->setting[0] as $key => $setting) {

                $type = (isset($setting["type"])) ? (string)$setting["type"] : false;
                $description = (isset($setting["description"])) ? (string)$setting["description"] : false;
                $name = ucfirst($key);
                $name = str_replace("_", " ", str_replace(".", " ", $name));
                $key = strtolower($group->prefix) . "_" . strtolower($key);

                switch ($type) {
                    case "number":
                        $form->Add(new InputNumeric($name, $key, array("requirements" => $description, "minValue" => -1), $setting));
                        break;

                    case "textarea":
                        $form->Add(new InputTextArea($name, $key, array("requirements" => $description), $setting));
                        break;

                    case "boolean":
                        $form->Add(new InputCheckbox($name, $key, array("requirements" => $description), $setting));
                        break;

                    case "readonly":
                        $form->Add(new InputTextReadonly($name, $key, array("requirements" => $description), $setting));
                        break;

                    default:
                        $form->Add(new InputText($name, $key, array("requirements" => $description), $setting));
                }
            }

        }

        $form->Add(new ButtonAdd(NULL, "Apply"));
        $form->Draw();

        Message::I()->Draw();

        return $this;


    }
}
