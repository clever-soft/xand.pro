<?php

class TemplatesWidget extends Widget
{

    //---
    public function init()
    {
    }

    //---
    public function getContent()
    {

        $modules = glob(PATH_REAL . DS . "public/tpl/*", GLOB_ONLYDIR);
        $data = array();

        foreach ($modules as $mod) {

            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }

            $xml = simplexml_load_file($fileXml);
            $data[] = (array)$xml;

        }

        $query = new QueryArray($data);

        $this->setProperty("title", "List of Libraries")
            ->SetDefaultOrder("sort", false)
            ->Add($query);

        $table = new Table(

            new TableColumn("sort", "Sort", 60, "center"),

            new TableColumn("name", "Name"),
            new TableColumnSpoiler("requirments", "Requirments"),
            new TableColumn("version", 'Version', 80, 'center'),
            new TableColumn("serverVersion", 'Server Version', 100, 'center'),

            new ButtonRefresh(NULL, NULL, array('script' => "DataAct")),
            new ButtonEdit(NULL, NULL, array('script' => "ModalAct"))

        );
        $table->setEvent("OnDrawRow",
            function ($obj, $item) {
                if ($item['version'] != $item['serverVersion'] && $item['serverVersion'] !== "-//-") {
                    $obj->buttons["Refresh"]->setProperty("enabled", true);
                } else {
                    $obj->buttons["Refresh"]->setProperty("enabled", false);
                }
            }
        );


        $table->SetPrimary("prefix");

        $this->Add($table);

        return $this;
    }


//---
    public
    function actionUpdateReady()
    {
        $xml = $this->GetXml($this->getPostData());
        $xml = (array)$xml;

        $form = new FormModal("Edit module settings", array($xml));
        $form->assignParent($this)->cloneAjax()->cloneState();


        $form->Add(new InputTextReadonly("Version", "version"));
        $form->Add(new InputText("Name", "name"));
        $form->Add(new InputNumeric("Sort", "sort"));


        $form->Add(
            new ButtonSave(NULL, "Update", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();

        exit();
    }


//---
    public
    function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $xml = $this->GetXml($data["name"]);

        foreach ($xml as $k => $s) {
            $xml->$k = (isset($data[$k]) && !empty($data[$k])) ? $data[$k] : (string)$xml->$k;
        }
        $xml->saveXML(PATH_REAL . DS . "lib/pack.{$data["name"]}/conf.xml");
    }

    public function actionRefresh()
    {
        $update = new Update();

        if ($update->updateWithRequirments(Update::TEMPLATE_PACK, $this->getPostData())) {
            Message::I()->Success("All Ok");
        } else {
            Message::I()->Error("Some New Errors ");
        }
    }

    public
    function postProcessData()
    {
        $jsonVersion = Update::getUpdateJson();
        $versions = json_decode($jsonVersion, true);
        $i = 0;
        foreach ($this->data as &$val) {
            $xml = $this->GetXml($val["prefix"]);
            $val['version'] = (string)$xml->version;
            if (isset($versions['tpl'][$val['prefix']]))
                usort($versions['tpl'][$val['prefix']], ['Update', 'sortversion']);
            $val['serverVersion'] = (isset($versions['tpl'][$val['prefix']]) && !empty($versions['tpl'][$val['prefix']])) ? array_pop($versions['tpl'][$val['prefix']]) : "-//-";
            if (isset($xml->settings->requirments) && !empty((array)$xml->settings->requirments)) {

                $arrR = [];
                $strR = "";
                foreach ($xml->settings->requirments as $r) {
                    if (isset($r->modules) && !empty($r->modules)) {
                        $tmp = [];
                        foreach ($r->modules as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Modules: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->libs) && !empty($r->libs)) {
                        $tmp = [];
                        foreach ($r->libs as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Librarys: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->tpls) && !empty($r->tpls)) {
                        $tmp = [];
                        foreach ($r->tpls as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Tamplates: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                }
                $strR = implode("<br>", $arrR);

            } else {
                $strR = "none";
            }
            $val['requirments'] = $strR;
            $val['sort'] = (isset($val['sort']) && !empty($val['sort'])) ? $val['sort'] : $i;
            $i++;
        }
    }

//---
    private
    function GetXml($tpl)
    {
        $fileXml = PATH_REAL . DS . "public/tpl/tpl-{$tpl}/conf.xml";
        if (!file_exists($fileXml)) {
            exit("conf file not found");
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;
    }
}