<?php

class ModulesWidget extends Widget
{
    private $allowExt = [
        'php', 'xml'
    ];

    //---
    public function init()
    {

    }

    //---
    public function getContent()
    {

        $modules = glob(PATH_REAL . DS . "mod/*", GLOB_ONLYDIR);
        $data = array();

        foreach ($modules as $mod) {
            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }

            $xml = simplexml_load_file($fileXml);
            $data[] = (array)$xml->settings;
        }


        $query = new QueryArray($data);

        $this->setProperty("title", "List of modules")
            ->SetDefaultOrder("sort", false)
            ->Add(new ButtonAppend(NULL, NULL, ["script" => "ModalAct"]))
            ->Add(new ButtonRefresh('RefreshMass', "Update", ["script" => "TableAct"]))
            ->Add($query);

        $table = new TableMass(
            new TableColumnLogicIconOffOn("active", "A", 30, "center"),
            new TableColumnLogicIconOffOn("ignore", "I", 30, "center"),
            new TableColumn("sort", "Sort", 60, "center"),
            new TableColumn("url", "Url", 120, "left"),
            new TableColumn("name", "Name"),
            new TableColumnSpoiler("requirments", "Requirments"),
            new TableColumn("version", 'Version', 80, 'center'),
            new TableColumn("serverVersion", 'Server Version', 100, 'center'),

            new ButtonRefresh(NULL, NULL, array('script' => "DataAct")),
            new ButtonEdit(NULL, NULL, array('script' => "ModalAct")),
            new ButtonDown("MakeZip", "Make Zip", ['script' => "DataAct"])

        );
        $table->setEvent("OnDrawRow",
            function ($obj, $item) {
                if ($item['version'] != $item['serverVersion'] && $item['serverVersion'] !== "-//-") {
                    $localArr = explode(".", $item['version']);
                    $serverArr = explode(".", $item['serverVersion']);

                    if ($localArr[0] != $serverArr[0]) {
                        $obj->buttons["Refresh"]->setProperty("enabled", false);
                        return;
                    }
                    if ($localArr[1] > $serverArr[1]) {
                        $obj->buttons["Refresh"]->setProperty("enabled", false);
                        return;
                    }
                    if ($localArr[1] == $serverArr[1]) {
                        if ($localArr[2] >= $serverArr[2]) {
                            $obj->buttons["Refresh"]->setProperty("enabled", false);
                            return;
                        }
                    }
                    $obj->buttons["Refresh"]->setProperty("enabled", true);
                } else {
                    $obj->buttons["Refresh"]->setProperty("enabled", false);
                }
            }
        );


        $table->SetPrimary("url");
        $this->Add($table);

        return $this;
    }

    public function actionMakeZip()
    {
        $pack = $this->getPostData();
        $path = PATH_REAL . DS . "mod/{$pack}/";
        $xml = $this->GetXml($pack);
        $xml->settings->version = Update::incrementVersion((string)$xml->settings->version);
        $this->SaveXml($pack, $xml);
        $zip = new ZipArchive();
        $ver = (string)$xml->settings->version;


        $pathZip = Update::$zipArchive . "/mod-{$pack}.v.{$ver}.zip";
        echo $pathZip;

        $ret = $zip->open($pathZip, ZipArchive::CREATE);

        if ($ret !== TRUE) {
            Message::I()->Error("Failed with code {$ret}");
        } else {
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($path),
                RecursiveIteratorIterator::LEAVES_ONLY
            );
            foreach ($files as $name => $file) {
                if (!$file->isDir()) {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($path));
                    $fileExt = end(@explode(".", $relativePath));
                    if (!empty($fileExt))
                        if (in_array($fileExt, $this->allowExt))
                            $zip->addFile($filePath, $relativePath);
                }
            }

            $zip->close();
            Message::I()->Success("All Ok");
        }
    }

    public
    function actionAddReady()
    {

        $xml = Update::getUpdateJson();
        $xml = json_decode($xml, true);

        $arr = [];
        $modules = glob(PATH_REAL . DS . "mod/*", GLOB_ONLYDIR);
        $data = [];
        foreach ($modules as $mod) {
            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }
            $xml_str = simplexml_load_file($fileXml);
            $data[] = (string)$xml_str->settings->url;
        }

        if (isset($xml['mod']) && !empty($xml['mod'])) {
            foreach ($xml['mod'] as $k => $x) {
                if (!in_array($k, $data)) {
                    $arr[$k] = $k;
                    continue;
                }
            }
        }


        $form = new FormModal("Add new module", array($xml));
        $form->assignParent($this)->cloneAjax()->cloneState();
        if (!empty($arr)) {
            $form->Add(new ControlSelect("New", "module", [], false, $arr));
            $form->Add(new ButtonSave("AddFinish", "Add", array("script" => "JsonModalCloseAct")));
        } else {
            $form->Add(new Label("Nothing new"));
        }


        $form->Add(
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    public function actionAddFinish()
    {
        $module = $this->getPostDataJson();
        $upd = new Update();
        if ($upd->updateWithRequirments(Update::MODULE_PACK, $module['module'])) {
            Message::I()->Success("All installed");
        } else {
            Message::I()->Error("All bad");
        }
    }

//---
    public
    function actionUpdateReady()
    {
        $xml = $this->GetXml($this->getPostData());
        $xml = (array)$xml->settings;

        $form = new FormModal("Edit module settings", array($xml));
        $form->assignParent($this)->cloneAjax()->cloneState();
        $form->Add(new InputText("Version", 'version'));
        $form->Add(new InputCheckbox("Active", "active"));
        $form->Add(new InputTextReadonly("Url", "url"));
        $form->Add(new InputTextReadonly("Name", "name"));
        $form->Add(new InputCheckbox("Ignore", "ignore"));
        $form->Add(new InputNumeric("Sort", "sort"));
        $form->Add(new InputVar("url", "url"));

        $form->Add(
            new ButtonSave(NULL, "Update", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();

        exit();
    }

//---
    public
    function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $xml = $this->GetXml($data["url"]);
        $xml->settings->active = $data['active'];
        $xml->settings->ignore = $data['ignore'];
        $xml->settings->version = $data['version'];

        $this->SaveXml($data["url"], $xml);

    }

    public
    function actionRefresh()
    {
        $update = new Update();

        if ($update->updateWithRequirments(Update::MODULE_PACK, $this->getPostData())) {
            //$component = new Update();
            //$component->actionUpdateMysql($this->getPostData());
            Message::I()->Success("Mod " . $this->getPostData() . " updated");
        } else {
            Message::I()->Error("Some Errors ");
        }

    }

    public
    function postProcessData()
    {
        $jsonVersion = Update::getUpdateJson();
        $versions = json_decode($jsonVersion, true);
        foreach ($this->data as &$val) {
            $xml = $this->GetXml($val["url"]);
            $val['version'] = (string)$xml->settings[0]->version;
            if (isset($versions['mod'][$val['url']]))
                usort($versions['mod'][$val['url']], ['Update', 'sortversion']);
            $val['serverVersion'] = (isset($versions['mod'][$val['url']]) && !empty($versions['mod'][$val['url']])) ? array_pop($versions['mod'][$val['url']]) : "-//-";
            if (isset($xml->settings->requirments) && !empty((array)$xml->settings->requirments)) {

                $arrR = [];
                $strR = "";

                foreach ($xml->settings->requirments as $r) {
                    if (isset($r->modules) && !empty($r->modules)) {
                        $tmp = [];
                        foreach ($r->modules as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Modules: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->libs) && !empty($r->libs)) {
                        $tmp = [];
                        foreach ($r->libs as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Librarys: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->tpls) && !empty($r->tpls)) {
                        $tmp = [];
                        foreach ($r->tpls as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Tamplates: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                }
                $strR = implode("<br>", $arrR);

            } else {
                $strR = "none";
            }
            $val['requirments'] = $strR;

            $val['ignore'] = (isset($val['ignore'])) ? (int)$val['ignore'] : 0;
            $val['active'] = (isset($val['active'])) ? (int)$val['active'] : 0;
            $val['ignore'] = (empty($val['ignore'])) ? 1 : 0;
        }
    }

    public function actionRefreshMass()
    {
        $ids = $this->getPostData();
        $idsArr = explode(",", $ids);
        $update = new Update();
        $error = false;
        foreach ($idsArr as $key => $val) {
            if ($val == 'undefined')
                unset($idsArr[$key]);
        }
        foreach ($idsArr as $lib) {
            if ($update->updateWithRequirments(Update::MODULE_PACK, $lib)) {
                continue;
            }
        }
        if ($error) {
            Message::I()->Error("Some New Errors ");
        } else {
            Message::I()->Success("All Ok");
        }
    }

//---
    private
    function GetXml($module)
    {
        $fileXml = PATH_REAL . DS . "mod/{$module}/conf.xml";
        if (!file_exists($fileXml)) {
            exit("conf file not found");
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;

    }

    private
    function SaveXml($module, $xml)
    {
        $fileXml = PATH_REAL . DS . "mod/{$module}/conf.xml";
        if (!file_exists($fileXml)) {
            exit("conf file not found: {$module}");
        }
        return $xml->saveXML($fileXml);
    }
}