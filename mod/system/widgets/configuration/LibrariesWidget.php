<?php

class LibrariesWidget extends Widget
{
    private $ignoreZip;

    //---
    public function init()
    {
        $this->ignoreZip = [
            "conf.xml.backup", "..", "."
        ];
    }

    //---
    public function getContent()
    {

        $modules = glob(PATH_REAL . DS . "lib/*", GLOB_ONLYDIR);
        $data = array();
        foreach ($modules as $mod) {
            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }

            $xml = simplexml_load_file($fileXml);
            $data[] = (array)$xml;

        }
        $query = new QueryArray($data);

        $this->setProperty("title", "List of Libraries")
            ->SetDefaultOrder("sort", false)
            ->Add(new ButtonAppend(NULL, NULL, ["script" => "ModalAct"]))
            ->Add(new ButtonRefresh('RefreshMass', "Update", ["script" => "TableAct"]))
            ->Add($query);

        $table = new TableMass(

            new TableColumn("sort", "Sort", 60, "center"),
            new TableColumnLogicIconOffOn("ignore", "I", 30, "center"),
            new TableColumn("name", "Name"),
            new TableColumnSpoiler("requirments", "Requirments"),
            new TableColumn("version", 'Version', 80, 'center'),
            new TableColumn("serverVersion", 'Server Version', 100, 'center'),

            new ButtonRefresh(NULL, NULL, ['script' => "DataAct"]),
            new ButtonEdit(NULL, NULL, ['script' => "ModalAct"]),
            new ButtonDown("MakeZip", "Make Zip", ['script' => "DataAct"])

        );
        $table->setEvent("OnDrawRow",
            function ($obj, $item) {
                if ($item['version'] != $item['serverVersion'] && $item['serverVersion'] !== "-//-") {
                    $localArr = explode(".", $item['version']);
                    $serverArr = explode(".", $item['serverVersion']);

                    if ($localArr[0] != $serverArr[0]) {
                        $obj->buttons["Refresh"]->setProperty("enabled", false);
                        return;
                    }
                    if ($localArr[1] > $serverArr[1]) {
                        $obj->buttons["Refresh"]->setProperty("enabled", false);
                        return;
                    }
                    if ($localArr[1] == $serverArr[1]) {
                        if ($localArr[2] >= $serverArr[2]) {
                            $obj->buttons["Refresh"]->setProperty("enabled", false);
                            return;
                        }
                    }
                    $obj->buttons["Refresh"]->setProperty("enabled", true);
                } else {
                    $obj->buttons["Refresh"]->setProperty("enabled", false);
                }
            }
        );


        $table->SetPrimary("name");
        $this->Add($table);

        return $this;
    }

    public function actionMakeZip()
    {
        $pack = $this->getPostData();
        $xml = $this->GetXml($pack);
        $xml->version = Update::incrementVersion((string)$xml->version);
        $this->SaveXml($pack, $xml);
        $zip = new ZipArchive();
        $ver = (string)$xml->version;
        $ret = $zip->open(Update::$zipArchive . "/lib-{$pack}.v.{$ver}.zip", ZipArchive::CREATE);
        if ($ret !== TRUE) {
            Message::I()->Error("Failed with code {$ret}");
        } else {
            $options = array('add_path' => '/', 'remove_all_path' => TRUE);
            $zip->addGlob(PATH_REAL . DS . "lib/pack.{$pack}/*.{php,xml}", GLOB_BRACE, $options);
            $zip->close();
            Message::I()->Success("All Ok");
        }
    }

    public function actionRefreshMass()
    {
        $ids = $this->getPostData();
        $idsArr = explode(",", $ids);
        $update = new Update();
        $error = false;
        foreach ($idsArr as $key => $val) {
            if ($val == 'undefined')
                unset($idsArr[$key]);
        }
        foreach ($idsArr as $lib) {
            if ($update->updateWithRequirments(Update::LIBRARY_PACK, $lib)) {
                continue;
            }
        }
        if ($error) {
            Message::I()->Error("Some New Errors ");
        } else {
            Message::I()->Success("All Ok");
        }
    }

    public
    function actionAddReady()
    {

        $xml = Update::getUpdateJson();
        $xml = json_decode($xml, true);

        $arr = [];
        $modules = glob(PATH_REAL . DS . "lib/*", GLOB_ONLYDIR);
        $data = [];
        foreach ($modules as $mod) {
            $fileXml = $mod . "/conf.xml";
            if (!file_exists($fileXml)) {
                continue;
            }
            $xml_str = simplexml_load_file($fileXml);
            $data[] = (string)$xml_str->name;
        }

        if (isset($xml['lib']) && !empty($xml['lib'])) {
            foreach ($xml['lib'] as $k => $x) {
                if (!in_array($k, $data)) {
                    $arr[$k] = $k;
                    continue;
                }
            }
        }


        $form = new FormModal("Add new library", array($xml));
        $form->assignParent($this)->cloneAjax()->cloneState();
        if (!empty($arr)) {
            $form->Add(new ControlSelect("New", "lib", [], false, $arr));
            $form->Add(new ButtonSave("AddFinish", "Add", array("script" => "JsonModalCloseAct")));
        } else {
            $form->Add(new Label("Nothing new", false));
        }


        $form->Add(
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    public function actionAddFinish()
    {
        $module = $this->getPostDataJson();
        $upd = new Update();
        if ($upd->updateWithRequirments(Update::LIBRARY_PACK, $module['lib'])) {
            Message::I()->Success("All installed");
        } else {
            Message::I()->Error("All bad");
        }
    }

//---
    public function actionUpdateReady()
    {
        $xml = $this->GetXml($this->getPostData());
        $xml = (array)$xml;

        $form = new FormModal("Edit module settings", array($xml));
        $form->assignParent($this)->cloneAjax()->cloneState();


        $form->Add(new InputText("Version", 'version'));
        $form->Add(new InputTextReadonly("Name", "name"));
        $form->Add(new InputCheckbox("Ignore", "ignore"));
        $form->Add(new InputNumeric("Sort", "sort"));
        $form->Add(new InputVar("name", "name"));


        $form->Add(
            new ButtonSave(NULL, "Update", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();

        exit();
    }

//---
    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $xml = $this->GetXml($data["name"]);
        //$xml->settings->active = $data['active'];
        $xml->ignore = $data['ignore'];
        $xml->version = $data['version'];

        $this->SaveXml($data["name"], $xml);
    }

    public function actionRefresh()
    {
        $update = new Update();

        if ($update->updateWithRequirments(Update::LIBRARY_PACK, $this->getPostData())) {
            Message::I()->Success("All Ok");
        } else {
            Message::I()->Error("Some New Errors ");
        }
    }

    public function postProcessData()
    {
        $jsonVersion = Update::getUpdateJson();
        $versions = json_decode($jsonVersion, true);
        $i = 0;
        foreach ($this->data as &$val) {
            $xml = $this->GetXml($val["name"]);
            $val['version'] = (string)$xml->version;
            if (isset($versions['lib'][$val['name']]))
                usort($versions['lib'][$val['name']], ['Update', 'sortversion']);
            $val['serverVersion'] = (isset($versions['lib'][$val['name']]) && !empty($versions['lib'][$val['name']])) ? array_pop($versions['lib'][$val['name']]) : "-//-";
            if (isset($xml->settings->requirments) && !empty((array)$xml->settings->requirments)) {

                $arrR = [];
                $strR = "";
                foreach ($xml->settings->requirments as $r) {
                    if (isset($r->modules) && !empty($r->modules)) {
                        $tmp = [];
                        foreach ($r->modules as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Modules: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->libs) && !empty($r->libs)) {
                        $tmp = [];
                        foreach ($r->libs as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Librarys: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                    if (isset($r->tpls) && !empty($r->tpls)) {
                        $tmp = [];
                        foreach ($r->tpls as $moduleName) {
                            foreach ($moduleName as $module => $version)
                                $tmp[] = $module . ": " . $version;
                        }
                        $arrR [] = "Tamplates: " . implode(", ", $tmp);
                        unset($tmp);
                    }
                }
                $strR = implode("<br>", $arrR);

            } else {
                $strR = "none";
            }
            $val['requirments'] = $strR;
            $val['sort'] = (isset($val['sort']) && !empty($val['sort'])) ? $val['sort'] : $i;
            $i++;

            $val['ignore'] = (isset($val['ignore'])) ? (int)$val['ignore'] : 0;

            $val['ignore'] = (empty($val['ignore'])) ? 1 : 0;
        }

    }

//---
    private function GetXml($lib)
    {
        $fileXml = PATH_REAL . DS . "lib/pack.{$lib}/conf.xml";
        if (!file_exists($fileXml)) {
            exit("conf file not found: {$lib}");
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;
    }

    private function SaveXml($lib, $xml)
    {
        $fileXml = PATH_REAL . DS . "lib/pack.{$lib}/conf.xml";
        if (!file_exists($fileXml)) {
            exit("conf file not found: {$lib}");
        }
        return $xml->saveXML($fileXml);
    }
}