<?php 

include_once("../../../lib/class.Xand.php"); 


//Io::Dump();
 
$nav = new Nav();
$cst   = new Widget('i18n', new Ajax(PATH_DS."mod/system/widgets/widget.i18n.php", false),$nav);
$query = new QuerySimple();
$query->Select(array("cms_i18n" => array("*")));

$cst->setProperty("title","Internationalization")
	->SetDefaultOrder("idCode", false)
 	->Add($query)
    ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")));
	
	$table = new Table(	
					   new TableColumn("idCode","ID", 40, "center"),
					   new TableColumn("translateEn", "Defualt  EN"),				  
					   new TableColumn("translateRu", "Translate RU"),
					   
					   new TableColumnDateCreate(),
					   new TableColumnDateUpdate(),											   
				 									 					
					   new ButtonEdit(NULL, NULL, array("script"=>"ModalAct")),
					   new ButtonDelete()
				   );
				 
$table->SetPrimary("idCode"); 

//---------------------------------------------------------------------------------
switch ($this->getPostAction())
{
	 
	  case 'AddReady' : 
	 
				 	$form = new FormModal("Translate add");
					$form->assignParent($cst)->cloneAjax()->CloneNav();	
				   
					$form->Add(new InputText("Defualt EN",  "translateEn")); 		 
					$form->Add(new InputText("Translate RU", "translateRu"));
									
				 
					$form->Add(	 
								new ButtonAdd(NULL, NULL, array("script"=>"JsonModalCloseAct")),
								new ButtonCancel(NULL, NULL, array("script"=>"ModalClose"))
							   );							
					$form->Draw();	
					exit();			
	 
	 case 'AddFinish' :	 		
					$arr_data = $this->getPostDataJson();
					$arr_data['translateEn'] = trim($arr_data['translateEn'], ' "');		
					$arr_data['translateRu'] = trim($arr_data['translateRu'], ' "');
					
					
					$arr_data['dateCreate'] = time();		
					$arr_data['dateUpdate'] = time();
					SDbQuery::table("cms_i18n")->enableMessages()->insert($arr_data);
					break;	
	 
	 
	 case 'Delete' :
					SDbQuery::table("cms_i18n")->enableMessages()->where(["idCode" => $this->getPostDataInt()]);
					break;	 
	  
	 case 'UpdateReady' : 
	
					$ARR = SDb::select("cms_i18n", "idCode=?", [$this->getPostDataInt()]);
					 
				 	$form = new FormModal("Block edit", $ARR);
					$form->assignParent($cst)->cloneAjax()->CloneNav();	
				   
					$form->Add(new InputText("Defualt EN",  "translateEn")); 		 
					$form->Add(new InputText("Translate RU", "translateRu"));
				 					
					$form->Add(	
								new InputVar("idCode", "idCode"),
								new ButtonSave(NULL, NULL, array("script"=>"JsonModalCloseAct")),
								new ButtonCancel(NULL, NULL, array("script"=>"ModalClose"))
							    );					
					$form->Draw();	
					exit();					
					
	case 'UpdateFinish' :	 		
					$arr_data = $this->getPostDataJson();
					$arr_data['translateEn'] = trim($arr_data['translateEn'], ' "');		
					$arr_data['translateRu'] = trim($arr_data['translateRu'], ' "');
									 
					$arr_data['dateUpdate'] = time();
					SDbQuery::table("cms_i18n")->enableMessages()->where(["idCode" => $arr_data["idCode"]])->update($arr_data);
					break;	
}			

//--------------------------------------------------------------------------------- 
  
$cst->Add($table);


if($cst->data != null)
{	 
	   foreach($cst->data as &$val)
	   {
		  
	   }
}


$cst->Draw();	
	

?>