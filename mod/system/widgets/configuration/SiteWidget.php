<?php

class SiteWidget extends Widget
{
    private $cpConfig;
    private $enc;

    //---
    public function init()
    {
        $this->cpConfig = new CpConfig();
        $this->enc = new Encrypter();
    }

    //---
    public function getContent()
    {

        $query = new QueryMulti();
        $query->Select(array("cp_config" => array("*")));
        $query->setPriorityOrder("groupName");

        $this->setProperty("title", "List of settings of Site")
            ->SetDefaultOrder("dateCreate", false)
            ->Add($query)
            ->Add(new Ppage(array(50)))
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")));


        $table = new Table(
            new TableColumnLogicIcon("isEncoded", "E", 25, "fa-lock"),
            new TableColumn("id", "ID", 30, "center"),
            new TableColumn("type", "Type", 90, "center"),
            new TableColumn("attr", "Key", 220, "left"),
            new TableColumnSpoiler("val", "Value", NULL),
            new TableColumnDateCreate("dateStart", "Start"),
            new TableColumnDateUpdate("dateEnd", "End"),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonSettings(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()
        );

        $table->SetPrimary("id")
            ->setProperty("lastGroup", "test");

        $table->setEvent("OnDrawRow", function ($obj, $item) {

            if ($obj->properties["lastGroup"] != $item["groupName"]) {
                $obj->DrawGroupSeparator(ucfirst($item["groupName"]));
                $obj->properties["lastGroup"] = $item["groupName"];
            }
        });


        $this->Add($table);

        return $this;
    }


    //---
    public function actionSettingsReady()
    {

        $ARR = $this->cpConfig->findAll("id=" . $this->getPostDataInt() . "");

        $form = new FormModal("Editing type of value", $ARR);
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputText("Group", "groupName"));
        $form->Add(new InputText("Key", "attr"));
        $form->Add(new InputCheckbox("Encoded", "isEncoded"));

        $form->Add(new ControlSelect("Type", "type", array("noChoose" => false), NULL, array(
            array("title" => "bool", "equal" => "bool", "data" => "bool"),
            array("title" => "textarea", "equal" => "textarea", "data" => "textarea"),
            array("title" => "select", "equal" => "select", "data" => "select"),
            array("title" => "text", "equal" => "text", "data" => "text"),
            array("title" => "data", "equal" => "data", "data" => "data")
        )));

        $form->Add(new InputTextArea("Allowed values", "allowedValues", array("height" => 100)));
        $form->Add(new InputTextArea("Description", "description", array("height" => 100)));

        $form->Add(
            new ButtonSave("SettingsFinish", "Save", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );

        $form->Draw();
        exit();

    }

    //---
    public function actionSettingsFinish()
    {
        $arr_data = $this->getPostDataJson();
      
        $arr_data["dateUpdate"] = time();

        if ($this->cpConfig->updateByAttributes(array("attr" => $arr_data['attr']), $arr_data)) {
            Message::I()->Success("Successfully updated");
        } else {
            Message::I()->Error($this->cpConfig->getLastError());
        }
    }


    //---
    public function actionAddReady()
    {
        $form = new FormModal("Adding a new value");
        $form->assignParent($this)->cloneAjax()->cloneState();

        $form->Add(new InputText("Group", "groupName"));

        $form->Add(new InputText("Key", "attr"));
        $form->Add(new ControlSelect("Type", "type", array("noChoose" => false), NULL, array(
            array("title" => "bool", "equal" => "bool", "data" => "bool"),
            array("title" => "textarea", "equal" => "textarea", "data" => "textarea"),
            array("title" => "select", "equal" => "select", "data" => "select"),
            array("title" => "text", "equal" => "text", "data" => "text"),
            array("title" => "data", "equal" => "data", "data" => "data")
        )));

        $form->Add(new InputTextArea("Allowed values", "allowedValues", array("height" => 100)));
        $form->Add(new InputTextArea("Description", "description", array("height" => 100)));


        $form->Add(new InputCheckbox("Encoded", "isEncoded"));
        $form->Add(new ButtonAdd(NULL, "Add", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );

        $form->Draw();
        exit();
    }

    //---
    public function actionAddFinish()
    {
        $arr_data = $this->getPostDataJson();
        $arr_data["dateCreate"] = time();
        $arr_data["dateUpdate"] = time();

        if ($this->cpConfig->insert($arr_data)) {
            Message::I()->Success("Successfully added");
        } else {
            Message::I()->Error($this->cpConfig->getLastError());
        }
    }


    //---
    public function actionUpdateReady()
    {

        $ARR = $this->cpConfig->findAll("id=" . $this->getPostDataInt());
        $ARR[0]["val"] = ($ARR[0]["isEncoded"]) ? "" : $ARR[0]["val"];
        $ARR[0]["description"] = "<div style='padding:5px 0; color: #aaa;'>" . $ARR[0]["description"] . "</div>";

        $form = new FormModal("Editing value", $ARR);
        $form->assignParent($this)->cloneAjax()->cloneState();
        $form->Add(new InputTextReadonly("Key", "attr"));

        switch ($ARR[0]["type"]) {
            case "bool":
                $form->Add(new InputCheckbox("Value", "val"));
                break;

            case "data":
            case "textarea":
                $form->Add(new InputTextArea("Values list", "val", array("height" => 200)));
                break;

            case "select":
                $allowedValues = explode("\n", $ARR[0]["allowedValues"]);
                $values = array();
                foreach ($allowedValues as $allowedValue) {
                    $values[] = array("title" => $allowedValue, "equal" => $allowedValue, "data" => $allowedValue);
                }

                $form->Add(new ControlSelect("Value", "val", array("noChoose" => false), NULL, $values));

                break;

            case "text":
                $form->Add(new InputText("Value", "val"));
                break;
        }

        $form->Add(new InputDiv("", "description"));

        $form->Add(
            new InputVar("attr", "attr"),
            new InputVar("isEncoded", "isEncoded"),
            new ButtonSave(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );

        $form->Draw();
        exit();
    }

    //---
    public function actionUpdateFinish()
    {
        $arr_data = $this->getPostDataJson();

        if ($arr_data["val"] != "") {
            $arr_data["dateUpdate"] = time();
            $arr_data["val"] = ($arr_data["isEncoded"]) ? $this->enc->encrypt($arr_data["val"]) : $arr_data["val"];

            if ($this->cpConfig->updateByAttributes(array("attr" => $arr_data['attr']), $arr_data)) {
                Message::I()->Success("Successfully updated");
            } else {
                Message::I()->Error($this->cpConfig->getLastError());
            }

        }
    }

    //---
    public function actionDelete()
    {
        $id = $this->getPostDataInt();

        if ($this->cpConfig->deleteByAttributes("id = {$id}")) {
            Message::I()->Success("Successfully removed");
        } else {
            Message::I()->Error($this->cpConfig->getLastError());
        }
    }


}
