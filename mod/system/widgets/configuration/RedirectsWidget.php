<?php

/**
 * Class RedirectsRuleWidget
 * @author Alexander S. <aleksandershtefan@gmail.com>
 */
class RedirectsWidget extends Widget
{
    /**
     *
     */
    public function init()
    {
        $this->tableName = RedirectsModel::table();
        $this->primaryKey = RedirectsModel::id();
    }

    /**
     * @return $this
     */
    public function GetContent()
    {
        $query = new QueryMulti();
        $query->Select([
            $this->tableName => ["*"]
        ]);


        $this->setProperty("title", "Redirects List")
            ->SetDefaultOrder($this->primaryKey, false)
            ->Add($query);

        $table = new Table(
            new TableColumn($this->primaryKey, "ID", 60, "center"),

            new TableColumn("fModule", "[from] Module", NULL, "center"),
            new TableColumn("tModule", "[to] Module", NULL, "center"),
            new TableColumn("fController", "[from] Controller", NULL, "center"),
            new TableColumn("tController", "[to] Controller", NULL, "center"),
            new TableColumn("fAction", "[from] Action", NULL, "center"),
            new TableColumn("tAction", "[to] Action", NULL, "center"),

            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(), new ButtonRule("0", "isActive", "equally")),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()
        );

        $table->SetPrimary($this->primaryKey);

        $this->Add($table)
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")));

        return $this;

    }

    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormModal("Add new redirect rule");
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputText('[from] Module', 'fModule', []));
        $form->Add(new InputText('[to] Module', 'tModule', []));
        $form->Add(new InputText('[from] Controller', 'fController', []));
        $form->Add(new InputText('[to] Controller', 'tController', []));
        $form->Add(new InputText('[from] Action', 'fAction', []));
        $form->Add(new InputText('[to] Action', 'tAction', []));
        $form->Add(new InputCheckbox('Active', 'isActive'));

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $arr_data = $this->getPostDataJson();

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->insert($arr_data);
    }

    /**
     *
     */
    public function actionUpdateReady()
    {
        $data = SDbQuery::table($this->tableName)
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->select();

        $form = new FormModal("Rule edit", $data);

        $form->assignParent($this)->cloneAjax()->cloneState();
        $form->Add(new InputVar($this->primaryKey, $this->primaryKey, ['width' => 500], $this->getPostDataInt()));
        $form->Add(new InputText('[from] Module', 'fModule', []));
        $form->Add(new InputText('[to] Module', 'tModule', []));
        $form->Add(new InputText('[from] Controller', 'fController', []));
        $form->Add(new InputText('[to] Controller', 'tController', []));
        $form->Add(new InputText('[from] Action', 'fAction', []));
        $form->Add(new InputText('[to] Action', 'tAction', []));
        $form->Add(new InputCheckbox('Active', 'isActive'));

        $form->Add(new ButtonSave(NULL, NULL, array('script' => 'JsonModalCloseAct')));
        $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $data[$this->primaryKey]])
            ->update($data);
    }

    /**
     *
     */
    public function actionPublished()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->update(["isActive" => 1]);
    }

    /**
     *
     */
    public function actionUnpublished()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->update(["isActive" => 0]);
    }

    /**
     *
     */
    public function actionDelete()
    {
        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->delete();
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {
        foreach ($this->data as &$val) {

        }
        return $this;
    }

}