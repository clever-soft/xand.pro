<?php

class SessionsWidget extends Widget
{
    //---
    public function init()
    {
    }

    //---
    public function getContent()
    {
        $query = new QueryMulti();
        $query->Select(array("cp_sessions" => array("*")));
        $query->setPriorityOrder("sid");

        $table = new TableMass(

            new TableColumnLogicIconOffOn("isActive", "S", 25),
            new TableColumn("code", "C", 25, "center", false, false),
            new TableColumn("ip", "IP", 100, "left"),
            new TableColumn("domain", "Domain", NULL, "left", false, false),
            new TableColumn("username", "Username", NULL, "left", false, false),
            new TableColumn("sid", "SID", NULL),
            new TableColumn("ua", "UA / Referer", NULL, "left", false, false),
            new TableColumn("dataLength", "DL", 50, "center", false, false),
            new TableColumnDateUpdate(),
            new TableColumnDateCreate(),
            new ButtonView(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()

        );

        $table->SetPrimary("sid");

        $this->Add($table)
            ->setProperty("title", "Sessions")
            ->SetDefaultOrder("dateUpdate", true)
            ->Add($query)
            ->Add(new ButtonDelete(NULL, "Delete", array("script" => "TableAct", "dialogEnable" => false)))
            ->Add(new ActionInputFilter("IP", array("field" => "ip")));

        return $this;
    }


    //----
    public function PostProcessData()
    {

        $country = new Country();

        foreach ($this->data as &$val) {
            //$val['code'] = $country->getHtmlBlock($val["ip"]);
            $val['dataLength'] = strlen($val['data']);

            $unique_start = 'username';
            $unique_end = 'password';
            preg_match('/' . preg_quote($unique_start, '/') . '(.*)' . preg_quote($unique_end, '/') . '/Us', $val['data'], $match);


            if (isset($match[1])) {
                $usernameArr = explode('"', $match[1]);
                $val['username'] = $usernameArr[2];
                $val['isActive'] = 1;
            } else {
                $val['isActive'] = 0;
                $val['username'] = "<span style='color:#777'>guest</span>";
            }

            $val['ua'] = (mb_strlen($val['ua'], "UTF-8") > 100)
                ? mb_substr(strip_tags($val['ua']), 0, 50, "UTF-8") . "..."
                : $val['ua'];
            $val['ua'] .= " / " . $val['referer'];

        }
        return $this;
    }

    //---
    public function actionDelete()
    {
        $arrData = explode(",", $this->getPostDataJson());
        $countRefreshed = 0;
        foreach ($arrData as $d) {
            $countRefreshed++;
            SDbQuery::table("cp_sessions")->where(["sid" => $d])->delete();
        }
        Message::I()->Success("{$countRefreshed} rows were removed");

    }

    public function actionView()
    {
        $data = SDb::select("cp_sessions", "sid=?", [$this->getPostDataJson()]);
        $form = new FormModal("Session detail", $data);
        $form->assignParent($this)->cloneAjax()->cloneState();
        $form->Add(new InputTextArea("UA", "ua"));
        $form->Add(new InputTextArea("Referer", "referer"));
        $form->Add(new InputTextArea("Data", "data", array("height" => 350)));
        $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
        $form->Draw();
        exit();
    }


}
