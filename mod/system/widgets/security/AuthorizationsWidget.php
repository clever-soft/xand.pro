<?php include_once("../../../lib/class.Xand.php");

Message::I()->Error("coming soon")->E();

include_once("../../stats/models/model.country.php");
Response::Fill();
SDb::SetDb("db2");
$country      = new Country();  

$state = new State();
$cst   = new Widget('logins', new Ajax(PATH_DS."mod/security/zones/zone.logins.php", false),$state);
$query = new QueryMulti("cp_security[*]");

$orderHelpers = new OrderHelpers(); 	  	 
  
	$table = new Table(
					  new TableColumn("id", "ID", 40, "center"), 	
					  new TableColumnDateCreate(),
					  new TableColumnLogicIconOffOn("isActive", "S", 25),		
					  new TableColumn("code", "C", 25, "center", false, false), 	 
					  new TableColumn("ip", "IP", NULL, "left"), 
					  new TableColumn("login", "Login", NULL), 
					  new TableColumn("info", "Description"), 
					  new TableColumn("browser", "UA")
					
				  ); 
				 
$table->SetPrimary("id");

  
$cst->Add($table) 
	->setProperty("title","Inspection system logging into the CP")
	->SetDefaultOrder("id", true)
 	->Add($query)
	->Add(new ActionInputFilter("IP", array("field" => "ip")))
	->Add(new ActionInputFilter("Login", array("field" => "login")))
	->Process();
	
	if($cst->data != null)
	{	 
		 foreach($cst->data as &$val)
		 {	
			
			$val["country"]  = $CC[strtoupper($orderHelpers->getCountry($val["ip"]))].", ".
								iconv("ISO-8859-1", "UTF-8", $orderHelpers->getCity($val["ip"])); 
			
			$val['code']     = $country->getHtmlBlock($val["ip"]); 
			 			 
			switch($val['info'])
			{
				case 1: $val['info'] = "Login successful"; 				break;
				case 2: $val['info'] = "Invalid username or password";  break;
				case 3: $val['info'] = "The wrong security code";		break;
				case 4: $val['info'] = "User is inactive"; 		  		break;
			}	   
								   
		 }
	}
		

$cst->Draw();	
	

?>