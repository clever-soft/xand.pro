<?php

class FilterWidget extends Widget
{

    private $model;

    //---
    public function init()
    {
        $this->model = new CpConfig;
    }

    //---
    public function actionAddFinish()
    {

        $data = $this->getPostDataJson();


        SDb::transactionStart();

        SDbQuery::table("cp_config")->where(['attr' => CpConfig::ATTR_ENABLE])->upsert(['attr' => CpConfig::ATTR_ENABLE, 'val' => $data[CpConfig::ATTR_ENABLE]]);
        SDbQuery::table("cp_config")->where(['attr' => CpConfig::ATTR_LIST])->upsert(['attr' => CpConfig::ATTR_LIST, 'val' => $data[CpConfig::ATTR_LIST]]);
        SDbQuery::table("cp_config")->where(['attr' => CpConfig::ATTR_IP])->upsert(['attr' => CpConfig::ATTR_IP, 'val' => $data[CpConfig::ATTR_IP]]);

        if(SDbBase::getLastError()) {
            Message::I()->Error("Failed to update record: ".SDbBase::getLastError());
            SDb::transactionRollBack();
            return;
        }

        Message::I()->Success("Config was updated successfully");
        SDb::transactionCommit();

    }

    //---
    public function DrawBody()
    {
        $rawData = SDbQuery::table("cp_config")->where(["groupName" => "security"])->select();
        $data = array();
        foreach ($rawData as $d) {
            $data[0][$d["attr"]] = $d["val"];
        }

        $form = new FormStatic("Rules of accesses", $data, array("width" => 600));
        $form->Add(new InputCheckbox("Enable", CpConfig::ATTR_ENABLE, array("requirements" => "")));

        $form->Add(new Ajax(PATH_DS."admin/widgets/system/security/filter", false, "html", "ajaxFilter"));

        $panel = new Panel("List of allowed countries");
        $panel->Add(new InputTextArea("Countries", CpConfig::ATTR_LIST, array("height" => 120, "titleDisable" => true,
            "requirements" => "format: 2 letter country code")));
        $form->Add($panel);

        $panel = new Panel("List of allowed IP");
        $panel->Add(new InputTextArea("IP", CpConfig::ATTR_IP, array("height" => 250, "titleDisable" => true,
            "requirements" => "format: *.*.*.*")));
        $form->Add($panel);

        $form->Add(new ButtonAdd(NULL, "Apply"));
        $form->Draw();

        Message::I()->Draw();

        return $this;

    }
}