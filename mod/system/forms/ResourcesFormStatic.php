<?php

/**
 * Class ResourcesFormStatic
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ResourcesFormStatic extends FormStatic
{
    /**
     * @param $action
     * @param bool $idUser
     * @param array $dataRoles
     */
    public function Prepare($action, $idUser = false, $dataRoles = array()) {

        $userModel = new UserModel();

        if ($idUser) {
            $panel = new Panel("User");

            $panel->Add(new ControlSelect("Username:",
                "idUser",
                array("onchange" => "ChangeUser(this);", "noChoose" => false),
                NULL,
                ControlSelect::DataFormat($userModel->findAll("", ['username', 'idUser']), ['username', 'idUser'])
            ));

            $superUser = $userModel->findField("idUser=?", "isSu", [$idUser]);

            $panel->Add(new InputCheckbox("Super user", "", array("disabled" => true, "ignore" => true), $superUser));

            $this->Add($panel);
        } else {
            $this->Add(new InputVar("idRole", "idRole"));
            $this->Add(new InputText("Role name", "name", array("placeholder" => "Sample: Moderator")));
        }

        $this->Add(new ButtonAdd($action, "Apply"));

        $this->_SetComponents($dataRoles);
    }

    /**
     * @param array $dataRoles
     */
    private function _SetComponents($dataRoles = array()) {

        $mods = glob(PATH_REAL . '/mod/*', GLOB_ONLYDIR);
        sort($mods);

        foreach ($mods as $mod) {

            $fileXml = $mod . "/conf.xml";
            $module = (string)basename($mod);


            if (!file_exists($fileXml) && !isset($controlAccess[$module])) {
                continue;
            }
            $xml = simplexml_load_file($fileXml);

            if ($xml->settings->active == 0 && !isset($controlAccess[$module])) {
                continue;
            }

            $this->Add(new FormSeparator("Module: " . $xml->settings->name));


            // в группе
            foreach ($xml->access->group as $groups) {
                $ca = new GrouperCheckall($groups->name);
                $allowedAuto = false;

                foreach ($groups->widgets[0] as $widgetPath => $widgetName) {
                    $widgetName = (string)$widgetName;
                    $resource = $xml->settings->url . "." . $groups->url . "." . $widgetPath;

                    $label = (isset($dataRoles[$resource])) ? "<b class='label labelGreen'>" . $dataRoles[$resource] . "</b>" : "";
                    $active = (isset($this->dataArray[0][$resource]) && $this->dataArray[0][$resource]) ? true : false;

                    if ($label || $active) $allowedAuto = true;

                    $ca->Add(new InputCheckboxCompact($widgetName, $resource,
                        array("requirements" => $label, "disabled" => (isset($dataRoles[$resource]))), ($active || $label)));
                }

                if (!empty($groups->widgets[0])) {
                    $ca->data = $allowedAuto;
                    $this->Add($ca);
                }

            }

            // без группы
            if (isset($xml->access->widgets[0]))
                foreach ($xml->access->widgets[0] as $widgetPath => $widgetName) {

                    $widgetName = (string)$widgetName;
                    $resource = $xml->settings->url . "." . $widgetPath;

                    $label = (isset($dataRoles[$resource])) ? "<b class='label labelGreen'>" . $dataRoles[$resource] . "</b>" : "";
                    $active = (isset($this->dataArray[0][$resource]) && $this->dataArray[0][$resource]) ? true : false;

                    $this->Add(new InputCheckboxCompact($widgetName, $resource,
                        array("requirements" => $label, "disabled" => (isset($dataRoles[$resource]))), ($active || $label)));

                }

            // опции модуля
            if (isset($xml->options[0])) {
                $ca = new GrouperCheckall("Module options");
                $allowedAuto = false;
                foreach ($xml->options[0] as $widgetPath => $widgetName) {


                    $widgetName = (string)$widgetName;

                    $resource = $xml->settings->url . "." . $widgetPath;
                    $resource = str_replace("_", ".", $resource);


                    $label = (isset($dataRoles[$resource])) ? "<b class='label labelGreen'>" . $dataRoles[$resource] . "</b>" : "";
                    $active = (isset($this->dataArray[0][$resource]) && $this->dataArray[0][$resource]) ? true : false;

                    if ($label || $active) $allowedAuto = true;

                    $ca->Add(new InputCheckboxCompact($widgetName, $resource,
                        array("requirements" => $label, "disabled" => (isset($dataRoles[$resource]))), ($active || $label)));

                }
                if (!empty($xml->options[0])) {
                    $ca->data = $allowedAuto;
                    $this->Add($ca);
                }
            }


            if ($xml->settings->url == "support") {

                $departments = SDb::select("tickets_departments");
                if ($departments) {
                    $ca = new GrouperCheckall("Departments");
                    foreach ($departments as $v) {
                        $k = "department." . $v["idDepartment"];

                        $active = (isset($data[0][$k])) ? ""
                            : ((isset($dataRoles[$k]))
                                ? "<b class='label labelGreen'>" . $dataRoles[$k] . "</b>"
                                : "");

                        $active = (isset($dataRoles[$k])) ? "<b class='label labelGreen'>" . $dataRoles[$k] . "</b>" : "";
                        $ca->Add(new InputCheckboxCompact($v["name"], $k, array("requirements" => $active, "disabled" => (isset($dataRoles[$k])))));
                    }
                }
                $this->Add($ca);
            }


        }

    }

}