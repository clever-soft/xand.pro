<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 26.10.15
 * Time: 13:14
 */

use PaysiteCash\Psc;

/**
 * Class EmulatorController
 * Эмуляция работы PSC
 */
class EmulatorController extends BaseController
{

    protected $lastRequest;

    public function defaultAction()
    {

    }


    /**
     * Экшен принимает запрос с транзакцией на оплату и возвращает ответ
     */
    public function actionTransaction()
    {
        $post = $this->post;
        if (!$post) {
            echo "NONE|ko|11|Invalid request||";
            return;
        }
        $this->lastRequest = $post;

        echo "PSC-" . date('Y-m-d') . "" . mt_rand(600000, 999999) . "|wait|0||" . $post['ref'] . "|";
        //KO eg NONE|ko|11||168888|
        //OK eg PSC-2015-07-10584718|wait|0||168889|
        return true;

    }


}