<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 23.11.15
 * Time: 10:46
 */
class CaptchaController extends BaseController
{
    /**
     *
     */
    public function actionDefault() {
        $key = $this->getParam('key');
        ControlCaptcha::DrawImage($key);
    }

    /**
     *
     */
    public function actionFf()
    {
        FFCaptcha::Draw();
    }

}