<?php

/**
 * Description of StoreController
 *
 * @author kubrey
 */
class StoreController extends BaseController {

    public function getAccessRules() {
        return array(
            'switch' => array('level' => '?', 'msg' => 'Access denied'),
        );
    }

    public function actionSwitch() {
        $url = PATH_DS . SITE_CURRENT_LANG . DS;
        foreach (Xand::$nav->path as $id => $upath) {
            if ($id > 2) {
                break;
            }
            $url.=$upath . DS;
        }
        
        $storeCode = (isset(Xand::$nav->path[3]) ? Xand::$nav->path[3] : false);
        StoreModel::model()->resetForceStore();
        if ($storeCode) {
            $forceStore = ($storeCode == 'us' ? 1 : 2);
            StoreModel::model()->switchTo($forceStore);
        }
        $st = StoreModel::get();
        echo $this->getStpl()->render(false,'phtml/helpers/storeswitch',
            array('url' => trim($url, "/"), 'store' => $st['idStore']));
    }

    public function actionDefault() {
        $this->httpError('/', 404);
    }

}
