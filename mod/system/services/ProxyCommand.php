<?php

/**
 * Description of ProxyCommand
 *
 * @author kubrey
 */
class ProxyCommand extends BaseConsole {

    public function run() {
        $proxy = new ProxyComponent;
//        var_dump(XAND_MAILHOST);

        $list = $proxy->search();
        echo "Get ".count($list)." proxy\n";
        $proxy->update($list);
        var_dump($proxy->getErrors());
    }

}
