<?php

/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 11.11.15
 * Time: 10:52
 */
class CpConfig extends BaseModel
{

    const ATTR_IP = "security.access.ip";
    const ATTR_LIST = "security.access.list";
    const ATTR_ENABLE = "security.access.enable";



    public function getTableName()
    {
        return 'cp_config';
    }

    public function getId()
    {
        return 'id';
    }

    /**
     * @param string $field
     * @return bool|null|string null - если нет такого аттрибута; false - ошибка mysql
     */
    public function __get($field)
    {
        $data = $this->findOne("attr=?", array('val'), array($field));
        if ($data) {
            return $data['val'];
        } else {
            return ($data) ? null : false;
        }
    }
}