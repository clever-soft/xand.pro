<?php

/**
 * Description of PagesModel
 *
 */
class PagesModel extends I18nModel
{

    public function __construct()
    {
        parent::__construct();
        $this->useCache = true;
        $this->cacheTime = 600;
    }

    /**
     * @return array|bool
     */
    public static function getOrderedByNpp()
    {
        $data = self::model()->findAll("idParent=3", [], [], ['npp' => 'ASC']);
//        $data = $this->selectSafe("select * from " . $this->getTableName() . " where idParent=3 order by rand() limit 0," . (int)$limit);
        return $data;
    }

    /**
     *
     * @param string $className
     *
     * @return PagesModel
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     *
     * @return array
     */
    public static function getBlocks()
    {
        $model = new BlocksModel();
        $model->useCache = true;
        $blocks = $model->findAllI18n($model->getTableName() . ".isActive=1 and " . $model->getTableName_i18n() . ".lang=?", [$model->getTableName() . '.code', 'content'], [SITE_CURRENT_LANG]);
        return $blocks;
    }

    /**
     *
     * @param string $path
     *
     * @return string
     */
    public static function getContentByUrl($path)
    {
        $data = self::model()->findOne("url=? AND isActive = 1", array($path));
        return $data;
    }

    /**
     * @param int $limit
     *
     * @return array|bool
     */
    public function getRandom($limit = 8)
    {
        $data = SDb::rows("select * from " . $this->getTableName() . " where idParent=2 order by rand() limit 0," . (int)$limit);
        return $data;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return "cms_textpages";
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return array(
            'idPage',
            'idParent',
            'npp',
            'pathNpp',
            'path',
            'url',
            'level',
            'hashTag',

            'tpl',
            'tplFolder',

            'title',
            'h1',
            'h2',
            'keywords',
            'description',
            'content',

            'isActive',
            'isSystem',
            'isMenu',
            'isFooter',
            'isFooterExtra',

            'javaScript',
            'menuClass',
            'menuIcon',
            'menuTitle',
            'menuDescription',

            'dateCreate',
            'dateUpdate'
        );
    }

    /**
     * @return array
     */
    public function get_translated_fields()
    {
        return array(
            'content', 'h1', 'h2', 'description', 'title', 'menuTitle', 'menuDescription'
        );
    }

    /**
     * @return string
     */
    public function getId()
    {
        return "idPage";
    }

    /**
     * @param $idPage
     *
     * @return array|bool
     */
    public function getTranslates($idPage)
    {
        $data = SDb::rows("select * from " . $this->getTableName_i18n() . " where idPage=? and lang=?", array($idPage, SITE_CURRENT_LANG));
        return $data;
    }

    /**
     * @return string
     */
    public function getTableName_i18n()
    {
        return "cms_textpages_i18n";
    }

    /**
     * @param bool $parent
     * @return array|bool
     */
    public function getMenu($parent = false)
    {

        $menu = $this->findAllWithTranslate(
            array(
                'where' => array(
                    'row' => 'cms_textpages.isActive=? AND cms_textpages.isMenu=? AND cms_textpages_i18n.lang=? AND idParent = ?',
                    'params' => array(1, 1, SITE_CURRENT_LANG, $parent)
                ),
                'order' => array("npp" => "ASC"),
                'fields' => array('menuTitle', 'menuDescription', 'url', 'menuIcon', 'idPage')
            ));

        foreach ($menu as &$m) {
            if ($this->count("idParent = ?", [$m['idPage']])) {
                $m['child'] = $this->getMenu($m['idPage']);
            }
        }

        return ['menu' => $menu];
    }

    /**
     * @param $data
     * @param $active
     * @return string
     */
    public static function buildMenu($data, $active)
    {
        $html = '';
        foreach ($data['menu'] as $menu) {
            $tmp = ($active == $menu['url']) ? 'menuActive active' : 'menuInactive';
            $parent = (isset($menu['child'])) ? "parent" : false;

            $drop = ($parent) ? "dropdown" : "";
            $html .= "<li class='nav-item {$drop} {$tmp}'>";

            $html .= "<a href='{{PathDs}}{{Lang}}/{$menu["url"]}' class='menuMain menuMain16 nav-link {$parent}'>";

            $html .= "<h4>{$menu['menuTitle']}</h4>";
            $html .= "<span>{$menu['menuDescription']}</span>";
            $html .= "</a>";
            if ($parent) {
                $html .= " <ul class='nav nav-justified navbar-fixed-top child'>";
                $html .= self::buildMenu($menu['child'], $active);
                $html .= " </ul>";
            }
            $html .= "</li>";
        }
        return $html;
    }

    /**
     *
     * @param array $arr_data
     * @param array $arr_data2
     * @param array $arr_parent
     *
     * @return null
     */

    public function InsertWidgetPage($arr_data = array(), $arr_data2 = array(), $arr_parent = array())
    {
        $arr_data['dateCreate'] = time();
        $arr_data['dateUpdate'] = time();

        $arr_data['npp'] = (int)$arr_data['npp'];
        $arr_data['url'] = Content::createUrl($arr_data['url']);

        $idPage = $this->holdRow();
        if (!$idPage) {
            Message::I()->Error("Error occurred: " . $this->getLastError());
            return;
        }

        foreach ($arr_data2 as $lang => $a) {
            $upd = array(
                "lang" => $lang,
                "idPage" => $idPage,
                "h1" => $a["h1"],
                "h2" => $a["h2"],
                "title" => $a["title"],
                "menuTitle" => $a["menuTitle"],
                "menuDescription" => $a["menuDescription"],
                "keywords" => $a["keywords"],
                "description" => $a["description"],
                "content" => $a["content"]
            );
            $this->upsertSimpleI18n("idPage=? and lang=?", $upd, [$idPage, $lang]);

        }


        if ($arr_data['url'] == "") {
            $arr_data['url'] = Content::createUrl($arr_data['title']);
        }


        // Добавляем родительские параметры
        if (!empty($arr_parent)) {
            $arr_data["path"] = $arr_parent['path'] . $idPage . DS;
            $arr_data["idParent"] = $arr_parent['idPage'];
            $arr_data["level"] = $arr_parent['level'] + 1;

            $separator = ($arr_data["hashTag"]) ? "#" : DS;
            $arr_data['url'] = $arr_parent['url'] . $separator . $arr_data['url'];

        } else {
            $arr_data["path"] = DS . $idPage . DS;
            $arr_data["idParent"] = 0;
            $arr_data["level"] = 0;
        }

        if (!$this->updateById($idPage, $arr_data)) {
            Message::I()->Error("Error occurred 2: " . $this->getLastError());
        } else {
            Message::I()->Success("Updated");
        }
    }

    /**
     *
     * @param array $arr_data
     * @param array $arr_data2
     * @param array $arr_parent
     *
     * @return null
     */

    public function UpdateWidgetPage($arr_data = array(), $arr_data2 = array(), $arr_parent = array())
    {
        $this->beginTransaction();
        foreach ($arr_data2 as $lang => $a) {
            $upd = array(
                "lang" => $lang,
                "idPage" => $arr_data["idPage"],
                "h1" => $a["h1"],
                "h2" => $a["h2"],
                "title" => $a["title"],
                "menuTitle" => $a["menuTitle"],
                "menuDescription" => $a["menuDescription"],
                "keywords" => $a["keywords"],
                "description" => $a["description"],
                "content" => $a["content"]
            );

            if (!SDbQuery::table("cms_textpages_i18n")->where(["idPage" => $arr_data['idPage'], "lang" => $lang])->upsert($upd)) {
                $this->rollbackTransaction();
                Message::I()->Error("Error occurred")->Draw();
                return;
            }

        }

        $arr_data['dateUpdate'] = time();
        $arr_data['npp'] = (int)$arr_data['npp'];

        if ($arr_data['url'] == "") {
            $arr_data['url'] = Content::createUrl($arr_data['title']);
        }

        // Добавляем родительский url для упрощения поиска по url
        if (!empty($arr_parent)) {

            $separator = ($arr_data["hashTag"]) ? "#" : DS;
            $arr_data['url'] = str_replace($arr_parent['url'] . $separator, "", $arr_data['url']);
            $arr_data['url'] = $arr_parent['url'] . $separator . $arr_data['url'];
        }
        if ($this->updateById($arr_data['idPage'], $arr_data)) {
            $this->commitTransaction();
            Message::I()->Success("Updated");
        } else {
            $this->rollbackTransaction();
            Message::I()->Error("Error occurred");
        }


    }

}
