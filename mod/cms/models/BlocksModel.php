<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 05.05.15
 * Time: 9:37
 */
class BlocksModel extends I18nModel
{
    public static function model($class = __CLASS__) {
        return parent::model($class);
    }

    public function getTableName() {
        return "cms_blocks";
    }

    public function getTableName_i18n() {
        return "cms_blocks_i18n";
    }

    public function getAttributes() {
        return array(
            'idBlock',
            'content',
            'idActive',
            'dateCreate',
            'dateUpdate'
        );
    }

    public function get_translated_fields() {
        return array(
            'content'
        );
    }

    public function getId() {
        return "idBlock";
    }
}