<?php

class SliderModel extends I18nModel
{

    /**
     * @return string
     */
    public function getId() {
        return "idSlide";
    }

    /**
     * @return string
     */
    public function getTableName() {
        return "cms_slider";
    }

    /**
     * @return string
     */
    public function getTableName_i18n() {
        return "cms_slider_i18n";
    }

    /**
     * @return array
     */
    public function getAttributes() {
        return array(
            'idSlide',
            'npp',
            'url',
            'imagePath',
            'imageAlt',
            'isActive',
            'countdown',
            'testMode',
            'timerActive',
            'timerStart',
            'timerEnd',
            'dateCreate',
            'dateUpdate'
        );
    }

    /**
     * @return array
     */
    public function get_translated_fields() {
        return array(
            'title', 'imageAlt', 'content'
        );
    }

    /**
     * @param $id
     * @param $mainData
     * @param $localeData
     * @return bool
     */
    public function update($id, $mainData, $localeData) {
        $this->setTableName($this->getTableName());
        $columnsBase = $this->getTableColumns($this->getTableName());
        $columnsChild = $this->getTableColumns($this->getTableName_i18n());
        if (!$columnsBase || !$columnsChild) {
            return false;
        }
        $this->beginTransaction();

        $main = [];
        foreach ($columnsBase as $col) {
            if (isset($mainData[$col])) {
                $main[$col] = $mainData[$col];
            }
        }

        if ($main['timerActive']) {
            $timerStart = new DateTime($main['timerStart']);
            $timerEnd = new DateTime($main['timerEnd']);

            $main['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $main['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $main['timerStart'] = 0;
            $main['timerEnd'] = 0;
        }
        $main['timerActive'] = (int)$main['timerActive'];
        $main['dateUpdate'] = time();

        if (!$this->updateById($id, $main)) {
            $this->rollbackTransaction();
            return false;
        }

        foreach ($localeData as $lang => $a) {
            $upd = [
                'lang' => $lang,
                'title' => $a['title'],
                'content' => $a['content'],
                'imageAlt' => $a['imageAlt'],
                'idSlide' => $id
            ];

            if (!$this->setTableName($this->getTableName_i18n())->upsertSimple('idSlide=? and lang=?', $upd, [$id, $lang])) {
                $this->rollbackTransaction();
                return false;
            }
        }

        $this->commitTransaction();
        return true;

    }

    public function create($mainData, $localeData) {
        $this->setTableName($this->getTableName());
        $columnsBase = $this->getTableColumns($this->getTableName());
        $columnsChild = $this->getTableColumns($this->getTableName_i18n());
        if (!$columnsBase || !$columnsChild) {
            return false;
        }
        $this->beginTransaction();

        $main = [];
        foreach ($columnsBase as $col) {
            if (isset($mainData[$col])) {
                $main[$col] = $mainData[$col];
            }
        }

        if ($main['timerActive']) {
            $timerStart = new DateTime($main['timerStart']);
            $timerEnd = new DateTime($main['timerEnd']);

            $main['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $main['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $main['timerStart'] = 0;
            $main['timerEnd'] = 0;
        }
        $main['timerActive'] = (int)$main['timerActive'];
        $main['dateUpdate'] = time();
        $main['dateCreate'] = time();

        $id = $this->insert($main, true);

        if (!$id) {
            $this->rollbackTransaction();
            return false;
        }

        foreach ($localeData as $lang => $a) {
            $upd = [
                'lang' => $lang,
                'title' => $a['title'],
                'content' => $a['content'],
                'imageAlt' => $a['imageAlt'],
                'idSlide' => $id
            ];

            if (!$this->setTableName($this->getTableName_i18n())->upsertSimple('idSlide=? and lang=?', $upd, [$id, $lang])) {
                $this->rollbackTransaction();
                return false;
            }
        }

        $this->commitTransaction();
        return true;
    }

    /**
     * Deleting slide with it's locale records(i18n)
     * @param $id
     * @return bool
     */
    public function remove($id) {
        $this->setTableName($this->getTableName());
        $this->beginTransaction();

        if (!$this->setTableName($this->getTableName_i18n())->deleteByAttributes("idSlide=?", [$id])) {
            $this->rollbackTransaction();
            return false;
        }
        $this->setTableName($this->getTableName());
        if (!$this->delete($id)) {
            $this->rollbackTransaction();
            return false;
        }

        $this->commitTransaction();
        return true;
    }


}