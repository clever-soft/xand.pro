<?php

class I18nModel extends BaseModel
{
    /**
     * @param array $values
     * @param bool $insertId
     * @param bool $ignore
     *
     * @return bool|int
     */
    public function insertI18n($values, $insertId = false, $ignore = false)
    {
        return $this->insert($values, $insertId, $ignore);
    }

    /**
     * Для вызовы функций идентичных модели, но с окончанием I18n(для запроса в таблицу *_i18n, например ->countI18n
     *
     * @param $name
     * @param $args
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
        if (strpos($name, 'I18n') === false) {
            return;
        }
        $method = str_replace('I18n', '', $name);

        if (method_exists($this, $method)) {
            $this->setTableName($this->getTableName_i18n());
            $data = call_user_func_array(array($this, $method), $args);
            $this->setTableName('');
            return $data;
        }
    }

    /**
     * Имя таблицы в БД, в которой хранятся переводы
     * @return string
     */
    public function getTableName_i18n()
    {
        return $this->getTableName() . '_i18n';
    }

    public function getTableName()
    {
        $class = get_called_class();
        echo self::model($class)->getTableName() . "_i18n";
    }

    public function getId()
    {
        $class = get_called_class();
        echo self::model($class)->getId();
    }

    /**
     * Получение элемента по идентификатору
     *
     * @param int $id - идентификатор
     *
     * @return array|bool - объект или false в случае, если нет объекта с таким идентификатором
     */
    public function findById($id)
    {
        return $this->findOneWithTranslate(array(
            'where' => array(
                'row' => $this->getTableName() . '.' . $this->getId() . ' = ?',
                'params' => array($id)
            )
        ));
    }

    /**
     * Запрос на выборку данных одной страницы с учётом перевода.
     *
     * @param array $params - ассоциативный многомерный массив с параметрами запроса.
     *                      Парамеры аналогичны параметрам для функции findAllWithTranslate() с тем лишь отличием, что
     *                      limit выставляется строго (0, 1).
     *
     * @return array|bool - результаты запроса или false (если результатов нет)
     */
    public function findOneWithTranslate($params = array())
    {
        if (!is_array($params)) return false;

        // Вызываем функцию findAllWithTranslate, но с учётом того, что должен быть выбран строго первый результат
        $params['limit'] = array(0, 1);

        $data = $this->findAllWithTranslate($params);
        if ($data && is_array($data) && !empty($data)) {
            return array_shift($data);
        } else {
            return false;
        }
    }

    /**
     * Общий запрос на выборку страниц с учетом переводов
     *
     * @param array $params - ассоциативный многомерный массив с параметрами запроса.
     *                      Если параметры переданы в неверном формате, то запрос выполнен не будет.
     *                      param array $params['where']  - объект с двуми полями - row (строка where-запроса с
     *                      холдерами) и params (массив параметров в порядке следования в строке). param array
     *                      $params['fields'] - список полей, значения которых нужно получить. Если передан пустой
     *                      массив, будут получены все поля. param array $params['limit']  - ограничения на количество
     *                      результатов в выборке. Если передан пустой массив, будут получены все результаты. param
     *                      array $params['order']  - ассоциативный массив вида '$field => $order_direction',
     *                      направление сортировки
     *
     * @return array|bool - результаты запроса или false (если результатов нет)
     */
    public function findAllWithTranslate($params = array())
    {
        if (!is_array($params)) return false;

        // Парсинг параметров sql-запроса
        $query_params = $this->parse_all_params($params);

        $query_params['fields'] = implode(",", $query_params['fields']);

        // Основной запрос
        $data = SDb::rows(
            "SELECT {$query_params['fields']} FROM " . $this->getTableName() . " AS " . $this->getTableName() . "
            LEFT JOIN " . $this->getTableName_i18n() . " AS " . $this->getTableName_i18n() . "
                ON " . $this->getTableName() . "." . $this->getId() . " = " . $this->getTableName_i18n() . "." . $this->getId() . "
                AND " . $this->getTableName_i18n() . ".lang = '" . SITE_CURRENT_LANG . "'
            WHERE {$query_params['where_row']}
            {$query_params['order']}
            {$query_params['limit']}"
            , $query_params['where_arr']
        );


        // Возвращаем false если данные не получены
        if (empty($data)) return false;

        return $data;
    }

    /**
     * Парсинг переданных параметров к виду, приемлемому для sql-запроса
     *
     * @param $params - переданные параметры.
     *
     * @return array - приемлемые для sql-запроса параметры в виде ассоциативного массива:
     * 'where_row' - строка where-части запроса, с холдерами
     * 'where_arr' - массив-список параметров для холдеров в порядке следования в where_row
     *   'fields'  - строка-список полей для select-а
     *   'order'   - строка для сортировки результатов запроса
     *   'limit'   - строка для лимита запроса
     */
    private function parse_all_params($params)
    {
        // Стандартные значения параметров sql-запроса
        $where_row = '';
        $where_arr = array();
        $fields = array();
        $limit_query = '';
        $order_by = '';

        // Обрабатываем список переданных параметров

        if (isset($params['where']) && is_array($params['where'])) {
            list($where_row, $where_arr) = $this->parse_where_params($params['where']);
        }

        if (isset($params['fields']) && is_array($params['fields'])) {
            $fields = $params['fields'];
        }
        $fields = $this->get_translated_field($fields); // Изменяем SELECT-ы для полей, требующих перевода

        if (isset($params['limit']) && is_array($params['limit'])) {
            $limit_query = $this->parse_limit_params($params['limit']);
        }

        if (isset($params['order']) && is_array($params['order'])) {
            $order_by = $this->parse_order_params($params['order']);
        }

        $result = array();
        $result['where_row'] = $where_row;
        $result['where_arr'] = $where_arr;
        $result['fields'] = $fields;
        $result['limit'] = $limit_query;
        $result['order'] = $order_by;

        return $result;
    }

    /**
     * Парсинг параметров для where-части sql-запроса
     *
     * @param $params - параметры для этой части
     *
     * @return array - строка с холдерами как первый параметр, список параметров запроса как второй параметр
     */
    private function parse_where_params($params)
    {
        $where_row = '';
        $where_arr = array();

        if (isset($params['row']) && isset($params['params'])) {
            $where_row = $params['row'];
            $where_arr = $params['params'];
        }

        return array($where_row, $where_arr);
    }

    /**
     * Получение списка дополнительной выборки в sql-запросе.
     * Этот список необходим для того, чтобы, если перевода нет, брать стандартное значение поля.
     *
     * @param $field_list
     *
     * @return array
     */
    private function get_translated_field($field_list)
    {

        $select_all_fields = empty($field_list);
        $fields_i18n = ($select_all_fields) ? $this->getAttributes() : $field_list;

        foreach ($fields_i18n as $index => $field) {
            if (in_array($field, $this->get_translated_fields())) {
                // Translate table's field
                $ttf = $this->getTableName_i18n() . "." . $field;
                // Origin table's field
                $otf = $this->getTableName() . "." . $field;
                $fields_i18n[$index] = "IF(ISNULL($ttf) OR $ttf = '', $otf, $ttf) as $field";
            } else {
                $fields_i18n[$index] = $this->getTableName() . "." . $field;
            }
        }

        return $fields_i18n;
    }

    /**
     * Список полей, которые могут иметь перевод
     * @return array
     */
    public function get_translated_fields()
    {
        return array();
    }

    /**
     * Парсинг limit-параметров sql-запроса
     *
     * @param $params - список параметров, массив из двух чисел - offset и rows
     *                offset - с какой строки вернуть результаты
     *                rows - на скольки строках необходимо ограничиться
     *
     * @return string - строка sql-запроса
     */
    private function parse_limit_params($params)
    {
        $limit_query = '';
        if (!empty($params) && (count($params) >= 2)) {
            $limit_query = "LIMIT {$params[0]}, {$params[1]}";
        }
        return $limit_query;
    }

    private function parse_order_params($params)
    {
        $order_by = 'ORDER BY ';
        foreach ($params as $order_field => $order_direction) {
            // Если не передано направление сортировки, то значение - это на самом деле поле, а направление сортировки берём стандартное
            if (empty($order_direction)) {
                $order_field = $order_direction;
                $order_direction = 'ASC';
            }

            $order_by .= "$order_field $order_direction,";
        }
        $order_by = trim($order_by, ',');
        return $order_by;
    }
}