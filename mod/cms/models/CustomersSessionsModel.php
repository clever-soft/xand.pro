<?php

/**
 * Description of CustomersSessionsModel
 *
 * @author kubrey
 */
class CustomersSessionsModel extends BaseModel{
    
    public function getTableName() {
        return 'customers_sessions';
    }
    
    public function getId() {
        return 'sid';
    }
}