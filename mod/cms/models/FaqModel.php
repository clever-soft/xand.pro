<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 05.05.15
 * Time: 9:37
 */

class FaqModel extends I18nModel
{
    public static function model($class = __CLASS__){
        return parent::model($class);
    }

    public function getTableName() {
        return "cms_faq";
    }

    public function getTableName_i18n() {
        return "cms_faq_i18n";
    }

    public function getAttributes() {
        return array(
            'idFaq',
            'idParent',
            'title',
            'content',
            'npp',
            'enable',
            'dateCreate',
            'dateUpdate'
        );
    }

    public function get_translated_fields()
    {
        return array(
            'title', 'content'
        );
    }

    public function getId() {
        return "idFaq";
    }
}