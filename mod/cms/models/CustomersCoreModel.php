<?php

/**
 * Модель кастомера
 * Реализует управление объектом покупателя
 *
 * @author kubrey
 */
class CustomersCoreModel extends BaseModel
{

    const SALT = "BP_SECRET!!SDFSDFSDFSsadasd";
    const AUTH_TYPE_EMAIL = 1;
    const AUTH_TYPE_FB = 2;
    const AUTH_TYPE_GOOGLE = 3;

    public $idAccount;
    public $idLocation;
    public $pathLocation;
    public $name; //?
    public $phone; //?
    //available fields;
    public $prefix;
    public $firstname;
    public $middlename;
    public $lastname;
    public $suffix;
    public $email;
//    public $group_id;
    public $dob;
    public $unlockCustomer;
    public $dpaName;
    public $dpaNumber;
    public $dpaBlz;
    public $dpaIban;
    public $dpaBic;
    public $pass;
    public $isActive;
    public $billingAddress;
    public $shippingAddress;
    public $bankData;
    private $addressFields = array('idAddress', 'type', 'street', 'countryCode', 'l1', 'l2', 'zipCode',
        'phone', 'fax', 'firstname', 'sex', 'city', 'company', 'lastname',
        'dob', 'sameAsBillingAddress');
    private $bankFields = array('dpaName', 'dpaNumber', 'dpaIban', 'dpaBic', 'dpaBlz');
    private $commonFields = array('idAccount', 'idGroup', 'email', 'dateRegister', 'isActive', 'dateLogin', 'fbid', 'gplusid', 'fbdata', 'gplusdata');
    private $sexPrefixes = array("de" => array(1 => "Herr", 2 => "Frau"), "en" => array(1 => "Mister", 2 => "Miss"));
    //
    public $dateUpdate;
    public $dateRegister;
    //
    protected $errors = array();
    protected $customer = array();
    protected static $encryptedFields = array('dpaNumber', 'dpaIban', 'dpaBic');

    public function getRules() {
        $this->rules = array(
            'idGroup' => array(parent::VALIDATION_NUMERIC, parent::VALIDATION_REQUIRED),
            'email' => array(parent::VALIDATION_EMAIL, parent::VALIDATION_REQUIRED),
            'idAccount' => array(parent::VALIDATION_NUMERIC),
            'billing[firstname]' => array(parent::VALIDATION_REQUIRED, array(self::VALIDATION_LENGTH_MIN => 2), array(self::VALIDATION_LENGTH_MAX => 60)),
            'billing[sex]' => array(parent::VALIDATION_NUMERIC),
            'billing[lastname]' => array(parent::VALIDATION_REQUIRED, array(self::VALIDATION_LENGTH_MIN => 2), array(self::VALIDATION_LENGTH_MAX => 60)),
            'billing[phone]' => array(parent::VALIDATION_REQUIRED, [self::VALIDATION_LENGTH_MIN => 6], [self::VALIDATION_REGEX => '%[0-9-+ ()]{6,25}%i']),
            'billing[l1]' => array(parent::VALIDATION_REQUIRED, parent::VALIDATION_NUMERIC, parent::VALIDATION_POSITIVE),
            'billing[city]' => array(parent::VALIDATION_REQUIRED),
            'billing[zipCode]' => array(parent::VALIDATION_REQUIRED, self::VALIDATION_NUMERIC),
            'billing[street]' => array(parent::VALIDATION_REQUIRED),
            'shipping[firstname]' => array(parent::VALIDATION_REQUIRED, array(self::VALIDATION_LENGTH_MIN => 2), array(self::VALIDATION_LENGTH_MAX => 60)),
            'shipping[sex]' => array(parent::VALIDATION_NUMERIC),
            'shipping[lastname]' => array(parent::VALIDATION_REQUIRED, array(self::VALIDATION_LENGTH_MIN => 2), array(self::VALIDATION_LENGTH_MAX => 60)),
            'shipping[phone]' => array(parent::VALIDATION_REQUIRED, [self::VALIDATION_LENGTH_MIN => 6], [self::VALIDATION_REGEX => '%[0-9-+ ()]{6,25}%i']),
            'shipping[l1]' => array(parent::VALIDATION_REQUIRED, parent::VALIDATION_NUMERIC, parent::VALIDATION_POSITIVE),
            'shipping[city]' => array(parent::VALIDATION_REQUIRED),
            'shipping[zipCode]' => array(parent::VALIDATION_REQUIRED, self::VALIDATION_NUMERIC),
            'shipping[street]' => array(parent::VALIDATION_REQUIRED),
            'dpaIban' => array(BasePayment::VALIDATE_IBAN),
            'dpaBic' => array(BasePayment::VALIDATE_BIC),
            'dpaBlz' => array(BasePayment::VALIDATE_BLZ),
            'dpaNumber' => array(BasePayment::VALIDATE_CC),
            'pass' => array(array(self::VALIDATION_LENGTH_MIN => 6), array(self::VALIDATION_LENGTH_MAX => 30), self::VALIDATION_REQUIRED),
            'passwordConfirm' => array(array(self::VALIDATION_LENGTH_MIN => 6), array(self::VALIDATION_LENGTH_MAX => 30), self::VALIDATION_REQUIRED, array(self::VALIDATION_EQUALS_TO => 'pass'))
        );
        return $this->rules;
    }

    /**
     * @param $data
     * @return array|bool
     */
    public static function doesEmailBelongToCustomer($data) {
        $check = self::model()->count("email=?", [$data['email']]);

        if (!$check) {
            return [false, [0 => ['email' => 'This email doesn\'t belong to any customer']]];
        }
        return true;
    }

    /**
     *
     * @return array [pass=>string,salt=>string]
     */
    public static function generatePass() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = mt_rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $salt = array();
        for ($i = 0; $i < 2; $i++) {
            $n = mt_rand(0, $alphaLength);
            $salt[] = $alphabet[$n];
        }
        return array('pass' => implode($pass), 'salt' => implode($salt));
    }

    /**
     * ID в таблицу
     * @return string
     */
    public function getId() {
        return 'idAccount';
    }

    public function getAddressFields() {
        return $this->addressFields;
    }

    /**
     * Префикс к юзеру
     * @param $val
     * @param $lang
     * @return string
     */
    public function SexToString($val, $lang) {
        $lang = strtolower($lang);
        return (isset($this->sexPrefixes[$lang][$val])) ? $this->sexPrefixes[$lang][$val] : "";
    }

    public function getAttributes() {
        return array(
            'idAccount',
            'idGroup',
            'email',
            'hash',
            'dateRegister',
            'isActive',
            'dateLogin',
            'dateRecovery',
            'dateUpdate',
            'dpaName',
            'dpaNumber',
            'dpaIban',
            'dpaBic',
            'dpaBlz',
            'fbid',
            'gplusid',
            'fbdata',
            'gplusdata',
            'isNotified',
            'idDealer',
            '*pass',
            '*passwordConfirm'
        );
    }

    /**
     * @param null $field
     * @param null $block
     * @return mixed|null
     */
    public function getLabel($field = null, $block = null) {
        $labels = array(
            'idAccount' => "Customer ID",
            'idGroup' => "Group ID",
            'email' => "Email",
            'hash' => "Password hash",
            'dateRegister' => "Register date",
            'isActive' => "Is active",
            'dateLogin' => "Login date",
            'dateRecovery' => "Recovery date",
            'dateUpdate' => "Date of last update",
            'dpaName' => "Account owner",
            'dpaNumber' => "Bank account number",
            'dpaIban' => "IBAN",
            'dpaBic' => "BIC",
            'dpaBlz' => "Bank code",
            //
            'billing[firstname]' => 'First name',
            'billing[lastname]' => 'Last name',
            'billing[zipCode]' => 'Zip code',
            'billing[sex]' => 'Sex',
            'billing[phone]' => 'Phone',
            'billing[fax]' => 'Fax',
            'billing[street]' => 'Street',
            'billing[countryCode]' => 'Country ISO code',
            'billing[l1]' => 'Country',
            'billing[l2]' => 'Region',
            'billing[city]' => 'City',
            'billing[dob]' => 'Date of birth',
            'billing[company]' => 'Company',
            //
            'shipping[firstname]' => 'First name',
            'shipping[lastname]' => 'Last name',
            'shipping[zipCode]' => 'Zip code',
            'shipping[sex]' => 'Sex',
            'shipping[phone]' => 'Phone',
            'shipping[fax]' => 'Fax',
            'shipping[street]' => 'Street',
            'shipping[countryCode]' => 'Country ISO code',
            'shipping[l1]' => 'Country',
            'shipping[l2]' => 'Region',
            'shipping[city]' => 'City',
            'shipping[dob]' => 'Date of birth',
            'shipping[company]' => 'Company',
            //
            'bank[dpaName]' => "Bank account Name",
            'bank[dpaNumber]' => "Bank account number",
            'bank[dpaIban]' => "IBAN",
            'bank[dpaBic]' => "BIC",
            'bank[dpaBlz]' => "BLZ",
            //
            'pass' => 'Password',
            'passwordConfirm' => 'Password confirm'
        );

        if ($block) {
            $key = $block . '[' . $field . ']';
        } else {
            $key = $field;
        }
        $data = in_array($key, array_keys($labels)) ? $labels[$key] : $field;
        return $data;
    }

    /**
     *
     * @return array
     */
    public function getBlockAttributes() {
        return array(
            'billing' => array('fields' => array('idAddress', 'idAccount', 'firstname', 'lastname', 'zipCode', 'sex', 'phone', 'fax', 'type', 'street', 'countryCode', 'l1', 'l2', 'city', 'company', 'sameAsBillingAddress', 'dob')),
            'shipping' => array('fields' => array('idAddress', 'idAccount', 'firstname', 'lastname', 'zipCode', 'sex', 'phone', 'fax', 'type', 'street', 'countryCode', 'l1', 'l2', 'city', 'company', 'sameAsBillingAddress', 'dob')),
            'bank' => array('fields' => array('dpaName', 'dpaBic', 'dpaIban', 'dpaNumber', 'dpaBlz'), 'type' => 'inner')
        );
    }

    /**
     * Получает все данные кастомера
     * @param int $id
     * @return boolean|array
     */
    public function getFullData($id) {

        $q = "select 
    " . $this->getTableName() . ".idGroup,
    " . $this->getTableName() . ".email,
    " . $this->getTableName() . ".dateRegister,
    " . $this->getTableName() . ".isActive,
    " . $this->getTableName() . ".dpaName,
    " . $this->getTableName() . ".dpaIban,
    " . $this->getTableName() . ".dpaBic,
    " . $this->getTableName() . ".dpaBlz,
    " . $this->getTableName() . ".dateLogin,
    " . $this->getTableName() . ".dpaNumber,
    " . $this->getTableName() . ".fbid,
    " . $this->getTableName() . ".gplusid,
    " . $this->getTableName() . "_addresses.idAddress,
    " . $this->getTableName() . "_addresses.type,
    " . $this->getTableName() . "_addresses.street,
    " . $this->getTableName() . "_addresses.countryCode,
    " . $this->getTableName() . "_addresses.l1,
    " . $this->getTableName() . "_addresses.l2,
    " . $this->getTableName() . "_addresses.zipCode,
        " . $this->getTableName() . "_addresses.dob,
    " . $this->getTableName() . "_addresses.phone,
    " . $this->getTableName() . "_addresses.fax,
    " . $this->getTableName() . "_addresses.firstname,
    " . $this->getTableName() . "_addresses.sex,
    " . $this->getTableName() . "_addresses.city,
    " . $this->getTableName() . "_addresses.company,
    " . $this->getTableName() . "_addresses.lastname,
        " . $this->getTableName() . "_addresses.sameAsBillingAddress
from
    " . $this->getTableName() . "
        left join
    " . $this->getTableName() . "_addresses ON " . $this->getTableName() . "." . $this->getId() . " = " . $this->getTableName() . "_addresses.idAccount
where
    " . $this->getTableName() . ".idAccount = ?";
        $data = SDb::rows($q, array($id));
        if (!$data) {
            return false;
        }

        $this->billingAddress = array();
        $this->shippingAddress = array();
        $this->model['_common'] = array();
        if ($data[0]['sameAsBillingAddress'] == 1) {

            unset($data[1]);
            foreach ($data[0] as $key => $val) {
                if (in_array($key, $this->addressFields)) {

                    $this->model['billing'][$key] = $val;
                    $this->model['shipping'][$key] = $val;
//                    var_dump($this->model['shipping']);
                }
                if (in_array($key, $this->bankFields)) {
                    $this->model['bank'][$key] = $val;
                }
                if (in_array($key, $this->commonFields)) {
                    $this->model[$key] = $val;
                    $this->model['_common'][$key] = $val;
                }
            }
        } else {
            if (count($data) == 1 && $data[0]['idAddress'] === NULL) {
                $this->model['billing'] = array();
                $this->model['shipping'] = array();
            }
            foreach ($data[0] as $key => $val) {
                if ($data[0]['type'] == 1) {
                    //billing
                    if (in_array($key, $this->addressFields)) {
                        $this->model['billing'][$key] = $val;
                    }
                } else {
                    if (in_array($key, $this->addressFields)) {
                        $this->model['shipping'][$key] = $val;
                    }
                }
                if (in_array($key, $this->bankFields)) {
                    $this->model['bank'][$key] = $val;
                }
                if (in_array($key, $this->commonFields)) {
                    $this->model[$key] = $val;
                }
            }
            if (count($data) == 1 && $data[0]['idAddress'] === NULL) {
                $this->model['billing'] = array();
                $this->model['shipping'] = array();
            } else {
                foreach ($data[1] as $k => $v) {
                    if ($data[1]['type'] == 1) {
                        //billing
                        if (in_array($k, $this->addressFields)) {
                            $this->model['billing'][$k] = $v;
                        }
                    } else {
                        if (in_array($k, $this->addressFields)) {
                            $this->model['shipping'][$k] = $v;
                        }
                    }
                    //остальные данные кроме адреса дублируются
                }
            }
        }

        $enc = new Encrypter();
        foreach ($this->model['bank'] as $k => $v) {
            if (in_array($k, self::$encryptedFields) && Encrypter::isEncrypted($v)) {
//                echo $k;
                $this->model['bank'][$k] = $enc->decrypt($v);
            }
        }

        return $this->model;
    }

    /**
     *
     * @return array
     */
    public function getAppliedData() {
        return $this->model;
    }


    /**
     *
     * @return string
     */
    public function getTableName() {
        return 'customers';
    }

    public function getClass() {
        return __CLASS__;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function disable($id) {
        return self::model()->updateById($id, array('isActive' => 0));
    }

    public function enable($id) {
        return self::model()->updateById($id, array('isActive' => 1));
    }

    public function doesEmailBelongsToDealer($email) {

    }

    /**
     * @param $id
     * @param bool $newPassword
     * @param bool $newHash
     * @return bool
     */
    public function resetPassword($id, $newPassword = false, $newHash = false) {
        if (!$newPassword && !$newHash) {
            $newPass = self::generatePass();
            $pass = $newPass[0];
            $passHash = self::generateHash($pass, $newPass[1]);
        } else {
            $pass = $newPassword;
            $passHash = $newHash;
        }
        //
        $acc = $this->findById($id);
        if (!$acc) {
            return false;
        }
        //
        if (!$this->updateById($id, array('hash' => $passHash))) {
            return false;
        }
        $m = new PHPMailer();
        $m->AddAddress($acc['email']);
        $m->From = 'info@polbox.tv';
        $m->MsgHTML('Dear customer! Your password was reset to the following:<br><br>' . $pass . "<br><br>polbox.tv Team");
        if (!$m->Send()) {
            $this->setError('Failed to send email');
            //todo - вернуть старый пароль
            return false;
        }
        return true;
    }

    /**
     * Получает пароль и генерирует хэш с солью
     * @param string $pass
     * @param string|null $salt соль, если не указана, то генерится рэндомно
     * @return string
     */
    public static function generateHash($pass, $salt = null) {
        $chars = array_merge(range('A', 'Z'), range(0, 9));
        $salt = (!$salt) ? $chars[mt_rand(0, count($chars) - 1)] . $chars[mt_rand(0, count($chars) - 1)] : $salt;
        return md5(md5($pass) . md5($salt)) . ":" . $salt;
    }

    /**
     * Соответствует ли пароль хэшу
     * @param string $pass
     * @param string $hash
     * @return boolean
     */
    public static function isPassCorrect($pass, $hash) {
        $salt = end(explode(':', $hash));
        return ($hash == self::generateHash($pass, $salt) ? true : false);
    }

    /**
     *
     * @param boolean $runValidation выполнять валидацию или нет
     * @return boolean
     */
    public function save($runValidation = true) {
        $passReset = (isset($this->model['passReset'])) ? $this->model['passReset'] : 0; //после валидации этот элемент массива будет удален
        if ($runValidation && !$this->validate()) {
            return false;
        }
        $idField = $this->getId();


        if (isset($this->model[$idField])) {
            $enc = new Encrypter();
            foreach ($this->model as $k => $val) {
                if (in_array($k, self::$encryptedFields)) {
                    $this->model[$k] = $enc->encrypt($val);
                }
            }

            //update
            $this->beginTransaction();
            $custAddrModel = new CustomersAddressesModel;
            try {
                $billingAddress = (isset($this->model['billing']) ? $this->model['billing'] : array());
                $shippingAddress = (isset($this->model['shipping']) ? $this->model['shipping'] : array());
//                var_dump($this->model);

                $diff = array_diff($billingAddress, $shippingAddress);
                if (!empty($billingAddress) && isset($billingAddress['idAddress']) && !empty($billingAddress['idAddress'])) {

                    $sameAsShipping = (isset($shippingAddress) && !empty($diff)) ? 0 : 1;

                    $billingAddress['sameAsBillingAddress'] = $sameAsShipping;
                    $updBilling = $custAddrModel->updateByRaw("idAddress=? and idAccount=? and `type`=?", $billingAddress, array($billingAddress['idAddress'], $this->model['idAccount'], 1));
                    if (!$updBilling) {
                        throw new Exception('Failed to update billing address: ' . $custAddrModel->getLastError());
                    }
                } elseif ($billingAddress) {

                    $sameAsShipping = (isset($shippingAddress) && !empty($diff)) ? 0 : 1;
                    $billingAddress['sameAsBillingAddress'] = $sameAsShipping;
                    unset($billingAddress['idAddress']);
                    $billingAddress['idAccount'] = $this->model['idAccount'];
                    $billingAddress['type'] = 1;
                    $updBilling = $custAddrModel->insert($billingAddress);
                    if (!$updBilling) {
                        throw new Exception('Failed to set billing address: ' . $custAddrModel->getLastError());
                    }
                }
                if ($shippingAddress && isset($shippingAddress['idAddress']) && !empty($shippingAddress['idAddress'])) {

                    $sameAsShipping = (!empty($diff)) ? 0 : 1;
                    $shippingAddress['sameAsBillingAddress'] = $sameAsShipping;
                    $updShipping = $custAddrModel->updateByRaw("idAddress=? and idAccount=? and `type`=?", $shippingAddress, array($shippingAddress['idAddress'], $this->model['idAccount'], 2));
                    if (!$updShipping) {
                        throw new Exception('Failed to update shipping address: ' . $custAddrModel->getLastError());
                    }
                } elseif ($shippingAddress) {
                    $sameAsShipping = (!empty($diff)) ? 0 : 1;
                    $shippingAddress['sameAsBillingAddress'] = $sameAsShipping;
                    unset($shippingAddress['idAddress']);
                    $shippingAddress['idAccount'] = $this->model['idAccount'];
                    $shippingAddress['type'] = 2;
                    $updShipping = $custAddrModel->insert($shippingAddress);
                    if (!$updShipping) {
                        throw new Exception('Failed to set shipping address: ' . $custAddrModel->getLastError());
                    }
                } else {

                }
                $mainData = array();
                foreach ($this->model as $f => $var) {
                    if (is_array($var) && $f != 'bank') {
                        continue;
                    } elseif (is_array($var) && $f == 'bank') {
                        foreach ($var as $fb => $varb) {
                            $mainData[$fb] = $varb;
                        }
                        continue;
                    }
                    $mainData[$f] = $var;
                }
                //
                if ($passReset == 1 && (!isset($this->model['hash']) || empty($this->model['hash']))) {
                    //cгенерировать новый пароль и выслать на мыло юзеру
                    if (!$this->resetPassword($this->model['idAccount'])) {
                        throw new Exception($this->getErrorsAsString());
                    }
                } elseif (isset($this->model['hash']) && !empty($this->model['hash'])) {
                    //меняем пароль и отсылаем юзеру
                    $hash = $this->generateHash($this->model['hash']);
                    if (!$this->resetPassword($this->model['idAccount'], $this->model['hash'], $hash)) {
                        throw new Exception($this->getErrorsAsString());
                    }
                }
                unset($mainData['hash']);
                $mainData['dateUpdate'] = time();
                //
                $updMain = $this->updateById($this->model['idAccount'], $mainData);

                if (!$updMain) {
                    throw new Exception('Failed to update customer data: ' . $this->getLastError());
                }
//                var_dump($mainData);
                //
                //
                $this->commitTransaction();

                return true;
            } catch (Exception $ex) {
                $this->setError($ex->getMessage());
                $this->rollbackTransaction();
                return false;
            }
        } else {
//            return false;
            //вставка только в базовую таблицу
            $params = array();
            foreach ($this->getAppliedData() as $f => $v) {
                if (!in_array($f, $this->commonFields)) {
                    continue;
                }

                $params[$f] = $v;
            }
            return $this->insert($params);
            //insert
            //создание пользователя
        }
        //return true;
    }

    /**
     * @param bool $worldwide
     * @return boolean|array
     */
    public function getCountries($worldwide = false) {
        $cacheKey = md5(__METHOD__ . $worldwide . BaseController::store());
        $cache = Mcache::get($cacheKey);
        if (!$cache) {
            $storeQ = (!$worldwide) ? 'and idStore=?' : '';
            $storeHolder = (!$worldwide) ? array(StoreModel::getStoreId()) : array();
            $data = SDb::rows("Select idLocation,iso,name from customers_location where `level`=1 $storeQ order by name ASC", $storeHolder);
            if (!$data) {
                return array();
            }
            Mcache::set($cacheKey, $data);
        } else {
            $data = $cache;
        }
        return $data;
    }

    /**
     * @outputBuffering enabled
     * @param int $parent
     * @return array
     */
    public function getRegions($parent) {
        $cacheKey = md5(__METHOD__ . $parent . BaseController::store());
        $cache = Mcache::get($cacheKey);
        if (!$cache) {
            $data = SDb::rows("Select idLocation,iso,name from customers_location where `level`>1 and idParentLocation=? order by name ASC", array($parent));
            if ($data === false) {
                return array();
            }
            Mcache::set($cacheKey, $data);
        } else {
            $data = $cache;
        }
        return $data;
    }

    /**
     * @return boolean
     */
    public static function isGuest() {
        if (!isset($_SESSION['customer']['idAccount']) || !is_numeric($_SESSION['customer']['idAccount'])) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function getCustomerId() {
        if (!self::isGuest()) {
            return $_SESSION['customer']['idAccount'];
        } else {
            return false;
        }
    }

    //---     
    public static function getHash($pass, $salt2 = "") {
        $pass = $pass . self::SALT . $salt2 . urlencode($pass);
        $pass .= urlencode($pass) . $pass . self::SALT . $salt2;
        return md5(substr(md5($pass . self::SALT . $salt2), 0, 15) . $pass);
    }

    /**
     *
     * @param string $email
     * @return boolean
     */
    public static function authByEmail($email) {
        $data = self::model()->findOne('email=?', array('*'), array($email));
        $enc = new Encrypter();
        if (!$data) {
            return false;
        }
        foreach ($data as $k => $val) {
            if (in_array($k, self::$encryptedFields) && Encrypter::isEncrypted($val)) {
                $data[$k] = $enc->decrypt($val);
            }
        }
        //
        $_SESSION['customer'] = $data;
        $dt = new DateTime();
        setcookie('authtype', self::AUTH_TYPE_EMAIL, $dt->modify('+1 month')->format('U'), '/');
        return true;
    }

    /**
     *
     */
    public static function refresh() {

        $cust = $data = self::model()->findOne("idAccount = ?", array(), array($_SESSION["customer"]["idAccount"]));
        $enc = new Encrypter();
        if (!$data) {
            return false;
        }
        foreach ($data as $k => $val) {
            if (in_array($k, self::$encryptedFields) && Encrypter::isEncrypted($val)) {
                $data[$k] = $enc->decrypt($val);
            }
        }
        $_SESSION['customer'] = $data;
    }

    /**
     *
     * @return array
     */
    public function getUser() {
        if (!self::isGuest()) {
            return $_SESSION['customer'];
        } else {
            return array();
        }
    }

    /**
     *
     */
    public function logout() {
        unset($_SESSION['customer']);
        CartModel::flushSession();
        setcookie('authtype', null, time() - 1, '/');
    }

    /**
     * По id локации возвращает массив со страной и регионом
     * @param int $id
     * @return boolean|array
     */
    public function synthLocationId($id) {
        $cust = new CustomersLocationModel;
        $data = $cust->findOne("idLocation=?", array(), array($id));
        if (!$data) {
            $this->setError($cust->getLastError());
            return false;
        }
        if ($data['level'] == 2) {
            return array('country' => $data['idParentLocation'], 'region' => $id);
        } else {
            return array('country' => $id, 'region' => 0);
        }
    }

    /**
     *
     * @param array $locs
     * @return array
     */
    public function getLocationNames(array $locs) {
        foreach ($locs as $k => $lc) {
            if (!is_numeric($lc) || $lc <= 0) {
                unset($locs[$k]);
            }
        }
        $c = count($locs);
        $holders = array();
        for ($i = 0; $i < $c; $i++) {
            $holders[] = '?';
        }
        $cust = new CustomersLocationModel;
        $data = $cust->findAll('idLocation IN(' . implode(',', $holders) . ')', array('iso', 'name', 'idLocation'), $locs);
        if (!$data) {
            return array();
        }
        $names = array();
        foreach ($data as $row) {
            $name = $row['name'];

            $names[$row['idLocation']] = array('name' => $name, 'iso' => $row['iso']);
        }
        return $names;
    }

    /**
     *
     * @param boolean $runValidation
     * @return boolean Description
     */
    public function upsertAddresses($runValidation = true) {
        $block = array();
        if (isset($this->model['shipping']) && !empty($this->model['shipping'])) {
            $block[] = 'shipping';
        }
        if (isset($this->model['billing']) && !empty($this->model['billing'])) {
            $block[] = 'billing';
        }
        if ($runValidation) {
            $this->setBlockValidation(true)->defineBlocks4Validation($block);
            if (!$this->validate()) {
                return false;
            }
        }


        $ids = $this->getAddressIds();

        // if ($ids === false) {
        //    return false;  - зачем?  вставка не выполниться никогда так
        //}

        $this->beginTransaction();
        $addrModel = new CustomersAddressesModel;
        try {

            if (in_array('shipping', $block) && isset($ids[2])) {
                $this->model['shipping']['idAddress'] = $ids[2];

                $do = $addrModel->updateByRaw("type=2 and idAddress=" . $ids[2], $this->model['shipping']);
                if (!$do) {
                    throw new Exception($addrModel->getLastError());
                }
            } elseif (in_array('shipping', $block)) {
                $this->model['shipping']['type'] = 2;
                $this->model['shipping']['idAccount'] = $_SESSION['customer']['idAccount'];

                $do = $addrModel->insert($this->model['shipping']);
                if (!$do) {
                    throw new Exception($addrModel->getLastError());
                }
            }

            if (in_array('billing', $block) && isset($ids[1])) {
                $this->model['shipping']['idAddress'] = $ids[1];
                $do = $addrModel->updateByRaw("type=1 and idAddress=" . $ids[1], $this->model['billing']);
                if (!$do) {
                    throw new Exception($addrModel->getLastError());
                }
            } elseif (in_array('billing', $block)) {
                $this->model['billing']['type'] = 1;
                $this->model['billing']['idAccount'] = $_SESSION['customer']['idAccount'];
                $do = $addrModel->insert($this->model['billing']);
                if (!$do) {
                    throw new Exception($addrModel->getLastError());
                }
            }

            $this->commitTransaction();
            return true;
        } catch (Exception $ex) {
            $this->setError($ex->getMessage());
            $this->rollbackTransaction();
            return false;
        }
    }

    /**
     *
     * @param int $id
     * @return boolean|array
     */
    public function getAddressIds($id = null) {
        if (!$id) {
            $id = $_SESSION['customer']['idAccount'];
        }
        $addrModel = new CustomersAddressesModel;
        $data = $addrModel->findAll('idAccount=?', array('idAddress', 'type'), array($id));
        if ($data === false) {
            $this->setError($addrModel->getLastError());
            return false;
        }
        $ids = array();
        foreach ($data as $row) {
//            var_dump($row);
            $ids[$row['type']] = $row['idAddress'];
        }
        return $ids;
    }

    /**
     *
     * @param array $weight
     * @return boolean|array
     */
    public function getRatesGroupped(array $weight) {
        if (count($weight) != 2) {
            return false;
        }
        $q = "select group_concat(destCountryId) as cntris,price,id,destCountryCode,customers_location.idStore,conditionValue
from shipping_tablerate
left join
customers_location on customers_location.idLocation = shipping_tablerate.destCountryId
 where conditionValue>=0 and conditionValue<=?  and destRegionId=0
group by price";
        $data = SDb::rows($q, array($weight[1]));

        if (!$data) {
            return false;
        }
        $table = array();
        $locs = array();
        $stores = array();

        $inrange = array();
        foreach ($data as $r) {
            $countries = explode(',', $r['cntris']);
            if ($r['conditionValue'] >= $weight[0]) {
                $inrange[] = array_unique($countries);
            }
        }
        $store = StoreModel::get();

        foreach ($data as $index => $row) {
            $countries = explode(',', $row['cntris']);
            $locations = $this->getLocationNames(array_unique($countries));

            if (in_array(array_unique($countries), $inrange) && $row['conditionValue'] < $weight[0]) {
                unset($data[$index]);
                continue;
            }

            $rowCountries = array();
            foreach ($locations as $loc) {
                $rowCountries[] = $loc['name'];
            }
            if (in_array('0', $locations)) {
                $rowCountries[] = I18n::__('rest of the countries', $store['lang']);
            }
            $table[] = array('countries' => implode(', ', $rowCountries), 'price' => $row['price'], 'store' => $row['idStore']);
            if (!isset($locs[implode(', ', $rowCountries)])) {
                $locs[implode(', ', $rowCountries)] = array();
            }
            $locs[implode(', ', $rowCountries)][] = $row['price'];
            $stores[implode(', ', $rowCountries)] = $row['idStore'];
        }

        $upset = array();
        foreach ($table as $ind => $rw) {
            if (in_array($rw['countries'], array_keys($locs)) && count($locs[$rw['countries']]) > 1) {
                unset($table[$ind]);
                $upset[] = $rw['countries'];
            }
        }
        $upset = array_unique($upset);

        foreach ($upset as $cnt) {
            $from = $locs[$cnt][0];
            $to = end($locs[$cnt]);
            $table[] = array('countries' => $cnt, 'price' => array($from, $to), 'store' => $stores[$cnt]);
        }

        return $table;
    }

    /*
     * @param array $fbdata
     * @return boolean
     */

    public static function authByFb($fbdata) {
        $addrModel = new CustomersAddressesModel;
        $class = self::model('CustomersModel');
        $data = $class->find('fbid=?', array('*'), array($fbdata['fbid']));
        if ($data === false) {
            Debug::logMsg(print_r(self::model()->getErrors(), 1), "pdo/errors.log");
            self::model()->setError($class->getLastError());
            return false;
        } elseif (empty($data)) {
            //регистрируем по сути
            $p = self::generatePass();
            $fbdata['dateRegister'] = time();
            $fbdata['dateLogin'] = time();
            $fbdata['isActive'] = 1;
            $fbdata['hash'] = self::getHash($p['pass']); //использован не будет
            $fbdata['dpaName'] = $fbdata['firstname']." ".$fbdata['lastname'];
            $billing = array('firstname' => $fbdata['firstname'], 'lastname' => $fbdata['lastname']);
            unset($fbdata['firstname']);
            unset($fbdata['lastname']);
            self::model()->beginTransaction();
            try {
                $idAccount = self::model()->insert($fbdata, true);
                if (!$idAccount) {
                    throw new Exception(self::model()->getLastError());
                }
                $billing['idAccount'] = $idAccount;
                $billing['type'] = 1;

                $insAddress = $addrModel->insert($billing);

                if (!$insAddress) {
                    throw new Exception($addrModel->getLastError());
                }
                self::model()->commitTransaction();
            } catch (Exception $ex) {
                Debug::logMsg(__METHOD__ . __LINE__, "fb.loc");
                self::model()->setError($ex->getMessage());
                Debug::logException($ex, 3, "customers");
                self::model()->rollbackTransaction();
                return false;
            }
            $data = self::model()->findById($idAccount);
        } elseif ($data['isActive'] != 1) {
            Debug::logMsg(__METHOD__ . __LINE__, "fb.loc");
            return false;
        }
        //
        $_SESSION['customer'] = $data;
        $dt = new DateTime();
        setcookie('authtype', self::AUTH_TYPE_FB, $dt->modify('+1 month')->format('U'), '/');
        return true;
    }

    /**
     *
     * @param array $gplusData
     * @return boolean
     */
    public static function authByGoogle($gplusData) {
        $addrModel = new CustomersAddressesModel;
        $data = self::model()->findOne('gplusid=?', array('*'), array($gplusData['gplusid']));
        if ($data === false) {
            self::model()->setError(self::model()->getLastError());
            return false;
        } elseif (empty($data)) {
            //регистрируем по сути
            $p = self::generatePass();
            $gplusData['dateRegister'] = time();
            $gplusData['dateLogin'] = time();
            $gplusData['isActive'] = 1;
            $gplusData['idGroup'] = 2;
            $gplusData['hash'] = self::getHash($p['pass']); //использован не будет
            $gplusData['dpaName'] = $gplusData['firstname']." ".$gplusData['lastname'];
            $billing = array('firstname' => $gplusData['firstname'], 'lastname' => $gplusData['lastname']);
            unset($gplusData['firstname']);
            unset($gplusData['lastname']);
            self::model()->beginTransaction();
            try {
                $idAccount = self::model()->insert($gplusData, true);
                if (!$idAccount) {
                    throw new Exception(self::model()->getLastError());
                }
                $billing['idAccount'] = $idAccount;
                $billing['type'] = 1;

                $insAddress = $addrModel->insert($billing);

                if (!$insAddress) {
                    throw new Exception($addrModel->getLastError());
                }
                self::model()->commitTransaction();
            } catch (Exception $ex) {
                self::model()->setError($ex->getMessage());
                self::model()->rollbackTransaction();
                return false;
            }
            $data = self::model()->findById($idAccount);
        } elseif ($data['isActive'] != 1) {
            return false;
        }

        //
        $_SESSION['customer'] = $data;
        $dt = new DateTime();
        setcookie('authtype', self::AUTH_TYPE_GOOGLE, $dt->modify('+1 month')->format('U'), '/');
        return true;
    }

}
