<?php

/**
 * Description of PagesModel
 *
 */
class ManualGroupsModel extends BaseModel {
    /** @var array - список категорий устройств со списком устройств для каждой */
    private static $devices = array(
        'computer' => array(
            'name' => 'Computer',
            'groups' => array(
                'windows' => array(
                    'name' => 'Windows',
                    'values' => array()
                ),
                'macos' => array(
                    'name' => 'Mac OS',
                    'values' => array()
                ),
                'linux' => array(
                    'name' => 'Linux',
                    'values' => array()
                )
            )
        ),

        'tv' => array(
            'name' => 'TV',
            'groups' => array(
                'smart-tv' => array(
                    'name' => 'Smart TV',
                    'values' => array()
                ),
                'console' => array(
                    'name' => 'Console',
                    'values' => array()
                ),
                'game-console' => array(
                    'name' => 'Game console',
                    'values' => array()
                )
            )
        ),

        'tablet' => array(
            'name' => 'Tablet',
            'groups' => array(
                'android' => array(
                    'name' => 'Android',
                    'values' => array()
                ),
                'iphone' => array(
                    'name' => 'iPhone\iPad',
                    'values' => array()
                )
            )
        )
    );
    
    public static function model($class = __CLASS__){
        return parent::model($class);
    }

    public function getTableName() {
        return "cms_manuals";
    }

    public static function getList()
    {
        $result = array();

        foreach (self::$devices as $device_group) {
            foreach ($device_group['groups'] as $key => $device) {
                $result[] = array('title' => $device['name'], 'data' => $key, 'equal' => $key);
            }
        }

        return $result;
    }

    public function getId() {
        return "id";
    }

    public static function formatData()
    {
        $data = self::model()->findAll();

        $result = self::$devices;

        // Список устройств без связей с группами
        $device_list = array();
        foreach ($result as $device_group) {
            foreach ($device_group['groups'] as $key => $device) {
                $device_list[$key] = array();
            }
        }

        // Обрабатываем данные из БД, разкидываем по нужным группам
        foreach ($data as $element) {
            $groupKey = $element['groupKey'];
            if (isset($device_list[$groupKey])) {
                // Переводим текст
                $element['softwareText'] = I18n::__('%'.$element['softwareText'].'%');
                $element['device'] = I18n::__('%'.$element['device'].'%');
                array_push($device_list[$groupKey], $element);
            }
        }

        // Вновь соединяем устройства с их группами
        foreach ($result as $dg_key => $device_group) {
            // Перевод названия группы
            $result[$dg_key]['name'] = I18n::__('%'.$result[$dg_key]['name'].'%');

            foreach ($device_group['groups'] as $key => $device) {
                // Переводим текст
                $result[$dg_key]['groups'][$key]['name'] = I18n::__('%'.$result[$dg_key]['groups'][$key]['name'].'%');
                $result[$dg_key]['groups'][$key]['values'] = $device_list[$key];
            }
        }

        return $result;
    }
}
