<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustGroupModel
 *
 * @author kubrey
 */
class CustGroupModel extends BaseModel{
    
    
     /**
     * ID в таблицу
     * @return string
     */
    public function getId() {
        return 'idAccount';
    }

    public function getAttributes() {
        return array(
        );
    }

    /**
     * @param string $className
     * @return mixed
     */
    public static function model($className = __CLASS__) {
        if (!parent::$instance) {
            parent::$instance = new self();
        }
        return parent::$instance;
    }

    /**
     * 
     * @return string
     */
    public function getTableName() {
        return 'customers_groups';
    }

    public function getClass() {
        return __CLASS__;
    }
}
