<?php

/**
 * Description of CpServices
 *
 * @author kubrey
 */
class CpServices extends BaseModel
{

    protected static $period = array("2min", "5min", "30min", "1hour", "24hour");

    public function getTableName() {
        return "system_services";
    }

    public function getId() {
        return "id";
    }

    public static function GetServicesList() {
        $output = array();

        $modules = self::getModulesList();

        foreach ($modules as $mod) {
            $dir = PATH_REAL . DS . "mod" . DS . $mod["module"] . DS . "services";
            if (!is_dir($dir)) {
                continue;
            }
            $files = scandir($dir);
            array_shift($files);
            array_shift($files);
            foreach ($files as $f) {
                if (strpos($f, "Command.php") === false) {
                    continue;
                }
                $output[] = array(
                    "path" => "mod" . DS . $mod["module"] . DS . "services" . DS . $f,
                    "file" => $f,
                    "class" => str_replace(".php", "", $f),
                    "module" => $mod['module']
                );
            }
        }

        return $output;
    }

    public static function getModulesList() {
        $output = array();
        $files = scandir(PATH_REAL . DS . "mod");

        array_shift($files);
        array_shift($files);

        foreach ($files as $f) {
            if (!is_dir(PATH_REAL . DS . "mod" . DS . $f)) {
                continue;
            }
            $output[] = array("module" => $f);
        }
        return $output;
    }

    public static function getPeriodsList() {
        foreach (self::$period as $f) {
            $output[] = array('period' => $f);
        }
        return $output;
    }

}
