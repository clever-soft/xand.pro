<?php

class NewsModel extends I18nModel
{
    public static function model($class = __CLASS__){
        return parent::model($class);
    }

    public function getTableName() {
        return "cms_news";
    }

    public function getTableName_i18n() {
        return "cms_news_i18n";
    }

    public function getAttributes() {
        return array(
            'idNews',
            'idCategory',
            'idPhoto',
            'isActive',
            'url',
            'datePublic',
            'dateCreate',
            'dateUpdate'
        );
    }

    public function get_translated_fields()
    {
        return array(
            'title', 'content', 'contentShort', 'description', 'keywords'
        );
    }

    public function getId() {
        return "idNews";
    }
}