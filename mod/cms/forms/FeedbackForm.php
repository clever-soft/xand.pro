<?php

/**
 * Class FeedbackForm
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FeedbackForm extends FFAjaxJson
{

    /**
     * FeedbackForm constructor.
     */
    public function __construct()
    {

        $this->class = "ff-vertical";
        $this->title = "";
        $this->action = PATH_DS . SITE_CURRENT_LANG . DS . "cms/contact/send";
        $this->id = md5($this->action);

        $this->Add(
            new FFInput("Name", "name", array("required" => "Please, enter your name")),
            new FFInputEmail("Email", "email", array("required" => "Please, enter your e-mail")),
            new FFTextarea("Message", "phone")
        );

        $this->Add(
            new FFBoxRequired(),
            new FFButtonSubmit("Send")
        );

        //$this->Fill();

    }


}