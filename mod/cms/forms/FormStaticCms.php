<?php

/**
 * Class FormStaticCms
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FormStaticCms extends FormStatic
{
    /**
     * @param WidgetBase $widget
     * @param bool $action
     * @param bool $idPage
     *
     * @return $this
     */
    public function Prepare(WidgetBase $widget, $action = false, $idPage = false)
    {

        $ARR = array();
        $this->assignParent($widget)->cloneAjax()->CloneNav();

        $action = ($action) ? $action : $widget->getPostAction();
        $idPage = ($idPage) ? $idPage : $widget->getPostDataInt();

        if ($action == 'UpdateReady') {

            $ARR = $widget->model->findById($idPage);
            $ARR_I18N = SDb::select($widget->model->getTableName_i18n(), "idPage = {$idPage}");

            foreach ($ARR_I18N as $val)
                foreach ($val as $field => $fieldVal) {
                    if (in_array($field, array("id", "idPage", "lang"))) continue;
                    $ARR["translate_{$val["lang"]}_{$field}"] = $fieldVal;
                }

            $url_array = $widget->parseItemUrl($ARR['url']);
            $ARR['url'] = $url_array['url'];
            $ARR['parentUrl'] = $url_array['parentUrl'] . '/';
            $this->dataArray = array($ARR);
            $this->name = 'Edit page: ' . $ARR["title"];

            $this->Add(new InputVar("idPage", "idPage"));
            $this->Add(new ButtonSave());
            $this->Add(new ButtonSaveAndContinue());

        } else {

            $this->name = 'Add new text page';
            $arr_parent = $widget->GetParentObject();
            $ARR['parentUrl'] = (isset($arr_parent['url']) ? $arr_parent['url'] : '') . '/';

            $this->Add(new ButtonAdd());
        }


        $gr = new GrouperTab("Main", NULL, array("height" => 0));

        if ($action == 'UpdateReady') {
            $gr->Add(new ControlSelect("Template", "tpl", array("noChoose" => false), NULL, AdminController::GetTemplatesList($ARR['tplFolder'])));
        } else {
            $gr->Add(new InputTextReadonly("Template folder", "tplFolder", NULL, "cms"));
            $gr->Add(new InputText("Template", "tpl"));
        }

        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputTextReadonly("Parent URL", "parentUrl", array("placeholder" => "parent url", "inputWidth" => 250)));
        $gr->Add(new InputText("URL", "url", array("placeholder" => "url")));
        $gr->Add(new InputText("CSS class", "menuClass"));
        $gr->Add(new InputText("Icon class", "menuIcon"));
        $gr->Add(new InputText("Javascript", "javaScript"));

        $gr->Add(new InputCheckbox("Page in page (hash anchor)", "hashTag"));
        $gr->Add(new InputCheckbox("Include in Menu", "isMenu"));
        $gr->Add(new InputCheckbox("Include in Footer", "isFooter"));
        $gr->Add(new InputCheckbox("Include in Footer Extra", "isFooterExtra"));

        $gr->Add(new InputCheckbox("Active", "isActive"));

        $this->Add($gr);

        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, array("height" => 0));
        $gr->Add(new InputText("H1", "h1", array("placeholder" => "h1")));
        $gr->Add(new InputText("H2", "h2", array("placeholder" => "h2")));
        $gr->Add(new InputText("Title", "title", array("placeholder" => "page title")));
        $gr->Add(new InputText("Menu title", "menuTitle", array("placeholder" => "Menu title")));
        $gr->Add(new InputText("Menu description", "menuDescription", array("placeholder" => "Menu description")));
        $gr->Add(new InputTextArea("Keywords", "keywords", array("placeholder" => "keywords")));
        $gr->Add(new InputTextArea("Description", "description", array("placeholder" => "description")));
        $gr->Add(new InputTextAreaEditor("Content", "content", array("init" => false, "height" => 400)));
        $this->Add($gr);

        foreach (explode(",", SITE_LANGS) as $l) {
            if ($l != SITE_DEFAULT_LANG) {
                $gr = new GrouperTab("Translate: {$l}", NULL, array("height" => 0));
                $gr->Add(new InputText("H1", "translate_{$l}_h1", array("placeholder" => "h1")));
                $gr->Add(new InputText("H2", "translate_{$l}_h2", array("placeholder" => "h2")));
                $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "page title")));
                $gr->Add(new InputText("Menu title", "translate_{$l}_menuTitle", array("placeholder" => "Menu title")));
                $gr->Add(new InputText("Menu description", "translate_{$l}_menuDescription", array("placeholder" => "Menu description")));
                $gr->Add(new InputTextArea("Keywords", "translate_{$l}_keywords", array("placeholder" => "keywords")));
                $gr->Add(new InputTextArea("Description", "translate_{$l}_description", array("placeholder" => "description")));
                $gr->Add(new InputTextAreaEditor("Content", "translate_{$l}_content", array("init" => false, "height" => 400)));
                $this->Add($gr);
            }
        }


        $this->Add(new ButtonCancel());

        return $this;

    }


}

