<?php

/**
 * Class FormStaticSlider
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FormStaticSlider extends FormStatic
{
    /**
     * @param WidgetBase $widget
     * @param bool $action
     * @param bool $idSlide
     *
     * @return $this
     */
    public function Prepare(WidgetBase $widget, $action = false, $idSlide = false) {

        $arr = array();
        $this->assignParent($widget)->CloneAjax()->CloneNav();

        $action = ($action) ? $action : $widget->getPostAction();
        $idSlide = ($idSlide) ? $idSlide : $widget->getPostDataInt();
        $imageData = "";

        $model = $widget->model;
        /**
         * @var SliderModel $model
         */
        if ($action == 'UpdateReady') {

            $arr = $model->findById($idSlide);
            $arrI18n = SDb::rows("select * from " . $model->getTableName_i18n() . " where idSlide=?", [$idSlide]);

            foreach ($arrI18n as $val) {
                foreach ($val as $field => $fieldVal) {
                    if (in_array($field, array("id", "idSlide", "lang"))) {
                        continue;
                    }
                    $arr["translate_{$val["lang"]}_{$field}"] = $fieldVal;

                }
            }


            $this->dataArray = array($arr);
            $this->name = 'Edit slide: #' . $arr["idSlide"];

            $this->Add(new InputVar("idSlide", "idSlide"));
            $this->Add(new ButtonSave());

            $imageData = ($arr["imagePath"]) ? '<img src="' . PATH_DS . 'uploads/slider' . DS . $arr["imagePath"] . '_50_30.jpg">' : "";

        } else {

            $this->name = 'Add new slide';
            $this->Add(new ButtonAdd());
        }


        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, array("height" => 0));
        $this->Add($gr);

        $gr->Add(new InputCheckbox("Test mode", "testMode"));
        $gr->Add(new InputCheckbox("Is Active", "isActive"));
        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputText("Url", "url"));
        $gr->Add(new InputUpload("Image path", "imagePath", array(
            "file" => PATH_DS . "admin/uploader/slider",
            "requirements" => "JPG/PNG, 1200x400 px.",
            "callback_data" => $imageData
        )));

        $grCC = new GrouperCheckboxRow("Enable timer", "timerActive", array(
            "titleWidth" => 160,
            "columnsWidth" => array(300, 300),
            "active" => (isset($this->dataArray[0]["timerActive"]) && $this->dataArray[0]["timerActive"])
        ));

        $grCC->Add(new InputDate("Start date the timer", "timerStart", array('requirements' => '')));
        $grCC->Add(new InputDate("Finish date the timer", "timerEnd", array('requirements' => '')));
        $gr->Add($grCC);


        $gr->parent->dataarray = $this->dataArray;


        foreach (explode(",", SITE_LANGS) as $l) {

            $gr = new GrouperTab("Content: {$l}", NULL, array("height" => 0));
            $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "Item title")));
            $gr->Add(new InputTextArea("Content", "translate_{$l}_content"));
            $gr->Add(new InputText("Image alt", "translate_{$l}_imageAlt"));
            $this->Add($gr);

        }


        $this->Add(new ButtonCancel());

        return $this;

    }


}

