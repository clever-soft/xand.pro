<?php

class RequestController extends BaseController
{
    /** @var PagesModel */
    protected $pagesM;
    protected $path;
    protected $page;


    public function __construct()
    {
        parent::__construct();
        $this->pagesM = new PagesModel();
        $this->path = (isset(Xand::$nav->path[0])) ? implode(DS, Xand::$nav->path) : "home";
    }

    //---
    public function defineAction($action = "")
    {
        return 'default';
    }

    /**
     * Получаение инфы, общей для всех страниц
     * @return boolean
     */
    private function setCommonData()
    {
        $this->page = $this->pagesM->findOneWithTranslate(array(
            'where' => array(
                'row' => 'url = ?',
                'params' => array($this->path)
            )
        ));
        if (!$this->page) {
            return false;
        }

        $this->getTpl($this->page["tplFolder"] . DS . $this->page["tpl"], NULL, SITE_CURRENT_LANG);
        $this->renderLayout($this->path);

        // --- Стандартные блоки данных
        $crit = trim(str_replace("/", ",", $this->page["path"]), ",");
        $critArr = explode(',', $crit);
        $critRow = str_repeat('?,', count($critArr));
        $critRow = trim($critRow, ',');

        $crumbs = $this->pagesM->findAllWithTranslate(array(
            'where' => array(
                'row' => $this->pagesM->getTableName() . '.' . 'idPage in (' . $critRow . ')',
                'params' => $critArr
            ),
            'fields' => array('url', 'menuTitle'),
            'order' => array("level" => "ASC")
        ));


        $menuName = (count($crumbs) > 1) ? $crumbs[count($crumbs) - 2]["menuTitle"] : "%Menu%";

        $this->tpl->assign("breadcrumbs", $crumbs);
        $this->tpl->assign("menuName", $menuName);

        $parentL1 = array_shift($critArr);
        #$url = $this->pagesM->findField("idPage=?", 'url', array($parentL1));
        $url = $this->pagesM->findOneWithTranslate(array(
            'where' => array(
                'row' => $this->pagesM->getTableName() . '.' . 'idPage = ?',
                'params' => array($parentL1)
            ),
            'fields' => array('url')
        ));
        $url = $url['url'];

        if ($url) {
            $this->tpl->data["menuActive"] = $url;
        } 

        return true;
    }


    /**
     *
     */
    public function actionDefault()
    {

        if (!$this->setCommonData()) {
            $this->httpError($this->path, 404, I18n::__("%Page not found%"), I18n::__("%Error%"));
        } else {

            $this->tpl->assignForm("main", new RequestForm);
            $this->render();
        }
    }


    //---
    public function actionSend()
    {

        $form = new RequestForm();
        $test = $form->Validate($this->post);

        if (!$test)
            MessageJson::I()->Errors($form->errors)->Json();

        if ($_SESSION["frontedCaptcha"] != $form->validated["captcha"])
            MessageJson::I()->Error("Security code is wrong!")->Json();

        $_SESSION['frontedCaptcha'] = rand(100000, 999999) . '';

        unset($form->validated["captcha"]);
  
        $html = "";
        foreach ($form->validated as $k => $v) {
            if ($v)
                $html .= "<p style='margin: 2px 0;'><b>" . $form->objects[$k]->title . ":</b><br />{$v}</p><br />";
        }

        $tpl = new MailTemplate("{{request}}");
        $tpl->assign("{{body}}", $html);
        $tpl->Send(ADMIN_EMAIL);

        MessageJson::I()->Success("Message was sent")->Refresh(2000)->Json();

    }


}