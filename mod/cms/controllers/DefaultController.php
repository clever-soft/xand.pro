<?php

/**
 * Class DefaultController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class DefaultController extends BaseControllerCms
{

    /**
     * @var PagesModel
     */

    protected $path;


    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->path = (isset(Xand::$nav->path[0])) ? implode(DS, Xand::$nav->path) : "home";
    }

    /**
     *
     */
    public function actionDefault()
    {

        if (count(Xand::$nav->path) == 2 && Xand::$nav->path[0] == "news") {
            $this->path = "news/*";
        }

        if (!$this->setCommonData()) {
            $this->httpError($this->path, 404, I18n::__("%Page not found%"), I18n::__("%Error%"));
        } else {



            $this->tpl->assignForm("main", new FeedbackForm);
            $this->render();

            $this->tpl->Load();

        }
    }

    /**
     * @return bool
     */
    private function setCommonData()
    {
        $this->page = $this->pagesM->findOneWithTranslate(array(
            'where' => array(
                'row' => 'url = ?',
                'params' => array($this->path)
            )
        ));
        if (!$this->page) {
            return false;
        }

        $this->getTpl($this->page["tplFolder"] . DS . $this->page["tpl"]);
        $this->renderLayout($this->path);
        $this->SetBreadcrumbs();

        $this->tpl->data["menuActive"] = $this->path;

        return true;
    }

    /**
     * @param string $action
     *
     * @return bool|mixed
     */
    protected function shiftAction($action)
    {
        $shift = array(
            'default' => $this->path,
            'index' => $this->path
        );
        return (isset($shift[$action]) ? $shift[$action] : false);
    }
}
