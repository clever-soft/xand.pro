<?php

/**
 * Class DefaultController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class SendController extends BaseControllerCms
{


    /**
     *
     */
    public function actionDefault()
    {
        $data = $_POST;
        unset($data['submit']);
        foreach ($data as $d) {
            if ($d == "") {
                echo("<h3 style='color: red; align-content: center;'>* Заполните все поля!</h3>");
                exit();
            }
            if (strlen($d) < 3) {
                echo("<h3 style='color: red; align-content: center;'>* Слишком короткое имя или сообщение!</h3>");
                exit();
            }
        }
        $qty = SDb::rows("SELECT * FROM mailing_queue WHERE ip=? AND dateCreate>?", [$_SERVER["REMOTE_ADDR"], strtotime("-1 minutes")]);
        if (count($qty) > 2) {
            echo("<h3 style='color: red; align-content: center;'>* Слишком много сообщений!</h3>");
            exit();
        }
        if (!filter_var($data["contactEmail"], FILTER_VALIDATE_EMAIL)) {
            echo("<h3 style='color: red; align-content: center;'>* Неправильный email!</h3>");
            exit();
        }
        $str = "From: - " . $data["contactName"] . "<br/> Email: " . $data["contactEmail"] . "<br/> Subject: " . $data["contactSubject"] . "<br/> Message: " . $data["contactMessage"];
        SDbQuery::table("mailing_queue")->insert([
            "idTpl" => "",
            "data" => $str,
            "email" => ADMIN_EMAIL,
            "ip" => $_SERVER["REMOTE_ADDR"],
            "dateCreate" => time()
        ]);
        echo('
        <h1>Thank you for contacting us</h1>
        <div class="content__text">
            <p><img src="data:image/webp;base64,UklGRtgEAABXRUJQVlA4IMwEAACQGACdASpkAGUAPm02lEgkIyGhorgMQIANiWkcIgE3AfP8aVpdoB7CfhWnr++8F9Ivjg9ZH/K8mf1WP/WbaIJfvwP+DAAe71gDtqwCgErqalPK215cEe/+1nPqK1lgEkXNaCe2kidJpK7WGzQFSvRGOjCuRczm1LbyXgFKjrc0fQP6BQHwCcWhEiklLM/RIJoczajNYLSM1EieMoWI4UL8fp+/kSeKSFbVRC6pya/eaKAkVJZE/hK5tN2Oa47MXNX1mhm9PkC7kR8cF9gAAP7+7AU0sAqDwq43r8o+hLvuZ/0sfO30GAt1Dh2y9l3fJglf8pAVgqTxPbAtH8e1LwgwS77+SKc6dCwFF5yP2Jn3R7UIX3Cdy0qNfsGF6WL6762oRYydWOv1TohLsN7OMV6SPRny530lDj4EQs5X1RtCkuP902WYKsKtIH0D9Bs6LdW9x/JXAd1vPiv+00atFxPFqedxepDuFSXgIXwLWrZ3zloRr36E2xDH21TrKEgRmU/7TQ+39RngTHsF0Nmr+F8gNZOSdE8S0UDlryeUoxDqU80zBdxG7mM0zQDimqHZKSLCNpra/acVF+bV4JARkGPruyrC9uIsmrCqngVett0ZetL9y/Gi58sFfiTFZ2mSc1wtn1B2cUkH5CPa00SeHKccwR5DwXnh/rxMJG75k2IHDYMb8QmgIBjVl5sACjKa/Dfm3cyfKeqxtCjFwh3N4vKL82g2PXzO9Y9YRe0eg4cwK6KBspH+L9AShQKKDNu5O8Tn38OHwB/KKAUYagTnG/aGxsUSACsDfo24CakjJxDSEND7j932fLqI59VhlJixLjRa5rQqA3qSVBPfuONXzG9/0J6blT3LfGSjnlXlopsR5y/PLun6odalZso5wlLb3B/2t2ralOUfGhlWVjkUfM73bRDYF7CcbC9XzPnrZnRiUwIwMnyb9H//aPcjNzulW9Ho06k83pNqvOz6IxnAm3+DFeIZ7y4JytSCI7czh26aSOzAWK1HrKjqUA/rHqiNN8SpoX8CWr4txrk8X9/PsVdKX81EqWLFpQdoGP+11vwuzLe4w3kJTRx01B8pYF5o6+1f9LvDo29i6YzBZgNkZPVLHfLF1AkkUKzdyUoJmy6ZEGkmmXQ4IbLlkZBwc0G9j0jV7G2V/w7UGKSs6gRO6CgKiBj+YtKzWkaMmsT22Aqd+JGAOzl9PDQssJvlOUxNxWIfeOtEZyZvTKvrOtimOpk178NMg22zGgDmK/A4BM7eWE1r+Os0akv8B2lyaJuYv1eMvnx0GbKFho7yvg09dJoYP4/znolVqlf5VHSscGiu4SaS+/aYonfe9m4HSFMIWnvPjQ89UsZ8Gu4wfXqu/7zPsEH17xoEG39P/THPkCdL+qvK91v5gyXoNgyWK7AHKjxacxzC/wjur3zE2+dfCYq1wSKZtHnF+x/hiN0OXx9O2sQYM5i84Y+zr20c97ZYQsNFJzdXw8A2Lie8+y98bdY968O42ayErBbTU5kINb3mVWrKEFic6Jk6mboWouIWR0Hr+JW8TAppCsQHi3noxoBaMEh7kuOHGatdoVLniiWSrH1T2c0JRLQBvUj7AH32AZNMQJ1drPJocByD8mlIaitLog2v6A8IAlCAAAAA" style="width: 100px; height: 101px; float: left; margin: 20px;" data-pagespeed-url-hash="618129857" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></p>
            <p>Our specialists will process your request and contact you as soon as possible. While you are waiting feel free to navigate through our website and learn more about us and our services.</p>
        </div></br></br>
      ');
    }


}
