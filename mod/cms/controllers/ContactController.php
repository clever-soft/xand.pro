<?php

/**
 * Class ContactController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ContactController extends BaseControllerCms
{

    private $_table = "cms_feedback";

    /**
     * FeedbackController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->path = (isset(Xand::$nav->path[0])) ? implode(DS, Xand::$nav->path) : "home";
    }

    /**
     *
     */
    public function actionDefault()
    {
        $this->httpError('/', 404);
    }


    /**
     *
     */
    public function actionSend()
    {


        //MessageJson::I()->Error("Security code is wrong!", "customerName")->Json();




        $form = new FeedbackForm;
        $test = $form->Validate($this->post);

        if (!$test)
            MessageJson::I()->Errors($form->errors)->Json();




        $form->validated["ip"] = $_SERVER['REMOTE_ADDR'];
        $form->validated["antispam"] = $form->validated["ip"] . date("Ymd");
        $form->validated["ua"] = Statistic::DetectAgent($_SERVER['HTTP_USER_AGENT']);
        $form->validated["dateCreate"] = time();


        if (SDbQuery::table($this->_table)
                ->where([
                    "antispam" => $form->validated['antispam']
                ])->count() > 10
        ) {
            exit(MessageJson::I()->Error("Too many messages per day")->Json());
        }


        $status = SDbQuery::table($this->_table)->insert($form->validated);

        if ($status) {

            $tpl = new MailTemplate("FEEDBACK");
            $tpl->assign("{{name}}", $form->validated["name"]);
            $tpl->assign("{{email}}", $form->validated["email"]);
            $tpl->assign("{{phone}}", $form->validated["phone"]);
            $tpl->Send(ADMIN_EMAIL);

            MessageJson::I()->Success("Message was sent", "reset")->Json();
        } else {
            MessageJson::I()->Error("Error")->Json();
        }


    }


}