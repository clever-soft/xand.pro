<?php

/**
 * Description of SiteMap
 *
 */
class Sitemap
{

    public $map = array();
    private function __construct(){}
    private function __clone(){}
    protected static $_instance;

    //---
    public static function I()
    {

        if (null === self::$_instance)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    //---
    public function Get()
    {
        $map  = array();
        $npps = array();

        //$data = Db::SelectFields("cms_textpages", "url, title, npp, level, idPage, path");
        $data = SDb::selectFields("cms_textpages", "url, title, npp, level, idPage, path");

        foreach($data as $d)
        {
            $npps[$d["idPage"]] = $d["npp"];
        }


        foreach($data as $d)
        {
            $pathEx = explode("/", $d["path"]);
            foreach($pathEx as &$p)
            {
                if($p == "") continue;
                $p = $npps[$p];
            }
            $d["path"] = implode("/", $pathEx);
            $pl        = $this->PathLongCreater($d["path"]);

            $map[$pl] = array($d["url"], $d["title"], $d["level"]);

            if($d["url"] == "shop/categories")
            {
                $map = array_merge($map, $this->GetShopMap($d));
            }

        }
       ksort($map);

       return $map;

    }

    //---
    private function GetShopMap($parent)
    {

        $output   = array();
        $cats     = SDb::selectFields("shop_groups", "url, name AS title, npp, idGroup");

        foreach($cats as $c)
        {

            $nppPath          = $parent["path"].$c["npp"];
            $nppPath          = $this->PathLongCreater($nppPath);
            $c["url"]         = PATH_DS.SITE_CURRENT_LANG.DS."shop".DS.$c["url"];
            $output[$nppPath] = array($c["url"], $c["title"], 3);

            $products = SDb::selectFields("shop_products", "url, name AS title", "idGroup = ?", [$c["idGroup"]]);

            foreach($products as $k => $p)
            {
                $nppPath          = $nppPath.$k;
                $nppPath          = $this->PathLongCreater($nppPath);
                $output[$nppPath] = array($c["url"] .DS.$p["url"], $p["title"], 4);
            }
        }

        return $output;

    }

    //---
    private function PathLongCreater($path)
    {
        $temp_str = "/";
        $value_arr = explode("/", $path);
        foreach($value_arr as &$valarr)
            if($valarr != "")
                $temp_str .= $this->Discharge($valarr)."/";
        return $temp_str;
    }

    //---
    private function Discharge($val)
    {
        if($val == "") return "00000";
        switch(strlen($val))
        {
            case 1: return "0000".$val;
            case 2: return "000".$val;
            case 3: return "00".$val;
            case 4: return "0".$val;
            case 5: return $val;
        }
    }




}
