<?php

/**
 * Description of CustomersModel
 *
 * @author kubrey
 */
class CustomersModel extends CustomersCoreModel {

    protected $registeredId;

    public function register($step = 1) {
        $post = $this->getAppliedData();
        switch ($step) {
            //проверяет, была ли попытка зарегиться с этим мылом\через secretKey
            //если да и времени прошло менее получаса - ошибка и сообщение, что подождите
            //если больше получаса - удаляем временную запись и создаем новую, чтобы по новой отослать письмо
            //если временной записи нет - создаем ее, проверяя мыло и т.д.
            case 1:
                $post['hash'] = CustomersModel::getHash($post["pass"]);
                $this->injectRule('email', array($this, 'isEmailUnique'));
                if (!$this->validateOnlyExisted(true)->validate()) {
                    return false;
                }
                $tmp = SDb::rows("select * from " . $this->getTableName() . "_tmp where secretKey=?", array($post['secret']));
                $this->beginTransaction();
                if ($tmp) {
                    $delta = time() - $tmp[0]["dateCreate"];
                    if ($delta < 1800) {
                        $delta = abs(ceil(($delta - 1800) / 60));
                        $this->setError(I18n::__("Letter has been sent, try again after {$delta} minutes"));
                    } else {
                        if (!$this->actionSafe("delete from " . $this->getTableName() . "_tmp where secretKey=?", array($post['secret']))) {
                            $this->setError('Error ocurried');
                            $this->rollbackTransaction();
                            return false;
                        }
                    }
                }
                if (!$this->actionSafe("insert into " . $this->getTableName() . "_tmp (secretKey,email,hash,dateCreate) values (?,?,?,?)", array($post['secret'], $post['email'], $post['hash'], time()))) {
                    $this->setError('Error ocurried');
                    $this->rollbackTransaction();
                    return false;
                }
                $this->commitTransaction();
                break;
            //вызываетя когда совершен переход по ссылки из мыла с регистрацией
            //если хэш верен - создаем юзера и удаляем временную запись
            case 2:
                $data = SDb::rows("select * from " . $this->getTableName() . "_tmp where secretKey=?", array($post['secret']));
                if (!$data) {
                    $this->setError('Error ocurried. Url is invalid');
                    return false;
                }
                $this->beginTransaction();
                if (!$this->actionSafe("delete from " . $this->getTableName() . "_tmp where secretKey=?", array($post['secret']))) {
                    $this->rollbackTransaction();
                    $this->setError('Error ocurried');
                    return false;
                }
                $cust['dateRegister'] = time();
                $cust['email'] = $data[0]['email'];
                $cust['hash'] = $data[0]['hash'];
                $cust['isActive'] = 1;
                $userId = $this->insert($cust, true);
                if (!$userId) {
                    $this->rollbackTransaction();
                    $this->setError('Error ocurried');
                    return false;
                }
                $this->registeredId = $userId;
                $this->commitTransaction();
                break;
        }
        return true;
    }

    public static function isEmailUnique($input) {
        $is = self::model()->count('email=?', array($input));
        return ($is === false || $is > 0) ? array(false, 'This email is already registered') : true;
    }

    /**
     * 
     * @return int
     */
    public function getLastRegistered() {
        return $this->registeredId;
    }

}
