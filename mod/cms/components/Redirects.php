<?php

/**
 * Description of Redirects
 *
 */
class Redirects extends BaseModel
{
    const GROUP_NAME = "redirects";
    const ATTR_ENABLE = "redirects.enable";
    const ATTR_RULES_HTTPS = "redirects.rules.https";

    public function getTableName() {
        return 'cp_config';
    }

    public function getId() {
        return 'id';
    }

    private function HeaderLocation301($url) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: {$url}");
        exit();
    }

    public function Run($path) {
        if (!isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && !Helpers::isLocal()) return;

        $currentProtocol = isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) ? $_SERVER["HTTP_X_FORWARDED_PROTO"] : (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? "https" : "http");

        $enable = $this->findField("attr=?", "val", [self::ATTR_ENABLE]);

        if (!$enable) return;

        if ($currentProtocol == "http") {

            $rules = $this->findField("attr=?", "val", [self::ATTR_RULES_HTTPS]);
            if (!$rules) return;

            $rulesArr = explode("\n", $rules);

            foreach ($rulesArr as $r) {
                $r = trim($r, "/*");

                if (strpos($path, $r) !== false) {
                    $this->HeaderLocation301("https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
                }

            }
        }


    }

}
