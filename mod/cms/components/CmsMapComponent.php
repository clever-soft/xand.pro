<?php

/**
 * Description of CmsMapComponent
 *
 * @author kubrey
 */
class CmsMapComponent extends BaseComponent {

    protected $map = array();
    protected $baseUrl;
    protected $lang;
    /** @var $pagesM PagesModel */
    protected $pagesM;
    protected $currUrl;
    protected $urlSteps = array();
    protected $currSubmap;
    protected $edgeMap = array();
    protected $newMap = array();

    public function getMapOld($lang = null) {
        $this->lang = ($lang) ? $lang : ( defined('SITE_CURRENT_LANG') ? SITE_CURRENT_LANG : SITE_DEFAULT_LANG);
        $this->baseUrl = PATH_DS . strtolower($this->lang) . DS;

        $this->pagesM = new PagesModel;
        #$pages = $this->pagesM->findAll("lang=? and level=?", array('url', 'title', 'h1', 'idParent', 'idPage'), array($this->lang, 0), array('npp' => 'ASC'));
        $pages = $this->pagesM->findAllWithTranslate(array(
            'where' => array(
                'row' => 'level = ?',
                'params' => array(0)
            ),
            'fields' => array('url', 'title', 'h1', 'idParent', 'idPage'),
            'order'  =>  array('npp' => 'ASC')
        ));
        if (!$pages) {
            $this->setError($this->pagesM->getLastError());
            return false;
        }

        $base = 0;
        foreach ($pages as $p) {
            if ($p['url'] == 'shop') {
                continue;
            }
            $this->currUrl = $this->baseUrl . $p['url'];
            $this->urlSteps = array();
            $this->edgeMap = array();
            $part = array($this->baseUrl . $p['url'] => $this->dig($p['idPage'], array()));
            if (is_string(current($part))) {
                $part[current(array_keys($part))] = array('_root' => current($part));
            }

            $this->map[$base]['tree'][] = $part;
            $base++;
        }

        return $this->map;
    }

    public function getMap($lang = null) {
        $this->lang = ($lang) ? $lang : ( defined('SITE_CURRENT_LANG') ? SITE_CURRENT_LANG : SITE_DEFAULT_LANG);
        $this->baseUrl = PATH_DS . strtolower($this->lang) . DS;

        $this->pagesM = new PagesModel;

        #$pages = $this->pagesM->findAll("isActive=? and lang=? and level=?", array('url', 'title', 'h1', 'idParent', 'idPage', 'level'), array(1, $this->lang, 0), array('level' => 'ASC', 'npp' => 'ASC'));
        $pages = $this->pagesM->findAllWithTranslate(array(
            'where'  =>  array(
                'row' => 'isActive = ? AND level = ?',
                'params' => array(1, 0)
            ),
            'fields' => array('url', 'title', 'h1', 'idParent', 'level', 'idPage'),
            'order'  =>  array('level' => 'ASC', 'npp' => 'ASC')
        ));
        if (!$pages) {
            $this->setError($this->pagesM->getLastError());
            return false;
        }

        foreach ($pages as $pg) {
            if($pg['url']=='shop'){
                continue;
            }
            $this->newMap[] = array('level' => $pg['level'], 'url' => $pg['url'], 'title' => $pg['title']);
            $this->getChild($pg['idPage']);
        }
        
        return $this->newMap;
    }

    /**
     * 
     * @param int $idParent
     * @return boolean
     */
    protected function getChild($idParent) {
        #$pages = $this->pagesM->findAll("isActive=? and lang=? and idParent=?", array('url', 'title', 'h1', 'idParent', 'idPage', 'level'), array(1, $this->lang, $idParent), array('level' => 'ASC', 'npp' => 'ASC'));
        $pages = $this->pagesM->findAllWithTranslate(array(
            'where'  => array(
                'row' => 'isActive = ? AND idParent = ?',
                'params' => array(1, $idParent)
            ),
            'fields' => array('url', 'title', 'h1', 'idParent', 'level', 'idPage'),
            'order'  => array('level' => 'ASC', 'npp' => 'ASC')
        ));
        if ($pages === false) {
            $this->setError($this->pagesM->getLastError());
            return false;
        }
        if(!$pages){
            return;
        }
        foreach ($pages as $pg) {
            $this->newMap[] = array('level' => $pg['level'], 'url' => $pg['url'], 'title' => $pg['title']);
            $this->getChild($pg['idPage']);
        }
    }

    protected function dig($idPage, $parent = array()) {
        $self = $this->pagesM->findById($idPage);
        if (!$self) {
            throw new Exception($this->pagesM->getLastError() . " or not found $idPage");
        }
        $parent = (is_null($parent) ? array() : $parent);
        $this->urlSteps[] = $this->baseUrl . $self['url'];
//        $index = array_push($parent, array($this->baseUrl . $self['url'] => array()));

        $index = 2;
//        var_dump($index,$parent);

        #$children = $this->pagesM->findAll("idParent=?", array(), array($idPage));
        $children = $this->pagesM->findAllWithTranslate(array(
            'where' => array(
                'row' => 'idParent = ?',
                'params' => array($idPage)
            )
        ));
        if ($children) {
            $parent['_root'] = $self['title'];
            foreach ($children as $child) {
//                 $parent[$index-1][$this->baseUrl . $self['url']][$this->baseUrl . $child['url']] = array('_root'=>$child['title']);
                $parent[$this->baseUrl . $child['url']] = null;
                $parent[$this->baseUrl . $child['url']] = $this->dig($child['idPage'], $parent[$this->baseUrl . $child['url']]);
            }
        } else {
            $parent = $self['title'];
        }
//        $parent[$index-1][$this->baseUrl . $self['url']]['_root'] = $self['title'];

        return $parent;
    }

}
