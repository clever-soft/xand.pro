<?php

/**
 * Class CmsInformersMenu
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersFaq extends FaqModel
{


    /**
     * @return array
     */
    public function getContent()
    {

        $faq = $this->findAllWithTranslate(array(
            'where' => array(
                'row' => 'enable = ?',
                'params' => array(1)
            ),
            'order' => array(
                'idParent' => 'ASC',
                'npp' => 'ASC'
            ),
            'limit' => array(0, 100)
        ));

        $data = $this->parseTree($faq, 'idFaq', 'idParent');

        return array(
            "faq" => $data
        );


    }

}