<?php

/**
 * Class CmsInformersMenu
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersMenu extends PagesModel
{

    /**
     * @return array
     */
    public function getContent()
    {
        $this->useCache = false;

        $menu = [];
        $menuRaw = $this->findAllWithTranslate(
            array(
                'where' => array(
                    'row' => 'cms_textpages.isActive=? AND cms_textpages.isMenu=? AND cms_textpages_i18n.lang=?',
                    'params' => array(1, 1, SITE_CURRENT_LANG)
                ),
                'order' => array("level" => "ASC", "npp" => "ASC"),
                'fields' => array('menuTitle', 'menuDescription', 'url', 'menuIcon', 'level', 'idPage', 'idParent')
            ));



        foreach ($menuRaw as $m) {

            $m["urlFull"] = PATH_DS . SITE_CURRENT_LANG . DS . $m["url"];
            // $m["urlFull"] = str_replace("/home", "", $m["urlFull"]);

            if ($m["level"] == 0) {
                $menu[$m["idPage"]] = $m;
            } else {
                if (!isset($menu[$m["idParent"]]["children"]))
                    $menu[$m["idParent"]]["children"] = [];

                $menu[$m["idParent"]]["children"][$m["idPage"]] = $m;

            }

        }

        return array(
            "menu" => $menu
        );


    }

}