<?php

/**
 * Class CmsInformersTeam
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersTeam
{

    /**
     * @return array
     */
    public function getContent()
    {

        $team = [
            ["name" => "Анатолий Чебан", "role" => "Основатель", "photo" => "01"],
            ["name" => "Елена Погорелова", "role" => "Технический Директор", "photo" => "02"],
            ["name" => "Светлана Рогова", "role" => "Менеджер работы с клиетами", "photo" => "03"],

            ["name" => "Иван Васильев", "role" => "Front-End Разработчик", "photo" => "04"],
            ["name" => "Андрей Шевченко", "role" => "Back-End Разработчик", "photo" => "05"],

            ["name" => "Игорь Ткач", "role" => "Системный администратор", "photo" => "06"],
            ["name" => "Андрей Прошкин", "role" => "Системный администратор", "photo" => "07"],
            ["name" => "Юрий Тихонов", "role" => "Системный администратор", "photo" => "08"]

        ];


        return array(
            "team" => $team
        );


    }

}