<?php

/**
 * Class CmsInformersPopularBrands
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersPopularBrands
{

    /**
     * @return array
     */
    public function getContent()
    {

        $brands = SDb::rows("SELECT 
                    shop_groups.name,  
                    shop_groups.imagePath,
                    shop_groups.url                    
                    FROM shop_groups 
                    WHERE 
                    idParent = 1
                     AND shop_groups.imagePath <> '' 
                    ORDER BY RAND()
                    LIMIT 0,10
                     "
        );

        return array(
            "popularBrands" => $brands
        );


    }

}