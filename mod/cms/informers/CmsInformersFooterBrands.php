<?php

/**
 * Class CmsInformersFooterBrands
 */
class CmsInformersFooterBrands extends ShopGroupsModel
{
    /**
     * CmsInformersFooterLinks constructor.
     */
    public function __construct()
    {
        $this->useCache = true;
    }

    /**
     * @return array
     */
    public function getContent()
    {

        //--- Usually
        $footer_brands = $this->findAllWithTranslate(array(
            'where' => array(
                'row' => 'deleted = ? AND idParent = 1',
                'params' => array(0)
            ),
            'fields' => array('name', 'url', 'imagePath'),
            'order' => array('npp' => 'ASC')
        )); 

        return array(
            "footer_brands" => $footer_brands
        );
    }

}