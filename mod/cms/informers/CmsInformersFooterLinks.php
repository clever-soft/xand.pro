<?php

/**
 * Class CmsInformersFooterLinks
 */
class CmsInformersFooterLinks extends PagesModel
{
    /**
     * CmsInformersFooterLinks constructor.
     */
    public function __construct()
    {
        $this->useCache = true;
    }

    /**
     * @return array
     */
    public function getContent()
    {

        //--- Usually
        $footer_elements = $this->findAllWithTranslate(array(
            'where' => array(
                'row' => 'isActive = ? AND isFooter = ?',
                'params' => array(1, 1)
            ),
            'fields' => array('idParent', 'idPage', 'menuTitle', 'url'),
            'order' => array('idParent' => 'ASC', 'npp' => 'ASC')
        ));

        //--- Extra
        $footer_elements_extra = $this->findAllWithTranslate(array(
            'where' => array(
                'row' => 'isActive = ? AND isFooterExtra = ?',
                'params' => array(1, 1)
            ),
            'fields' => array('menuTitle', 'url'),
            'order' => array('npp' => 'ASC')
        ));

        return array(
            "footer_items_extra" => $footer_elements_extra,
            "footer_items" => $this->parseTree($footer_elements, 'idPage', 'idParent', 0));

    }

}