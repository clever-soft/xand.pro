<?php
class CmsInformersSitemap extends SitemapComponent
{
    //---
    public function __construct(){}

    //---
    public function getContent()
    {
        return array("sitemap" => $this->build());
    }

}