<?php
class CmsInformersShortNews extends PagesModel
{
    //---
    public function __construct(){}

    //---
    public function getContent()
    {
        //---  список новостей
        $newsList = new QueryMulti();
        $newsList->Select(array(
            "cms_news"       => array("url","datePublic"),
            "cms_news_i18n"  => array("contentShort")
        ))
            ->Join("cms_news_i18n", "cms_news_i18n.idNews=cms_news.idNews AND cms_news_i18n.lang = '" . SITE_CURRENT_LANG . "'")
            ->AddFilter("cms_news.isActive = 1 ORDER BY RAND() LIMIT 0,1");

        return array("shortNews" => $newsList->Execute());

    }

}