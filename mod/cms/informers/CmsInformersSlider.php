<?php

/**
 * Class CmsInformersSlider
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersSlider extends SliderModel
{
    /**
     * CmsInformersSlider constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function getContent()
    {

        $slides = $this->findAllWithTranslate(
            array(
                'where' => array(
                    'row' => 'isActive=?',
                    'params' => array(1)
                ),
                'order' => array("npp" => "ASC"),
                'fields' => array("title", "url", "content", "imagePath")
            ));


        foreach ($slides as &$slider) {
            $slider['imagePath'] = 'uploads/slider/' . $slider['imagePath'] . '.jpg';
        }

        return array("slides" => $slides);

    }

}