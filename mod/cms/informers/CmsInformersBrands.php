<?php

/**
 * Class CmsInformersBrands
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class CmsInformersBrands
{

    /**
     * @return array
     */
    public function getContent()
    {

        $brands = SDbM::rows("SELECT 
                    shop_groups_i18n.name, 
                    shop_groups_i18n.description,
                    shop_groups.imagePath,
                    shop_groups.url                    
                    FROM shop_groups
                    JOIN shop_groups_i18n ON shop_groups_i18n.idGroup = shop_groups.idGroup
                    WHERE 
                    idParent = 1
                     AND shop_groups_i18n.description <> ''
                     AND shop_groups_i18n.lang = ?
                    ORDER BY shop_groups_i18n.name
                     ",

            [SITE_CURRENT_LANG]
        );

        return array(
            "brands" => $brands
        );


    }

}