<?php

/**
 * Class DictionaryWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class DictionaryWidget extends Widget
{
    /**
     *
     */
    public function init()
    {
        $this->tableName = "dictionary";
        $this->primaryKey = "id";
    }

    /**
     * @return $this
     */
    public function getContent()
    {
        $query = new QueryMulti();
        $query->Select(array(
            $this->tableName => array("*")
        ));

        $this->setProperty("title", I18n::__("Dictionary"))
            ->SetDefaultOrder($this->primaryKey, false)
            ->Add($query);

        $table = new TableMass(
            new TableColumnLogicIconOffOn("status", "", 30),
            new TableColumn("lang", "", 30, "left"),
            new TableColumn("module", "Module", 100, "left"),
            new TableColumnSpoiler("original", "Original"),
            new TableColumnSpoiler("translate", "Translate"),
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),

            new ButtonDouble(new ButtonPublish(),
                new ButtonUnpublish(),
                new ButtonRule("0", "status", "equally")),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()
        );

        $table->SetPrimary($this->primaryKey);

        $this->Add(new ActionTabFilter(I18n::__("Language"), array("table" => $this->tableName, "field" => "lang"),
            ControlSelect::DataFormat(SDb::rows("select distinct lang from " . $this->tableName), ["lang", "lang"])));

        $this->Add(new ActionSelectFilter("Module", [
            'table' => $this->tableName,
            'field' => 'module'
        ],
            ControlSelect::DataFormat(SDb::rows("select distinct module from " . $this->tableName),
                ["module", "module"]), new RuleEqual("module", $this->tableName)
        ));

        $this->Add(new ActionBoolean(I18n::__("Translated"), ["table" => $this->tableName, "field" => 'status']));
        $this->Add(new ActionInputFilter("Original phrase", ['field' => 'original']));
        $this->Add(new ActionInputFilter("Translate phrase", ['field' => "translate"]));
        $this->Add(new ButtonDelete(NULL, NULL, array("script" => "TableAct")));
        $this->Add($table);

        return $this;
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {
        if (!$this->data) {
            return $this;
        }
        foreach ($this->data as &$row) {
            $row['original'] = htmlspecialchars($row['original']);
            $row['translate'] = htmlspecialchars($row['translate']);
        }

        return $this;
    }

    /**
     *
     */
    public function actionUpdateReady()
    {

        $data = SDbQuery::table($this->tableName)
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->selectRow();

        $data['original'] = htmlspecialchars($data['original']);
        $data['translate'] = htmlspecialchars($data['translate']);
        $form = new FormModal(I18n::__("Edit translation") . " :: " . strtoupper($data["lang"]), [$data]);
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputVar("id", "id"));
        $form->Add(new InputVar("lang", "lang"));
        $form->Add(new InputDiv(I18n::__("Original"), "original"));
        $form->Add(new InputTextArea(I18n::__("Translation"), "translate"));
        $form->Add(new InputCheckbox(I18n::__("Set for similar phrases"), "similarMode", [], true));
        $form->Add(new InputCheckbox(I18n::__("Done"), "status", [], 1));

        $form->Add(
            new ButtonAdd("UpdateFinish", "Save", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $id = $data[$this->primaryKey];
        $data['dateUpdate'] = time();


        if ($data["similarMode"]) {

            unset($data["similarMode"]);
            unset($data["id"]);

            $original = SDbQuery::table($this->tableName)->where([$this->primaryKey => $id])->selectCell("original");

            SDbQuery::table($this->tableName)
                ->enableMessages()
                ->where(["original" => $original, "lang" => $data["lang"]])
                ->update($data);


        } else {
            unset($data["similarMode"]);

            SDbQuery::table($this->tableName)
                ->enableMessages()
                ->where([$this->primaryKey => $id])
                ->update($data);

        }


    }

    /**
     *
     */
    public function actionPublished()
    {
        $id = $this->getPostDataInt();

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $id])
            ->update([
                "dateUpdate" => time(),
                "status" => 1
            ]);

    }

    /**
     *
     */
    public function actionUnpublished()
    {

        SDbQuery::table($this->tableName)
            ->enableMessages()
            ->where([$this->primaryKey => $this->getPostDataInt()])
            ->update([
                "dateUpdate" => time(),
                "status" => 0
            ]);

    }

    /**
     *
     */
    public function actionDelete()
    {
        $ids = $this->getPostData();
        $idsArr = explode(",", $ids);

        foreach ($idsArr as $id) {

            if (empty($id)) continue;

            SDbQuery::table($this->tableName)
                ->enableMessages()
                ->where([$this->primaryKey => $id])
                ->delete();
        }
    }


}