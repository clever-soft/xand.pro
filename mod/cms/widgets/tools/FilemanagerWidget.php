<?php

/**
 * Class FileManagerWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FileManagerWidget extends Widget
{
    /**
     * @return $this
     */
    public function drawBody() {

        $tpl = new TemplateCms("widget", "tpl/tpl-admin/plugins/filemanager/phtml");
        $tpl->Assign("fm", new FileManager());
        $tpl->Load();

        return $this;


    }
}



