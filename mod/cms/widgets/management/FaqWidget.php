<?php

/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 04.05.15
 * Time: 13:41
 */
class FaqWidget extends Widget
{
    public $tableName = 'cms_faq';
    public $tableName_i18n = 'cms_faq_i18n';
    public $id_field = 'idFaq';
    protected $langsTED;

    /**
     *
     */
    public function init() {
        $langsArr = explode(",", SITE_LANGS);
        $this->langsTED = array();

        foreach ($langsArr as $lang) {
            $this->langsTED[] = array("data" => $lang, "equal" => $lang, "title" => $lang);
        }

        $this->nav->param["path"] = (isset($this->nav->param["path"]))
            ? $this->nav->param["path"]
            : 0;
    }

    /**
     * @return $this
     */
    public function getContent() {

        $this->nav->param["path"] = (isset($this->nav->param["path"])) ? $this->nav->param["path"] : 0;

        $query = new QueryMulti();
        $query->Select(array(
            $this->tableName => array("*"),
            $this->tableName_i18n => array("GROUP_CONCAT({$this->tableName_i18n}.lang) AS langs")

        ));

        $query->LeftOuterJoin($this->tableName_i18n, "{$this->tableName_i18n}.{$this->id_field} = {$this->tableName}.{$this->id_field}")
            ->GroupBy("{$this->tableName}.{$this->id_field}")
            ->addFilter("{$this->tableName}.idParent = {$this->nav->param["path"]}");

        $bc = new Breadcrumbs($this->tableName, $this->id_field);
        $bc->Calculate($this->nav->param["path"]);
        $this->Add($bc);

        if ($this->nav->param["path"] != 0) {

            $par = SDb::cell("SELECT idParent FROM {$this->tableName} WHERE {$this->id_field} = ?", [$this->nav->param["path"]]);
            $this->Add(new ButtonUp($par));
        }


        $this->setProperty("title", "FAQ list")
            ->SetDefaultOrder($this->id_field, false)
            ->Add(new ButtonAppend(NULL, "Add new"))
            ->Add($query);

        $table = new TableMass(
            new TableColumn($this->id_field, "ID", 30, "center"),
            new TableColumn("npp", "#", 30, "center"),
            new TableColumn("langs", "Langs", 120, "center"),
            new TableColumn("title", "Title"),
            new TableColumnSpoiler("content", "Content"),
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),
            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(), new ButtonRule("0", "enable", "equally")),
            new ButtonEdit(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA)),
            new ButtonClone(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA)),
            new ButtonDelete()
        );

        $table->SetPrimary($this->id_field)
            ->setProperty("lastGroup", "test")
            ->setEvent("OnDrawRow", function ($obj, $item) {
            });

        $this->Add($table)
            ->Add(new ButtonDelete("Delete", "Delete", array("script" => ButtonBase::SCRIPT_TABLE)));

        return $this;
    }

    /**
     * @return $this
     */
    public function postProcessData() {
        foreach ($this->data as &$val) {
            $val['title'] = $this->Link("{$this->uid}.Link({path:{$val['idFaq']}}, {});", $val['title']);
        }

        return $this;
    }

    /**
     *
     */
    public function actionAddReady() {
        $form = new FormStatic("Add new page");
        $form->assignParent($this)->cloneAjax()->cloneNav();

        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, array("height" => 0));
        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputText("Title", "title", array("placeholder" => "Item title")));
        $gr->Add(new InputTextAreaEditor("Content", "content", array("init" => false, "height" => 400)));
        $form->Add($gr);

        foreach (explode(",", SITE_LANGS) as $l) {
            if ($l != SITE_DEFAULT_LANG) {
                $gr = new GrouperTab("Translate: {$l}", NULL, array("height" => 0));
                $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "Item title")));
                $gr->Add(new InputTextAreaEditor("Content", "translate_{$l}_content", array("init" => false, "height" => 400)));
                $form->Add($gr);
            }
        }

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonAct")),
            new ButtonCancel(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA))
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish() {
        $arr_data_raw = $this->getPostDataJson();
        list($arr_data, $arr_data2) = $this->parse_fields($arr_data_raw);

        $arr_data['dateCreate'] = time();
        $arr_data['dateUpdate'] = time();

        $idFaq = SDb::emptyRow($this->tableName, $this->id_field);

        foreach ($arr_data2 as $lang => $a) {
            SDb::upsert($this->tableName_i18n, [
                "lang" => $lang,
                $this->id_field => $idFaq,
                "title" => $a["title"],
                "content" => $a["content"]
            ], "{$this->id_field} = ? AND lang = ?", [$idFaq, $lang]);
        }

        // Добавляем родительские параметры
        $arr_parent = SDb::row("SELECT * FROM {$this->tableName} WHERE {$this->id_field} = ?", [$this->nav->param["path"]]);

        if ($arr_parent) {
            $arr_data["idParent"] = $arr_parent["idFaq"];
            $arr_data["path"] = $arr_parent['path'] . $idFaq . DS;

        } else {
            $arr_data["idParent"] = 0;
            $arr_data["path"] = DS . $idFaq . DS;
        }

        SDb::update($this->tableName, $arr_data, "{$this->id_field} = ?", [$idFaq]);
    }

    /**
     * Парсинг введённых данных
     * @param $data - введённые данные
     * @return array
     */
    private function parse_fields($data) {
        $arr_data = array();
        $arr_data_i18n = array();

        foreach ($data as $k => $a) {
            // Проверяем, поле обычное или перевод
            if (strpos($k, "translate") !== false) {
                // Поле для перевода имеет следующий шаблон имени - translate_{lang}_{field_name}
                $ar_field_name = explode("_", $k);
                $arr_data_i18n[$ar_field_name[1]][$ar_field_name[2]] = $a;
            } else {
                $arr_data[$k] = $a;
                // Дополнительная запись для дефолтного языка
                $arr_data_i18n[SITE_DEFAULT_LANG][$k] = $a;
            }
        }

        return array(
            $arr_data,
            $arr_data_i18n
        );
    }

    /**
     *
     */
    public function actionUpdateReady() {
        $ARR = SDb::select($this->tableName, $this->id_field . "=?", [$this->getPostDataInt()]);
        $this->check_children($this->getPostDataInt());

        $form = new FormStatic("FAQ item edit", $ARR);
        $form->assignParent($this)->cloneAjax()->cloneNav();

        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, array("height" => 0));
        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputText("Title", "title", array("placeholder" => "Item title")));
        $gr->Add(new InputTextAreaEditor("Content", "content", array("init" => false, "height" => 400)));
        $form->Add($gr);

        // Поля для заполнения языков
        foreach (explode(",", SITE_LANGS) as $l) {
            if ($l != SITE_DEFAULT_LANG) {
                $row = SDb::row("SELECT * FROM {$this->tableName_i18n} WHERE lang = ? AND {$this->id_field} = ?", [$l, $this->getPostDataInt()]);
                $gr = new GrouperTab("Translate: {$l}", NULL, array("height" => 0));
                $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "Item title", "dataForce" => $row["title"])));
                $gr->Add(new InputTextAreaEditor("Content", "translate_{$l}_content", array("init" => false, "height" => 400, "dataForce" => $row["content"])));
                $form->Add($gr);
            }
        }

        $form->Add(
            new InputVar($this->id_field, $this->id_field),
            new ButtonSave(NULL, NULL, array("script" => "JsonAct")),
            new ButtonCancel(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA))
        );

        $form->Draw();
        exit();
    }

    /**
     * Проверка элемента на наличие детей
     * @param $id - идентификатор элемента
     * @return bool - есть дети у элемента или нет
     */
    private function check_children($id) {
        $children = $arr_del = SDb::select($this->tableName, "idParent = ?", [$id]);
        return !empty($children);
    }

    /**
     *
     */
    public function actionUpdateFinish() {

        $arr_data_raw = $this->getPostDataJson();
        list($arr_data, $arr_data2) = $this->parse_fields($arr_data_raw);

        foreach ($arr_data2 as $lang => $a) {
            SDbQuery::table($this->tableName_i18n)
                ->where([
                    $this->id_field => $arr_data[$this->id_field],
                    "lang" => $lang
                ])->upsert([
                    "lang" => $lang,
                    $this->id_field => $arr_data[$this->id_field],
                    "title" => $a["title"],
                    "content" => $a["content"]
                ]);
        }

        $arr_data['dateUpdate'] = time();

        SDbQuery::table($this->tableName)
            ->where([$this->id_field => $arr_data[$this->id_field]])
            ->enableMessages()
            ->update($arr_data);
    }

    /**
     *
     */
    public function actionClone() {
        $id = $this->getPostDataInt();

        $cloneSource = SDb::row("SELECT * FROM {$this->tableName} WHERE {$this->id_field} = ?", [$id]);
        $idNew = SDb::emptyRow($this->tableName, $this->id_field);
        $cloneSource['dateUpdate'] = time();
        $cloneSource['dateCreate'] = time();
        unset($cloneSource[$this->id_field]);
        SDb::update($this->tableName, $cloneSource, "{$this->id_field} = ?", [$idNew]);
    }

    /**
     *
     */
    public function actionPublished() {
        SDb::update($this->tableName, ["enable" => 1, "dateUpdate" => time()], "{$this->id_field} = ?", [$this->getPostDataInt()]);
    }

    /**
     *
     */
    public function actionUnpublished() {
        SDb::update($this->tableName, ["enable" => 0, "dateUpdate" => time()], "{$this->id_field} = ?", [$this->getPostDataInt()]);
    }

    /**
     *
     */
    public function actionDelete() {
        $arrData = explode(",", $this->getPostData());
        $countRefreshed = 0;

        foreach ($arrData as $d) {
            if ($this->check_children($d)) {
                Message::I()->Error(" Cannot remove block with nested elements, remove them first");
            } else {
                $countRefreshed++;

                SDb::delete($this->tableName, "{$this->id_field} = ?", [$d]);
                SDb::delete($this->tableName_i18n, "{$this->id_field} = ?", [$d]);
            }
        }

        if ($countRefreshed) {
            Message::I()->Success(" {$countRefreshed} items was deleted");
        }
    }
}