<?php

/**
 * Class BlocksWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class BlocksWidget extends WidgetCrud
{

    /**
     *
     */
    public function init()
    {
        $this->tableName = "cms_blocks";
        $this->primaryKey = "idBlock";

    }

    /**
     * @return $this
     */
    public function getContent()
    {

        $query = new QueryMulti();

        $query->Select(array(
            $this->tableName => array("*")
        ));

        $this->setProperty("title", "Management of static blocks")
            ->SetDefaultOrder($this->primaryKey, false)
            ->Add($query)
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => ButtonBase::SCRIPT_MODAL)))
            ->Add(new ButtonDelete(NULL, NULL, array("script" => ButtonBase::SCRIPT_TABLE)));

        $table = new TableMass(
            new TableColumnLogicIcon("isActive", "A", 40),
            new TableColumn($this->primaryKey, "ID", 80, "center"),
            new TableColumn("code", "Code", 250, "left"),
            new TableColumnSpoiler("content", "Content"),
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),

            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDouble(new ButtonPublish(),
                new ButtonUnpublish(),
                new ButtonRule("0", "isActive", "equally")),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()
        );

        $table->SetPrimary($this->primaryKey);

        $this->Add($table);


        return $this;
    }

    /**
     *
     */
    public function actionAddReady()
    {


        $form = new FormModal("New static block");
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputCheckbox("Active", "isActive"));
        $form->Add(new InputText("Code", "code"));

        $form->Add(new InputTextArea("Content (default)", "content"));

        foreach (explode(",", SITE_LANGS) as $l)
            $form->Add(new InputTextArea("Translate: " . $l, $l . "_translate"));

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );

        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $arr_data = $this->getPostDataJson();
        $arr_data2 = array();

        foreach ($arr_data as $k => $a) {
            if (strpos($k, "translate") !== false) {
                $strArr = explode("_", $k);
                $arr_data2[$strArr[0]] = $a;
            }
        }


        SDbQuery::table($this->tableName)
            ->insert(array(
                    'dateCreate' => time(),
                    'dateUpdate' => time(),
                    'isActive' => $arr_data["isActive"],
                    'code' => $arr_data["code"],
                    'content' => $arr_data["content"])
            );

        $idBlock = SDbBase::getLastInsertedId();

        foreach ($arr_data2 as $lang => $a) {

            SDbQuery::table("cms_blocks_i18n")
                ->where([
                    $this->primaryKey => $idBlock,
                    "lang" => $lang
                ])
                ->upsert([
                    "lang" => $lang,
                    $this->primaryKey => $idBlock,
                    "content" => $a
                ]);
        }
        Message::I()->Success("Added successfully");
    }


    /**
     *
     */
    public function actionUpdateReady()
    {

        $ARR = SDbQuery::table($this->tableName)->where(["idBlock" => $this->getPostDataInt()])->select();


        $form = new FormModal("Edit block", $ARR);
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputCheckbox("Active", "isActive"));
        $form->Add(new InputTextArea("Code", "code"));

        $form->Add(new InputTextArea("Content (default)", "content"));
        foreach (explode(",", SITE_LANGS) as $l) {
            $val = SDb::cell("SELECT content FROM cms_blocks_i18n WHERE lang=? AND idBlock=?", [$l, $this->getPostDataInt()]);
            $form->Add(new InputTextArea("Translate: " . $l, $l . "_translate", NULL, $val));
        }


        $form->Add(
            new InputVar($this->primaryKey, $this->primaryKey),
            new ButtonSave(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {


        $arr_data = $this->getPostDataJson();



        $idBlock = $arr_data[$this->primaryKey];
        $arr_data2 = array();

        foreach ($arr_data as $k => $a) {
            if (strpos($k, "translate") !== false) {
                $strArr = explode("_", $k);
                $arr_data2[$strArr[0]] = $a;
            }
        }



        foreach ($arr_data2 as $lang => $a) {


            SDbQuery::table("cms_blocks_i18n")
                ->where([
                    $this->primaryKey => $idBlock,
                    "lang" => $lang
                ])
                ->upsert([
                    "lang" => $lang,
                    $this->primaryKey => $idBlock,
                    "content" => $a
                ]);

        }


        SDbQuery::table($this->tableName)
            ->where([
                "idBlock" => $arr_data[$this->primaryKey]
            ])
            ->update([
                "dateUpdate" => time(),
                "code" => $arr_data["code"],
                "isActive" => $arr_data["isActive"],
                "content" => $arr_data["content"]
            ]);
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {
        foreach ($this->data as &$val) {
            $val["code"] = "{{Data:block-" . $val["code"] . "}}";
        }

        return $this;
    }


    /**
     *
     */
    public function actionDelete()
    {
        $data = $this->extractMassID();
        $counter = 0;
        if (!empty($data)) {
            foreach ($data as $d) {

                if (!is_numeric($d)) continue;

                SDbQuery::table($this->tableName)
                    ->where([$this->primaryKey => $d])
                    ->delete();


                SDbQuery::table("cms_blocks_i18n")
                    ->where([$this->primaryKey => $d])
                    ->delete();

                if (SDbBase::getLastError()) {
                    Message::I()->Error(SDbBase::getLastError());
                    return;
                }

                $counter++;
            }

            $text = I18n::__("rows successfully removed");
            Message::I()->resetPostProcess()->Success("{$counter} {$text}");

        } else {
            Message::I()->Warning("Not selected any rows to delete");
        }



    }


}