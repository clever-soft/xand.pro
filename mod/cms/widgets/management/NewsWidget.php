<?php

/**
 * Class NewsWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class NewsWidget extends WidgetNested
{
    public $tableName = 'cms_news';
    public $tableName_i18n = 'cms_news_i18n';
    public $id_field = 'idNews';
    protected $model;
    protected $languagesTED;

    /**
     *
     */
    public function init()
    {

        $this->model = new NewsModel();
        $this->model->useCache = false;

        $languagesArr = explode(",", SITE_LANGS);
        $this->languagesTED = array();

        foreach ($languagesArr as $lang) {
            $this->languagesTED[] = array("data" => $lang, "equal" => $lang, "title" => $lang);
        }

        $this->nav->param["path"] = (isset($this->nav->param["path"]))
            ? $this->nav->param["path"]
            : 0;
    }

    /**
     * @return $this
     */
    public function getContent()
    {
        $this->nav->param["path"] = (isset($this->nav->param["path"]))
            ? $this->nav->param["path"] : 0;

        $query = new QueryMulti();
        $query->Select(array(
            $this->tableName => array("*"),
            $this->tableName_i18n => array("title", "content")

        ));

        $query->LeftOuterJoin($this->tableName_i18n,
            "{$this->tableName_i18n}.{$this->id_field} = {$this->tableName}.{$this->id_field}
                    AND {$this->tableName_i18n}.lang = '" . SITE_DEFAULT_LANG . "'")
            ->GroupBy("{$this->tableName}.{$this->id_field}");


        $this->setProperty("title", "News list")
            ->SetDefaultOrder($this->id_field, false)
            ->Add(new ButtonAppend(NULL, "Add new item"))
            ->Add($query);

        $table = new TableMass(
            new TableColumnLogicIconOffOn("isActive", "A"),
            new TableColumn($this->id_field, "ID", 60, "center"),
            new TableColumnDateShort("datePublic", "Published"),
            new TableColumn("title", "Title"),
            new TableColumn("url", "URL"),
            new TableColumnSpoiler("contentShort", "Content"),
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),

            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(),
                new ButtonRule("0", "isActive", "equally")),
            new ButtonEdit(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA)),
            new ButtonClone(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA)),
            new ButtonDelete()
        );

        $table->SetPrimary($this->id_field)
            ->setProperty("lastGroup", "test")
            ->setEvent("OnDrawRow", function ($obj, $item) {
            });

        $this->Add($table)
            ->Add(new ButtonDelete("Delete", "Delete", array("script" => ButtonBase::SCRIPT_TABLE)));

        return $this;
    }

    /**
     * @return $this
     */
    public function postProcessData()
    {
        foreach ($this->data as &$val) {
            $val['content'] = strip_tags($val['content']);
        }

        return $this;
    }

    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormStatic("Add new page");
        $form->assignParent($this)->cloneAjax()->cloneNav();

        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, array("height" => 0));
        $gr->Add(new InputCheckbox("Active", "isActive"));
        $gr->Add(new InputDate("Publication date", "datePublic"));

        $gr->Add(new InputText("Title", "title", array("placeholder" => "Item title")));
        $gr->Add(new InputText("Keywords", "keywords", array("placeholder" => "Item keywords")));
        $gr->Add(new InputText("Description", "description", array("placeholder" => "Item description")));
        $gr->Add(new InputTextArea("Content short", "contentShort", array("placeholder" => "Item short description")));
        $gr->Add(new InputTextAreaEditor("Content full", "content", array("height" => 500)));

        $gr->Add(new InputUploadPhotoTmb("Picture", "idPhoto",
            array(
                "file" => PATH_DS . "admin/uploader/news",
                "fileType" => "image/jpg,image/png",
                //"tmbData"	    =>  $ARR["pathData"],
                "requirements" => "Any image format *.jpeg",
                "callback_data" => false
            )));

        $form->Add($gr);

        foreach (explode(",", SITE_LANGS) as $l) {
            if ($l != SITE_DEFAULT_LANG) {
                $gr = new GrouperTab("Translate: {$l}", NULL, array("height" => 0));
                $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "Item title")));
                $gr->Add(new InputText("Keywords", "translate_{$l}_keywords", array("placeholder" => "Item keywords")));
                $gr->Add(new InputText("Description", "translate_{$l}_description", array("placeholder" => "Item description")));
                $gr->Add(new InputTextArea("Content short", "translate_{$l}_contentShort", array("placeholder" => "Item short description")));
                $gr->Add(new InputTextAreaEditor("Content", "translate_{$l}_content", array("init" => false, "height" => 500)));
                $form->Add($gr);
            }
        }

        $form->Add(
            new ButtonAdd(),
            new ButtonCancel()
        );
        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {

        list($arr_data, $arr_data2) = $this->ParseI18nFields($this->getPostDataJson());

        //$idNews = PdoK::insert($this->tableName, array($this->id_field => NULL), false, false, false, array(), true);
        SDbQuery::table($this->tableName)->insert([$this->id_field => NULL]);
        $idNews = SDb::getLastInsertedId();
        if (!$idNews) {
            Message::I()->Error(SDb::GetLastError());
            return;
        }
        $arr_data['idPhoto'] = (int)$arr_data['idPhoto'];
        $arr_data['dateCreate'] = time();
        $arr_data['dateUpdate'] = time();
        $arr_data['datePublic'] = (int)strtotime($arr_data['datePublic']);
        $arr_data['isActive'] = (int)$arr_data['isActive'];
        $arr_data['url'] = (empty($arr_data['title'])) ? "news-" . $idNews : Content::createUrl($arr_data['title']);


        /* обработка фото */
        if (!(empty($arr_data['idPhotoData']))) {
            $this->_createThumbnail($arr_data);
        }
        unset($arr_data["idPhotoData"]);

        /* обработка фото */

        unset($arr_data["title"]);
        unset($arr_data["keywords"]);
        unset($arr_data["description"]);
        unset($arr_data["content"]);
        unset($arr_data["contentShort"]);

        foreach ($arr_data2 as $lang => $a) {
            SDb::upsert($this->tableName_i18n, [
                "lang" => $lang,
                $this->id_field => $idNews,
                "title" => $a["title"],
                "content" => $a["content"],
                "contentShort" => $a["contentShort"],
                "keywords" => $a["keywords"],
                "description" => $a["description"]
            ], "{$this->id_field} = ? AND lang = ?", [$idNews, $lang]);
        }

        SDb::update($this->tableName, $arr_data, "{$this->id_field} = ?", [$idNews]);

    }

    /**
     * @param array $data
     */
    private function _createThumbnail($data = array())
    {
        $pathData = explode(",", $data['idPhotoData']);
        $uniqueName = SDb::cell("SELECT path FROM gallery_photos WHERE idPhoto = ?", [$data['idPhoto']]);
        $targetPath = PATH_REAL . DS . PATH_PUBLIC . "{$uniqueName}_tmb.jpg";
        $source = imagecreatefromjpeg(PATH_REAL . DS . PATH_PUBLIC . "{$uniqueName}.jpg");
        $target = imagecreatetruecolor(200, 200);

        //Db::Update("gallery_photos", "idPhoto = {$data['idPhoto']}", array("isTmp" => 0, "pathData" => $data['idPhotoData']));
        //SDb::update("gallery_photos", "idPhoto = ?", [$data['idPhoto'], array("isTmp" => 0, "pathData" => $data['idPhotoData'])]);
        SDbQuery::table("gallery_photos")->where(["idPhoto" => $data['idPhoto']])->update(array("isTmp" => 0, "pathData" => $data['idPhotoData']));

        imagecopyresampled($target, $source, 0, 0, $pathData[0], $pathData[1], 200, 200, $pathData[2], $pathData[3]);
        imagejpeg($target, $targetPath, 95);
        imagedestroy($target);
        imagedestroy($source);
    }

    /**
     *
     */
    public function actionUpdateReady()
    {

        $ARR = $this->model->findById($this->getPostDataInt());

        if ($ARR["idPhoto"]) {
            $photo = SDb::row("SELECT path, pathData FROM gallery_photos WHERE idPhoto = ?", [$ARR["idPhoto"]]);

            $ARR["path"] = $photo["path"];
            $ARR["pathData"] = $photo["pathData"];
        } else {
            $ARR["path"] = "";
            $ARR["pathData"] = "";
        }

        foreach (explode(",", SITE_LANGS) as $l) {

            $fields = implode(",", $this->model->get_translated_fields());
            $row = SDb::row("SELECT {$fields} FROM {$this->model->getTableName()}_i18n WHERE lang=? and idNews=?", [$l, $this->getPostDataInt()]);

            /*$row = PdoK::findOne(
                $this->model->getTableName() . "_i18n",
                $this->model->get_translated_fields(),
                'lang=? and idNews=?',
                array($l, $this->getPostDataInt()));*/


            if ($l == SITE_DEFAULT_LANG) {
                $ARR = array_merge($ARR, $row);

            } else {

                foreach ($row as $k => $r) {
                    $ARR["translate_{$l}_{$k}"] = $r;
                }
            }
        }
        $form = new FormStatic("News item edit", $ARR);
        $form->assignParent($this)->cloneAjax()->cloneNav();

        $gr = new GrouperTab("Default (" . SITE_DEFAULT_LANG . ")", NULL, ["height" => 0]);
        $gr->Add(new InputCheckbox("Active", "isActive"));
        $gr->Add(new InputDate("Publication date", "datePublic"));


        $gr->Add(new InputTextReadonly("URL", "url", array("placeholder" => "URL")));
        $gr->Add(new InputText("Title", "title", ["placeholder" => "Item title"]));
        $gr->Add(new InputText("Keywords", "keywords", array("placeholder" => "Item keywords")));
        $gr->Add(new InputText("Description", "description", array("placeholder" => "Item description")));
        $gr->Add(new InputTextArea("Content short", "contentShort", array("placeholder" => "Item short description")));
        $gr->Add(new InputTextAreaEditor("Content full", "content", array("height" => 500)));

        $gr->Add(new InputUploadPhotoTmb("Picture", "idPhoto",
            [
                "file" => PATH_DS . "admin/uploader/news",
                "fileType" => "image/jpg,image/png",
                "tmbData" => $ARR["pathData"],
                "requirements" => "Any image format *.jpeg",
                "callback_data" => ($ARR["path"]) ? PATH_DS . $ARR["path"] . ".jpg" : false
            ]));

        $form->Add($gr);

        foreach (explode(",", SITE_LANGS) as $l) {
            if ($l != SITE_DEFAULT_LANG) {

                $gr = new GrouperTab("Translate: {$l}", NULL, array("height" => 0));
                $gr->Add(new InputText("Title", "translate_{$l}_title", array("placeholder" => "Item title")));
                $gr->Add(new InputText("Keywords", "translate_{$l}_keywords", array("placeholder" => "Item keywords")));
                $gr->Add(new InputText("Description", "translate_{$l}_description", array("placeholder" => "Item description")));
                $gr->Add(new InputTextArea("Content short", "translate_{$l}_contentShort", array("placeholder" => "Item short description")));
                $gr->Add(new InputTextAreaEditor("Content", "translate_{$l}_content", array("init" => false, "height" => 500)));
                $form->Add($gr);
            }
        }

        $form->Add(
            new InputVar($this->id_field, $this->id_field),
            new ButtonSave(NULL, NULL, array("script" => "JsonAct")),
            new ButtonCancel(NULL, NULL, array("script" => ButtonBase::SCRIPT_DATA))
        );

        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {

        list($arr_data, $arr_data2) = $this->ParseI18nFields($this->getPostDataJson());

        foreach ($arr_data2 as $lang => $a) {
            /* SDb::upsert($this->tableName_i18n, [
                 "lang" => $lang,
                 $this->id_field => $arr_data[$this->id_field],
                 "title" => $a["title"],
                 "content" => $a["content"],
                 "contentShort" => $a["contentShort"],
                 "keywords" => $a["keywords"],
                 "description" => $a["description"]
             ], "{$this->id_field} = ? AND lang = ?", [$arr_data[$this->id_field], $lang]);*/


            SDbQuery::table($this->tableName_i18n)->where([
                $this->id_field => $arr_data[$this->id_field],
                "lang" => $lang
            ])->upsert([
                "lang" => $lang,
                $this->id_field => $arr_data[$this->id_field],
                "title" => $a["title"],
                "content" => $a["content"],
                "contentShort" => $a["contentShort"],
                "keywords" => $a["keywords"],
                "description" => $a["description"]
            ]);
        }

        $arr_data['idPhoto'] = (int)$arr_data['idPhoto'];
        $arr_data['dateUpdate'] = time();
        $arr_data['datePublic'] = (int)strtotime($arr_data['datePublic']);
        $arr_data['isActive'] = (int)$arr_data['isActive'];

        if (isset($arr_data['urlEdit']) && $arr_data['urlEdit'] == true) {
            if (!$this->_checkUniqueUrl($arr_data['url'], $arr_data['idNews'])) {
                Message::I()->Error("This not unique url");
                return;
            }
        } elseif (!isset($arr_data['url']) && empty($arr_data['url'])) {
            $arr_data['url'] = (empty($arr_data['title'])) ? "news-" . $arr_data[$this->id_field] : Content::createUrl($arr_data['title']);
        }

        if (!(empty($arr_data['idPhotoData']))) {
            $this->_createThumbnail($arr_data);
        }


        unset($arr_data["idPhotoData"],
            $arr_data["title"],
            $arr_data["keywords"],
            $arr_data["description"],
            $arr_data["content"],
            $arr_data["contentShort"],
            $arr_data["urlEdit"]
        );

        SDb::update($this->tableName, $arr_data, "{$this->id_field} = ?", [$arr_data[$this->id_field]]);
    }

    /**
     *
     */
    public function actionClone()
    {
        $id = $this->getPostDataInt();

        $cloneSource = SDb::row("SELECT * FROM {$this->tableName} WHERE {$this->id_field} = ?", [$id]);
        SDbQuery::table($this->tableName)->insert([$this->id_field => NULL]);
        $idNews = SDb::getLastInsertedId();
        if (!$idNews) {
            Message::I()->Error(SDb::getLastError());
            return;
        }

        $cloneSource['dateCreate'] = time();
        $cloneSource['dateUpdate'] = time();
        $cloneSource['url'] = $cloneSource['url'] . "-1";

        unset($cloneSource[$this->id_field]);
        SDb::update($this->tableName, $cloneSource, "{$this->id_field} = ?", [$idNews]);
    }

    /**
     *
     */
    public function actionPublished()
    {
        SDb::update($this->tableName, ["isActive" => 1, "dateUpdate" => time()], "{$this->id_field} = ?", [$this->getPostDataInt()]);
    }

    /**
     *
     */
    public function actionUnpublished()
    {
        SDb::update($this->tableName, ["isActive" => 0, "dateUpdate" => time()], "{$this->id_field} = ?", [$this->getPostDataInt()]);
    }

    /**
     *
     */
    public function actionDelete()
    {
        $arrData = explode(",", $this->getPostData());
        $countRefreshed = 0;

        foreach ($arrData as $d) {
            $countRefreshed++;
            SDbQuery::table($this->tableName)->where([$this->id_field => $d])->delete();
            SDbQuery::table($this->tableName_i18n)->where([$this->id_field => $d])->delete();
        }

        if ($countRefreshed) {
            Message::I()->Success(" {$countRefreshed} items was deleted");
        }
    }

    private function _checkUniqueUrl($url, $idNews)
    {
        $row = SDbQuery::table($this->tableName)->where(["url" => $url])->select();
        if (count($row) > 0) {
            if (count($row) == 1 && $row[0]['idNews'] == $idNews) {
                return true;
            }
            return false;
        }
        return true;
    }
}