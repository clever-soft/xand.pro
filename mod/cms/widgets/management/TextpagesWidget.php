<?php

/***
 * Class TextpagesWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 *
 *  ToDo:  Добавлен HashTag
 *
 */
class TextpagesWidget extends WidgetNested
{
    public $model;

    protected $langsTED;
    protected $csiMovePages;

    public function init()
    {

        $this->model = new PagesModel();
        $this->model->useCache = false;

        $langsArr = explode(",", SITE_LANGS);
        $this->langsTED = array();

        foreach ($langsArr as $lang) {
            $this->langsTED[] = array("data" => $lang, "equal" => $lang, "title" => $lang);
        }
        $this->nav->param["path"] = (isset($this->nav->param["path"]))
            ? $this->nav->param["path"]
            : 0;


        $this->csiMovePages = new ControlSelectInteractive("New parent", "idParent", [
            "action" => "MoveCallback",
            "table" => "cms_textpages",
            "fieldPrimary" => "idPage",
            "fieldParent" => "idParent",
            "fieldPath" => "path",
            "fieldTitle" => "title",

        ]);

    }

    /**
     * @return $this
     */
    public function getContent()
    {

        $this->init();

        // $this->actionUpdateReady();

        $this->nav->param["path"] = (isset($this->nav->param["path"]))
            ? $this->nav->param["path"] : 0;

        $bc = new Breadcrumbs("cms_textpages", "idPage");
        $bc->Calculate($this->nav->param["path"]);
        $this->Add($bc);

        $query = new QueryMulti();
        $query->Select(array(

            "cms_textpages" => array("*"),
            "cms_textpages_i18n" => array("GROUP_CONCAT(cms_textpages_i18n.lang SEPARATOR ', ') AS langs")

        ));

        $query->LeftOuterJoin("cms_textpages_i18n", "cms_textpages_i18n.idPage = cms_textpages.idPage")
            ->GroupBy("cms_textpages.idPage");

        if ($this->nav->param["path"] != 0) {
            $query->AddFilter("cms_textpages.idParent = {$this->nav->param["path"]}");
            $par = $this->model->findField($this->model->getId() . "=?", "idParent", array($this->nav->param["path"]));
            $this->Add(new ButtonUp($par));
        } else {
            $query->AddFilter("cms_textpages.level = 0");
        }


        $this->setProperty("title", "Text pages list")
            ->SetDefaultOrder("npp", false)
            ->Add(new ButtonAppend(NULL, "Add new"))
            ->Add($query);

        $table = new TableMass(
            new TableColumnLogicIcon("isSystem", "S", 22, "fa-lock"),
            new TableColumnLogicIcon("isMenu", "M", 22, "fa-indent", "center"),
            new TableColumnLogicIcon("isFooter", "F", 22, "fa-indent", "center"),
            new TableColumnLogicIcon("isFooterExtra", "FE", 22, "fa-indent", "center"),
            new TableColumnLogicIcon("hashTag", "H", 22, "fa-hashtag"),
            new TableColumn("idPage", "ID", 42, "center"),
            new TableColumn("npp", "#", 42, "center"),
            new TableColumn("title", "Title"),
            // new TableColumn("menuTitle", "Menu title"),
            new TableColumn("url", "URL"),
            new TableColumn("tpl", "Template", 120, "left"),
            new TableColumn("langs", "Langs", NULL, "center"),
            new TableColumn("path", "Path", 120, "left"),

            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),

            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(),
                new ButtonRule("0", "isActive", "equally")),
            new ButtonMove(),
            new ButtonEdit(NULL, NULL, array("script" => "DataAct")), new ButtonClone(NULL, NULL, array("script" => "DataAct")),
            new ButtonDelete()
        );

        $table->SetPrimary("idPage")
            ->setProperty("lastGroup", "test")
            ->setEvent("OnDrawRow", function ($obj, $item) {

                if ($item['isSystem']) {
                    $obj->buttons['Delete']->setProperty("enabled", false);
                } else {
                    $obj->buttons['Delete']->setProperty("enabled", true);
                }
            });

        $this->Add($table)
            ->Add(new ActionInputFilter("Url", array("field" => "url")))
            ->Add(new ButtonDelete("Delete", "Delete", array("script" => "TableAct")));

        return $this;
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {

        foreach ($this->data as &$val) {
            $val['title'] = $this->Link($this->uid . ".Link({path:{$val['idPage']}}, {});", $val['title']);
        }

        return $this;
    }

    /**
     *
     */
    public function actionMoveCallback()
    {

        $this->csiMovePages->DrawInteractive($this->getPostDataInt(), $this->ajax->eid);
        exit();
    }


    /**
     *
     */
    public function actionMoveReady()
    {
        $arr = $this->model->findById($this->getPostDataInt());
        $url_array = $this->parseItemUrl($arr['url']);
        $arr['url'] = $url_array['url'];
        $arr['parentUrl'] = $url_array['parentUrl'] . DS;

        $form = new FormModal("Moving page: " . $arr["title"], $arr);
        $form->assignParent($this)->cloneAjax()->CloneNav();
        $form->Add(new InputTextReadonly("Parent URL", "parentUrl", array("placeholder" => "parent url", "inputWidth" => "250")));
        $form->Add(new InputText("URL", "url", array("placeholder" => "url")));
        $form->Add($this->csiMovePages);
        $form->Add(new InputVar("idPage", "idPage"));
        $form->Add();

        $form->Add(
            new ButtonSave("MoveFinish", "Move", array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );

        $form->Draw();
        exit();
    }

    /**
     *
     */
    public function actionMoveFinish()
    {

        $arr = $this->getPostDataJson();

        $idPage = $arr["idPage"];
        $arr_parent = $this->model->findById($arr['idParent']);
        $upd = array();

        if (!empty($arr_parent)) {
            $upd["path"] = $arr_parent['path'] . $idPage . DS;
            $upd["idParent"] = $arr_parent['idPage'];
            $upd["level"] = $arr_parent['level'] + 1;
            $upd['url'] = $arr_parent['url'] . DS . $arr['url'];

        } else {
            $upd["path"] = DS . $idPage . DS;
            $upd['url'] = $arr['url'];
            $upd["idParent"] = 0;
            $upd["level"] = 0;
        }
        if ($this->model->updateById($idPage, $upd)) {
            Message::I()->Success("Updated");
        } else {
            Message::I()->Error("Failed to update");
        }
    }

    /**
     *
     */
    public function actionAddReady()
    {
        $formSC = new FormStaticCms;
        $formSC->Prepare($this)->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateReady()
    {
        $formSC = new FormStaticCms;
        $formSC->Prepare($this)->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinishContinue()
    {
        $this->actionUpdateFinish();
        Message::I()->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $arr_data_raw = $this->getPostDataJson();
        list($arr_data, $arr_data2) = $this->parseI18nFields($arr_data_raw);
        $this->model->InsertWidgetPage($arr_data, $arr_data2, $this->GetParentObject());
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {

        $arr_data_raw = $this->getPostDataJson();

        list($arr_data, $arr_data2) = $this->parseI18nFields($arr_data_raw);
        $this->model->UpdateWidgetPage($arr_data, $arr_data2, $this->GetParentObject());

    }

    /**
     *
     */
    public function actionContentReady()
    {
        $arr = $this->model->findById($this->getPostDataInt());

        $form = new FormModal("Content edit", $arr);
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new InputTextAreaEditor("Content", "content", array("height" => 500, "titleDisable" => true)));

        $form->Add(
            new InputVar("idPage", "idPage"), new ButtonSave("ContentFinish", NULL, array("script" => "JsonModalCloseAct")), new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit();
    }

    public function actionClone()
    {
        $idPage = $this->getPostDataInt();

        $cloneSource = $this->model->findById($idPage);
        $idNew = $this->model->holdRow();
        $cloneSource["path"] = str_replace("/{$idPage}/", "/{$idNew}/", $cloneSource["path"]);
        $cloneSource['dateUpdate'] = time();
        $cloneSource['dateCreate'] = time();
        $cloneSource['url'] .= "-" . $idNew;
        unset($cloneSource['idPage']);

        foreach ($cloneSource as $key => $val) {
            if ($val == "" || $val == NULL)
                unset($cloneSource[$key]);
        }

        if ($this->model->updateById($idNew, $cloneSource)) {
            Message::I()->Success("Cloned successfully");
        } else {
            Message::I()->Error("Error ocurred; " . $this->model->getLastError());
        }
    }

    public function actionContentFinish()
    {
        $arr_data = $this->getPostDataJson();
        $arr_data['dateUpdate'] = time();
        if ($this->model->updateById($arr_data['idPage'], $arr_data)) {
            Message::I()->Success("Updated successfully");
        } else {
            Message::I()->Error("Error occurred: " . $this->model->getLastError());
        }
    }


    public function actionPublished()
    {
        if ($this->model->updateById($this->getPostDataInt(), array('isActive' => 1, 'dateUpdate' => time()))) {
            Message::I()->Success("Updated successfully");
        } else {
            Message::I()->Error("Error occurred: " . $this->model->getLastError());
        }
    }

    public function actionUnpublished()
    {
        if ($this->model->updateById($this->getPostDataInt(), array('isActive' => 0, 'dateUpdate' => time()))) {
            Message::I()->Success("Updated successfully");
        } else {
            Message::I()->Error("Error occurred: " . $this->model->getLastError());
        }
    }

    public function actionDelete()
    {
        $arrData = explode(",", $this->getPostData());
        $countRefreshed = 0;
        foreach ($arrData as $d) {
            $countRefreshed++;
            $arDel = $this->model->findAll("path like ?", array($this->model->getId()), array("%{$d}%"));

            if ($arDel) {
                foreach ($arDel as $row) {
                    $this->model->delete($row[$this->model->getId()]);
                    SDb::delete($this->model->getTableName_i18n(), "{$this->model->getId()}=?", [$row[$this->model->getId()]]);
                }
            } else {
                $this->model->delete($d);
                SDb::delete($this->model->getTableName_i18n(), "{$this->model->getId()}=?", [$d]);
            }
        }
        Message::I()->Success(" {$countRefreshed} pages was deleted");
    }


}
