<?php

class ManualsWidget extends Widget
{
    public function getContent() {
        $query = new QuerySimple();
        $query->Select(array("cms_manuals" => array("*")));

        $this->setProperty("title", "Devices manuals")
            ->SetDefaultOrder("id", false)
            ->Add($query)
            ->Add(new ButtonAppend(NULL, "Add new", array("script" => "ModalAct")))
            ->Add(new ActionSelectFilter(
                "Group name",
                array(
                    "table" => "cms_manuals",
                    "field" => "groupKey"
                ),
                ManualGroupsModel::getList(),
                new RuleLike("groupKey", "cms_manuals")
            ))
            ->Add(new ActionInputFilter(
                "device",
                array("field" => "device")
            ));

        $table = new Table(
            new TableColumn("id", "ID", 60, "center"),
            new TableColumn("groupKey", "Group key", NULL, "left"),
            new TableColumn("device", "Device", NULL, "left"),
            new TableColumn("softwareUrl", "Link to software"),
            new TableColumn("manualUrl", "Link to manual"),
            new TableColumn("softwareText", "Text of software link"),
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),
            new ButtonEdit(NULL, NULL, array("script" => "ModalAct")),
            new ButtonDelete()
        );

        $table->SetPrimary("id");

        $this->Add($table);

        return $this;
    }

    public function PostProcessData(){
    }

    public function actionAddReady() {
        $form = new FormModal("Translate add");
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new ControlSelect("Group key", "groupKey", array("noChoose" => false), NULL, ManualGroupsModel::getList()));
        $form->Add(new InputText("Device", "device"));
        $form->Add(new InputText("Link to software", "softwareUrl"));
        $form->Add(new InputText("Text of software link", "softwareText"));
        $form->Add(new InputText("Link to manual", "manualUrl"));

        $form->Add(
            new ButtonAdd(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit;
    }

    public function actionAddFinish() {
        $arr_data = $this->getPostDataJson();
        $arr_data['dateCreate'] = time();
        $arr_data['dateUpdate'] = time();
        SDbQuery::table("cms_manuals")->enableMessages()->update($arr_data);
    }

    public function actionDelete() {
        SDbQuery::table("cms_manuals")->enableMessages()->where(["id" => $this->getPostDataInt()])->delete();
    }

    public function actionUpdateReady() {
        $ARR = SDb::select("cms_manuals", "id=?", [$this->getPostDataInt()]);

        $form = new FormModal("Device manual editor", $ARR);
        $form->assignParent($this)->cloneAjax()->CloneNav();

        $form->Add(new ControlSelect("Group key", "groupKey", array("noChoose" => false), NULL, ManualGroupsModel::getList()));
        $form->Add(new InputText("Device", "device"));
        $form->Add(new InputText("Link to software", "softwareUrl"));
        $form->Add(new InputText("Text of software link", "softwareText"));
        $form->Add(new InputText("Link to manual", "manualUrl"));


        $form->Add(
            new InputVar("id", "id"),
            new ButtonSave(NULL, NULL, array("script" => "JsonModalCloseAct")),
            new ButtonCancel(NULL, NULL, array("script" => "ModalClose"))
        );
        $form->Draw();
        exit;
    }

    public function actionUpdateFinish() {
        $arr_data = $this->getPostDataJson();
        $arr_data['dateUpdate'] = time();
        SDbQuery::table("cms_manuals")->enableMessages()->where(["id" => $arr_data["id"]])->update($arr_data);
    }
}