<?php

/**
 * Class FeedbackWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FeedbackWidget extends Widget
{
    /**
     *
     */
    public function init()
    {
        $this->tableName = "cms_feedback";
        $this->primaryKey = "id";
    }

    /**
     * @return $this
     */
    public function getContent()
    {

        $query = new QuerySimple();
        $query->Select(array($this->tableName => array("*")));

        $this->setProperty("title", "Feedback list")
            ->SetDefaultOrder("dateCreate", false)
            ->Add($query);

        $table = new TableMass(
            new TableColumn($this->primaryKey, "ID", 40, "center"),
            new TableColumn("name", "Name"),
            new TableColumn("email", "Mail", 160),
            new TableColumn("phone", "Phone", 160),
            new TableColumn("ip", "IP", 160),
            new TableColumn("ua", "UserAgent", 160),
            new TableColumnDateCreate(),
            new ButtonView("ViewReady", "View", array("script" => "ModalAct")),
            new ButtonDelete()

        );

        $table->SetPrimary($this->primaryKey);

        switch ($this->getPostAction()) {
            case 'ViewReady':

                $cComment = SDbQuery::table($this->tableName)
                    ->where(["id" => $this->getPostDataInt()])
                    ->select();

                $cComment[0]["text"] = nl2br($cComment[0]["text"]);

                $form = new FormModal($cComment[0]["name"] . " (" . $cComment[0]["email"] . ")", $cComment);
                $form->assignParent($this)->cloneAjax()->CloneNav();
                $form->Add(new InputDiv("Content:", "text", array("height" => 300, "titleDisable" => true)));
                $form->Add(new ButtonCancel(NULL, "Close", array("script" => "ModalClose")));
                $form->Draw();
                exit();

            case 'Delete' :
                $arrData = explode(",", $this->getPostData());
                $countRefreshed = 0;

                foreach ($arrData as $d) {
                    $countRefreshed++;

                    if (!is_numeric($d))
                        continue;

                    SDbQuery::table($this->tableName)
                        ->enableMessages()
                        ->where(["id" => $d])
                        ->delete();

                }
                break;
        }

        $this->Add($table)
            ->Add(new ButtonDelete("Delete", "Delete", array("script" => "TableAct")));

        return $this;

    }


}
