<?php

/**
 * Class RedirectsWidget
 * @author Alexander S. <aleksandershtefan@gmail.com>
 */
class RedirectsWidget extends Widget
{

    protected $model;

    /**
     *
     */
    public function init()
    {
        $this->model = new Redirects;
    }

    /**
     *
     */
    public function actionAddFinish()
    {
        $data = $this->getPostDataJson();

        SDb::transactionStart();

        SDbQuery::table("cp_config")->where(['attr' => Redirects::ATTR_ENABLE])->upsert(['attr' => Redirects::ATTR_ENABLE, 'groupName' => Redirects::GROUP_NAME, 'type' => 'bool', 'val' => $data[Redirects::ATTR_ENABLE]]);
        SDbQuery::table("cp_config")->where(['attr' => Redirects::ATTR_RULES_HTTPS])->upsert(['attr' => Redirects::ATTR_RULES_HTTPS, 'groupName' => Redirects::GROUP_NAME, 'type' => 'textarea', 'val' => $data[Redirects::ATTR_RULES_HTTPS]]);

        if (SDbBase::getLastError()) {
            Message::I()->Error("Failed to update record: " . SDbBase::getLastError());
            SDb::transactionRollBack();
            return;
        }

        Message::I()->Success("Config was updated successfully");
        SDb::transactionCommit();

    }

    /**
     * @return $this
     */
    public function DrawBody()
    {
        //---
        //$rawData = $this->model->findAll("groupName='redirects'");
        $rawData = SDbQuery::table("cp_config")->where(["groupName" => "redirects"])->select();
        $data = array();
        foreach ($rawData as $d) {
            $data[0][$d["attr"]] = $d["val"];
        }

        $form = new FormStatic("Redirects management", $data, array("width" => 600));

        $form->Add($this->ajax);
        $form->Add(new InputCheckbox("Enable", Redirects::ATTR_ENABLE, array("requirements" => "")));

        $panel = new Panel("HTTPS Rules");
        $panel->Add(new InputTextArea("Redirects rules", Redirects::ATTR_RULES_HTTPS, array("height" => 220, "titleDisable" => true,
            "requirements" => "format: /customers/* ")));
        $form->Add($panel);

        $form->Add(new ButtonAdd(NULL, "Apply"));
        $form->Draw();

        Message::I()->Draw();

        return $this;

    }
}