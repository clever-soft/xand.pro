<?php

/**
 * Class SliderWidget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 *
 */
class SliderWidget extends WidgetNested
{
    public $tableName = 'cms_slider';
    public $tableName_i18n = 'cms_slider_i18n';
    public $id_field = 'idSlide';
    public $model;
    public $languagesTED;

    /**
     *
     */
    public function init()
    {

        $this->model = new SliderModel();
        $this->model->useCache = false;

        $languagesArr = explode(",", SITE_LANGS);
        $this->languagesTED = array();

        foreach ($languagesArr as $lang) {
            $this->languagesTED[] = array("data" => $lang, "equal" => $lang, "title" => $lang);
        }

    }

    /**
     * @return $this
     */
    public function getContent()
    {

        $query = new QueryMulti();
        $query->Select(array(
            $this->model->getTableName() => array("*"),
            $this->model->getTableName_i18n() => array("title", "content")
        ));

        $query->LeftOuterJoin($this->tableName_i18n,
            "{$this->tableName_i18n}.{$this->id_field} = {$this->tableName}.{$this->id_field}
                    AND {$this->tableName_i18n}.lang = '" . SITE_DEFAULT_LANG . "'")
            ->GroupBy("{$this->tableName}.{$this->id_field}");


        $this->SetProperty("title", "Slider list")
            ->SetDefaultOrder($this->id_field, false)
            ->Add(new ButtonAppend(NULL, "Add new"))
            ->Add($query);

        $table = new TableMass(
            new TableColumn("idSlide", "ID", 40, "center"), //
            new TableColumn("npp", "#", 40, "center"),
            new TableColumn("title", "Title", 160), //
            new TableColumn("url", "URL", 160), //
            new TableColumnSpoiler("content", "Content"), //
            new TableColumnDate("timerStart", "Start time"), //
            new TableColumnDate("timerEnd", "End time"), //
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),
            new ButtonDouble(new ButtonPublish(), new ButtonUnpublish(),
                new ButtonRule("0", "isActive", "equally")),
            new ButtonEdit(), //
            new ButtonDelete()
        );

        $table->SetPrimary($this->id_field)
            ->SetProperty("lastGroup", "test")
            ->SetEvent("OnDrawRow", function ($obj, $item) {
            });

        $this->Add($table);

        return $this;
    }

    /**
     * @return $this
     */
    public function PostProcessData()
    {
        foreach ($this->data as &$val) {
            $val['content'] = strip_tags($val['content']);
        }

        return $this;
    }

    /**
     *
     */
    public function actionAddReady()
    {
        $form = new FormStaticSlider();
        $form->Prepare($this, "AddReady")->Draw();
        exit();
    }

    /**
     *
     */
    public function actionAddFinish()
    {

        list($arr_data, $arr_data2) = $this->ParseI18nFields($this->getPostDataJson());
        $idSlide = SDbQuery::table($this->tableName)->insert([$this->id_field => NULL]);
        if (!$idSlide) {
            Message::I()->Error(SDb::getLastError());
            return;
        }

        foreach ($arr_data2 as $lang => $a) {
            SDbQuery::table($this->tableName_i18n)
                ->where([
                    $this->id_field => $idSlide,
                    "lang" => $lang
                ])->upsert([
                    "lang" => $lang,
                    $this->id_field => $idSlide,
                    "title" => $a["title"],
                    "content" => $a["content"],
                    "imageAlt" => $a["imageAlt"]
                ]);
        }

        unset($arr_data["title"]);
        unset($arr_data["content"]);
        unset($arr_data["imageAlt"]);

        if ($arr_data['timerActive']) {
            $timerStart = new DateTime($arr_data['timerStart']);
            $timerEnd = new DateTime($arr_data['timerEnd']);

            $arr_data['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $arr_data['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $arr_data['timerStart'] = 0;
            $arr_data['timerEnd'] = 0;
        }
        $arr_data['timerActive'] = (int)$arr_data['timerActive'];
        $arr_data['dateCreate'] = time();
        $arr_data['dateUpdate'] = time();

        SDbQuery::table($this->tableName)->enableMessages()->where([$this->id_field => $idSlide])->update($arr_data);

    }

    /**
     *
     */
    public function actionUpdateReady()
    {
        $form = new FormStaticSlider();
        $form->Prepare($this, "UpdateReady", $this->getPostDataInt())->Draw();
        exit();
    }

    /**
     *
     */
    public function actionUpdateFinish()
    {

        list($arr_data, $arr_data2) = $this->ParseI18nFields($this->getPostDataJson());

        $idSlide = (int)$arr_data["idSlide"];
        unset($arr_data["idSlide"]);

        foreach ($arr_data2 as $lang => $a) {

            SDbQuery::table($this->tableName_i18n)
                ->where([
                    $this->id_field => $idSlide,
                    "lang" => $lang
                ])->upsert([
                    "lang" => $lang,
                    $this->id_field => $idSlide,
                    "title" => $a["title"],
                    "content" => $a["content"],
                    "imageAlt" => $a["imageAlt"]
                ]);
        }

        unset($arr_data["title"]);
        unset($arr_data["content"]);
        unset($arr_data["imageAlt"]);

        if ($arr_data['timerActive']) {
            $timerStart = new DateTime($arr_data['timerStart']);
            $timerEnd = new DateTime($arr_data['timerEnd']);

            $arr_data['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $arr_data['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $arr_data['timerStart'] = 0;
            $arr_data['timerEnd'] = 0;
        }


        $arr_data['timerActive'] = (int)$arr_data['timerActive'];
        $arr_data['dateUpdate'] = time();

        SDbQuery::table($this->tableName)->enableMessages()->where([$this->id_field => $idSlide])->update($arr_data);
    }

    /**
     *
     */
    public function actionPublished()
    {
        SDbQuery::table($this->tableName)->enableMessages()->where([$this->id_field => $this->getPostDataInt()])->update(["isActive" => 1, "dateUpdate" => time()]);
    }

    /**
     *
     */
    public function actionUnpublished()
    {
        SDbQuery::table($this->tableName)->enableMessages()->where([$this->id_field => $this->getPostDataInt()])->update(["isActive" => 0, "dateUpdate" => time()]);
    }

    /**
     *
     */
    public function actionDelete()
    {
        $arrData = explode(",", $this->getPostData());
        $countRefreshed = 0;

        foreach ($arrData as $d) {
            $countRefreshed++;
            SDbQuery::table($this->tableName)->where([$this->id_field => $d])->delete();
            SDbQuery::table($this->tableName_i18n)->where([$this->id_field => $d])->delete();
        }

        if ($countRefreshed) {
            Message::I()->Success(" {$countRefreshed} items was deleted");
        }
    }

}

/*

class SliderWidget extends Widget
{

    protected $slides;

    public function init()
    {
        $this->slides = new SliderModel();
    }


    public function getContent()
    {
        $query = new QuerySimple();
        $query->Select(array("slider" => array("*")));

        $title = "Slider";

        $this->SetProperty("name", $title)
            ->SetDefaultOrder("id", false)
            ->Add(new ButtonAppend())
            ->Add($query);

        $table = new TableMass(
            new TableColumn("id", "ID", 40, "center"), //
            new TableColumn("npp", "#", 30, "center"),
            new TableColumn("name", "Name"), //
            new TableColumnDate("timerStart", "Start time"), //
            new TableColumnDate("timerEnd", "End time"), //
            new TableColumnDateCreate(),
            new TableColumnDateUpdate(),
            new ButtonEdit(), //
            new ButtonDelete()
        );


        $table->SetPrimary("id");

        $this->Add($table)
            ->Add(new ButtonDelete("Delete", "Delete", array("script" => "TableAct")));

        return $this;
    }

    /
    public function PostProcessData()
    {

        foreach ($this->data as &$val) {
            $text = array();
            if ($val["isActiveEu"])
                $text[] = "<strong style='color:#0099FF'>EU</strong>";
            if ($val["isActiveUs"])
                $text[] = "<strong style='color:#FF005A'>US</strong>";
            $textImplode = implode(" + ", $text);
            $textImplode = ($textImplode) ? $textImplode : "<span style='color:#777'>off</span>";
            $val["name"] .= ($val["name"]) ? " { {$textImplode} }" : "";

            $val["name"] = trim($val["name"], " +");
        }
        return $this;
    }

    public function actionAddReady()
    {
        $form = new FormStatic("Adding a new slide");
        $form->assignParent($this)->CloneAjax()->CloneState();

        $gr = new GrouperRow("Main", NULL, array("show" => true));

        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputText("Name", "name"));
        $grCC = new GrouperCheckboxRow("Enable timer", "timerActive", array("titleWidth" => 160, "columnsWidth" => array(300, 300)));
        $gr->Add($grCC);

        $grCC->Add(new InputDate("Start date the timer", "timerStart", array('requirements' => '')));
        $grCC->Add(new InputDate("Finish date the timer", "timerEnd", array('requirements' => '')));

        $gr->Add(new InputCheckbox("Test mode", "testMode"));
        $gr->Add(new InputCheckbox("Is Active", "isActiveEu"));
        $gr->Add(new InputText("Url", "urlEu"));
  $gr->Add(new InputUpload("Image path", "imagePathEu", array("file" => PATH_DS . "admin/uploader/cms/eu",
            "requirements" => "JPG/PNG, 1200x400 px.", "server" => "eu"), ""));

        $gr->Add(new InputText("Image alt", "imageAltEu"));
        $gr->Add(new InputTextAreaEditor("Text", "textEu"));


        $form->Add($gr);
        $form->Add(
            new ButtonAdd(),
            new ButtonCancel()
        );
        $form->Draw();

        exit();
    }


    public function actionAddFinish()
    {
        $data = $this->getPostDataJson();

        if ($data['timerActive']) {
            $timerStart = new DateTime($data['timerStart']);
            $timerEnd = new DateTime($data['timerEnd']);

            $data['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $data['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $data['timerStart'] = 0;
            $data['timerEnd'] = 0;
        }
        $data['dateCreate'] = time();
        $data['dateUpdate'] = time();
        if ($this->slides->insert($data)) {
            Message::I()->Info("Slide successfully added");
        } else {
            Message::I()->Error("" . $this->slides->getLastError());
        }

    }


    public function actionUpdateReady()
    {
        $data = $this->slides->findById($this->getPostDataInt());
        $form = new FormStatic("Edit slide " . $data['id'], array($data));
        $form->assignParent($this)->CloneAjax()->CloneState();

        $ARR = Db::Select("slider", "id=" . $data['id']);
        if (isset($ARR[0]['timerActive']) && $ARR[0]['timerActive']) {
            $ARR[0]['timerStart'] = date('d.m.Y', $ARR[0]['timerStart']);
            $ARR[0]['timerEnd'] = date('d.m.Y', $ARR[0]['timerEnd']);
        }
        $callback_data_eu = ($ARR[0]["imagePathEu"]) ? '<img src="' . PATH_DS . 'uploads/slider/eu' . DS . $ARR[0]["imagePathEu"] . '_50_30.jpg">' : "";
        $callback_data_us = ($ARR[0]["imagePathUs"]) ? '<img src="' . PATH_DS . 'uploads/slider/us' . DS . $ARR[0]["imagePathUs"] . '_50_30.jpg">' : "";

        $gr = new GrouperRow("Main", NULL, array("show" => true));
        $gr->Add(new InputNumeric("Consecutive number", "npp"));
        $gr->Add(new InputText("Name", "name"));
        $grCC = new GrouperCheckboxRow("Enable timer", "timerActive", array("titleWidth" => 160,
            "active" => $ARR[0]['timerActive'],
            "columnsWidth" => array(300, 300)));
        $gr->Add($grCC);

        $grCC->Add(new InputDate("Start date the timer", "timerStart", array('requirements' => '',
            'dataForce' => $ARR[0]['timerStart'])));

        $grCC->Add(new InputDate("Finish date the timer", "timerEnd", array('requirements' => '',
            'dataForce' => $ARR[0]['timerEnd'])));

        $gr->Add(new InputCheckbox("Test mode", "testMode"));
        $form->Add($gr);

        $gr = new GrouperRow("EU Store", NULL, array("show" => true));

        $gr->Add(new InputCheckbox("Is Active", "isActiveEu"));
        $gr->Add(new InputText("Url", "urlEu"));
        $gr->Add(new InputText("Image alt", "imageAltEu"));
        $gr->Add(new InputTextAreaEditor("Text", "textEu"));
        $gr->Add(new InputUpload("Image path", "imagePathEu", array("file" => PATH_DS . "admin/uploader/cms/eu",
            "requirements" => "JPG/PNG, 1200x400 px.", "server" => "eu",
            "callback_data" => $callback_data_eu), ""));

        $form->Add($gr);

        $gr = new GrouperRow("US Store", NULL, array("show" => true));
        $gr->Add(new InputCheckbox("Is Active", "isActiveUs"));
        $gr->Add(new InputText("Url", "urlUs"));
        $gr->Add(new InputText("Image alt", "imageAltUs"));
        $gr->Add(new InputTextAreaEditor("Text", "textUs"));

        $gr->Add(new InputUpload("Image path", "imagePathUs", array("file" => PATH_DS . "admin/uploader/cms/us",
            "requirements" => "JPG/PNG, 1200x400 px.", "server" => "us",
            "callback_data" => $callback_data_us), ""));

        $form->Add($gr);
        $form->Add(
            new InputVar("id", "id"),
            new ButtonSave(),
            new ButtonCancel()
        );
        $form->Draw();

        exit();
    }


    public function actionUpdateFinish()
    {
        $data = $this->getPostDataJson();
        $data['dateCreate'] = time();
        $data['dateUpdate'] = time();
        if ($data['timerActive']) {
            $timerStart = new DateTime($data['timerStart']);
            $timerEnd = new DateTime($data['timerEnd']);

            $data['timerStart'] = $timerStart->modify('+2 hour')->format('U');
            $data['timerEnd'] = $timerEnd->modify('+2 hour')->format('U');
        } else {
            $data['timerStart'] = 0;
            $data['timerEnd'] = 0;
        }
        $id = $data[$this->slides->getId()];
        unset($data[$this->slides->getId()]);
        if ($this->slides->updateById($id, $data)) {
            Message::I()->Info("Slide updated");
        } else {
            Message::I()->Error("" . $this->slides->getLastError());
        }

    }

    public function actionDelete()
    {
        $arrData = explode(",", $this->getPostData());
        $countRefreshed = 0;
        foreach ($arrData as $d) {
            $countRefreshed++;
            $arr_del = Db::Select("slider", "path LIKE '%/{$d}/%'");
            if (!empty($arr_del)) {
                DbMulti::Delete("slider", "id", $arr_del);
            } else {
                Db::Delete("slider", "id = {$d}");
            }
        }
        Message::I()->Success(" {$countRefreshed} slides was deleted");
    }

//    public function actionPublished() {
//        if ($this->slides->updateById($this->getPostDataInt(), array('showSlide' => 1))) {
//            Message::I()->Info("Slide enabled");
//        } else {
//            Message::I()->Error("" . $this->slides->getLastError());
//        }
//    }
//
//    public function actionUnpublished() {
//        if ($this->slides->updateById($this->getPostDataInt(), array('showSlide' => 0))) {
//            Message::I()->Info("Slide disabled");
//        } else {
//            Message::I()->Error("" . $this->slides->getLastError());
//        }
//    }


}
*/
