<?php

class GaWidget extends Widget
{

    public function DrawBody()
    {

        $file = PATH_PUBLIC . DS. PATH_TPL . DS . PATH_THEME . DS . "phtml/cms/blocks/ga.phtml";

        //----
        switch ($this->getPostAction())
        {
            case 'AddFinish' :

                $data = $this->getPostDataJson();
                file_put_contents(PATH_REAL.DS.$file, $data["code"]);
                Message::I()->Success("Code was updated successfully");
                break;
        }

        $data = file_get_contents(PATH_REAL.DS.$file);


        $form = new FormStatic("Google Analytics code", array(array("code" => $data)));
        $form->Add(new Ajax(PATH_DS . "admin/widgets/cms/plugins/ga", false, "html", "ajaxGa"));
        $form->Add(new InputTextReadonly("File", "file", false, $file));
        $form->Add(new InputTextArea("Google code", "code", array("height" => 300)));



        //---
        $form->Add(new ButtonAdd(NULL, "Save"));


        ?><div style="width:600px;">
        <?php $form->Draw();?>
        <?php Message::I()->Draw(); ?>
        </div>

        <?php
        return $this;


    }
}



