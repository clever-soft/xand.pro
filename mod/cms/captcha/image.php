<?php
include_once("../../../lib/class.Xand.php");

class ControlCaptchaBig extends ControlCaptcha
{	 
	public static function SetCode()
	{
		$_SESSION['captcha'] = rand(1000,9999).'';	
		return $_SESSION['captcha'];
	}	
  
	//--- 	
	public static function DrawImage()
	{	
		
		$str = self::SetCode();	
		$x0 = 80; 
		$y0 = 41; 
		$ks = 1; 
		
		$img = imagecreate($x0, $y0);
		//imageantialias ($img,true);
		
		$backgroundColor = ImageColorAllocate($img, 250, 250, 250); 
		$blackColor		 = ImageColorAllocate($img, 210, 210, 210); 
		$fontColor       = ImageColorAllocate($img, 0, 0, 0);		
		$shiftX          = 8;
		
		//imagettftext($img, 24, 0 , 0, 24 , $blackColor, PATH_REAL.DS."lib/pack.Form/captcha/cap.ttf", rand(1000000, 99999999));
		 
		
		for($k=0; $k<4; $k++)
		{ 	
			$ugol=rand(-20,20);	
			imagettftext($img, 20, $ugol , $k*16+$shiftX, 28 ,	$fontColor , PATH_REAL.DS."lib/pack.Form/captcha/cap.ttf", $str[$k]);
			
		}		
		
		 
		ImageLine($img, $x0-1 , 0 , $x0-1 , $y0 ,    $blackColor);
		
		
		header("Content-type: image/png");
		ImagePng($img);
		ImageDestroy($img);
		
	}	
	 
} 

ControlCaptchaBig::DrawImage();

?>