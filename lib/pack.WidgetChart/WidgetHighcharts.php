<?php

/**
 **/

class WidgetHighcharts extends ClassBase
{	
	protected $eid;
	public  $series;
	
	public  $niceColors = array('#4486ff', '#7bdc34', '#34d6dc', '#b634dc', '#dc4c34', '#ddcc00');
	 
	
	//---
	function __construct($eid)
	{  		
		$this->eid = $eid; 
		$this->setProperty("title", "")
	 		 ->setProperty("height", 360)
			 ->setProperty("step", false)
			 ->setProperty("type", "spline")  
			 ->setProperty("dataReverse", false)
			 ->setProperty("dateColumnName", "dateU");
		
	}
		
	//---
	public function SetSeries($data, $columns)
	{ 	
		$iter = 0;
		 
		foreach($columns as $columnKey => $columnName)
		{
			$columnKeyArr = array();
			if(strpos($columnKey, ".") !== false)
			{
				$columnKeyArr = explode(".", $columnKey);
			} 
			
			$colData = array();
			
			foreach($data as $d)
			{     
				$jTime = $d[$this->properties["dateColumnName"]]."000";
				$val   = 0;
				 
				if(! empty($columnKeyArr))
				{
					if(isset($d[$columnKeyArr[0]][$columnKeyArr[1]]))	  
						$val = (float) $d[$columnKeyArr[0]][$columnKeyArr[1]];
				}
				else
				{
					if(isset($d[$columnKey]))	
						$val = (float) $d[$columnKey];
				}

			
				$colData[] = array((float) $jTime, $val);  
	 
				 
				
			} 
			
			if($this->properties["dataReverse"])
				$colData = array_reverse($colData);


			$this->series[]  = array("name"  => $columnName,
									 "dataGrouping" => array("enabled" => true),
									 "type"  => $this->properties["type"],
									 "step"  => $this->properties["step"],
									 "color" => $this->niceColors[$iter],
									 "data"  => $colData);
									 
			
			$iter++;
		} 
	} 
	
	 
}
?>