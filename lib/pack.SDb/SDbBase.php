<?php

/**
 * Class SDbBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class SDbBase
{

    public static $connections;
    public static $current = "db";
    public static $errors = [];
    public static $rowAffected = 0;


    const SQL_QUOTE = "`";

    /**
     * @param $key
     */
    public static function SetDb($key)
    {
        self::$current = $key;
    }


    /**
     * @return bool|string
     */
    public static function getLastError()
    {
        return (!empty(self::$errors))
            ? implode(BR, self::$errors)
            : false;
    }

    /**
     * @return mixed
     */
    public static function connect()
    {
        if (!isset(self::$connections[self::$current])) {

            if (!isset(Xand::$db[self::$current]))
                die ('Configuration not found');

            try {

                self::$connections[self::$current] = new \PDO(
                    'mysql:dbname=' . Xand::$db[SDbBase::$current]['name'] . ";" .
                    'host=' . Xand::$db[SDbBase::$current]['host'] . ";charset=utf8;",

                    Xand::$db[SDbBase::$current]['user'],
                    Xand::$db[SDbBase::$current]['password']
                );

                $stmt = self::$connections[self::$current]->prepare("SET sql_mode=?");
                $stmt->execute([implode(',', [
                    //'STRICT_TRANS_TABLES',
                    'NO_ZERO_IN_DATE',
                    'NO_ZERO_DATE',
                    'ERROR_FOR_DIVISION_BY_ZERO',
                    'NO_AUTO_CREATE_USER',
                    'NO_ENGINE_SUBSTITUTION'
                ])]);

                self::execute('SET NAMES utf8;');

                self::$connections[self::$current]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connections[self::$current]->setAttribute(PDO::ATTR_PERSISTENT, true);


            } catch (PDOException $e) {
                die ('DB Error: ' . $e->getMessage());
            }

        }

    }

    /**
     * @param $query
     * @param array $arguments
     * @return mixed
     */
    public static function query($query, $arguments = [])
    {
        if (!is_array($arguments)) {
            $arguments = func_get_args();
            array_shift($arguments);
        }

        return self::execute($query, $arguments);
    }


    /**
     * @param $query
     * @param array $holders
     *
     * @return mixed
     */
    public static function execute($query, $holders = [])
    {
        if (!isset(self::$connections[self::$current])) {
            self::connect();
        }

        if (DEV_DB_LOG)
            $time = microtime(true);

        $query = str_replace("%%", "%", $query);
        $stmt = self::$connections[self::$current]->prepare($query);


        try {
            if (!empty($holders)) {

                foreach ($holders as $key => $value) {

                    if (is_int($value))
                        $param = PDO::PARAM_INT;
                    elseif (is_float($value))
                        $param = PDO::PARAM_STR;
                    elseif (is_bool($value))
                        $param = PDO::PARAM_BOOL;
                    elseif (is_null($value))
                        $param = PDO::PARAM_NULL;
                    elseif (is_string($value))
                        $param = PDO::PARAM_STR;
                    else
                        $param = FALSE;

                    $paramNumber = $key + 1;

                    if ($param !== false) {
                        $stmt->bindValue($paramNumber, $value, $param);
                    }

                }
            }

            $stmt->execute();
            self::$rowAffected = $stmt->rowCount();

        } catch (Exception $e) {

            Debug::logMsg(
                PHP_EOL . $e->getMessage() .
                PHP_EOL . $query .
                PHP_EOL . Debug::smartBacktrace() .
                PHP_EOL . print_r($holders, true) .
                PHP_EOL . PHP_EOL,
                "sdb/errors-" . date("Y-m-d") . ".log"
            );

            self::$errors[] = $e->getMessage();
        }

        if (DEV_DB_LOG) {
            $time = microtime(true) - $time;
            Debug::PushDbLog($query, round($time, 4));
        }

        return $stmt;
    }

    /**
     * @return mixed
     */
    public static function getLastInsertedId()
    {
        return self::$connections[self::$current]->lastInsertId();
    }


}