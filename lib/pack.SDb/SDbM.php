<?php

/**
 * Class SDbM
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class SDbM extends SDbBase
{

    public static $cacheTime = 86400;

    /**
     * @param $query
     * @param array $arguments
     *
     * @return bool|mixed
     */
    public static function row($query, $arguments = [])
    {

        $key = self::getKey($query, $arguments);
        $data = MCache::get($key);

        if ($data !== false) {
            return $data;
        }

        $arr = SDb::row($query, $arguments);
        MCache::set($key, $arr, self::$cacheTime);

        return $arr;

    }

    /**
     * @param $query
     * @param array $arguments
     *
     * @return bool|mixed
     */
    public static function rows($query, $arguments = [])
    {

        $key = self::getKey($query, $arguments);
        $data = MCache::get($key);

        if ($data !== false) {
            return $data;
        }

        $arr = SDb::rows($query, $arguments);
        MCache::set($key, $arr, self::$cacheTime);

        return $arr;

    }


    /**
     * @param $query
     * @param array $arguments
     *
     * @return bool|mixed
     */
    public static function cell($query, $arguments = [])
    {

        $key = self::getKey($query, $arguments);
        $data = MCache::get($key);

        if ($data !== false) {
            return $data;
        }

        $arr = SDb::cell($query, $arguments);
        MCache::set($key, $arr, self::$cacheTime);

    }


    /**
     * @param $query
     * @param array $arguments
     *
     * @return string
     */
    protected static function getKey($query, $arguments = [])
    {
        $query .= (is_array($arguments)) ? implode(",", $arguments) : $arguments;
        return md5($query);
    }


}