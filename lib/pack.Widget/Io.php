<?php

/**
 *
 * @DEPRECATED
 * функционал перенесен в Post
 * валидация в моделях или формах (переспектива)
 *
 **/
class Io
{

    public static $p;
    public static $pPath;
    public static $pData;
    public static $pDataJson;
    public static $pDataInt;

    public static $pAct;


    //---  VALIDATOR
    public static function ValidMail($input)
    {
        return filter_var($input, FILTER_VALIDATE_EMAIL);
    }

    public static function ValidText32($input)
    {
        return (mb_strlen($input, "UTF-8") > 1 && mb_strlen($input, "UTF-8") < 33) ? true : false;
    }

    public static function ValidText2to32($input)
    {
        return (mb_strlen($input, "UTF-8") > 1 && mb_strlen($input, "UTF-8") < 33) ? true : false;
    }

    public static function ValidText2to64($input)
    {
        return (mb_strlen($input, "UTF-8") > 1 && mb_strlen($input, "UTF-8") < 64) ? true : false;
    }

    public static function ValidText2to16($input)
    {
        return (mb_strlen($input, "UTF-8") > 1 && mb_strlen($input, "UTF-8") < 17) ? true : false;
    }

    public static function ValidText1200($input)
    {
        return (mb_strlen($input, "UTF-8") > 1 && mb_strlen($input, "UTF-8") < 1200) ? true : false;
    }

    //---  POST HELPER
    public static function PostBool($key)
    {
        return (isset($_POST[$key])) ? (!empty($_POST[$key])) ? true : false : false;
    }

    public static function PostNumeric($key)
    {
        return (isset($_POST[$key])) ? (int)$_POST[$key]
            : -1;
    }

    public static function PostTextStrict($key)
    {
        $input = (isset($_POST[$key])) ? $_POST[$key] : "";
        $input = preg_replace("/[^A-ZА-ЯЁ0-9 ]+/ui", "", $input);
        return trim($input);
    }

    public static function PostText($key)
    {
        $input = (isset($_POST[$key])) ? $_POST[$key] : "";
        $input = preg_replace("/[^A-ZА-ЯЁ0-9,#-_.@ ]+/ui", "", $input);
        return trim($input);
    }

    public static function PostEmail($key)
    {
        $input = (isset($_POST[$key])) ? $_POST[$key] : "";
        $input = preg_replace("/[^A-Z0-9-_.@ ]+/ui", "", $input);
        return trim($input);
    }

    //---  POST HELPER
    public static function StripSlashesArray($array)
    {
        return is_array($array) ? array_map('Io::StripSlashesArray', $array) : stripslashes($array);
    }

    /**
     *
     */
    public static function Fill()
    {
        //  $_POST 			   = self::StripSlashesArray($_POST);
        self::$pPath = (isset($_POST['PATH'])) ? $_POST['PATH'] : false;
        self::$pAct = (isset($_POST['ACT'])) ? preg_replace("/[^A-ZА-ЯЁ]+/ui", "", $_POST['ACT']) : false;
        self::$pData = (isset($_POST['DATA'])) ? $_POST['DATA'] : false;
        self::$pDataInt = (self::$pData) ? (int)self::$pData : false;
        self::$pDataJson = (isset($_POST['DATA'])) ? (substr_count($_POST['DATA'], "{") > 0)
            ? self::JsonDecode($_POST['DATA']) : false
            : false;
        self::$p = $_POST;

    }


    /**
     * @param $input
     *
     * @return array
     */

    public static function JsonDecode($input)
    {

        $input = str_replace("^", "%", $input);

        $data = json_decode($input);
        $ARR = array();

        foreach ($data as $key => $val) {
            $val = urldecode($val);
            $ARR[$key] = self::ContentDecode($val);
        }
        return $ARR;
    }

    /**
     * @param $input
     *
     * @return string
     */
    public static function ContentDecode($input)
    {
        $trtr = array(

            "#AM#" => "&",
            "#OK#" => "'",
            "#DK#" => '"',
            "#PL#" => "+",
            "#SL#" => "\\",
            "#SLO#" => "/",
            "#BR#" => "\n",

        );

        return strtr($input, $trtr);
    }

    /**
     *
     */
    public static function Dump()
    {
        $dump = array();
        $dump['postPath'] = self::$pPath;
        $dump['postData'] = self::$pData;
        $dump['postDataInt'] = self::$pDataInt;
        $dump['postDataJson'] = self::$pDataJson;
        $dump['postAct'] = self::$pAct;

        Debug::Dump($dump);
    }


}
