<?php

/**
 * Class StateRestore
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class StateRestore extends RequestParams
{
    public $name = "state";

    /*
	 //----
	 public function Update()
	 {

		if(! isset($_SESSION[AUTH_KEY])) return;   
	 	$hash = md5($_SESSION[AUTH_KEY]["idUser"].$this->parent->ajax->controller);

		$data = serialize($this->parent->nav->param);
		$query = "INSERT INTO users_states
				  SET hash = '{$hash}', 
					  val  = '{$data}'  
				  ON DUPLICATE KEY UPDATE val = '{$data}'";	
				  				  
		Db::Action($query);	 			

	 }	 
	 
	 
	 //----
	 public function UpdateExt($path, $data = array())
	 {
		 if(! isset($_SESSION[AUTH_KEY])) return;

		 $path    = "/".trim($path, "#");
		 $pathExp = explode("/", $path); 
		 $zone    = array_pop($pathExp);	     
 		 $path    = implode("/", $pathExp)."/zones/zone.".$zone.".php";
		 $hash    = md5($_SESSION[AUTH_KEY]["idUser"].$path);  	 
		 
		 $nav    = DbOnce::Field("users_states", "val", "hash = '{$hash}'");
		 $navUns = array();
		 if($nav) 
			$navUns = unserialize($nav);   
		 
		 $navUns = array_replace($navUns, $data); 
		 $nav    = serialize($navUns);
		 $query = "INSERT INTO users_states
								SET hash = '{$hash}', 
									val  = '{$nav}'  
								ON DUPLICATE KEY UPDATE val = '{$nav}'";					  
		 Db::Action($query);
	 }

    */

    /**
     *
     */
    public function Reset()
    {
        if (!isset($_SESSION[AUTH_KEY])) return;
        $hash = md5($_SESSION[AUTH_KEY]["idUser"] . $this->parent->properties['url']);

        $this->parent->nav->param =
            array(
                'page' => 1,
                'ppage' => false
            );

        SDbQuery::table("users_states")->where(["hash" => $hash])->delete();

    }

    /**
     *
     */
    public function Read()
    {
        if (!isset($_SESSION[AUTH_KEY])) return;

        $url = str_replace("filters", "widgets", $this->parent->properties['url']);
        $hash = md5($_SESSION[AUTH_KEY]["idUser"] . $url);
        //$data = DbOnce::Field("users_states", "val", "hash = '{$hash}'");
        $data = implode(SDbQuery::table("users_states")->where(["hash" => $hash])->selectRow(["val"]));

        if ($data) {
            $dataUns = unserialize($data);
            $dataUns['page'] = 1;
            $this->parent->nav->param = array_replace(
                $this->parent->nav->param,
                $dataUns
            );


        }

        //  echo $this->parent->properties['url'];
        //  echo "Read";
    }


    /**
     * 
     */
    public function Write()
    {
        if (!isset($_SESSION[AUTH_KEY])) return;

        if ($this->getPostAction() == "filtersReset")
            $this->Reset();

        $url = str_replace("filters", "widgets", $this->parent->properties['url']);
        $hash = md5($_SESSION[AUTH_KEY]["idUser"] . $url);
        $data = serialize($this->parent->nav->param);

        $query = "INSERT INTO users_states
                    SET hash = '{$hash}',
                    val  = '{$data}'
                    ON DUPLICATE KEY UPDATE val = '{$data}'";

        //Db::Action($query);
        SDb::execute($query);

        // echo $this->parent->properties['url'];
        //echo "Write";
    }
}
