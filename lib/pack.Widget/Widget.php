<?php

/**
 * Class Widget
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Widget extends WidgetBase
{
    /**
     * Widget constructor.
     *
     * @param $widgetName
     * @param array $properties
     */
    public function __construct($widgetName, $properties = array())
    {

        $this->checkFullVersion();

        $this->uid = "ajax" . $widgetName;

        // Defaults
        $this->setProperty("messageEnable", true)
            ->setProperty("ajaxAllowed", true)
            ->setProperty("url", false)
            ->setProperty("global", true)
            ->setProperty("tplName", "main")
            ->setProperty("tplFolder", "tpl/tpl-lib/phtml/widget")
            ->setProperty("init", false);


        if (!empty($properties)) {
            foreach ($properties as $key => $val) {
                $this->properties[$key] = $val;
            }
        }

        // Nav
        $this->nav = new Nav();
        $this->nav->setUid($this->uid);
        $path = $this->getPostPath();

        // StateRestore
        $this->stateRestore = new StateRestore();
        $this->stateRestore->assignParent($this);

        // AJAX
        if ($this->properties["url"]) {
            $this->ajax = new Ajax(PATH_DS . $this->properties["url"], false);
            $this->ajax->setExemplarId($this->uid);
            $this->ajax->setZone($this->uid);
            $this->ajax->assignParent($this);
        }

        // AJAX
        if ($this->properties["init"]) {
            if ($this->properties["ajaxAllowed"]) {
                $this->ajax->createJsExemplar();

                if (!$this->properties["global"]) {
                    $this->ajax->setNavigation("external");
                }
            }

            $this->stateRestore->Read();

        } else {
            if ($path) {
                $this->nav->parse($path);
            }

            $this->stateRestore->Write();
        }

    }


    /**
     * @return $this
     */
    public function RenderFilters()
    {
        if (!isset($this->properties["filtersDisabled"]) && isset($this->table))
            $this->Add(new ActionTableColumns("Table columns"));

        return $this;
    }

    /**
     * @return $this
     */
    public function RenderBody()
    {

        // if(! isset($this->nav->param['order']))
        //     $this->nav->param['order'] = false;

        // if(! isset($this->nav->param['desc']))
        //    $this->nav->param['desc'] = false;

        if ($this->table && !$this->table->columns) {
            if (isset($this->query)) {
                $this->table->findColumns($this->query->tables[0]);
            } else {
                Message::I()->Error("The table can not produce autofill column Query object without")->E();
                exit();
            }
        }

        if (!isset($this->ppage)) {
            $ppage = new Ppage();
            $ppage->assignParent($this);
            $this->Add($ppage);
        } else {
            $this->ppage->assignParent($this);
        }


        if (!isset($this->pager)) {
            $pager = new Pager();
            $pager->assignParent($this);
            $this->Add($pager);
        } else {
            $this->pager->assignParent($this);
        }

        if ($this->query) {

            $criteria = $this->extractCriteria();

            if (!empty($criteria))
                foreach ($criteria as $c) {
                    $this->query->addFilter($c);
                }


            $data = $this->query->Process()->Execute();

            if ($data) {
                foreach ($data as $d) {
                    $this->data[$d[$this->table->primary]] = $d;
                }
            }

            if ($this->data != null) {
                if (method_exists($this, "postProcessData")) {
                    $this->postProcessData();
                }
            }
        }
        return $this;
    }

    /**
     *
     */
    public function DrawFilters()
    {

        if (!$this->properties["init"]) return;

        if (!isset($this->properties["filtersDisabled"]) && !empty($this->actions)) {

            echo "<script>";
            echo "GF.Init('{$this->ajax->eid}');";
            echo "</script>";


            foreach ($this->actions as $key => $action) {
                $action->Draw();
            }

            echo "<script>";
            echo "GF.Draw();";
            echo "</script>";

        }

    }

    /**
     *
     */
    public function DrawBody()
    {

        $tpl = new TemplateCms($this->getProperty("tplName"), $this->getProperty("tplFolder"));
        $tpl->assign("widget", $this)
            ->Load();
    }

    /**
     * @return $this
     */
    public function getContent()
    {
        return $this;
    }

    /**
     * @return $this
     */
    public function postProcessData()
    {
        return $this;
    }

    /**
     * @param $args0
     * @param $args1
     * @param bool $args2
     * @param bool $args3
     * @param bool $args4
     * @param bool $args5
     *
     * @return string
     */
    public function Link($args0, $args1, $args2 = false, $args3 = false, $args4 = false, $args5 = false)
    {
        $args2 = ($args2) ? "style='{$args2}'" : "";     // style
        $args3 = ($args3) ? "class='{$args3}'" : "";    // class
        $args4 = ($args4) ? "id='{$args4}'" : "";    // id
        $args5 = ($args5) ? $args5 : "";    // attr

        return "<a href=\"\" onclick=\"{$args0} return false;\" {$args4} {$args3} {$args2} {$args5}>{$args1}</a>";
    }

    /**
     * @param string $href
     * @param string $title
     *
     * @return string
     */
    public function urlAdmin($href = "#", $title = "Untitled")
    {
        return "<a href=\"{$href}\" onclick=\"CAdminMenu.Click(this); return false;\">{$title}</a>";
    }


    /**
     *
     */
    public function init()
    {

    }

    /**
     * @param string $action
     */
    public function triggerEvent($action)
    {

        if (isset($this->eventHandlers[$action])) {
            foreach ($this->eventHandlers[$action] as $cb) {
                if (!is_callable($cb)) {
                    continue;
                }
                $cb($this->getPostData());
            }
        }
    }

    /**
     * @param $action
     * @param $cb
     *
     * @return bool
     */
    protected function on($action, $cb)
    {
        if (!$cb instanceof Closure) {
            return false;
        }
        if (isset($this->eventHandlers[$action])) {
            $this->eventHandlers[$action][] = $cb;
        } else {
            $this->eventHandlers[$action] = [$cb];
        }
    }


    /**
     * @param string $string
     *
     * @return array
     */
    public function extractMassID($string = "")
    {
        if (!$string)
            $string = $this->getPostData();

        $result = [];
        $raw = explode(",", $string);

        foreach ($raw as $r) {
            if (!is_numeric($r)) continue;
            if ($r == 0) continue;

            $result[] = $r;
        }

        return $result;
    }


    /**
     * @param PdoQueryBase $query
     * @param array $mappings массив переименований колонок(например если нужно добавить таблицу)
     *
     * @return PdoQueryBase
     */
    protected function setParamsAsFilters($query, $mappings = array())
    {
        $params = $this->getParams();
        foreach ($params as $key => $val) {
            if (isset($mappings[$key])) {
                $key = $mappings[$key];
            }
            $query->addFilter(array($key . "=?", array($val)));
        }
        return $query;
    }

    /**
     * @return SimpleXMLElement
     */
    private
    function GetXml()
    {
        $fileXml = PATH_REAL . DS . "conf.xml";
        if (!file_exists($fileXml)) {
            exit("Conf file not found. Please reinstall CMS");
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;
    }

    protected function checkFullVersion($mes = true)
    {

        $xml = $this->GetXml();

        $version = (string)$xml->group[2]->setting->version;

        $serverVersion = json_decode(Update::getUpdateJson(), true);

        if (isset($serverVersion['full']) && !empty($serverVersion['full'])) {
            usort($serverVersion['full'], ["Update", "sortversion"]);
            $serverVersion = array_pop($serverVersion['full']);
            if (isset($version) && !empty($version)) {
                $localArr = explode(".", $version);
                $serverArr = explode(".", $serverVersion);

                if ($localArr[0] != $serverArr[0]) {
                    return false;
                }

                if ($localArr[1] > $serverArr[1]) {
                    return false;
                }
                if ($localArr[1] == $serverArr[1]) {
                    if ($localArr[2] >= $serverArr[2]) {
                        return false;
                    }
                }
            }
        }

        if ($mes === true) {
            Message::I()->Info("Your site is needed <a class='buttonForUpdate' href='/admin#system/configuration/full' data-href='/admin#system/configuration/full' onclick='GoToUpdate($(this));'>update</a>");
        }
        return true;

    }
}