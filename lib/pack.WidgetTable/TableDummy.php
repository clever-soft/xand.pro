<?php

/**
 * Class TableDummy
 *
 * @тип  	 Class
 * @package    Widget
 * @версия   2
 *
 *
 * v2 - Добавлено PostProcessing
 * v3 - Добавлено primary
 *
 * */
class TableDummy extends TableBase {


    //--
    public function Draw() {
        if (empty($this->data))
            $this->data = $this->parent->data;

        if (empty($this->primary))
            Message::Fast("EI|Укажите первичный ключ! (5 параметр конструктора обьекта TableColumn)");


        if (!empty($this->buttons) && $this->buttons[0]->parent == NULL) {
            foreach ($this->buttons as $b)
                $b->assignParent($this->parent);
        }

        //var_dump($this->data);	 
        ?>

        <!-- <div style="max-height:600px;" class="niceScroll">-->


        <?php
        echo " 
			  <div class='correctorTarget' style='display:none;'>
			  <table border='0' class='table table-striped table-bordered table-hover' cellpadding='0' cellspacing='0'>";
        echo "<thead id='thead'>";

        if ($this->properties["numberOfString"]) {
            echo "<th align='center' width='30px'></th>";
        }

        foreach ($this->columns as $item) {

            if (isset($this->parent->objects['state']->param["_ATC"]) &&
                    !isset($this->parent->objects['state']->param["_ATC" . $item->key]))
                continue;

            $this->parent->objects['state']->freeze();
            $class = ($item->key == $this->parent->objects['state']->param['order']) ? "class='tableHeadActive'" : "";
            echo "<th {$class} {$item->align} {$item->width}";

            $this->parent->objects['state']->param['desc'] =
                    ($item->key == $this->parent->objects['state']->param['order']) ?
                    ( ($this->parent->objects['state']->param['desc']) ? false : true ) : true;

            $this->parent->objects['state']->param['order'] = $item->key;

            if ($item->enableSort == true)
                echo "onclick=\"{$this->parent->objects['ajax']->eid}.Go('{$this->parent->objects['state']->toString()}'); return false;\"";
            if ($item->enableSort == false)
                echo "style='cursor:default;'";
            echo ">";

            echo "{$item->name}";
            $class = "";
            if ($item->key == $this->parent->objects['state']->param['order']) {
                $class = ($this->parent->objects['state']->param['desc']) ? "up" : "down";
            }
            //if($item->enableSort == true)
            // echo "<div class='tableHeadOrder tableHeadOrder_{$class}'></div>"; 
            echo "</th>";
            $this->parent->objects['state']->unFreeze();
        }


        if ($this->buttons)
            echo '<th align="center"> Actions </th>';
        echo "</thead>";

        echo "</table></div>";
        echo "<div class='tableScrollWrapper'>
		      <div style='overflow-y:scroll; max-height:600px; margin-top:2px; margin-right: -19px;' class='correctorSource'>
			
			
			  <table border='0' class='table table-striped table-bordered table-hover' cellpadding='0' cellspacing='0' 
			    	 id='table{$this->parent->objects['ajax']->eid}'>";


        $counter = -1;

        if (!empty($this->data))
            foreach ($this->data as $key => $row) {
                $counter++;


                if ($this->isEvent("OnDrawRow"))
                    $this->events["OnDrawRow"]($this, $row);

                $tableRowDeleted = ($this->getProperty("rowDeleted")) ? "tableRowDeleted" : "";
                $class = ($this->getProperty("rowClass")) ? "tableRows " . $this->getProperty("rowClass") : "tableRows";

                echo "<tr class='{$class} {$tableRowDeleted}'>";

                if ($this->properties["numberOfString"]) {
                    $count = ($counter + 1) + ($this->parent->objects["pager"]->currentPage - 1) * $this->parent->objects["pager"]->countRowsOnPage;
                    echo "<td align='center' width='30px'>" . $count . "</td>";
                }
                foreach ($this->columns as $key2 => $item) {

                    if (isset($this->parent->objects['state']->param["_ATC"]) &&
                            !isset($this->parent->objects['state']->param["_ATC" . $item->key]))
                        continue;

                    if (!isset($this->data[$key][$item->key]))
                        $this->data[$key][$item->key] = "-//-";
                    if (method_exists($item, "PostProcessing"))
                        $this->data[$key][$item->key] = $item->PostProcessing($this->data[$key][$item->key]);
                    $class = "";
                    if ($item->key == $this->parent->objects['state']->param['order']) {
                        $class = ($counter % 2 != 0) ? "class='tableRowActiveNc'" : "class='tableRowActiveC'";
                    }



                    echo "<td {$class} {$item->align} {$item->width}>{$this->data[$key][$item->key]}</td>";
                    $class = "";
                }

                if ($this->buttons) {

                    $actionsWidth = 21 * (count($this->buttons)) + 6;
                    $actionsWidth = ($actionsWidth > 60) ? $actionsWidth : 60;

                    echo "<td align='center' width='{$actionsWidth}'>";
                    if ($this->getProperty("enableDrawButtons")) {

                        foreach ($this->buttons as $key => $item) {
                            $primary = (isset($item->properties['primary'])) ? $item->properties['primary'] : $this->primary;
                            if ($item instanceof ButtonBase)
                                $item->SetData($row[$primary])->Draw();
                            else
                                $item->SetData($row[$primary])->SetFieldData($row[$item->GetFieldNameDb()])->Draw();
                        }
                    }
                    echo "</td>";
                }
                echo "</tr>";
            }
        echo "</table></div></div>";
        ?><!--</div>-->


        <script>

        //------		
            function Corrector()
            {
                $(".correctorSource > table > tbody > tr:first").find("td").each(function(index, element) {
                    $(".correctorTarget > table > thead > tr > th").eq(index).attr("width", $(element).css('width'));


                });
            }

        //------	
            $(document).ready(function() {
                $(window).resize(function() {
                    Corrector();
                });

                Corrector();
            });
        //------   	
            $(".tableRows").click(function() {

                $(".tableRows").removeClass("tableRowsSelected");
                $(this).addClass("tableRowsSelected");

            });
        //jQuery('.niceScroll').jScrollPane();

        </script>

        <?php
        if (empty($this->data)) {
            Message::I()->Error($this->properties["messageEmpty"]);
        }
    }

    //--
}
?>