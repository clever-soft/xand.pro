<?php

/**
 * Class ActionSelectInteractive
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   2
 *
 *	v2 - Исправлен метод RefreshSelect
 *
 **/


class ActionSelectInteractive extends ActionBase
{
		
	public function DrawInner()
	{  		
	 
		$this->parent->nav->freeze();
			$this->parent->nav->param[$this->properties['format_ierarchy'][1]]	= 'replace'.$this->uid;
			$this->parent->nav->param['page']	= 1;
			$link = $this->parent->nav->toString();
					
		$this->parent->nav->unFreeze();
				
		echo "<script>				
		function RefreshSelect(obj){			
		   var path 	   = '{$link}';
		   var replacement = $(obj).attr('value');			   
		   path 		   = path.replace('replace{$this->uid}', replacement);					
		  
		   {$this->parent->ajax->zone}.Go(path);
		 }		 		 
		 </script>";  		
		
		$this->properties['sort'] 			= (isset($this->properties['sort'])) 			? $this->properties['sort'] : NULL;	
		$this->properties['filter'] 		= (isset($this->properties['filter'])) 		    ? "AND {$this->properties['filter']}"  : "";	
		$this->properties['format_out']  	= (isset($this->properties['format_out'])) 	    ? $this->properties['format_out']      : array("id","name");
		$this->properties['format_ierarchy']= (isset($this->properties['format_ierarchy'])) ? $this->properties['format_ierarchy'] : array("TABLE","id","idp","path");	
		

		//echo ($this->data);
		
		//var_dump($this->parent->nav->param);
		
		$this->data = (isset($this->parent->nav->param[$this->properties['format_ierarchy'][1]])) ?
					   $this->parent->nav->param[$this->properties['format_ierarchy'][1]] : 0;
					   
	//	var_dump($this->data);
		
		
		$csi = new ControlSelect("",$this->properties['format_ierarchy'][0], array("titleWidth" => 10, "width" => 180));	
		$csi->setProperty("onchange","RefreshSelect(this);");
		
		//Db::$log = "";
			
			$current	= Db::Select($this->properties['format_ierarchy'][0], 
									 $this->properties['format_ierarchy'][1]." = {$this->data} {$this->properties['filter']}",
									 $this->properties['sort']
									 );		
						
			$childrens  = Db::Select($this->properties['format_ierarchy'][0], 
									 $this->properties['format_ierarchy'][2]." = {$this->data} {$this->properties['filter']}",
									 $this->properties['sort']
									 );
		
		//echo Db::$log;
		
	//	var_dump($current);
		//var_dump($childrens);
		
		$up_array = array();
		
		if($current) 
		{
				$arr_node = explode("/", $current[0][$this->properties['format_ierarchy'][3]]);				
				//var_dump($current);
				$iterator = 0;
					
				foreach($arr_node as $val)
				{	
					if($val == "") continue;
										
					$idp  = DbOnce::Field($csi->key,$this->properties['format_ierarchy'][2], $this->properties['format_ierarchy'][1]. " = ".$val);		
					$idp_data = Db::Select($this->properties['format_ierarchy'][0], 	
										   $this->properties['format_ierarchy'][2]. " = {$idp} {$this->properties['filter']}",
										   $this->properties['sort']);								
				
					
									
					foreach($idp_data as $key => $parent)
					{
												
						$csi->properties['noChooseVal'] = $parent[$this->properties['format_ierarchy'][1]];	
						
						$csi->dataArray[$key]['data']  =  $parent[$this->properties['format_ierarchy'][1]];	
						$csi->dataArray[$key]['title'] =  $parent[$this->properties['format_out'][1]];
						$csi->dataArray[$key]['equal'] =  $parent[$this->properties['format_ierarchy'][1]];						
						
						if($val == $csi->dataArray[$key]['equal'])	array_push($up_array, $csi->dataArray[$key]['data']);						
						$csi->properties['noChooseVal']			     = (isset($up_array[$iterator-1])) ?	 $up_array[$iterator-1] : "";
						
					}	
									
					$csi->data = $val;	
					$csi->Draw();
					$csi->dataArray = NULL;
					
					$iterator++;		
				}
		}
				
		if($childrens)
		{
							
				foreach($childrens as $key => $child)
				{
					$csi->dataArray[$key]['data']  =  $child[$this->properties['format_ierarchy'][1]];	
					$csi->dataArray[$key]['title'] =  $child[ $this->properties['format_out'][1]];
					$csi->dataArray[$key]['equal'] =  $child[$this->properties['format_ierarchy'][1]];					
				}				
				
				$csi->properties['noChooseVal'] = "";						
				$csi->data = NULL;				
				$csi->Draw();
		}
	
	}
}		
?>