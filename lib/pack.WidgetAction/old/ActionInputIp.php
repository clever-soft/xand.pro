<?php
class ActionInputIp extends ActionBase
{
	public function GetDefaultRule()
	{
		return new RuleIp();	 
	}
		
	public function DrawInner()
	{	 
		$val = (isset($this->parent->nav->param[$this->rule->field])) ? $this->parent->nav->param[$this->rule->field] : "" ;
		
		$this->parent->nav->freeze();
		 
		
		$this->parent->nav->param[$this->rule->field]	= "replace".$this->uid;
		$this->parent->nav->param['page']	= 1;
		
		echo "<div class='action_input'>";			
		$link = $this->parent->nav->toString();
		unset($this->parent->nav->param[$this->rule->field]);
		$link2 = $this->parent->nav->toString();
		
		?>
      		    <div class='input-prepend' style="margin-top:1px;">             
					   <span class='add-on'><i class='fa fa-search'></i></span>
                       <input class='span2' type='text' id="<?php echo $this->uid; ?>" value="<?php echo $val; ?>"
                        style='width:170px; text-indent:5px; height:22px;' onkeydown="<?php echo $this->uid; ?>Search(event);">
					   
				</div>   
        
    
        <div style="text-align:right">                          
      
        <button class="btn btn-small" onclick="<?php echo $this->uid; ?>Reset(); return false;">Reset</button>  
        <button class="btn btn-small btn-primary" onclick="<?php echo $this->uid; ?>Click(); return false;">OK</button> 
         </div> 
       
        
        <script>
		function <?php echo $this->uid; ?>Reset()
		{
		 	var str  = '<?php echo $link2; ?>';
			<?php echo $this->parent->ajax->zone.".Go(str);"; ?>
		}
		
		function <?php echo $this->uid; ?>Search(ev)
		{
			if(ev.which == 13)	<?php echo $this->uid; ?>Click();
		}
		
		function <?php echo $this->uid; ?>Click()
		{
		  var str = '<?php echo $link; ?>';		  
		  var q = $('#<?php echo $this->uid; ?>').val();		  
			  q = q.replace(/-/g, "#DEF#");	  			  
		  str = str.replace('replace<?php echo $this->uid; ?>', q);
		  <?php echo $this->parent->ajax->zone.".Go(str);"; ?>
		}		
		</script>       
        <?php	
			
		echo "</div>";		
		$this->parent->nav->unFreeze();
	}
}
?>