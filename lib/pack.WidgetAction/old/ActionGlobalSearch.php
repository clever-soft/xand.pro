<?php

/**
 * Class ActionMenuFilter
 *
 * @тип  	 Class
 * @package    Widget
 * @версия   2
 *
 *
 * v2 - Добавлено NO_CHOOSE_PARAMS
 *
 **/

class ActionGlobalSearch extends ActionBase
{
	
	public function Draw()
	{	
 	 	?> 
        <div id="globalSearchWrap">
       		 <div class="input-append"> 
                    <input id="globalSearch"   
                           type="text" 
                           placeholder="Global search" 
                           onkeyup="globalSearchKeyUp(event);"   
                           onkeydown="globalSearchKeyDown(event);" 
                           onfocus="globalSearchOnFocus();" 
                           /> 
                     <span class="add-on"><i class='fa fa-search'></i></span>                                       
          	  </div> 
              <div id="globalSearchOutWrap" onkeydown="globalSearchKeyDown(event);"></div> 
		  </div>             
        <?php  
	}
}
?>