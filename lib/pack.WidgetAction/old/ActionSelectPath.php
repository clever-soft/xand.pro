<?php
class ActionSelectPath extends ActionBase implements ActionUpable
{
	public function Draw()
	{   	
		$this->parent->nav->freeze();
		
		echo "<div class='cnst_filter_head'>".$this->name."</div>";
		echo "<select class='cnst_filter' >";
		
		if(!$this->parent->nav->param)
			$selected = "";
		else 					
			$selected = $this->parent->nav->param[$this->field];
								 
		foreach($this->data as $dkey => $dval)
		{				   
		  					 
		  $sel = ($selected == $dval) ?  "selected='selected'" : "";						 
		  $this->parent->nav->param[$this->field] = $dval;
		  $link = $this->parent->nav->toString();
		  ?> <option value="<?php echo $dval;?>" <?php echo $sel; ?>>	<?php echo $dkey; ?> </option> <?php	 					 					 					 
		}	
			  
		 echo "</select>";
		 echo "<div class='action_separator'></div>";		
		
		 echo "<script>";				
		 echo "$('#".$this->uid."').change(function(){";					
		 echo "Ajax.Go($(this).attr('value'));";
		 echo "});";				
		 echo "</script>";		
		
		$this->parent->nav->unFreeze();
	}
}
?>