<?php

class ActionRangeSinglePickerFilter extends ActionBase implements ActionUpable {

    //-------------------------------------------------------------------------------------
    public function DrawInner() {
        $rand = rand(100, 1000) . rand(100, 1000);
        $startYear = 2012;
        $scale = array();
        for ($y = $startYear; $y <= date('Y'); $y++) {
            if ($y != date('Y')) {
                for ($m = 1; $m <= 12; $m++) {
                    if ($m < 10) {
                        $mn = '0' . $m;
                    } else {
                        $mn = $m;
                    }
                    $stamp = strtotime($y . '-' . $mn . '-01');
                    $scale[$y][$m - 1] = date('M Y', $stamp);
                }
            } else {
                for ($m = 1; $m <= date('n'); $m++) {
                    if ($m < 10) {
                        $mn = '0' . $m;
                    } else {
                        $mn = $m;
                    }
                    $stamp = strtotime($y . '-' . $mn . '-01');
                    $scale[$y][$m - 1] = date('M Y', $stamp);
//                    $scale[] = date('M Y', $stamp);
                }
            }
        }
        $unique = "slider" . $rand;
        $val = (isset($this->parent->nav->param[$this->rule->field])) ? $this->parent->nav->param[$this->rule->field] : "";
        if (!empty($val)) {
            $dates = explode(':', $val);
            $dateArr1 = explode('|', $dates[0]);
            $date_1 = explode('_', $dateArr1[0]);
            $date_2 = explode('_', $dateArr1[1]);
            $date1 = array(
                0 => array(
                    'y' => $date_1[0],
                    'm' => $date_1[1],
                    'd' => $date_1[2]
                ),
                1 => array(
                    'y' => $date_2[0],
                    'm' => $date_2[1],
                    'd' => $date_2[2]
                )
            );

            $dateArr2 = explode('|', $dates[1]);
            $date_3 = explode('_', $dateArr2[0]);
            $date_4 = explode('_', $dateArr2[1]);
            $date2 = array(
                0 => array(
                    'y' => $date_3[0],
                    'm' => $date_3[1],
                    'd' => $date_3[2]
                ),
                1 => array(
                    'y' => $date_4[0],
                    'm' => $date_4[1],
                    'd' => $date_4[2]
                )
            );
        } else {
            $d = new DateTime();
            $dn = new DateTime();
            $d->modify('-1 month');
            $date1 = array(
                0 => array(
                    'y' => $d->format('Y'),
                    'm' => $d->format('m'),
                    'd' => $d->format('d')
                ),
                1 => array(
                    'y' => $dn->format('Y'),
                    'm' => $dn->format('m'),
                    'd' => $dn->format('d')
                )
            );

            $d2 = new DateTime();
            $d2->modify('-2 month');
            $dn2 = new DateTime();
            $dn2->modify('-1 month');
            $date2 = array(
                0 => array(
                    'y' => $d2->format('Y'),
                    'm' => $d2->format('m'),
                    'd' => $d2->format('d')
                ),
                1 => array(
                    'y' => $dn2->format('Y'),
                    'm' => $dn2->format('m'),
                    'd' => $dn2->format('d')
                )
            );
        }
//        Debug::Dump($date1);
//        Debug::Dump($date2);
        $this->parent->nav->freeze();


        $this->parent->nav->param[$this->rule->field] = "replace" . $this->uid;
        $this->parent->nav->param['page'] = 1;

        $link = $this->parent->nav->toString();
        unset($this->parent->nav->param[$this->rule->field]);
        $link2 = $this->parent->nav->toString();
        ?>

        <link rel="stylesheet" type="text/css" href="<?php echo PATH_DS; ?>lib/pack.Jquery/styles/classic.css"/>          
        <script src="<?php echo PATH_DS; ?>lib/pack.Jquery/jQDateRangeSlider-withRuler-min.js"></script> 
        <script src="<?php echo PATH_DS; ?>lib/pack.Jquery/jquery.mousewheel.min.js"></script> 

        <div style="height:100px; overflow:hidden;color:#446384;font-weight: bolder;">
            <div style="height:25px;color:#446384;font-weight: bolder;text-align: center;">
                <label>From: </label>  <label id="rdpDate<?= $rand; ?>1"><?= $date1[0]['y'] . '-' . $date1[0]['m'] . '-' . $date1[0]['d']; ?></label>                   
                <label>To: </label>    <label id="rdpDate<?= $rand; ?>2"><?= $date1[1]['y'] . '-' . $date1[1]['m'] . '-' . $date1[1]['d']; ?></label>                                    
            </div>
            <div id="<?= $unique; ?>1" style="margin-bottom:3px;"></div>
            <input type="hidden" id="<?= $this->uid; ?>" value=""/>


            <div>

                <script>
                    var scale = new Array();
        <?php foreach ($scale as $ksc => $sc): ?>
                        scale['<?= $ksc; ?>'] = new Array();
            <?php foreach ($sc as $k => $dt): ?>
                            scale['<?= $ksc; ?>']['<?= $k; ?>'] = '<?= $dt; ?>';
            <?php endforeach; ?>
        <?php endforeach; ?>
        //                    console.log(scale);
                    $("#<?= $unique; ?>1").dateRangeSlider({
                        wheelMode: "zoom",
                        bounds: {
                            min: new Date(2012, 0, 1),
                            max: new Date()
                        },
                        step: {
                            days: 1
                        },
                        defaultValues: {
                            min: new Date(<?= $date1[0]['y']; ?>, <?= $date1[0]['m'] - 1; ?>, <?= $date1[0]['d']; ?>),
                            max: new Date(<?= $date1[1]['y']; ?>, <?= $date1[1]['m'] - 1; ?>, <?= $date1[1]['d']; ?>)
                        },
                        scales: [{
                                first: function(value) {
                                    return value;
                                },
                                end: function(value) {
                                    return value;
                                },
                                next: function(value) {
                                    var next = new Date(value);
                                    return new Date(next.setMonth(value.getMonth() + 1));
                                },
                                label: function(value) {
                                    return scale[value.getFullYear()][value.getMonth()];
                                }

                            }],
                        formatter: function(val) {
                            var days = val.getDate(),
                                    month = val.getMonth() + 1,
                                    year = val.getFullYear();
                            return days + "/" + month + "/" + year;
                        }});
                    $("#<?= $unique; ?>1").bind("userValuesChanged", function(e, data) {

                        var dateValues = $("#<?= $unique; ?>1").dateRangeSlider("values");
                        var days = dateValues.min.getDate(),
                                month = dateValues.min.getMonth() + 1,
                                year = dateValues.min.getFullYear();
                        $("#rdpDate<?= $rand; ?>1").text(year + "-" + month + "-" + days);
                        var days = dateValues.max.getDate(),
                                month = dateValues.max.getMonth() + 1,
                                year = dateValues.max.getFullYear();
                        $("#rdpDate<?= $rand; ?>2").text(year + "-" + month + "-" + days);
                        $("#<?= $unique; ?>1").dateRangeSlider({
                            range: {
                                min: {days: 1},
                            }});
                        $("#<?= $unique; ?>2").dateRangeSlider({
                            range: {
                                min: {days: 1},
                            }});
                        var diff = dateDiffInDays(dateValues.min, dateValues.max);
                        var dateValues2 = $("#<?= $unique; ?>2").dateRangeSlider("values");
                        var diff2 = dateDiffInDays(dateValues2.min, dateValues2.max);
                        if (diff2 !== diff) {
                            var m = new Date((dateValues2.min.getTime()) + diff * 24 * 3600 * 1000);
                            dateValues2.max = (new Date(m.getFullYear(), m.getMonth(), m.getDate()));
                            //                            console.log(m.getFullYear());
                            //                            console.log(dateValues2);
                            $("#<?= $unique; ?>2").dateRangeSlider("values", new Date(dateValues2.min.toString()), new Date(dateValues2.max.toString()));
                        }

                    });

                    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
                    // a and b are javascript Date objects
                    function dateDiffInDays(a, b) {
                        // Discard the time and time-zone information.
                        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
                        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
                        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
                    }

                    $("#<?= $unique; ?>left1").click(function() {
                        $('#<?= $unique; ?>1').dateRangeSlider('scrollLeft', 1);
                        return false;
                    });
                    $("#<?= $unique; ?>right1").click(function() {
                        $('#<?= $unique; ?>1').dateRangeSlider('scrollRight', 1);
                        return false;
                    });
                    setTimeout(function() {
                        console.log('to red');
                        $(".ui-rangeSlider-bar:first").addClass('red');
                        $(".ui-rangeSlider-leftHandle:first").addClass('red');
                        $(".ui-rangeSlider-rightHandle:first").addClass('red');
                    }, 1500);
                </script>
        <!--                <p style="text-align:center;">
                    <a class="button_scroll_left" href="javascript:" id="<?= $unique; ?>left1">
                        <i class="icon-chevron icon-chevron-left"></i>
                        Scroll left
                    </a>
                    <a class="button_scroll_left" href="javascript:" id="<?= $unique; ?>right1">                       
                        Scroll right
                        <i class="icon-chevron icon-chevron-right"></i>
                    </a>
                </p>-->
            </div>
            <div style="text-align:right">

                <button type="button" class="bBlue" onclick="<?php echo $this->uid; ?>Click();
                        return false;"
                        style="float:right; margin-right:1px; padding:0;  height:25px; line-height:25px; padding:0 8px;">OK</button>
                <button type="button" class="bGray" onclick="<?php echo $this->uid; ?>Reset();
                        return false;" 
                        style="float:right; margin-right:2px; padding:0;  height:25px; line-height:25px; padding:0 8px;">Reset</button>
            </div>
            <script>
                    function <?php echo $this->uid; ?>Reset() {

                        var str = '<?php echo $link2; ?>';
        <?php echo $this->parent->ajax->zone . ".Go(str);"; ?>
                    }
                    function <?php echo $this->uid; ?>Click()
                    {

                        var dateValues = $("#<?= $unique; ?>1").dateRangeSlider("values");
                        var days = dateValues.min.getDate(),
                                month = dateValues.min.getMonth() + 1,
                                year = dateValues.min.getFullYear();
                        var xdays = dateValues.max.getDate(),
                                xmonth = dateValues.max.getMonth() + 1,
                                xyear = dateValues.max.getFullYear();
                        $('#<?php echo $this->uid; ?>1').val();
//                        var dateValues2 = $("#<?= $unique; ?>2").dateRangeSlider("values");
                     
                        $('#<?php echo $this->uid; ?>').val(year + '_' + month + '_' + days + '|' + xyear + '_' + xmonth + '_' + xdays);
                        var str = '<?php echo $link; ?>';
                        console.log('<?= $val; ?>');
                        var q = $('#<?php echo $this->uid; ?>').val();
                        q = q.replace(/-/g, "#DEF#");
                        str = str.replace('replace<?php echo $this->uid; ?>', q);
                        console.log(str);
                        //                    alert(str);
        <?php echo $this->parent->ajax->zone . ".Go(str);"; ?>
                    }
            </script>

        </div>

        <?php
    }

}
?>
