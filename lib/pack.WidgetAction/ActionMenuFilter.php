<?php

class ActionMenuFilter extends ActionBase
{
    public function GetOptions() {
        $options = array();
        $selected = (isset($this->parent->nav->param[$this->rule->field])) ? $this->parent->nav->param[$this->rule->field] : "NO_CHOOSE_PARAMS";

        $options[] = array(
            "",
            ((isset($this->properties["noChooseValue"])) ? $this->properties["noChooseValue"] : "not selected"),
            ($selected == "NO_CHOOSE_PARAMS")
        );

        if ($this->data) {
            $this->prepareData();

            foreach ($this->data as $val) {
                $options[] = array($val['data'], $val['title'], ($selected == $val['equal'] && $selected != "NO_CHOOSE_PARAMS"));
            }
        }
        return $options;
    }

    /**
     * @return bool
     */
    protected function prepareData() {
        if (!$this->data) {
            return false;
        }
        $updData = array();
        foreach ($this->data as $key => $row) {
            if (is_array($row) && isset($row['data']) && isset($row['equal']) && isset($row['title'])) {
                return true;//уже в подготовленном формате
            } else {
                if (is_array($row)) {
                    //если $this->data - многомерный массив
                    list($k, $v) = each($row);
                    $updData[] = array('data' => $k, 'title' => $v, 'equal' => $k);
                } else {
                    $updData[] = array('data' => $key, 'equal' => $key, 'title' => (string)$row);
                }
            }
        }
        $this->data = $updData;
        return true;
    }
}