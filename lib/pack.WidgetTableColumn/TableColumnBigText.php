<?php

class TableColumnBigText extends TableColumn
{	
	
	public $strlen;
	public $title;

	//---
	function __construct()
	{
		$args=func_get_args();
		$this->key        = (isset($args[0])) 		? 	$args[0] 					: 	"undefined key";
		$this->name 	  = (isset($args[1])) 		? 	$args[1] 					: 	$args[0];
		$this->width 	  = (isset($args[2])) 		? 	"width = '".$args[2]."px'" 	: 	"";
		$this->align	  = (isset($args[3])) 		? 	"align = '".$args[3]."'"	: 	"align = 'left'";
		$this->strlen	  = (isset($args[4])) 		? 	$args[4]					: 	40;
		$this->primary	  = (isset($args[5])) 		? 	$args[5]					: 	false;
		$this->enableSort = (isset($args[6])) 		? 	$args[6]					: 	true;
	}

	//---
	public function PostProcessing($str)
	{

        $str = strip_tags($str);
		$str = preg_replace('/<BR>/',", ", $str);

		if (strlen($str) > $this->strlen)
		{
			$strCut = substr($str, 0, $this->strlen) . " ...";

			return "<span class='tableHiddenText' title='{$str}'>{$strCut}</span>";
		}
		else
		{
			return $str;
		}

	}

	//---
	public function GetToolTip($controller){}
	
}

	
?>