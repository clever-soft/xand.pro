<?php

class TableColumnLightningIcon extends TableColumn
{	
	function __construct()
	{  
		$args=func_get_args(); 		
		$this->key 	= (isset($args[0])) 			? 	$args[0] 					: 	"undefined key";
		$this->name 	= (isset($args[1])) 		? 	$args[1] 					: 	$args[0];
		$this->width 	= (isset($args[2])) 		? 	"width = '".$args[2]."px'" 	: 	"width = '100px'";
		$this->align	= "align = 'center'";		  
	}
	
	public function PostProcessing($str)
	{
		switch($str)
		{
			case 1:  $icon = "#8ED74D"; break;
			case 2:  $icon = "#FF3F2A"; break;			
			case 3:  $icon = "#F4C400"; break;		
			default: $icon = "rgba(202, 213, 221, 0.506)";
		}
		 
		return "<i class='fa fa-bolt' style='font-size:16px; color: {$icon};'></i>";
	}
	 
	public function GetToolTip($controller){}
	
}

	
?>