<?php

class TableColumnDateCreateSec extends TableColumnDate
{	
	function __construct()
	{  		
		$arg 			  = func_get_args();		
		$this->formatLong = (isset($arg[0])) ? true : false; 			
		
		$this->key 		= "dateCreate";
		$this->name 	= "Created";
		$this->width 	= "width = '130px'";
		$this->align	= "align = 'center'";		  
	}	
	
	public function PostProcessing($str)
	{		  
		if($str == "" || $str == 0)
		{
			return "-//-";
		}
		else
		{
			if(! is_numeric($str))
			{
				$dt = DateTime::createFromFormat("Y-m-d H:i:s", $str);    	  
			    $str =  $dt->modify('+2 hour')
						   ->format('U');				
			}
			
			return Date::FormateSec($str);
			
		} 
	    
	}
	
	
	public function GetToolTip($controller){}
}

	
?>