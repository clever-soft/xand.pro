<?php

class TableColumnDateSec extends TableColumnDate
{	
	function __construct()
	{  		
		$args=func_get_args(); 		
		$this->key 			= (isset($args[0])) 	? 	$args[0] : 	"undefined key";	
		$this->name 		= (isset($args[1])) 	? 	$args[1] : 	$args[0];
		$this->formatLong   = (isset($args[2])) ? true : false; 
		
		$this->width 	= "width = '130px'";
		$this->align	= "align = 'center'";	   
	}	
	
	public function PostProcessing($str)
	{		  
		if($str == "" || $str == 0)
		{
			return "-//-";
		}
		else
		{
			if(! is_numeric($str))
			{
				$dt = DateTime::createFromFormat("Y-m-d H:i:s", $str);    	  
			    $str =  $dt->modify('+2 hour')
						   ->format('U');				
			}
			
			return Date::FormateSec($str);
			
		} 
	    
	}
	
	
	public function GetToolTip($controller){}
}

	
?>