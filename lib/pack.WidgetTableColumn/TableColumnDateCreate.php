<?php

class TableColumnDateCreate extends TableColumnDate
{	
	public $formatLong;
	
	function __construct()
	{  		
		$arg 			  = func_get_args();		
		$this->formatLong = (isset($arg[0])) ? true : false; 
		
		$this->key 	= "dateCreate";
		$this->name 	= "Created";
		$this->width 	= ($this->formatLong) ? "width = '160px'" : "width = '120px'";
		$this->align	= "align = 'center'";	 
	}
		
    public function GetToolTip($controller){}
}

	
?>