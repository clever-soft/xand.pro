<?php

class TableColumnLogicIcon extends TableColumn
{

    public $iconClass;
    public $title;

    function __construct() {
        $args = func_get_args();
        $this->key = (isset($args[0])) ? $args[0] : "undefined key";
        $this->name = (isset($args[1])) ? $args[1] : $args[0];
        $this->width = (isset($args[2])) ? "width = '" . $args[2] . "px'" : "width = '100px'";
        $this->iconClass = (isset($args[3])) ? $args[3] : "fa-check";
        $this->title = (isset($args[4])) ? " title='" . $args[4] . "'" : "";

        $this->align = "align = 'center'";

    }

    public function PostProcessing($str) {
        return ($str && $str!="-//-") ? "<i class='fa {$this->iconClass}' style='color: #8ed74d; ' {$this->title}></i>"
            : "<i class='fa {$this->iconClass}' style='color: rgba(202, 213, 221, 0.506); ' {$this->title}></i>";
    }

    public function GetToolTip($controller) {
    }

}