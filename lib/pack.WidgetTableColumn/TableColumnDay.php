<?php

class TableColumnDay extends TableColumn
{	
	function __construct()
	{  
		$args=func_get_args(); 		
		$this->key 			= (isset($args[0])) 	? 	$args[0] : 	"undefined key";	
		$this->name 		= (isset($args[1])) 	? 	$args[1] : 	$args[0];
		$this->formatLong   = (isset($args[2]))     ?   true : false;
		$this->width 	    = (isset($args[3]))     ? 	"width = '".$args[3]."px'" 	: 	"width = '60px'";
		$this->align		= "align = 'center'";
	}
	
	public function PostProcessing($str)
	{		  
		if($str == "" || $str == 0)
		{
			return "-//-";
		}
		else
		{
			return date("d", $str);
		} 
	    
	}
	
	public function GetToolTip($controller){}
	
	
}

	
?>