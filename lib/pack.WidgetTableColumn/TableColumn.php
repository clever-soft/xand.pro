<?php

class TableColumn
{
    public $key = "";
    public $name = "";
    public $width = "";
    public $align = "";
    public $primary = false;
    public $enableSort = true;

    function __construct() {
        $args = func_get_args();
        $this->key = (isset($args[0])) ? $args[0] : "undefined key";
        $this->name = (isset($args[1])) ? $args[1] : $args[0];
        $this->width = (isset($args[2])) ? "width = '" . $args[2] . "px'" : "";
        $this->align = (isset($args[3])) ? "align = '" . $args[3] . "'" : "align = 'left'";
        $this->primary = (isset($args[4])) ? $args[4] : false;
        $this->enableSort = (isset($args[5])) ? $args[5] : true;
    }


    public function GetToolTip($controller) {
        return "";
    }
}