<?php

/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 31.05.2017
 * Time: 10:09
 */
abstract class Clean
{
    private $files;
    private $pack;
    private $packName;
    private $folders;
    private $version;

    private $fieldName;

    /**
     * @return bool
     */
    protected function fileClean()
    {

        if (!isset($this->version) || empty($this->version))
            return false;
        else
            $verAprove = $this->__compareVersions();

        if (!$verAprove)
            return false;
        if (isset($this->files) && !empty($this->files) && is_array($this->files))
            foreach ($this->files as $file) {
                if (file_exists(PATH_REAL . DS . $this->pack . DS . $this->packName . DS . $file))
                    unlink(PATH_REAL . DS . $this->pack . DS . $this->packName . DS . $file);
            }

        if (isset($this->folders) && !empty($this->folders) && is_array($this->folders))
            foreach ($this->folders as $dir) {

                if (is_dir($dir))
                    $this->__delTree($dir);
            }
        return true;
    }

    /**
     * @return bool
     */
    public function baseClean()
    {
        if (isset($this->fieldName) && !empty($this->fieldName) && is_array($this->fieldName)) {
            $sql = [];
            foreach ($this->fieldName as $field) {
                if (strpos($field, "/")) {
                    $tmp = explode("/", $field);
                    $sql[] = "ALTER TABLE `{$tmp[0]}` DROP COLUMN `{$tmp[1]}`;";
                } else {
                    $sql[] = "DROP TABLE `{$field}`;";
                }
            }
            if (!empty($sql) && is_array($sql))
                foreach ($sql as $s) {
                    SDb::query($s);
                }
        }
        return true;
    }

    protected function setPack($pack)
    {
        $this->pack = $pack;
        return $this;
    }

    protected function setPackName($packName)
    {
        $this->packName = $packName;
        return $this;
    }

    protected function setFiles($files)
    {
        $this->files = $files;
        return $this;
    }

    protected function setDirectory($folders)
    {
        foreach ($folders as $folder) {
            $this->folders[] = PATH_REAL . DS . $this->pack . DS . $this->packName . DS . $folder;
        }

        return $this;
    }

    /**
     * @param $version
     * @return $this
     */
    protected function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @param array $fieldName
     * @return $this
     */
    protected function setBase(array $fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }


    private function __compareVersions()
    {
        $xml = $this->GetXml();
        //Если нет файла, то считается, что версия самая первая
        if (!$xml)
            $b = "1.0.0";
        else
            if ($this->pack == "mod")
                $b = (string)$xml->settings->version;
            else
                $b = (string)$xml->version;

        $localArr = explode(".", $this->version);
        $serverArr = explode(".", $b);
        if ($localArr[0] != $serverArr[0]) {
            return false;
        }
        if ($localArr[1] > $serverArr[1]) {
            return false;
        }
        if ($localArr[1] == $serverArr[1]) {
            if ($localArr[2] > $serverArr[2]) {
                return false;
            }
        }
        return true;
    }

    private function GetXml()
    {
        $fileXml = PATH_REAL . DS . $this->pack . DS . $this->packName . "/conf.xml";
        if (!file_exists($fileXml)) {
            return false;
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;

    }

    private function __delTree($folderIn = false)
    {
        if (!$folderIn)
            $folderIn = $this->folders;
        $folder = trim($folderIn, DS) . DS;
        var_dump($folder);
        $f = glob($folder . '*', GLOB_MARK);

        if (count($f) > 0)
            foreach ($f as $e) {
                if (is_dir($e))
                    $this->__delTree($e);
                else
                    unlink($e);
            }
        rmdir($folder);
        return true;
    }

    abstract protected function run();
}