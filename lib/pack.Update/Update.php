<?php

/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 12.04.2017
 * Time: 10:03
 */
class Update
{

    const MODULE_PACK = "mod";
    const LIBRARY_PACK = "lib";
    const TEMPLATE_PACK = "tpl";


    private $server = "http://framework.xand.pro";
    private $tempFolder = "system/temp";

    private $_requirmentsInclude = [];


    public $link;

    public static $zipArchive = PATH_REAL."/../framework.xand.pro/public/v4.0";

    public function getFullUpdate($fullVersion = XAND_VERSION)
    {

        $json = self::getUpdateJson($fullVersion);
        $arr = json_decode($json, true);
        usort($arr['full'], ["Update", "sortversion"]);
        $lastVersion = array_pop($arr['full']);
        if (empty($lastVersion)) {
            return false;
        }
        $fileXml = dirname(realpath(NULL)) . DS . "conf.xml";
        if (!file_exists($fileXml)) {
            return false;
        }
        $xml = simplexml_load_file($fileXml);
        $version = (string)$xml->group[2]->setting->version;
        if (isset($version) && !empty($version)) {
            $localArr = explode(".", $version);
            $serverArr = explode(".", $lastVersion);

            if ($localArr[0] != $serverArr[0]) {
                return false;
            }

            if ($localArr[1] > $serverArr[1]) {
                return false;
            }
            if ($localArr[1] == $serverArr[1]) {
                if ($localArr[2] >= $serverArr[2]) {
                    return false;
                }
            }
            if ($this->makeLink("full", NULL, $fullVersion, $lastVersion)) {
                if (Config::makeDirectory($this->tempFolder)) {
                    $to = dirname(realpath(NULL)) . DS . $this->tempFolder . DS . basename($this->link);
                    if (@copy($this->link, $to)) {
                        $zip = new ZipArchive();
                        $zip->open($to);
                        if (file_exists(dirname(realpath(NULL)) . DS . "conf.xml")) {
                            @copy(dirname(realpath(NULL)) . DS . "conf.xml", dirname(realpath(NULL)) . "conf.xml.backup");
                        }
                        $modules = glob(dirname(realpath(NULL)) . DS . "mod/*", GLOB_ONLYDIR);
                        foreach ($modules as $mod) {
                            $fileXml = $mod . "/conf.xml";
                            if (file_exists($fileXml)) {
                                @copy($fileXml, $fileXml . ".backup");
                            }
                        }
                        $libs = glob(dirname(realpath(NULL)) . DS . "lib/*", GLOB_ONLYDIR);
                        foreach ($libs as $lib) {
                            $fileXml = $lib . "/conf.xml";
                            if (file_exists($fileXml)) {
                                @copy($fileXml, $fileXml . ".backup");
                            }
                        }
                        $zip->extractTo(dirname(realpath(NULL)));
                        $zip->close();
                        $this->_rmdirRecursive(dirname(realpath(NULL)) . DS . $this->tempFolder);
                        $xml->group[2]->setting->version = $lastVersion;
                        $xml->saveXML(dirname(realpath(NULL)) . DS . "conf.xml");
                        $this->actionUpdateMysql(false, true, Xand::$db['db']['name']);
                        Message::I()->Info("Your site is updated. New version is " . $lastVersion);
                        return true;
                    } else {
                        $errors = error_get_last();
                        Debug::Dump($errors);
                        Message::I()->Error("COPY ERROR: " . $errors['type']);
                        return false;
                    }
                }
            }


        }
        return true;

    }

    /**
     * @param $pack
     * @param $name
     * @param string $fullVersion
     * @param string $version
     * @return bool
     * ToDo: Сделать универсальным.
     * Возможно использовать из вне для либ, но не модулей.
     * Для модулей требуется обновление с зависимостью, иначе не подтянутся базы.
     */
    final public function getUpdatePack($pack, $name, $fullVersion = XAND_VERSION, $version = "")
    {
        if (empty($pack) || empty($name))
            return false;

        if ($this->makeLink($pack, $name, $fullVersion, $version)) {
            if (Config::makeDirectory($this->tempFolder)) {
                $to = dirname(realpath(NULL)) . DS . $this->tempFolder . DS . basename($this->link);
                if (@copy($this->link, $to)) {
                    $zip = new ZipArchive();
                    $zip->open($to);
                    switch ($pack) {
                        case "lib":
                            $name = "pack." . $name;
                            break;
                        case "mod":
                            break;
                        case "tpl":
                            $name = "tpl-" . $name;
                            $pack = "public/tpl";
                            break;
                        default:
                            break;
                    }
                    if (file_exists(dirname(realpath(NULL)) . DS . $pack . DS . $name . DS . "conf.xml")) {
                        @copy(dirname(realpath(NULL)) . DS . $pack . DS . $name . DS . "conf.xml", dirname(realpath(NULL)) . DS . $pack . DS . $name . DS . "conf.xml.backup");
                    }
                    $zip->extractTo(dirname(realpath(NULL)) . DS . $pack . DS . $name);
                    $zip->close();
                    $this->_rmdirRecursive(dirname(realpath(NULL)) . DS . $this->tempFolder);
                    return true;
                } else {
                    $errors = error_get_last();
                    Debug::Dump($errors);
                    Message::I()->Error("COPY ERROR: " . $errors['type']);
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * @param $pack
     * @param $name
     * @param string $fullVersion
     * @param string $version
     * @return bool
     */
    public function makeLink($pack, $name, $fullVersion = XAND_VERSION, $version = "")
    {
        $ver = explode(".", $fullVersion);
        $ver = $ver[0] . "." . $ver[1];
        $json = self::getUpdateJson($fullVersion);
        $arr = json_decode($json, true);
        if ($pack == "full") {
            usort($arr[$pack], ["Update", "sortversion"]);
            $lastVersion = array_pop($arr[$pack]);
        } else {
            usort($arr[$pack][$name], ["Update", "sortversion"]);
            $lastVersion = array_pop($arr[$pack][$name]);
        }

        if (empty($lastVersion)) {
            return false;
        }
        $version = (empty($version)) ? $lastVersion : $version;
        if ($pack == "full") {
            $this->link = $this->server . DS . "v" . $ver . DS . $pack . "-v." . $version . ".zip";
        } else {
            $this->link = $this->server . DS . "v" . $ver . DS . $pack . "-" . $name . ".v." . $version . ".zip";
        }

        return true;
    }

    /**
     * @param string $ver
     * @return bool|mixed
     */
    public static function getUpdateJson($ver = XAND_VERSION)
    {

        $ver = explode(".", $ver);
        $ver = $ver[0] . "." . $ver[1];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'http://xand.pro/update.php');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "version=" . $ver);
            $out = curl_exec($curl);
            //Debug::Dump($out) ;
            curl_close($curl);
            return $out;
        }
        return false;
    }

    /**
     * @param $dir
     */
    private function _rmdirRecursive($dir)
    {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) $this->_rmdirRecursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }

    /**
     * @param $data
     * @param bool $install
     * @param bool $fullName
     */
    public function actionUpdateMysql($data, $install = false, $fullName = false)
    {
        ini_set('max_execution_time', 1800);

        try {
            SDb::SetDb("db_update");
            $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'tables';
            $driver = new Driver();
            $additionalTableInfo = array();
            switch ($action) {
                case "tables":
                    $tables = Driver::getInstance()->getCompareTables();
                    $additionalTableInfo = Driver::getInstance()->getAdditionalTableInfo();
                    break;
                case "views":
                    $tables = Driver::getInstance()->getCompareViews();
                    break;
                case "procedures":
                    $tables = Driver::getInstance()->getCompareProcedures();
                    break;
                case "functions":
                    $tables = Driver::getInstance()->getCompareFunctions();
                    break;
                case "indexes":
                    $tables = Driver::getInstance()->getCompareKeys();
                    break;
                case "triggers":
                    $tables = Driver::getInstance()->getCompareTriggers();
                    break;
                case "rows":
                    $rows = Driver::getInstance()->getTableRows($_REQUEST['baseName'], $_REQUEST['tableName']);
                    break;
            }
            $basesName = array(
                'fArray' => $driver->first_base_name,
                'sArray' => $driver->second_base_name
            );
            if (isset($tables))
                $diff = $this->_getDiffTable($tables);
            if ($fullName) {
                $tables = SDb::query("SHOW TABLES FROM " . $fullName);
                $t = [];
                foreach ($tables as $table) {
                    $t[] = $table[0];
                }
                $tables = $t;
            } else {
                if (!empty($data))
                    $tables = $this->__checkModulModels($data, $install);
            }
            $sql = "";
            if (!empty($diff))
                $sql = $this->_makeSQL($diff, $tables, $additionalTableInfo);
            SDb::SetDb("db");
            if (!empty($sql))
                foreach ($sql as $s) {
                    SDb::query($s);
                    if (!empty(SDb::getLastError()))
                        Debug::logMsg(SDb::getLastError(), "MysqlError.log");
                }

        } catch (Exception $e) {
            Debug::logMsg("Error: " . $e, "Update.log");
        }
    }

    /**
     * @param $pack
     * @param $module
     * @return bool
     */
    public function updateWithRequirments($pack, $module)
    {
        ini_set('max_execution_time', 1800);
        if ($this->__getRequirments($pack, $module)) {
            Message::I()->Success(lcfirst($pack) . " " . $module . " updated with requirments");
        } else {
            return false;
        }
        return true;
    }

    /**
     * @param string $pack
     * @param $module
     * @param string $globalVersion
     * @param bool $version
     * @param bool $needRequirments
     * @return bool
     */
    private function __getRequirments($pack = self::MODULE_PACK, $module, $globalVersion = XAND_VERSION, $version = false, $needRequirments = true)
    {

        if (isset($this->_requirmentsInclude[$pack]) && in_array($module, $this->_requirmentsInclude[$pack]))
            return true;
        if (!isset($this->_requirmentsInclude[$pack]) || !in_array($module, $this->_requirmentsInclude[$pack]))
            $this->_requirmentsInclude[$pack][] = $module;

        switch ($pack) {
            case self::MODULE_PACK:
                $path = PATH_REAL . DS . "mod/";
                $module_xml = $module;
                $xml = $this->_getXml($path, $module_xml);
                if (isset($xml->settings->ignore) && (string)$xml->settings->ignore == '1') {
                    Message::I()->Error("This pack are ignored");
                    return false;
                }
                $localVersion = (isset($xml->settings->version) && (string)$xml->settings->version != '') ? (string)$xml->settings->version : "1.0.0";
                break;
            case self::LIBRARY_PACK:
                $path = PATH_REAL . DS . "lib/";
                $module_xml = "pack." . $module;
                $xml = $this->_getXml($path, $module_xml);
                if (isset($xml->ignore) && (string)$xml->settings->ignore == '1') {
                    Message::I()->Error("This pack are ignored");
                    return false;
                }
                $localVersion = (isset($xml->version) && (string)$xml->version != "") ? (string)$xml->version : "1.0.0";
                break;
            case self::TEMPLATE_PACK:
                $path = PATH_REAL . DS . "public/tpl/";
                $module_xml = "tpl-" . $module;
                $xml = $this->_getXml($path, $module_xml);
                if (isset($xml->ignore) && (string)$xml->settings->ignore == '1') {
                    Message::I()->Error("This pack are ignored");
                    return false;
                }
                $localVersion = (isset($xml->version) && (string)$xml->version != "") ? (string)$xml->version : "1.0.0";
                break;
            default:
                return false;
        }
        $serverVersion = json_decode(self::getUpdateJson($globalVersion), true);
        unset($serverVersion['full']);
        foreach ($serverVersion as &$arr) {
            foreach ($arr as $k => $m) {
                if (is_array($m)) {
                    usort($m, ["Update", "sortversion"]);
                    $arr[$k] = array_pop($m);
                }
            }
        }

        $needVersion = (empty($version)) ? (isset($serverVersion[$pack][$module])) ? $serverVersion[$pack][$module] : $version : $version;


        if ($localVersion == $needVersion && $needVersion == '1.0.0')
            $checkVersion = true;
        else
            $checkVersion = $this->__compareVersions($localVersion, $needVersion);

        if (!$checkVersion)
            return false;


        if ($xml === false) {
            $this->getUpdatePack($pack, $module, $globalVersion, $needVersion);
            if ($pack == "mod") {
                $this->actionUpdateMysql($module);
            }
            Message::I()->Success(lcfirst($pack) . " " . $module . " installed");
            $xml = $this->_getXml($path, $module_xml);
        } else {
            if ($localVersion != $needVersion) {
                $this->getUpdatePack($pack, $module, $globalVersion, $needVersion);

                if ($pack == "mod") {
                    $this->actionUpdateMysql($module);
                }
                Message::I()->Success(lcfirst($pack) . " " . $module . " Version: " . $needVersion . " updated");
                $xml = $this->_getXml($path, $module_xml);
            }
        }

        if (!isset($xml->settings->requirments))
            Message::I()->Error("Didn't have requirments " . $module);
        //Зависимости на цикл
        if ($needRequirments)
            foreach ($xml->settings->requirments as $r) {
                if (isset($r->modules) && !empty($r->modules)) {
                    $arrR ['mod'] = explode(",", (string)$r->modules);
                    $tmp = [];
                    foreach ($r->modules as $moduleName) {
                        foreach ($moduleName as $moduleKey => $version)
                            $tmp[$moduleKey] = (string)$version;
                    }
                    $arrR ['mod'] = $tmp;
                    unset($tmp);

                    foreach ($arrR ['mod'] as $moduleKey => $version) {
                        $this->__getRequirments(self::MODULE_PACK, $moduleKey, $globalVersion, $version);
                    }
                }
                if (isset($r->libs) && !empty($r->libs)) {
                    $tmp = [];
                    foreach ($r->libs as $libName) {
                        foreach ($libName as $lib => $version)
                            $tmp[$lib] = (string)$version;
                    }
                    $arrR ['lib'] = $tmp;
                    unset($tmp);
                    foreach ($arrR['lib'] as $lib => $version) {
                        $this->__getRequirments(self::LIBRARY_PACK, $lib, $globalVersion, $version);
                    }
                }
                if (isset($r->tpls) && !empty($r->tpls)) {
                    $tmp = [];
                    foreach ($r->tpls as $tplsName) {
                        foreach ($tplsName as $tpls => $version)
                            $tmp[$tpls] = (string)$version;
                    }
                    $arrR ['tpl'] = $tmp;
                    unset($tmp);
                    foreach ($arrR ['tpl'] as $tpls => $version) {
                        $this->__getRequirments(self::TEMPLATE_PACK, $tpls, $globalVersion, $version);
                    }
                }
            }
        $classClean = $module . $pack . "Clean";

        if (class_exists($classClean))
            (new $classClean)->run();
        return true;
    }

    /**
     * @param $a - Local Version
     * @param $b - Server Version
     * @return bool
     */
    private function __compareVersions($a, $b)
    {
        $localArr = explode(".", $a);
        $serverArr = explode(".", $b);
        if ($localArr[0] != $serverArr[0]) {

            return false;
        }
        if ($localArr[1] > $serverArr[1]) {
            return false;
        }
        if ($localArr[1] == $serverArr[1]) {
            if ($localArr[2] >= $serverArr[2]) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    private function _checkVersions()
    {
        $serverVersion = json_decode(self::getUpdateJson(), true);
        unset($serverVersion['full']);
        foreach ($serverVersion as &$arr) {
            foreach ($arr as $k => $m) {
                if (is_array($m)) {
                    usort($m, ["Update", "sortversion"]);
                    $arr[$k] = array_pop($m);
                }
            }
        }


        foreach ($this->_requirmentsInclude as $pack => $modules) {
            switch ($pack) {
                case self::MODULE_PACK:
                    $path = PATH_REAL . DS . "mod/";
                    foreach ($modules as $key => $mod) {
                        $xml = $this->_getXml($path, $mod);
                        $versionArrLocal = explode(".", (string)$xml->settings->version);
                        if (!isset($serverVersion['mod'][$mod])) {
                            unset($modules[$key]);
                            break;
                        }
                        $versionArrServer = explode(".", $serverVersion['mod'][$mod]);
                        if ((int)$versionArrLocal[0] != (int)$versionArrServer[0]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] > (int)$versionArrServer[1]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] == (int)$versionArrServer[1]) {
                            if ((int)$versionArrLocal[2] >= (int)$versionArrServer[2]) {
                                unset($modules[$key]);
                                break;
                            }
                        }
                    }

                    break;
                case self::LIBRARY_PACK:
                    $path = PATH_REAL . DS . "lib/";
                    foreach ($modules as $key => $lib) {

                        $xml = $this->_getXml($path, "pack." . $lib);
                        $versionArrLocal = explode(".", (string)$xml->version);


                        if (!isset($serverVersion['lib'][$lib])) {
                            unset($modules[$key]);
                            break;
                        }
                        $versionArrServer = explode(".", $serverVersion['lib'][$lib]);

                        if ((int)$versionArrLocal[0] != (int)$versionArrServer[0]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] > (int)$versionArrServer[1]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] == (int)$versionArrServer[1]) {
                            if ((int)$versionArrLocal[2] >= (int)$versionArrServer[2]) {
                                unset($modules[$key]);
                                break;
                            }
                        }
                    }
                    break;
                case self::TEMPLATE_PACK:
                    $path = PATH_REAL . DS . "public/tpl/";
                    foreach ($modules as $key => $tpl) {

                        $xml = $this->_getXml($path, "tpl-" . $tpl);
                        $versionArrLocal = explode(".", (string)$xml->version);


                        if (!isset($serverVersion['tpl'][$tpl])) {
                            unset($modules[$key]);
                            break;
                        }
                        $versionArrServer = explode(".", $serverVersion['tpl'][$tpl]);

                        if ((int)$versionArrLocal[0] != (int)$versionArrServer[0]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] > (int)$versionArrServer[1]) {
                            unset($modules[$key]);
                            break;
                        }
                        if ((int)$versionArrLocal[1] == (int)$versionArrServer[1]) {
                            if ((int)$versionArrLocal[2] >= (int)$versionArrServer[2]) {
                                unset($modules[$key]);
                                break;
                            }
                        }
                    }
                    break;
                default:
                    return false;
            }
            $this->_requirmentsInclude[$pack] = $modules;
        }
        return true;
    }

    private function __checkModulModels($modul, $install = false)
    {
        if ($install) {
            $modules = glob(INSTALL_PATH_REAL . DS . "mod/" . $modul . DS . "models/*.php");
        } else {
            $modules = glob(PATH_REAL . DS . "mod/" . $modul . DS . "models/*.php");
        }

        $tables = [];
        foreach ($modules as $module) {
            $model = str_replace(".php", "", basename($module));
            if ($model == "I18nModel") continue;
            if (class_exists($model)) {
                $obj = new $model;
                if (is_subclass_of($obj, "BaseModel")) {
                    if (method_exists($obj, "getTableName_i18n"))
                        $tables[] = $obj->getTableName_i18n();
                    $tables[] = (new $model)->getTableName();
                }
            }
        }

        return $tables;
    }


    /**
     * @param $arr
     * @param array $tables
     * @param null $additionalTableInfo
     * @return array
     * ToDo: Расширить запросы.
     */
    private function _makeSQL($arr, $tables = [], $additionalTableInfo = NULL)
    {
        $sql = [];
        foreach ($arr as $k => $a) {
            if (!in_array($k, $tables)) continue;
            $sqlText = "";
            if (isset($a['isNew']) && $a['isNew'] == true) {

                $sqlText .= "CREATE TABLE {$k} (";
                $tmpText = [];


                foreach ($a as $fieldName => $fieldArr) {
                    $inArr = [];
                    if ($fieldName == "isNew") continue;
                    $inArr['dTypeFull'] = (!empty($fieldArr['dTypeFull'])) ? " " . $fieldArr['dTypeFull'] : "";
                    $inArr['dtype_length'] = (!empty($fieldArr['dtype_length'])) ? " " . $fieldArr['dtype_length'] : "";
                    $inArr['isNull'] = ((empty($fieldArr['uniqkey']) || $fieldArr['uniqkey'] !== "PRI") && !empty($fieldArr['isNull']) && $fieldArr['isNull'] == "NO") ? " NOT NULL" : (!empty($fieldArr['isNull']) && $fieldArr['isNull'] == "YES") ? " NULL" : "";

                    if (!empty($fieldArr['uniqkey']) && $fieldArr['uniqkey'] == "PRI") {
                        $inArr['extra'] = (!empty($fieldArr['extra'])) ? " UNIQUE " . strtoupper($fieldArr['extra']) : "";
                    } else {
                        $inArr['extra'] = (!empty($fieldArr['extra'])) ? " " . strtoupper($fieldArr['extra']) : "";
                    }

                    if (!empty($fieldArr['uniqkey']) && $fieldArr['uniqkey'] === "PRI") {
                        if (strtoupper($inArr['extra']) === " AUTO_INCREMENT" || strtoupper($inArr['extra']) === " UNIQUE AUTO_INCREMENT")
                            $tmp[] = "PRIMARY KEY (" . $fieldName . ")";
                    }

                    if (!empty($fieldArr['uniqkey']) && $fieldArr['uniqkey'] == "UNI") {
                        $tmp[] = "UNIQUE INDEX `{$fieldName}` (" . $fieldName . ")";
                    }
                    if ($fieldArr['dtype'] == 'varchar') {
                        $inArr['defaultValue'] = (!empty($fieldArr['defaultValue'])) ? " DEFAULT '" . $fieldArr['defaultValue'] . "'" : "";
                    } else {
                        $inArr['defaultValue'] = (!empty($fieldArr['defaultValue'])) ? " DEFAULT " . $fieldArr['defaultValue'] : "";
                    }

                    $tmpText[] = " `" . $fieldName . "`" . $inArr['dTypeFull'] . $inArr['isNull'] . $inArr['defaultValue'] . $inArr['extra'];
                }
                $sqlText .= implode(" , ", $tmpText);

                if (!empty($tmp)) {
                    $sqlText .= ", " . implode(", ", $tmp);
                    unset($tmp);
                }
                $sqlText .= ")";
                if (isset($additionalTableInfo[$k]['sArray']['engine']))
                    $sqlText .= " ENGINE=" . $additionalTableInfo[$k]['sArray']['engine'];

            } else {
                $sqlText .= "ALTER TABLE {$k} ";
                $tmp = [];
                foreach ($a as $fieldName => $fieldArr) {
                    $inArr = [];
                    $inArr['dTypeFull'] = (!empty($fieldArr['dTypeFull'])) ? " " . $fieldArr['dTypeFull'] : "";
                    $inArr['isNull'] = (!empty($fieldArr['isNull']) && $fieldArr['isNull'] == "NO") ? " NOT NULL" : (!empty($fieldArr['isNull']) && $fieldArr['isNull'] == "YES") ? " NULL" : "";
                    if ($fieldArr['dtype'] == 'varchar') {
                        $inArr['defaultValue'] = (!empty($fieldArr['defaultValue'])) ? " DEFAULT '" . $fieldArr['defaultValue'] . "'" : "";
                    } else {
                        $inArr['defaultValue'] = (!empty($fieldArr['defaultValue'])) ? " DEFAULT " . $fieldArr['defaultValue'] : "";
                    }
                    $tmp[] = "	ADD COLUMN " . $fieldName . $inArr['dTypeFull'] . $inArr['isNull'] . $inArr['defaultValue'];
                }
                $sqlText .= implode(" , ", $tmp);
            }

            $sql[] = $sqlText;
        }
        return $sql;
    }

    /**
     * @param array $arr
     * @return array
     */

    private function _getDiffTable($arr)
    {
        $newArr = [];
        if (isset($arr) && !empty($arr)) {
            foreach ($arr as $k => $a) {
                if (!isset($a['fArray']) || empty($a['fArray']))
                    $newArr[$k]['isNew'] = true;
                if (isset($a['sArray']) && !empty($a['sArray'])) {
                    foreach ($a['sArray'] as $sk => $sa) {
                        if (isset($sa['isNew']) && $sa['isNew'] == true) {
                            $newArr[$k][$sk] = $sa;
                        }
                    }
                }
            }
        }
        return $newArr;
    }

    static function sortversion($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        $aArr = explode(".", $a);
        $bArr = explode(".", $b);
        if ((int)$aArr[0] < (int)$bArr[0]) {
            return -1;
        } elseif ((int)$aArr[0] > (int)$bArr[0]) {
            return 1;
        } else {
            if ((int)$aArr[1] < (int)$bArr[1]) {
                return -1;
            } elseif ((int)$aArr[1] > (int)$bArr[1]) {
                return 1;
            } else {
                if ((int)$aArr[2] < (int)$bArr[2]) {
                    return -1;
                } else {
                    return 1;
                }

            }
        }
    }

    /**
     * @param $path
     * @param $module
     * @return bool|SimpleXMLElement
     */
    private
    function _getXml($path, $module)
    {
        $fileXml = $path . $module . "/conf.xml";
        if (!file_exists($fileXml)) {
            return false;
        }
        $xml = simplexml_load_file($fileXml);
        return $xml;

    }

    public static function incrementVersion($ver = "1.0.0")
    {
        $arrV = explode(".", $ver);
        $arrV[2] = $arrV[2] + 1;
        return implode(".", $arrV);
    }
}