<?php

/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 31.05.2017
 * Time: 10:09
 */
class UpdatelibClean extends Clean
{

    public function run() {
        $this->setPack("lib");
        $this->setPackName("pack.Cms");
        $this->setVersion("1.0.14");
        $this->setFiles([
            "Clean.php"
        ]);
        $this->fileClean();
    }
}