<?php

abstract class BaseDriver
{
    protected $_dsn = array();

    protected static $_instance = null;

    public $first_dsn = "";
    public $second_dsn = "";
    public $first_base_name = "";
    public $second_base_name = "";

    const ROW_COUNT = 100;

    function __construct()
    {
        $this->first_base_name = Xand::$db["db"]['name'];
        $this->second_base_name = Xand::$db["db_update"]['name'];
        $this->first_dsn = 'mysql://' . Xand::$db["db"]['user'] . ':' . Xand::$db["db"]['password'] . '@' . Xand::$db["db"]['host'] . '/' . Xand::$db["db"]['name'];
        $this->second_dsn = 'mysql://' . Xand::$db["db_update"]['user'] . ':' . Xand::$db["db_update"]['password'] . '@' . Xand::$db["db_update"]['host'] . '/' . Xand::$db["db_update"]['name'];
    }

    protected function _getFirstConnect()
    {
        return $this->_getConnect($this->first_dsn, $this->first_base_name);
    }


    protected function _getSecondConnect()
    {
        return $this->_getConnect($this->second_dsn, $this->second_base_name);
    }

    protected function _getConnect($dsn)
    {
        if (!isset($this->_dsn[$dsn])) {
            $pdsn = parse_url($dsn);

            $dsn = 'mysql:host=' . $pdsn['host'] . ';dbname=' . substr($pdsn['path'], 1, 1000) . ("mysql" !== 'pgsql' ? ';charset=utf8' : '');
            $this->_dsn[$dsn] = new PDO($dsn, $pdsn['user'], isset($pdsn['pass']) ? $pdsn['pass'] : '', array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ));
        }
        return $this->_dsn[$dsn];
    }

    protected function _select($query, $connect, $baseName)
    {
        $out = array();

        $query = str_replace('<<BASENAME>>', $baseName, $query);

        $stmt = $connect->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $out[] = $row;
        }
        return $out;
    }


    protected function _getCompareArray($query, $diffMode = false, $ifOneLevelDiff = false)
    {

        $out = array();
        $fArray = $this->_prepareOutArray($this->_select($query, $this->_getFirstConnect(), $this->first_base_name), $diffMode, $ifOneLevelDiff);
        $sArray = $this->_prepareOutArray($this->_select($query, $this->_getSecondConnect(), $this->second_base_name), $diffMode, $ifOneLevelDiff);

        $allTables = array_unique(array_merge(array_keys($fArray), array_keys($sArray)));
        sort($allTables);

        foreach ($allTables as $v) {
            $allFields = array_unique(array_merge(array_keys((array)@$fArray[$v]), array_keys((array)@$sArray[$v])));
            foreach ($allFields as $f) {
                if (!isset($fArray[$v][$f])) {
                    if (is_array($sArray[$v][$f])) $sArray[$v][$f]['isNew'] = true;;
                }
                if (!isset($sArray[$v][$f])) {
                    if (is_array($fArray[$v][$f])) $fArray[$v][$f]['isNew'] = true;
                }
            }
            $out[$v] = array(
                'fArray' => @$fArray[$v],
                'sArray' => @$sArray[$v]
            );
        }
        return $out;
    }

    private function _prepareOutArray($result, $diffMode, $ifOneLevelDiff)
    {
        $mArray = array();
        foreach ($result as $r) {
            if ($diffMode) {
                foreach (explode("\n", $r['ARRAY_KEY_2']) as $pr) {
                    $mArray[$r['ARRAY_KEY_1']][$pr] = $r;
                }

            } else {
                if ($ifOneLevelDiff) {
                    $mArray[$r['ARRAY_KEY_1']] = $r;
                } else {
                    $mArray[$r['ARRAY_KEY_1']][$r['ARRAY_KEY_2']] = $r;
                }
            }
        }
        return $mArray;
    }

    public function getCompareTables()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getAdditionalTableInfo()
    {
        return array();
    }

    public function getCompareIndex()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getCompareProcedures()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getCompareFunctions()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getCompareViews()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getCompareKeys()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getCompareTriggers()
    {
        throw new Exception(__METHOD__ . ' Not work');
    }

    public function getTableRows($baseName, $tableName, $rowCount = self::ROW_COUNT)
    {
        if (!$baseName) throw new Exception('$baseName is not set');
        if (!$tableName) throw new Exception('$tableName is not set');
        $rowCount = (int)$rowCount;
        $tableName = preg_replace("$[^A-z0-9.,-_]$", '', $tableName);
        switch ("mysql") {
            case "mssql":
            case "dblib":
                $query = 'SELECT TOP ' . $rowCount . ' * FROM ' . $baseName . '..' . $tableName;
                break;
            case "pgsql":
            case "mysql":
                $query = 'SELECT * FROM ' . $tableName . ' LIMIT ' . $rowCount;
                break;

        }
        if ($baseName === $this->first_base_name) {
            $result = $this->_select($query, $this->_getFirstConnect(), $this->first_base_name);
        } else {
            $result = $this->_select($query, $this->_getSecondConnect(), $this->second_base_name);
        }

        if ($result) {
            $firstRow = array_shift($result);

            $out[] = array_keys($firstRow);
            $out[] = array_values($firstRow);

            foreach ($result as $row) {
                $out[] = array_values($row);
            }
        } else {
            $out = array();
        }

        if ("utf-8" != 'utf-8' && $out) {
            // $out = array_map(function($item){ return array_map(function($itm){ return iconv(DATABASE_ENCODING, 'utf-8', $itm); }, $item); }, $out);
        }

        return $out;
    }


}