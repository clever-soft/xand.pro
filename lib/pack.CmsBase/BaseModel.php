<?php

/**
 * Class BaseModel
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
abstract class BaseModel extends BaseValidator implements InterfaceModel
{

    protected static $instance;
    public $useCache = false;
    public $cacheTime = 86400;
    protected $model;
    //
    protected $errors = array();
    protected $currentCount = 0;
    protected $searchObj;
    protected $tableName = '';
    private $isReturnedFromCache = false;
    protected $lastInserted;

    protected $attributes = [];
    protected $attribute;

    public function __construct()
    {
        parent::__construct();
        $this->model = array();
    }

    /**
     * @return string
     */
    public static function table()
    {
        $class = get_called_class();
        return self::model($class)->getTableName();
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setTableName($name)
    {
        $this->tableName = $name;
        return $this;
    }

    /**
     *
     * @param string $className
     *
     * @return \BaseModel
     */
    public static function model($className = __CLASS__)
    {
        if ($className == __CLASS__) {
            $className = get_called_class();
        }

        if (!self::$instance || !$className != __CLASS__) {
            self::$instance = new $className();
        }
        return self::$instance;
    }

    /**
     * @return string
     */
    public static function id()
    {
        return self::model()->getId();
    }


    /**
     * @return SDbQuery
     */
    public static function queryBuilder()
    {
        $class = get_called_class();
        return SDbQuery::table(self::model($class)->getTableName());
    }



    /**
     * @param $data
     * @param $key
     * @param $val
     *
     * @return array
     */
    public static function setRowsAsList($data, $key, $val)
    {
        if (!$data) {
            return [];
        }
        $ret = [];
        foreach ($data as $row) {
            if (!isset($row[$key]) || !isset($row[$val])) {
                continue;
            }
            $ret[$row[$key]] = $row[$val];
        }
        return $ret;
    }


    /**
     * @param $data
     * @param $id_field
     * @param $parent_id_field
     * @param int $level
     *
     * @return array
     */
    public function parseTree($data, $id_field, $parent_id_field, $level = 0)
    {
        $result = array();
        if (!empty($data))
            foreach ($data as $item) {
                $id = intval($item[$id_field]);
                $parent_id = intval($item[$parent_id_field]);

                if ($parent_id == $level) {
                    $result[$id] = $item;
                    $result[$id]['children'] = array();

                } elseif (isset($result[$parent_id])) {
                    if (!isset($result[$parent_id]['children'])) {
                        $result[$parent_id]['children'] = array();
                    }
                    array_push($result[$parent_id]['children'], $item);
                }
            }

        return $result;
    }

    /**
     *
     * @return \BaseModel
     */
    public function beginTransaction()
    {
        SDb::transactionStart();
        return $this;
    }

    /**
     *
     * @return \BaseModel
     */
    public function rollbackTransaction()
    {
        SDb::transactionRollBack();
        return $this;
    }

    /**
     *
     * @return boolean|int
     */
    public function holdRow()
    {
        SDbQuery::table($this->getActualTableName())->insert(array($this->getId() => NULL));
        $last = SDbBase::getLastInsertedId();

        if ($last) {
            return $last;
        } else {
            $this->setError(SDbBase::getLastError());
        }
        return false;
    }

    /**
     * @return string
     */
    private function getActualTableName()
    {
        if ($this->tableName) {
            return $this->tableName;
        } else {
            return $this->getTableName();
        }
    }

    /**
     *
     * @return \BaseModel
     */
    public function commitTransaction()
    {
        SDb::transactionCommit();
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLastQuery()
    {
        return "";
    }

    /**
     *
     * @return string
     */
    public function getClass()
    {
        return __CLASS__;
    }

    /**
     * @param null|string $field
     * @param null|string $block
     *
     * @return null|string
     */
    public function getLabel($field = null, $block = null)
    {
        $labels = array();

        if ($field && in_array($labels, array_keys($labels))) {
            return $labels[$field];
        } else {
            return $field;
        }
    }

    /**
     * @deprecated since 01.04.15
     * Better use selectSafe|actionSafe
     *
     * @param string $sql
     *
     * @return boolean|array
     */
    public function query($sql)
    {
        if ($this->useCache) {
            $cacheKey = md5(__METHOD__ . $sql);
            $cache = MCache::Get($cacheKey);
            $this->isReturnedFromCache = false;
            if ($cache) {
                $this->isReturnedFromCache = true;
                return $cache;
            }
        }
        $data = SDbBase::execute($sql);
        if (!$data) {
            $this->setError(SDbBase::getLastError());
            return false;
        }
        if ($this->useCache) {
            MCache::Set($cacheKey, $data, $this->cacheTime);
        }
        return $data;
    }

    /**
     * Был ли ответ по последней выборке из кэша
     * @return boolean
     */
    public function isLastFromCache()
    {
        return $this->isReturnedFromCache;
    }

    /**
     *
     * @outputBuffering enabled
     *
     * @param string $filter sql where
     * @param array $fields поля вернуть
     * @param array $values холдеры
     * @param array $order сортировка вида [0=> [field=>ASC|DESC] ]
     * @param string $limit
     * @param int $offset
     *
     * @return boolean|array
     */
    public function findAll($filter = '', array $fields = array(), array $values = array(), array $order = array(), $limit = '', $offset = 0)
    {
        if ($this->useCache) {
            $cacheKey = md5(__METHOD__ . $filter . serialize($fields) . serialize($values) . serialize($order) . $limit . $offset);
            $cache = MCache::Get($cacheKey);
            $this->isReturnedFromCache = false;
            if ($cache) {
                $this->isReturnedFromCache = true;
                return $cache;
            }
        }
        $realOrder = array();
        if (count($order) != count($order, COUNT_RECURSIVE)) {
            //массив двумерный -> приводим к одномерному
            foreach ($order as $orderPair) {
                list($orderKey, $orderAsc) = each($orderPair);
                $realOrder[$orderKey] = $orderAsc;
            }

        } else {
            $realOrder = $order;
        }
        $orderStr = '';
        if (is_array($realOrder)) {
            foreach ($realOrder as $key => $ro) {
                $orderStr .= $this->getActualTableName() . "." . $key . " " . $ro . ",";
            }
            $orderStr = substr($orderStr, 0, -1);
        }
        if (!empty($fields))
            $fields = implode(",", $fields);
        else
            $fields = "*";

        $data = SDb::selectFields($this->getActualTableName(), $fields, $filter, $values, $orderStr, $limit);

        if ($data === false) {
            $this->setError(SDb::getLastError());
            return false;
        } else {
            if ($this->useCache) {
                MCache::Set($cacheKey, $data, $this->cacheTime);
            }
            return $data;
        }
    }

    /**
     * Загружает в объект данные формы
     * @param array $data Форма
     * @param bool $strict - Если true, то в model идут только те ключи, котороые указаны в attributes модели
     * @return \BaseModel
     */
    public function applyFormData(array $data = array(), $strict = false)
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $this->getAttributes()) && $strict) {
                continue;
            }
            $arr = $this->isFormArray($key);
            if (!$arr[0]) {
                $this->model[$key] = $value;
            } else {
                $this->model[$arr[1]][$arr[2]] = $value;
            }
        }
        return $this;
    }

    /**
     * Определеяет имя инпута, заданное как 'data[param]' как массив
     *
     * @param string $param
     *
     * @return array массив(status,Имя блока, имя параметра)|array(false)
     */
    protected function isFormArray($param)
    {
        if (strpos($param, ']') !== false && strpos($param, '[') !== false) {
            $arr = explode('[', $param);
            $baseElem = $arr[0];
            $elem = trim($arr[1], ']');
            return array(true, $baseElem, $elem);
        }
        return array(false);
    }

    /**
     *
     * @return \BaseModel
     */
    public function resetAppliedData()
    {
        $this->model = array();
        $this->errors = array();
        return $this;
    }

    public function getAppliedData()
    {
        return $this->model;
    }

    /**
     *
     * @param int $id
     *
     * @return array
     */
    public function findById($id)
    {
        return $this->findOne($this->getId() . "=?", array(), array($id));
    }

    /**
     * @param string $filter
     * @param array $fields
     * @param array $values
     * @param array $order
     *
     * @return array
     */
    public function findOne($filter = '', array $fields = array(), array $values = array(), array $order = array())
    {
        if (empty($fields))
            $fields = ['*'];
        $fields = implode(",", $fields);
        return $this->find($filter, $fields, $values, $order);
    }

    /**
     * Запрашивает 1 запись
     * @outputBuffering enabled
     *
     * @param string $filter where поле
     * @param array $fields запрашиваемые поля
     * @param array $values значения холдеров
     * @param array $order eg [0]=>[['id']=>'DESC']
     *
     * @return array|bool
     *
     */
    public function find($filter = '', $fields, array $values = array(), array $order = array())
    {

        if (is_array($fields))
            $fields = implode(",", $fields);

        if ($this->useCache) {
            $cacheKey = md5(__METHOD__ . $filter . serialize($fields) . serialize($values) . serialize($order), 1);
            $this->isReturnedFromCache = false;
            $cache = MCache::Get($cacheKey);
            if ($cache) {
                $this->isReturnedFromCache = true;
                return $cache;
            }
        }

        $dataArr = SDb::selectFields($this->getActualTableName(), $fields, $filter, $values, $order, "0,1");
        $data = (!empty($dataArr)) ? $dataArr[0] : [];

        if ($data === false) {
            $this->setError(SDb::getLastError());
            return false;
        } elseif (!$data) {
            return array();
        } else {
            //ToDo: Anton Выяснить. Что за хрень с параметром и почему строка такая.  $this->$param = $val;
            foreach ($data as $param => $val) {
                if (in_array($param, $this->getAttributes())) {

                    $this->attribute[$param] = $val;
                }
            }
            if ($this->useCache) {
                MCache::Set($cacheKey, $data, $this->cacheTime);
            }
            return $data;
        }
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        if (empty($this->attributes)) {
            $fields = SDb::rows("SHOW FIELDS FROM " . $this->getActualTableName());
            if ($fields)
                foreach ($fields as $field) {
                    $this->attributes[] = $field['Field'];
                }
        }
        return $this->attributes;
    }

    public function runQuery($sql)
    {

    }

    /**
     *
     * @param string $filter
     * @param string $field
     * @param array $values
     * @param array $order
     *
     * @return boolean|string
     */
    public function findField($filter = '', $field = '', array $values = array(), array $order = array())
    {
        if (!$field) {
            $this->setError("No field set");
            return false;
        }
        $data = $this->findOne($filter, array($field), $values, $order);
        if ($data === false) {
            return false;
        }
        if (!$data || !isset($data[$field])) {
            return null;
        }
        return $data[$field];
    }

    /**
     *
     * @param string $filter
     * @param array $values Холдеры pdo
     *
     * @return int|boolean
     */
    public function count($filter = '', $values = array())
    {
        $sql = "SELECT count(*) AS cnt FROM " . $this->getActualTableName() . " WHERE " . ($filter ? $filter : 1);
        $do = SDb::rows($sql, $values);
        if (!$do) {
            return false;
        }
        if (isset($do[0]) && isset($do[0]['cnt'])) {
            return $do[0]['cnt'];
        }
        return false;
    }

    /**
     * @outputBuffering enabled
     *
     * @param string $sql
     * @param array $holders
     * @param string|array|boolean $fields строка с перечислением полей, которые надо выбрать. Срабатывает, если в
     *                                     запросе идет select [fields] from ...
     *
     * @return boolean|array
     */
    public function selectSafe($sql, $holders = array(), $fields = false)
    {

        if ($this->useCache) {
            $cacheKey = md5(__METHOD__ . $sql . serialize($holders) . serialize($fields));
            $before = microtime(true);
            $cache = MCache::Get($cacheKey);
            $this->isReturnedFromCache = false;
            if ($cache !== false) {
                $this->isReturnedFromCache = true;
                return $cache;
            }
        }


        if ($fields && strpos($sql, "[fields]") !== false) {
            $fields = (is_array($fields) ? implode(',', $fields) : $fields);
            $sql = str_replace("[fields]", $fields, $sql);
        }
        if (!is_array($holders)) {

        }

        $data = SDb::rows($sql, $holders);

        if ($data === false) {
            $this->setError(SDb::getLastError());
            return false;
        }
        if ($this->useCache) {
            MCache::Set($cacheKey, $data, $this->cacheTime);
        }
        return $data;
    }

    /**
     *
     */
    public function test()
    {
        echo $this->getActualTableName();
    }

    /**
     *
     * @param boolean $runValidation
     *
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $params = $this->getAttributes();

        $values = array();
        foreach ($params as $par) {
            if (isset($this->model[$par])) {
                $values[$par] = $this->model[$par];
            }
        }

        $res = SDbQuery::table($this->getActualTableName())->insert($values);
        if (!$res) {
            $this->setError(SDb::getLastError());
            return false;
        }
        $this->lastInserted = SDb::getLastInsertedId();
        return true;
    }

    /**
     *
     * @param int $id
     *
     * @return boolean
     */
    public function delete($id)
    {

        if (!SDbQuery::table($this->getActualTableName())->where([$this->getId() => $id])->delete()) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return true;
    }


    /**
     *
     * @param int $id
     * @param array $values
     *
     * @return boolean
     */
    public function updateById($id, array $values)
    {

        if (!SDbQuery::table($this->getActualTableName())->where([$this->getId() => $id])->update($values)) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return true;
    }


    /**
     *
     * @param array $values
     * @param boolean $lastInsertId
     * @param bool $ignore
     *
     * @return boolean|int
     */
    public function insert(array $values, $lastInsertId = false, $ignore = false)
    {
        $ins = SDb::insert($this->getActualTableName(), $values);
        if (!$ins) {
            $this->setError(SDb::getLastError());
            return false;
        }
        if ($lastInsertId) {
            return SDb::getLastInsertedId();
        }
        return true;
    }
    /**
     *
     * @param boolean $runValidation
     * @param string $update
     * @param array $updateHolders
     * @return boolean
     */
    public function upsert($runValidation = true, $update = '', array $updateHolders = array())
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }
        $params = $this->getAttributes();
        $vals = [];

        foreach ($params as $par) {
            if (isset($this->model[$par])) {
                $vals[$par] = $this->model[$par];
            }
        }

        if (!SDb::insertExt($this->getActualTableName(), $vals, false, false, $update, $updateHolders)) {
            $this->setError(SDb::GetLastError());
            return false;
        }
        return true;
    }
    /**
     * Обновляет таблицу по одному или нескольким полям
     *
     * @param array $where
     * @param array $values
     *
     * @return boolean
     */
    public function updateByAttributes(array $where, array $values)
    {
        if (empty($where) || !is_array($where)) {
            $this->setError('No attributes');
            return false;
        }

        SDbQuery::table($this->getActualTableName())->where($where)->update($values);
        if (SDb::getLastError()) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return true;
    }


    /**
     *
     * @param array $order
     *
     * @return string
     */
    private function prepareOrder($order = array())
    {
        $orderBy = array();
        $orderSql = '';
        foreach ($order as $f => $asc) {
            $orderBy[] = '`' . $f . '` ' . ((strtolower($asc) == 'desc') ? 'DESC' : 'ASC');
            if ($asc == end($order)) {
                $orderSql = 'ORDER BY ' . implode(',', $orderBy);
            }
        }

        return $orderSql;
    }

    /**
     *
     * @param string $filter
     * @param array $fields
     * @param array $holders
     * @param array $order [field=>ASC|DESC]
     * @param array $joinOn массив [tableBaseField,tableI18nField] - по умолчанию берется поле $this->getId()
     *
     * @return boolean|array
     */
    public function findOneI18n($filter = "", $fields = array(), $holders = array(), $order = array(), $joinOn = array())
    {
        if (!$fields) {
            $this->setError("Fields must be specified");
            return false;
        }

        $baseOn = (isset($joinOn[0]) ? $joinOn[0] : $this->getId());
        $i18nOn = (isset($joinOn[1]) ? $joinOn[1] : $this->getId());
        $tableBase = $this->getActualTableName();
        $tableI18n = $tableBase . "_i18n";
        $fieldsExt = array();
        foreach ($fields as $f) {
            $fieldsExt[] = "CASE WHEN ($tableI18n.$f IS NOT NULL AND $tableI18n.$f!='')THEN $tableI18n.$f ELSE $tableBase.$f END AS $f";
        }

        $where = ($filter) ? "where $filter" : "";
        $orderSql = $this->prepareOrder($order);


        $sql = "select [fields] from $tableBase "
            . "left join $tableI18n on $tableBase.$baseOn=$tableI18n.$i18nOn "
            . " " . $where . " " . $orderSql . " limit 0,1";


        $data = $this->selectSafe($sql, $holders, implode(',', $fieldsExt));
        $data = array_pop($data);
        return $data;
    }

    /**
     *
     * @param string $filter
     * @param array $fields Если поля указаны без таблицы - подразумевается, что они есть в обеих таблицах
     *                      Если указана таблица(table.field) - поле ищется только в указанной таблице
     * @param array $holders
     * @param array $order [field=>ASC|DESC]
     * @param int|string $limit
     * @param int $offset
     * @param array $joinOn массив [tableBaseField,tableI18nField] - по умолчанию берется поле $this->getId()
     *
     * @return boolean|array
     */
    public function findAllI18n($filter = "", $fields = array(), $holders = array(), $order = array(), $limit = '', $offset = 0, $joinOn = array())
    {
        if (!$fields) {
            $this->setError("Fields must be specified");
            return false;
        }
        $baseOn = (isset($joinOn[0]) ? $joinOn[0] : $this->getId());
        $i18nOn = (isset($joinOn[1]) ? $joinOn[1] : $this->getId());
        $tableBase = $this->getActualTableName();
        $tableI18n = $tableBase . "_i18n";
        $fieldsExt = array();
        foreach ($fields as $f) {
            if (strpos($f, ".") !== false) {
                //table
                $fieldsExt[] = $f;
            } else {
                $fieldsExt[] = "CASE WHEN ($tableI18n.$f IS NOT NULL AND $tableI18n.$f!='')THEN $tableI18n.$f ELSE $tableBase.$f END AS $f";
            }
        }

        $where = ($filter) ? "where $filter" : "";
        $orderSql = $this->prepareOrder($order);
        $limitSql = ($limit && is_numeric($limit) ? " limit $offset,$limit" : "");

        $fieldsText = implode(',', $fieldsExt);
        $sql = "select {$fieldsText} from $tableBase "
            . "left join $tableI18n on $tableBase.$baseOn=$tableI18n.$i18nOn "
            . " " . $where . " " . $orderSql . " " . $limitSql;

        $data = SDb::rows($sql, $holders);

        return $data;
    }


    /**
     *
     * @param string $sql
     * @param array $holders
     *
     * @return boolean
     */
    public function actionSafe($sql, $holders = array())
    {
        $do = SDbBase::execute($sql, $holders);
        if ($do === false) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return $do;
    }

    /**
     * @param array $filter
     *
     * @return string eg "?,?,?" - по количеству элментов в фильтре
     */
    public function getAmountOfHolders($filter = array())
    {
        $length = count($filter);
        $holders = "";
        for ($i = 0; $i < $length; $i++) {
            $holders .= "?,";
        }

        return rtrim($holders, ",");
    }


    /**
     * @param array $errors
     * @param string|null $arrayParent если нужно указать в ошибке поле вида payment[method] а в $errors
     *                                 [0]=>array('method'=>'some error')
     *
     * @return $this
     */
    protected function fillErrors($errors, $arrayParent = null)
    {
        if (!$errors || !is_array($errors)) {
            return $this;
        }
        foreach ($errors as $err) {
            if (is_array($err) && $arrayParent) {
                foreach ($err as $key => $errVal) {
                    $this->setError(array($arrayParent . "[$key]" => $errVal));
                }
            } else {
                $this->setError($err);
            }
        }
        return $this;
    }

    /**
     *
     * @param string $filter
     * @param array $holders
     * @return boolean
     */
    public function deleteByAttributes($filter, $holders = array())
    {
        if (!SDb::delete($this->getActualTableName(), $filter, $holders)) {
            $this->setError(SDb::GetLastError());
            return false;
        }
        return true;
    }

    /**
     *
     * @param string $where
     * @param array $data
     * @param array $whereHolders
     * @return boolean
     */
    public function upsertSimple($where, $data, $whereHolders = array())
    {
        $check = $this->findOne($where, array(), $whereHolders);
        if ($check) {
            if (!SDb::update($this->getActualTableName(), $data, $where, $whereHolders)) {
                $this->setError(SDb::GetLastError());
                return false;
            }
            return true;
        } else {
            return $this->insert($data);
        }
    }

    /**
     * @return mixed|int
     */
    public function getLastInserted()
    {
        return $this->lastInserted;
    }


    /**
     * Инкрементируем указанное поле
     * @param string $where
     * @param string $field
     * @param string $extraUpdate
     * @param array $holders
     * @return array|bool
     */
    public function increment($where, $field, $extraUpdate = "", $holders = [])
    {
        $setBlock = "`$field`=`$field`+1";
        if ($extraUpdate) {
            $setBlock .= "," . $extraUpdate;
        }
        $sql = "update " . $this->getActualTableName() . " set $setBlock  where $where";
        return $this->actionSafe($sql, $holders);
    }


    /**
     * @param $keyField
     * @param $valField
     * @param string $filter
     * @param array $holders
     * @return array
     */
    public function findControlList($keyField, $valField, $filter = '', $holders = [])
    {
        $select = array_unique([$keyField, $valField]);
        $data = $this->findAll($filter, $select, $holders);
        $res = [];
        if (!$data) {
            return $res;
        }
        foreach ($data as $row) {
            $res[$row[$keyField]] = $row[$valField];
        }

        return $res;
    }

    /**
     * @param $where
     * @param array $whereHolders
     * @param bool $runValidation
     * @return bool
     */
    public function saveUpdate($where, $whereHolders = [], $runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $params = $this->getAttributes();

        $vals = [];
        foreach ($params as $par) {
            if (isset($this->model[$par])) {
                $vals[$par] = $this->model[$par];
            }
        }
        $do = $this->updateByRaw($where, $vals, $whereHolders);
        if (!$do) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return true;
    }

    /**
     * Обновляет таблицу по одному или нескольким полям
     * @param string $filter
     * @param array $values
     * @param array $holders
     * @return boolean
     */
    public function updateByRaw($filter, array $values, $holders = array())
    {
        if (empty($filter) || !is_string($filter)) {
            $this->setError('No attributes');
            return false;
        }
        $select = array();
        $fval = $holders;

        if (!SDb::update($this->getActualTableName(), $values, $filter, $fval)) {
            $this->setError(SDb::getLastError());
            return false;
        }
        return true;
    }

    /**
     * Переключение булевского поля(1 меняется на 0, 0 на 1)
     * @param string $filter
     * @param string $field
     * @param array $holders
     * @param string $update additional fields to update along with the toggled one
     * @param array $updateHolders
     * @return bool
     */
    public function toggle($filter, $field, $holders = array(), $update = "", $updateHolders = [])
    {
        $upd = ($update) ? "`$field`= NOT `$field` , " . $update : "`$field`= NOT `$field`";
        foreach ($holders as $hold) {
            $updateHolders[] = $hold;
        }
        return $this->actionSafe("update " . $this->getActualTableName() . " set $upd where $filter", $updateHolders);
    }

    /**
     * @param string $field
     * @param string $filter
     * @param array $holders
     * @param array $order
     * @param string|int $limit
     * @param int $offset
     * @return array|bool
     */
    public function findFieldAsList($field, $filter = "", $holders = array(), $order = array(), $limit = '', $offset = 0)
    {
        if (!$field) {
            $this->setError("No field set");
            return false;
        }

        $where = ($filter) ? "where $filter" : "";
        $orderSql = $this->prepareOrder($order);
        $limitSql = ($limit && is_numeric($limit) ? " limit $offset,$limit" : "");
        $q = "select `$field` from " . $this->getActualTableName() . " " . $where . " " . $orderSql . " " . $limitSql;
        $data = $this->selectSafe($q, $holders);
        if ($data === false) {
            return false;
        }
        if (!$data) {
            return $data;
        }

        $list = array();
        foreach ($data as $row) {
            $list[] = $row[$field];
        }

        return $list;
    }
}
