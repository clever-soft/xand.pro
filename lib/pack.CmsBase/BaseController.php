<?php

/**
 * Class BaseController
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class BaseController extends BaseComponent
{

    const EVENT_ACTION_BEFORE = 'preaction';
    const EVENT_ACTION_AFTER = 'postaction';
    const DEFAULT_CONTROLLER = 'default';

    protected $tpl;
    protected $post = array();
    protected $get = array();
    protected $home;

    protected $currentModule, $currentController, $currentAction;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->post = filter_input_array(INPUT_POST);
        $this->get = filter_input_array(INPUT_GET);
        if (!$this->get && isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI']) {
            $url = parse_url($_SERVER['REQUEST_URI']);
            if (isset($url['query']) && $url['query']) {
                parse_str($url['query'], $get);
                $this->get = $get;
            }
        }
        $this->home = DIRECTORY_SEPARATOR . SITE_CURRENT_LANG;
    }

    /**
     *
     * @param int $code
     *
     * @return string|null
     */
    protected static function getCode($code)
    {
        switch ((int)$code) {
            case 200:
                return 'OK';
            case 404:
                return 'Not found';
            case 403:
                return 'Forbidden';
            case 500:
                return 'Internal server error';
        }
        return null;
    }


    /**
     *
     * @param string $key
     * @param mixed|null $onEmpty значение которое вернется, если ключа нет
     *
     * @return mixed
     */
    public function getPost($key, $onEmpty = null)
    {
        return (isset($this->post[$key]) ? $this->post[$key] : $onEmpty);
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return array();
    }

    /**
     * Настройки приватности для экшенов
     * level:<br>
     * ? - гость - нет доступа
     * @ - юзер - обычно вместе с фильтром по полю юзера
     * для указания фильтра среди юзеров<br>
     * например по группе: добавляем в массив '_idGroup'=>[1,3...] <br>
     * где поле модели цказывается как _field, а значение - массив допустимых значений, например допустимых груп
     *
     * так же msg=>сообщение, которые выводится юзеру
     * call=>callback - функция, вызываемая если юзеру ограничен доступ
     * callSuccess =>callback, функция выполняемая если все ok
     * @return array
     */
    public function getAccessRules()
    {
        return array();
    }

    /**
     *
     * @param string $name контроллер
     * @param string $module модуль
     * @param string $action экшн
     *
     * @return boolean
     */
    public function doesControllerActionExist($name, $module, $action)
    {
        $controller = ucfirst(strtolower($name)) . "Controller";
        if (!class_exists($controller)) {
            $path = PATH_REAL . DS . "mod/" . $module . "/controllers/" . $controller . ".php";
            if (!is_file($path)) {
                return false;
            }
            include_once $path;
        }

        //        $aliases = $this->getControllerAliases($name, $module);
        //        $act = (isset($aliases[$action]) ? $aliases[$action] : $action);
        if (method_exists(new $controller, "action" . ucfirst(strtolower($action)))) {
            return true;
        }
        return false;
    }

    /**
     * метод для обработки виртуальных действий
     * должен быть переопределен в контроллерах
     */
    public function artificialAction()
    {

    }

    /**
     *
     * @return boolean
     */
    public function isAjax()
    {
        if (strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
            return true;
        }
        return false;
    }

    /**
     * выполняет действие
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     *
     * @return boolean
     */
    public function run($module, $controller, $action)
    {
        Xand::$currentModule = $module;

        list(Xand::$currentModule, $controller, $action) = $this->redirectRules(Xand::$currentModule, $controller, $action);

        $modulePath = PATH_REAL . DS . PATH_MODULES . DS . strtolower(Xand::$currentModule);
        if (!is_dir($modulePath)) {

            Xand::$currentModule = XAND_DEFAULT_MODULE;
            $modulePath = PATH_REAL . DS . PATH_MODULES . DS . strtolower(Xand::$currentModule);

            if (!is_dir($modulePath)) {
                header("location: " . PATH_DS);
                exit();
            }
        }

        $controllerFile = $modulePath . "/controllers/" . ucfirst(strtolower($controller)) . "Controller.php";
        if (!file_exists($controllerFile)) {
            $controller = self::DEFAULT_CONTROLLER;
            $controllerFile = $modulePath . "/controllers/" . ucfirst(strtolower($controller)) . "Controller.php";
            if (!is_file($controllerFile)) {
                header("location: " . PATH_DS);
                exit();
            }
        }

        require_once $controllerFile;
        $controllerName = ucfirst(strtolower($controller)) . "Controller";

        $contr = new $controllerName();

        //check aliases
        $al = $contr->getAliases();
        if (!empty($al) && array_key_exists(strtolower($action), $al)) {
            $action = $al[strtolower($action)];
        }
        $method = "action" . ucfirst(strtolower($action));
        $rawAction = strtolower(substr_replace($method, '', 0, 6));

        $this->currentAction = $method;


        $shift = $contr->shiftAction($rawAction);
        if ($shift) {
            $this->currentAction = "action" . ucfirst(strtolower($shift));
        }

        if (!method_exists($contr, $this->currentAction)) {
            $vaction = $contr->defineAction($action);
            if (!$vaction) {
                $this->errors[] = "Action " . $module . "::" . $controller . "->" . $action . " not found";
                return false;
            }
            $this->currentAction = "action" . ucfirst(strtolower($vaction));
        } else {
            $vaction = $action;
        }

        call_user_func_array(array($contr, $this->currentAction), array());
        return true;
    }

    /**
     *
     * @param type $module
     * @param type $controller
     * @param type $action
     *
     * @return array
     */
    protected function redirectRules($module, $controller, $action)
    {
        $redirects = new RedirectsModel();
        $redirectsAll = $redirects->findAll("fModule = '{$module}' AND isActive = 1");
        if (!$redirectsAll)
            return array($module, $controller, $action);
        foreach ($redirectsAll as $redirect) {
            if ($module == $redirect['fModule']) {
                if (isset($redirect['tModule']) && !empty($redirect['tModule'])) {
                    if (empty($redirect['fController']) && empty($redirect["fAction"]))
                        $module = $redirect['tModule'];
                }
                if ($controller == $redirect['fController'] || empty($redirect['fController'])) {
                    if (isset($redirect['tController']) && !empty($redirect['tController'])) {
                        $module = $redirect['tModule'];
                        if (empty($redirect["fAction"])) {
                            $controller = $redirect['tController'];
                        }
                    }
                    if ($action == $redirect['fAction'] || empty($redirect['fAction'])) {
                        $module = $redirect['tModule'];
                        if (!empty($redirect['tController']))
                            $controller = $redirect['tController'];
                        if (isset($redirect['tAction']) && !empty($redirect['tAction'])) {
                            $action = $redirect['tAction'];
                        }
                    }
                }
            }
        }
    }

    /**
     * fallback, сработает, если в контроллере не объявлен дефолтный экшен
     */
    public function actionDefault()
    {
        echo "Error actionDefault";
    }

    /**
     * fallback для экшена валидации через ajax
     */
    public function action__ajaxValidate()
    {
        $this->renderAsJson(true);
    }

    /**
     *
     * @param string $template
     * @param bool|string $dir
     *
     * @return type
     */
    protected function getTpl($template, $dir = false)
    {
        if (!$this->tpl instanceof TemplateCms) {
            $this->tpl = new TemplateCms($template, $dir, SITE_CURRENT_LANG);
        }

        return $this->tpl;
    }

    /**
     * Возвращает параметр, если он был передан как /mod/contr/act?id=1 (id) - для get
     *
     * @param $key
     *
     * @return null
     */
    protected function getParam($key)
    {
        if (isset($this->get[$key])) {
            return $this->get[$key];
        }
        if (isset($this->post[$key])) {
            return $this->post[$key];
        }

        return null;
    }

    /**
     * Проверяет доступ к действия для текущего юзера/гостя
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     *
     * @return boolean
     */
    protected function checkAccess($module, $controller, $action)
    {
        //запрос на доступность действия
        if ($action == 'ok' && $module == 'wtf') {
            return false;
        }
        return true;
    }


    /**
     *
     * @param string $from часть урла, которая выступает в качестве экшена
     *
     * @return bool
     */
    protected function shiftAction($from)
    {
        return false;
    }

    /**
     *
     * @return string
     */
    protected function getControllerName()
    {
        return str_replace('Controller', '', __CLASS__);
    }

    /**
     *
     * @return string
     */
    protected function getModuleName()
    {
        return '';
    }

    /**
     *
     * @param string $name Имя контроллера
     * @param string $module Имя модуля
     *
     * @return array
     */
    protected function getControllerAliases($name, $module)
    {
        $contr = ucfirst(strtolower($name)) . "Controller";
        if (!class_exists($contr)) {
            $path = PATH_REAL . DS . "mod/" . $module . "/controllers/" . $contr . ".php";
            if (!is_file($path)) {
                return array();
            }
            include_once $path;
        }
        $c = new $contr;
        return $c->getAliases();
    }

    /**
     * Определяет какое действие вызвать при виртуальном action
     *
     * @param string $action
     *
     * @return boolean
     */
    protected function defineAction($action = '')
    {
        return 'default';
    }

    /**
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     * @param string $type
     */
    protected function runEventHandlers($module, $controller, $action, $type = self::EVENT_ACTION_BEFORE)
    {
    }

    /**
     * вывод контента
     */
    protected function render()
    {
        if ($this->page) {
            foreach ($this->page as $k => $p) {

                if ($k == "content") {
                    $p = $this->tpl->Render($p);
                }

                $this->tpl->assign($k, $p);
            }
        }

    }


}
