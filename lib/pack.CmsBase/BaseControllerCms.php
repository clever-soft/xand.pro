<?php

/**
 * Class BaseControllerCms
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class BaseControllerCms extends BaseController
{

    protected $path;
    protected $pagesM;
    protected $page;


    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->pagesM = new PagesModel();
    }


    /**
     * @param $path
     *
     * @return $this|bool
     */
    protected function renderLayout($path)
    {

        if (!$this->tpl instanceof TemplateCms) {
            return false;
        }

        $this->tpl->assign('languages', explode(",", SITE_LANGS));
        $this->tpl->assign("menuActive", $path);
        $this->tpl->assign("menuLeft", $this->GetSiteMenu($this->page["path"]));

        //$this->getMenuL1($path);
        $this->getTextBlocks();


        return $this;
    }


    /**
     * @param $materializedPath
     *
     * @return array
     */
    public function getSiteMenu($materializedPath) {
        $menu = array();
        $path = trim(str_replace("//", "/", "/0/" . $materializedPath), "/");
        $pathArrShift = $pathArr = explode("/", $path);
        array_shift($pathArrShift);

        foreach ($pathArr as $k => $p) {
            if (!is_numeric($p)) {
                continue;
            }

            $menuLevelItems = $this->pagesM->findAllWithTranslate(array(
                'where' => array(
                    'row' => 'isActive=1 AND idParent = ? AND isSystem = 0',
                    'params' => array($p)
                ),
                'fields' => array('url', 'menuTitle', 'idPage'),
                'order' => array('npp' => 'ASC')
            ));

            if ($menuLevelItems) {
                $next = (isset($pathArrShift[$k])) ? $pathArrShift[$k] : -1;
                foreach ($menuLevelItems as &$items) {
                    $items["active"] = ($items["idPage"] == $next);
                }
                $menu[$p] = $menuLevelItems;
            }
        }

        $this->tpl->assign('menuName', I18n::__("%Menu%"));


        return $menu;
    }



    /**
     * @param string $path uri
     * @param int $code    http code
     * @param string $content
     * @param string $title
     */
    protected function httpError($path, $code, $content = '', $title = '')
    {
        $msg = self::getCode($code);
        if ($msg) {
            header("HTTP/1.1 " . $code . " " . $msg);
        }
        $this->getTpl("cms/" . $code);
        $this->renderLayout($path);

        $this->tpl
            ->assign("path", $path)
            ->assign("h1", (empty($title) ? I18n::__("%Error%") : $title))
            ->assign("title", (empty($title) ? I18n::__("%Error%") : $title))
            ->assign("content", $content)
            ->Load();
    }

    /**
     *
     * @return int
     */
    public function getStore()
    {
        if (isset($_SESSION['store']['idStore'])) {
            return $_SESSION['store']['idStore'];
        }
        return null;
    }


    /**
     * @return $this
     */
    protected function getTextBlocks()
    {
        $blocks = PagesModel::getBlocks();

        if ($blocks) {
            foreach ($blocks as $k => $p) {
                $p["content"] = nl2br($p["content"]);
                $this->tpl->assign("block-" . $p["code"], $p["content"]);
            }
        }

        return $this;
    }


    /**
     *
     */
    protected function SetBreadcrumbs()
    {

        $criteria = trim(str_replace("/", ",", $this->page["path"]), ",");
        $criteriaArr = explode(',', $criteria);
        $criteriaRow = str_repeat('?,', count($criteriaArr));
        $criteriaRow = trim($criteriaRow, ',');

        $crumbs = $this->pagesM->findAllWithTranslate(array(
            'where' => array(
                'row' => $this->pagesM->getTableName() . '.' . 'idPage in (' . $criteriaRow . ')',
                'params' => $criteriaArr
            ),
            'fields' => array('url', 'menuTitle'),
            'order' => array("level" => "ASC")
        ));

        $menuName = (count($crumbs) > 1) ? $crumbs[count($crumbs) - 2]["menuTitle"] : "%Menu%";

        $this->tpl->assign("menuName", $menuName);
        $this->tpl->assign("breadcrumbs", $crumbs);

    }


}
