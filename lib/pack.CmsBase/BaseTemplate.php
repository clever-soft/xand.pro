<?php

/**
 * Class BaseTemplate
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class BaseTemplate
{

    public $data = array();

    /**
     * @param $arr
     *
     * @return $this
     */
    public function assignArray($arr)
    {
        if (!($arr)) return $this;

        foreach ($arr as $k => $v)
            $this->assign($k, $v);

        return $this;
    }

    /**
     * @param $key
     * @param $val
     *
     * @return $this
     */
    public function assign($key, $val)
    {
        $this->data[$key] = $val;
        return $this;
    }
}
