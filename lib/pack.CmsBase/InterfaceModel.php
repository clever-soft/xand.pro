<?php

/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 13.04.2017
 * Time: 10:28
 */
interface InterfaceModel
{
    function getTableName();

    function getId();
}