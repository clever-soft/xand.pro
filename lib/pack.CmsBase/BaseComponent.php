<?php

/**
 * Содержит самый базовый функционал, не нагружать логикой
 *
 * @author kubrey
 */
class BaseComponent
{

    protected $errors = array();
    protected $log;

    public function __construct()
    {
        $this->log = strtolower(__CLASS__) . ".log";
    }

    public function setError($error)
    {
        $this->errors[] = $error;
    }

    public function getLastError()
    {
        if ($this->errors) {
            return end($this->errors);
        }
        return null;
    }

    /**
     *
     * @param int $last - вернуть последние $last ошибок
     * @return array
     */
    public function getErrors($last = 0)
    {
        if ((int)$last > 0 && $this->errors) {
            $errs = $this->errors;
            $ret[] = array_pop($errs);
            $ret[] = array_pop($errs);
            return $ret;
        }
        return $this->errors;
    }

    /**
     *
     * @return string
     */
    public function getErrorsPretty()
    {
        if (!$this->errors) {
            return '';
        }
        $errors = [];
        foreach ($this->errors as $err) {
            $errors[] = (is_array($err) ? current($err) : $err);
        }
        return implode('<br/>', $errors);
    }

    /**
     *
     * @return string
     */
    public function getErrorsAsString()
    {
        return serialize($this->errors);
    }

    /**
     *
     * @param array $arr
     * @return stdClass
     */
    public static function arrayToObject($arr)
    {
        return json_decode(json_encode($arr), false);
    }

    /**
     *
     * @param object $obj
     * @return array
     */
    public static function objectToArray($obj)
    {
        if (!is_object($obj)) {
            return array();
        }
        return json_decode(json_encode($obj), true);
    }

    /**
     * Для переноса массива ошибок из одного класса в другой
     * Обычно для случаев вызова одной модели в другой
     * @param array $errors
     * @param null|string $container если массив ошибок нужно скопировать с ключами вида [$container[$field]=>$val]
     * @return $this
     */
    public function copyErrors($errors = [], $container = null)
    {
        foreach ($errors as $err) {
            if (is_array($err) && count($err) == 1) {
                list($key, $val) = each($err);
                $err = [$key = $container . "[" . $key . "]" => $val];
            }
            $this->setError($err);
        }
        return $this;
    }

    /**
     * @param $msg
     * @param null|string $file
     * @return $this
     */
    public function doLog($msg, $file = null)
    {
        $logFile = $file ? $file : $this->log;
        Debug::logMsg($msg, $logFile);
        return $this;
    }

    /**
     * @return $this
     */
    public function flushErrors()
    {
        $this->errors = [];
        return $this;
    }

}
