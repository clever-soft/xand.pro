<?php

class FF extends FFBase
{
    //---
    public function Render()
    {

        $htmlObjects = $this->RenderObjects();

        $html = "<form id='{$this->id}'
                        method='POST'
                        class='ff'
                        action='{$this->action}'
                        enctype='{$this->enctype}'>

                        <ul style='padding-bottom: 10px;'>
                            <li/><li>
                            <li/><h4>{$this->title}</h4><li>
                            </li><li>
                        </ul>

                        {$htmlObjects}

                        <ul style='padding-bottom: 10px;'>
                            <li/><li>
                            <li/><div id='{$this->id}Response' style='padding-top: 10px;'></div></h4><li>
                            </li><li>
                        </ul>

                    </form>

                    ";



        return $html;
    }

}