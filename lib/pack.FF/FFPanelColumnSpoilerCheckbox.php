<?php

/**
 * Class FFPanelColumnSpoilerCheckbox
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFPanelColumnSpoilerCheckbox extends FFPanelColumnSpoiler
{
    public $script;
    public $show;

    /**
     * @param $options
     */
    public function SetDefaults($options)
    {
        $this->script = (isset($options["script"])) ? $options["script"] : "ColumnSpoilerCheckboxChange(this);";
        $this->show = (isset($options["show"])) ? $options["show"] : true;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {

        $checked = ($data) ? " checked='checked'" : "";
        $showIcon = ($this->show) ? "minus" : "plus";
        $showBlock = ($this->show) ? " groupColumnSpoilerWrapActive " : "";

        $html = "<div>
                    <div class='groupColumnSpoilerWrap {$showBlock}'>
                    <h3 onclick='GroupColumnSpoiler(this); return false;'>
                            <i class='fa fa-{$showIcon}-square-o'></i>
                            <span>{$this->title}</span>
                    </h3>

                    <div style='padding: 0 0 5px 28px;'>
                            <input  type='checkbox' 
                                        name='{$this->name}'
                                        onchange='{$this->script}'
                                        class='' {$checked} />

                                {$this->placeholder}
                    </div>

                    <div class='groupSpoiler'>
                    <div class='groupColumnsSpoiler'>";
        
        $html .= $this->RenderContainer();
        $html .= "</div></div></div></div>";

        if ($checked) {
            $html .= "<script> 
                  document.addEventListener('DOMContentLoaded', function (event) {
                             ColumnSpoilerCheckboxChange($('#{$this->key}'));
                    }); 
                </script>";
        }

        return $html;

    }


}