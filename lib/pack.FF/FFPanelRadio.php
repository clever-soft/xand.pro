<?php

/**
 * Class FFPanelRadio
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFPanelRadio extends FFObjectBase implements IFFContainer
{
    public $group;
    public $active;
    public $title2;
    public $options = array();

    /**
     * @param $options
     */
    protected function SetDefaults($options)
    {
        if (isset($options["group"])) $this->group = $options["group"];
        if (isset($options["title2"])) $this->title2 = $options["title2"];
    }

    /**
     * @return string
     */
    protected function RenderTitle()
    {
        return "";
    }

    /**
     * Чтобы указать двумерный name для радио нужно передать в options['group'=>['parent','child']] ->
     * name=parent[child]
     *
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        if (is_array($this->group)) {
            list($topBlock, $childBlock) = $this->group;
            $name = "$topBlock" . "[" . $childBlock . "]";
            $this->active = (isset($this->model[$topBlock][$childBlock]) && ($this->model[$topBlock][$childBlock] == $this->key)) ? true : false;
        } else {
            $name = $this->group;
            $this->active = (isset($this->model[$this->group]) && ($this->model[$this->group] == $this->key)) ? true : false;
        }

        $html = "";
        $html .= "<li style='text-align: left;' class='radioWrap'>";

        $selected = ($this->active) ? "checked='checked'" : "";
        $panelClass = ($this->active) ? "" : "panelRadioHidden";

        $html .= "<div class='panelRadioWrap'>
                      <label style='padding: 0; padding-bottom: 5px;'>
                            <input  type='radio'
                                    name='{$name}'
                                    value='{$this->key}'
                                    {$selected}
                                    onchange='PanelRadioChange(this);'
                                    />
                            <span>{$this->title}</span>
                            <span class='title2'>{$this->title2}</span>
                      </label>

                      <div class='panelRadio {$panelClass}'>";

        $html .= "<div class='panelRadioColumn'>";
        $html .= $this->RenderContainer();
        $html .= "</div>";

        if ($this->placeholder)
            $html .= "<div class='panelRadioColumn panelRadioPlaceholder' >{$this->placeholder}</div >";

        $html .= "</div><div style='clear: both'></div></div>";
        $html .= "</li>";

        return $html;

    }

}