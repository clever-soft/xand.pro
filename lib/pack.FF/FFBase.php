<?php

/**
 * Class FFBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFBase extends FFObjectBase
{

    public $title;
    public $action;
    public $id;

    public $actions = array();
    public $objects = array();
    public $model = array();
    public $tooltips = array();

    public $validated = array();
    public $errors = array();

    protected $validateJS = true;
    protected $dataType = "html";
    protected $actionsAlign = "center";
    protected $actionsPadding = 0;
    protected $callbacks = array();

    protected $objectsFlat = array();
    protected $enctype = 'text/plain';



    /**
     * FFBase constructor.
     *
     * @param string $title
     * @param string $action
     * @param array $options
     */
    public function __construct($title = "Unnamed", $action = "", $options = array())
    {

        $this->title = $title;
        $this->action = $action;
        $this->id = (isset($options['id']) ? $options['id'] : md5($this->action));
        $this->dataType = (isset($options['dataType']) ? $options['dataType'] : $this->dataType);

        $this->callbacks = (isset($options['callbacks']) && is_array($options['callbacks']) ? $options['callbacks'] : $this->callbacks);
        $this->class = (isset($options["class"])) ? $options["class"] : 'ff-horizontal';
 
        $this->validateJS = (isset($options["validateJS"])) ? $options["validateJS"] : $this->validateJS;
        $this->tooltips = (isset($options["tooltips"])) ? $options["tooltips"] : $this->tooltips;
        $this->actionsAlign = (isset($options["actionsAlign"])) ? $options["actionsAlign"] : $this->actionsAlign;
        $this->actionsPadding = (isset($options["actionsPadding"])) ? $options["actionsPadding"] : $this->actionsPadding;

    }

    /**
     * @param $enctype
     */
    public function setEnctype($enctype)
    {
        $possibleEnctypes = array('text/plain', 'multipart/form-data', 'application/x-www-form-urlencoded');
        if (!in_array($enctype, $possibleEnctypes)) {
            return;
        }
        $this->enctype = $enctype;
    }

    /**
     * @return string
     */
    protected function RenderActions()
    {
        $html = "";
        foreach ($this->actions as $key => $o) {
            $html .= $o->Render(false);
        }

        return $html;
    }

    /**
     * @return string
     */
    protected function RenderObjects()
    {
        $html = "";

        foreach ($this->objects as $key => $o) {
            if ($o instanceof IFFContainer) {
                $o->Fill($this->model);
            }

            $value = (isset($this->model[$key])) ? $this->model[$key] : "";
            $html .= $o->Render($value);
        }

        return $html;
    }

    /**
     *
     */
    public function Draw()
    {
        echo $this->Render(false);
    }


    /**
     * @param $inputArr
     *
     * @return bool
     */
    public function Validate($inputArr)
    {
        $this->GetNestingObjects($this->objects); 
        return $this->RunValidate($this->objectsFlat, $inputArr);
    }

    /**
     * @param $inputArr
     * @param bool $blockName
     *
     * @return bool
     */
    public function ValidateBlock($inputArr, $blockName = false)
    {
        $this->GetNestingObjects($this->objects, $blockName);
 
        return $this->RunValidate($this->objectsFlat, $inputArr);
    }

    /**
     * @param $inputArr
     *
     * @return bool
     */
    public function ValidateNested($inputArr)
    {
        $flatInputArr = array();

        foreach ($inputArr as $k => $a) {
            if (is_array($a)) {
                foreach ($a as $k1 => $a1) {
                    $flatInputArr[$k1] = $a1;
                }
            } else {
                $flatInputArr[$k] = $a;
            }
        }

        return $this->Validate($flatInputArr);
    }


    /**
     * @param array $objects
     * @param array $inputArr
     *
     * @return bool
     */
    private function RunValidate($objects = array(), $inputArr = array())
    {
        $this->validated = $inputArr;
        $this->errors = array();

        foreach ($objects as $key => $object) {
            $value = (isset($inputArr[$key])) ? $inputArr[$key] : "";

            if (!$object->slaveKey) {
                $test = $object->requiredRule->Test($value, $object);
            } else {
                $slaveValue = (isset($inputArr[$object->slaveKey])) ? $inputArr[$object->slaveKey] : false;
                $test = $object->requiredRule->Test($value, $object, array($object->slaveKey => $slaveValue));
            }

            if ($test["errors"]) {
                $this->errors[$key] = $test["errors"];
            }

            $this->validated[$key] = $test["corrected"];
        }

        $this->objectsFlat = array();

        return (empty($this->errors)) ? true : false;

    }

    /**
     * @param $objects
     * @param string $block
     */
    private function GetNestingObjects($objects, $block = "")
    {
        foreach ($objects as $o) {
            if (!$o->key) continue;

            if ($o->key2 == $block) {
                foreach ($o->objects as $o2) {
                    if ($o2->required) {
                        $this->objectsFlat[$o2->key] = $o2;
                    }
                }

            }

            if (!$block && $o->required) {
                $this->objectsFlat[$o->key] = $o;
            }

            if (!empty($o->objects)) {
                $this->GetNestingObjects($o->objects, $block);
            }

        }

    }


}