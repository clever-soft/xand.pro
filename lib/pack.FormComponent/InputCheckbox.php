<?php

class InputCheckbox extends InputBase
{
    //---------------
    public function SetDefaults()
    {
        $this->setProperty("eventOnClick", "")
            ->setProperty("titleWidth", 160)
            ->setProperty("disabled", false);

    }

    //---------------
    public function GetDataJson()
    {
        if ($this->properties['ignore'] == false && !$this->properties['disabled'])
            return ", '" . $this->key . "': CheckboxGetData('{$this->domId}')";
    }

    //---------------
    public function Draw()
    {
        $this->data = (isset($this->properties['dataForce'])) ? $this->properties['dataForce'] : $this->data;

        $check = ($this->data == 1) ? 'Sel' : 'UnSel';
        $class = ($this->properties['disabled']) ? " checkboxDisabled checkboxDisabled{$check}" : "checkbox{$check}";

        if (!isset($this->properties['titleDisable'])) {
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>";
            if (!isset($this->properties['titleRight']))
                echo "<span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
        }

        echo "<a href='' id='{$this->domId}' class='checkbox {$class}' onclick='CheckboxClick(this); return false;'>
				" . $this->getProperty('requirements') . "
			  </a>";

        if (isset($this->properties['titleRight'])) echo "<label class='inputCheckboxLabel' for='{$this->domId}'>{$this->name}</label>";

        if ($this->getProperty('warning') != "")
            echo "<div style='margin-top:5px'>
				   	<div class='messageWrap messageError' style='border-radius: 0; padding:10px; display:inline-block;'>{$this->properties['warning']}</div>
				  </div>";


        if (!isset($this->properties['titleDisable'])) echo "</div>";

    }

}

?>