<?php

class ControlSpoiler extends ControlTable {

    //---------------
    public function Draw() {

        $cssShow = ($this->getProperty('show')) ? "controlGrouperDown" : "controlGrouperUp";
        $sign = ($this->getProperty('show')) ? "▼" : "►";

        echo "<div class ='formElement'>
		 	 <div class = 'controlGrouperTitle' onclick='GrouperSpoiler{$this->domId}(this);'><span>{$sign}</span> {$this->name}</div>
		 	 <div class='{$this->getProperty('class')} {$cssShow}'>";
        ?> 
         

        <?php
        echo $this->data;
        echo "<div style='clear:both'></div>";
        echo "</div></div>";
        ?>

        <script>
            function GrouperSpoiler<?php echo $this->domId; ?>(dom)
            {
                var cont =  $(dom).next();  
                if(cont.hasClass("controlGrouperUp"))
                {
                    cont.removeClass("controlGrouperUp").addClass("controlGrouperDown").slideDown(500);
                    $(dom).find("span").html("▼");
                }
                else
                {
                    cont.removeClass("controlGrouperDown").addClass("controlGrouperUp").slideUp(500);
                    $(dom).find("span").html("►");
                }			
                			
                if(Wnd.Align !== undefined) Wnd.Align();			
                			 	 
            }
        </script>



        <?php
    }

    //---------------
}
?>

