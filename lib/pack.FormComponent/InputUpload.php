<?php
 
class InputUpload extends InputBase
{

    //---
	public function SetDefaults()
	{
		$this->setProperty("ignore", false)			
		     ->setProperty("callback_width", "300px")
			 ->setProperty("callback_style", "")
			 ->setProperty("callback_custom", false)	
		 	 ->setProperty("requirements", "")
			 ->setProperty("titleWidth",160) 
			 ->setProperty("fileType", "");
	}

    //---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."_value').val()";		
	}

    //---
	public function DrawCallback()
	{
		?>
		 <div id='<?php echo $this->domId; ?>_callback'><?php echo $this->getProperty('callback_data'); ?></div>	                       
      <?php	
	}

    //---
	public function CallbackToString()
	{	
		return "<div style='margin:5px 0 0 0;' id='".$this->domId."_callback' ".$this->getProperty('callback_style')." >".$this->getProperty('callback_data')."</div>";
    }

    //---
	public function Draw()
	{
			//var_dump($this);
			
	 		$this->properties["height"] = ($this->getProperty("height") != "") ? " style='height: {$this->properties['height']} ;'" : "";
			
			$this->properties["fileType"] = ($this->getProperty("fileType") != "") ? " accept='{$this->properties['fileType']}'" : "";	
			
						 
			?>
			 	 
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formElement">
                     <tr>                           
						  <td valign="middle" width="<?php echo $this->properties['titleWidth']; ?>px">                          
						  <?php  echo "<span>{$this->name}</span>";  ?>   
                          </td>   
                          
                          <td width="120px">
                            <form  action="<?php echo PATH_DS.$this->getProperty('file'); ?>" 
                            method="post" id="form_<?php echo $this->domId; ?>" enctype="multipart/form-data" class="inputUploadForm">
                               
                               <div class="inputUploadMask">                               		
                                    <span id="<?php echo $this->domId; ?>_text">Choose file</span>    
                                    <span id="<?php echo $this->domId; ?>_loading"  class="inputUploadLoading"></span> 
                              	   
                               </div>
                               <input type="file" class='inputUpload' name="file_upload" <?php echo $this->properties["fileType"]; ?>
                               onchange="InputUploadProccess('<?php echo $this->domId; ?>'); return false;">
                              
                               <input id="<?php echo $this->domId; ?>" type="hidden" name="domId" style="height:24px" value="<?php echo $this->domId; ?>">
                               <input id="<?php echo $this->domId; ?>_value" type="hidden" name="domId_value" style="height:24px" value="<?php echo $this->data; ?>">  
                            </form>
                          </td>                         
                                                              
                          <td valign="middle">                        
						  <?php
                          	 if($this->getProperty('requirements') != "") 	
							 {	
								echo "<div class='formRequirements'>".$this->properties['requirements']."</div>";		 						
							 }
                          ?>   
                          </td></tr>                          
                          <tr>
                          <td></td><td colspan="2">
                          	<div style="padding-top:5px;">
                         	   <?php $this->DrawCallback();	 ?> 
                            </div>
                          </td>
                          </tr>
                          
                          </table> 

			<?php			
			
	}
	//---
}
?>