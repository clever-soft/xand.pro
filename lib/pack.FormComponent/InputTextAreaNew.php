<?php
/**
 * Class InputTextArea
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   2
 *
 *
 * 
 *
 **/
class InputTextAreaNew extends InputBase
{
    //---
	public function SetDefaults()
	{
		$this->setProperty("height",50)
			 ->setProperty("titleWidth",160) 
			 ->setProperty("placeholder","") 
			 ->setProperty("requirements","");
	}

	//---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)
            return ", '".$this->key."': getContentById('".$this->domId."')";
	}

    //---
	public function Draw()
	{ 
		$read = (isset($this->properties['readonly'])) ? 'readonly="'.$this->properties['readonly'].'"' : "" ;

		if(! isset($this->properties['titleDisable']))				   
	 	echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
		  	  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
			
		echo "<textarea id='{$this->domId}' class='inputTextarea' {$read} placeholder='{$this->properties['placeholder']}'
				  style='height:{$this->properties['height']}px;'>{$this->data}</textarea>";
		
		 if($this->getProperty('requirements') != "") echo "<div class='formRequirements'>".$this->properties['requirements']."</div>";	
		 
		if(! isset($this->properties['titleDisable'])) echo "</div>";

	}
    //---
}
?>

