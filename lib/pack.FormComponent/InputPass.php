<?php
/**
 * Class InputPass
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   2
 *
 *  v2 - изменен метод Draw
 * 
 *
 **/
 
class InputPass extends InputText
{
	//----------------------------------------------------------------------------------------------------
	public function Draw()
	{
		$this->data = str_replace("'",'"',$this->data);

		$inputWidth = ($this->properties['inputWidth']) ? "style='width: ".$this->properties['inputWidth']."px'" : "";

	 	echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>	
			  <input id='{$this->domId}' type='password'   {$inputWidth}
			   class='{$this->properties['class']}' placeholder='{$this->properties['placeholder']}'
			  {$this->properties['eventKeyDown']} 
			   value='{$this->data}'/>";	

		
		if($this->getProperty('requirements') != "") 	 
			echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";							
		 
	  	if($this->getProperty('warning') != "") 
			echo "<div style='margin-top:5px'>
				   	<div class='messageWrap messageError' style='border-radius: 0; padding:10px; display:inline-block;'>{$this->properties['warning']}</div>
				  </div>";			 
		   
		echo "</div>";	
	}
}
?>