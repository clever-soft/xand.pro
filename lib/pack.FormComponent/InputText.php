<?php
class InputText extends InputBase
{	
	//---
	public function SetDefaults()
	{
		$this->setProperty("class","inputText") 
			 ->setProperty("requirements","") 
			 ->setProperty("placeholder","") 
			 ->setProperty("titleWidth", 160)
			 ->setProperty("inputWidth", 300)
			 ->setProperty("eventKeyDown","");

	}	

	//---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)
            return ", '".$this->key."': getContentById('".$this->domId."')";
	}

	//---
	public function Draw()
	{
		if(isset($this->properties['dataForce']))
			$this->data = $this->properties['dataForce'];

		$this->data = str_replace("'",'"',$this->data);
	 	if(! isset($this->properties['titleDisable']))
		echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
			 	
		$inputWidth = ($this->properties['inputWidth']) ? "style='width: ".$this->properties['inputWidth']."px'" : "";
						  	
		 echo "<input id='{$this->domId}' type='text'  {$inputWidth} name='{$this->key}'
		 			  class='{$this->properties['class']}' {$this->properties['eventKeyDown']} 
	          		  placeholder='{$this->properties['placeholder']}' value='{$this->data}'/>";
		
		if($this->getProperty('requirements') != "")
			echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";
	
	  	if($this->getProperty('warning') != "") 
			echo "<div style='margin-top:5px'>
				   	<div class='messageWrap messageError' style='border-radius: 0; padding:10px; display:inline-block;'>{$this->properties['warning']}</div>
				  </div>";	
		
		if(! isset($this->properties['titleDisable'])) echo "</div>";		   
	}

}
?>

