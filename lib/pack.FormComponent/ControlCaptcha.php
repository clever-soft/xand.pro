<?php

class ControlCaptcha extends InputBase
{

    /**
     * @param null|string $key
     * @param null|int $value
     * @return string
     */
    public static function SetCode($key = null, $value = null) {
        if (is_numeric($value)) {
            $_SESSION['captcha' . $key] = (string)$value;
        } else {
            $_SESSION['captcha' . $key] = (string)mt_rand(100000, 999999);
        }
        return $_SESSION['captcha' . $key];
    }

    /**
     * @param null|string $key
     * @return string
     */
    public static function GetCode($key = null) {
        return (isset($_SESSION['captcha' . $key])) ? $_SESSION['captcha' . $key] : "ERROR";
    }

    /**
     * @param null|string $key
     */
    public static function DrawImage($key = null) {
        $str = self::SetCode($key);
        $x0 = 100;
        $y0 = 25;
        $ks = 1;

        $img = imagecreate($x0, $y0);
        //imageantialias ($img,true);

        $backgroundColor = imagecolorallocate($img, 255, 255, 255);
        $blackColor = imagecolorallocate($img, 210, 210, 210);
        $fontColor = imagecolorallocate($img, 0, 0, 0);
        $shiftX = 10;
        if (function_exists('imagettftext')) {
            imagettftext($img, 24, 0, 0, 24, $blackColor, PATH_REAL . DS . PATH_PUBLIC . "tpl/tpl-lib/plugins/captcha/cap.ttf", $str);
        } else {
            //пока не установлена поддержка freetype И jpeg - картинка не будет выводится, пишем 666666, чтобы можно было обойти капчу
            self::SetCode($key, "666666");
        }


        for ($k = 0; $k < 6; $k++) {
            $ugol = rand(-20, 20);
            imagettftext($img, 16, $ugol, $k * 12 + $shiftX, 19, $fontColor, PATH_REAL . DS . PATH_PUBLIC . "tpl/tpl-lib/plugins/captcha/cap.ttf", $str[$k]);
        }

        imageline($img, 0, 0, 0, $y0, $blackColor);
        imageline($img, $x0 - 1, 0, $x0 - 1, $y0, $blackColor);
        imageline($img, 0, 0, $x0 - 1, 0, $blackColor);
        imageline($img, 0, $y0 - 1, $x0 - 1, $y0 - 1, $blackColor);

        header("Content-type: image/png");
        imagepng($img);
        imagedestroy($img);
    }

    /**
     *
     */
    public function SetDefaults() {
        $this->setProperty("titleWidth", 160)
            ->setProperty("path", PATH_DS . SITE_DEFAULT_LANG . "/system/captcha");
    }

    /**
     * @return string
     */
    public function GetDataJson() {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "':$('#" . $this->domId . "').val()";
        return false;
    }

    /**
     *
     */
    public function Draw() {
        echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
        ?>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100px">
                    <img src="<?php echo $this->getProperty("path") . "?" . date("U"); ?>"
                         id="img_<?php echo $this->key; ?>"/>
                </td>
                <td align="center" width="25px">
                    <a href="" onclick="CaptchaRefresh(this);
                                return false;" style="margin: 0; ">
                        <i class="buttonTable buttonRefresh" style="margin: 0; "></i>
                    </a>
                </td>
                <td align="left">
                    <input style="width: 90px" id='<?php echo $this->domId; ?>' type='text' class='inputText'>
                </td>
            </tr>
        </table>
        <?php
        echo "</div>";
    }

}