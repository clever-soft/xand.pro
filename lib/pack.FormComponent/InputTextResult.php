<?php

class InputTextResult extends InputText
{
    //---
	public function SetDefaults()
	{
		$this->setProperty("height","20px")
			 ->setProperty("width_input","400px")
			 ->setProperty("class","inputText");  		
	}

    //---
	public function GetDataJson()
	{
		if($this->properties['ignore'] == false)	return ", '".$this->key."':$('#".$this->domId."').val()";			
	}

    //---
	public function Draw()
	{	 	
		echo "<div style='padding-left:{$this->properties['width_input']};'>
		<div style='float:left; margin-left:-{$this->properties['width_input']}; width:{$this->properties['width_input']};'>
		<input id='{$this->domId}' type='text' class='{$this->properties['class']}' value='{$this->data}' height='{$this->properties['height']}'/>
		</div>
		<div id='result{$this->domId}' style='padding:0 0 0 20px;'></div>
		</div>";	
	}

}?>

