<?php

/**
 * Class ControlRadio
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlRadio extends InputBase
{
    /**
     * @param $source
     * @param $format
     *
     * @return array|bool
     */
    public static function DataFormat($source, $format)
    {
        //echo "source";
        //var_dump($source);

        $output = array();

        if (count($format) == 2)
            $format[2] = $format[1];

        if ($source) {
            foreach ($source as $key => $item) {

                $output[$key]['title'] = isset($item[$format[0]]) ? $item[$format[0]] : "";
                $output[$key]['equal'] = isset($item[$format[1]]) ? $item[$format[1]] : "";
                $output[$key]['data'] = isset($item[$format[2]]) ? $item[$format[2]] : "";
            }
        } else
            return false;
        return $output;
    }

    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("titleWidth", 160)
            ->setProperty("ignore", false)
            ->setProperty("width", 200)
            ->setProperty("onChange", "")
            ->setProperty("noChoose", true)
            ->setProperty("noChooseVal", "");
    }

    /**
     * @return string
     */
    public function getDataJson()
    {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "':" . $this->domId;
    }

    /*
     *
     */
    public function ManualAdd($arr)
    {
        $this->dataArray[key($arr)] = $arr[key($arr)];
    }

    /**
     * @param $arr
     * @param $arr2
     */
    public function Fill($arr, $arr2)
    {
        foreach ($arr as $key => $val) {
            $this->dataArray[$val[$arr2[1]]] = $val[$arr2[0]];
        }
    }

    /**
     *
     */
    public function Draw()
    {

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";


        echo "<div class='radioWrap'>";

        if (!empty($this->dataArray)) {

            $this->convertData();
            $title = ($this->getProperty("noChoose")) ? "Not selected" : $this->dataArray[0]['title'];

            if ($this->getProperty("noChoose")) {
                $class_clr = ($this->data == false) ? "radioSel" : "radioUnSel";
                echo "<a href='' rel='{$this->properties['noChooseVal']}' onclick='RadioClick{$this->domId}(this); return false;' 
					class='radio{$this->domId} {$class_clr}'>Not selected</a>";
            } else {
                if ($this->data == false) {
                    $this->data = $this->dataArray[0]["data"];
                }
            }

            foreach ($this->dataArray as $val) {
                if (!isset($val['equal']))
                    $val['equal'] = $val['data'];

                $class_clr = ($val['equal'] == $this->data) ? "radioSel" : "radioUnSel";

                echo "<a href='' rel='{$val['data']}' onclick='RadioClick{$this->domId}(this); return false;' 
					class='radio{$this->domId} {$class_clr}'><label style='float: left; display: block; line-height: 16px;'>{$val['title']}</label></a>";
            }
        }

        echo "</div>";

        echo " 
		<script>
			var {$this->domId} = '{$this->data}';
			function RadioClick{$this->domId}(dom)
			{ 
				 $('.radio{$this->domId}').removeClass('radioSel').addClass('radioUnSel');
				 $(dom).removeClass('radioUnSel').addClass('radioSel');
				 {$this->domId} = $(dom).attr('rel'); 				 
				 " . $this->properties['onChange'] . "			 				 
			} 		
		</script>";

        if ($this->getProperty('requirements') != "")
            echo "<div class='formRequirements' style='margin: 10px 0 0 0;'>{$this->properties['requirements']}</div>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";
    }


}