<?php

/**
 * Class ControlTableKeyVal
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlTableKeyVal extends ControlTable
{
    /**
     *
     */
    public function Draw()
    {
        $cssShow = ($this->getProperty('show')) ? "controlGrouperDown" : "controlGrouperUp";
        $sign = ($this->getProperty('show')) ? "fa-chevron-down" : "fa-chevron-up";

        $width = ($this->getProperty('width')) ? "width: " . $this->getProperty('width') . "px;" : "";

        $doNotStretch = ($this->getProperty('doNotStretch')) ? "display:inline-block;" : "";

        if (empty($this->properties["columnNames"])) {
            $this->properties["columnNames"] = ["Option", "Value"];
        }


        echo "<div class ='formElement' style='{$width}{$doNotStretch}'>
		 	 <div class = 'controlGrouperTitle' onclick='GrouperSpoiler(this);'>
			 	<i style='float: left; padding: 6px 6px 0  0;' class='fa {$sign}'></i> {$this->name}</div>
		 	 <div class='{$this->getProperty('class')} {$cssShow}' style='padding: 1px;'>";
        ?>
        <table class='table table-striped'>
            <thead>
            <th style='padding:4px 8px' width="50%" align="left"><?= $this->properties["columnNames"][0] ?></th>
            <th style='padding:4px 8px' align="left"><?= $this->properties["columnNames"][1] ?></th>
            </thead>

            <?php
            $counter = 0;

            if (isset($this->properties["sort"])) {
                if ($this->properties["sort"] == "key")
                    ksort($this->data);
                else
                    arsort($this->data);
            }

            if (!empty($this->data) && is_array($this->data)) {
                foreach ($this->data as $k => $v) {

                    if (isset($this->properties["ignoreEmpty"]) && $v == "")
                        continue;
                    $class = ($counter % 2 != 0) ? "class='tableRows tableRowNc'" : "class='tableRows tableRowC'";

                    echo "<tr {$class}>";
                    echo "<td style='padding:2px 8px' align='left'>{$k}</td>
                      <td style='padding:2px 8px' align='left'>{$v}</td>";
                    echo "</tr>";

                    $counter++;
                }
            } else {

            }
            ?>
        </table>

        <?php
        echo "<div style='clear:both'></div>";
        echo "</div></div>";

    }

}