<?php

class InputTextReadonly extends InputText
{	
	//---------------
	public function SetDefaults()
	{
		$this->setProperty("class","inputTextRead") 
			 ->setProperty("titleWidth",160)
             ->setProperty("requirements","")
             ->setProperty("placeholder","")
             ->setProperty("inputWidth", 300)
             ->setProperty("eventKeyDown","");
	}

    //---
	public function GetDataJson()
	{		
		return false;
	}

    //---
	public function Draw()
	{

        $this->data = str_replace("'",'"',$this->data);
        if(! isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>
			   <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";

        $inputWidth = ($this->properties['inputWidth']) ? "style='width: ".$this->properties['inputWidth']."px'" : "";

        echo "<input id='{$this->domId}' type='text'  {$inputWidth} readonly
		 			  class='{$this->properties['class']}' {$this->properties['eventKeyDown']}
	          		  placeholder='{$this->properties['placeholder']}' value='{$this->data}'/>";

        if($this->getProperty('requirements') != "")
            echo "<label class='formRequirements'>{$this->properties['requirements']}</label>";

        if($this->getProperty('warning') != "")
            echo "<div style='margin-top:5px'>
				   	<div class='messageWrap messageError' style='border-radius: 0; padding:10px; display:inline-block;'>{$this->properties['warning']}</div>
				  </div>";

        if(! isset($this->properties['titleDisable'])) echo "</div>";

    }
    //---
}

?>

