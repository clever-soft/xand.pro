<?php

/**
 * Class ControlSelect
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlSelect extends InputBase
{
    /**
     * @param $source
     * @param array $format
     *
     * @return array|bool
     */
    public static function DataFormat($source, $format = []) {
        $output = array();

        if (count($format) == 2) {
            $format[2] = $format[1];
        }
        if (!$format) {
            //переданы просто key=>val
            $iter = 0;
            foreach ($source as $key => $item) {
                $output[$iter]['title'] = $item;
                $output[$iter]['equal'] = $key;
                $output[$iter]['data'] = $key;
                $iter++;
            }
            return $output;
        }

        if ($source) {
            foreach ($source as $key => $item) {

                $output[$key]['title'] = isset($item[$format[0]]) ? $item[$format[0]] : "";
                $output[$key]['equal'] = isset($item[$format[1]]) ? $item[$format[1]] : "";
                $output[$key]['data'] = isset($item[$format[2]]) ? $item[$format[2]] : "";
            }
        } else
            return false;
        return $output;
    }

    /**
     *
     */
    public function SetDefaults() {
        $this->SetProperty("titleWidth", 160)
            ->SetProperty("ignore", false)
            ->SetProperty("class", "controlSelect")
            ->SetProperty("width", 200)
            ->SetProperty("noChoose", true)
            ->SetProperty("noChooseTitle", "Not selected")
            ->SetProperty("noChooseVal", "");
    }

    /**
     * @return string
     */
    public function GetDataJson() {
        if ($this->properties['ignore'] == false)
            return ", '" . $this->key . "':$('#" . $this->domId . "').val()";
    }

    /**
     * @param $arr
     */
    public function ManualAdd($arr) {
        $this->dataArray[key($arr)] = $arr[key($arr)];
    }

    /**
     * @param $arr
     * @param $arr2
     */
    public function Fill($arr, $arr2) {
        foreach ($arr as $key => $val) {
            $this->dataArray[$val[$arr2[1]]] = $val[$arr2[0]];
        }
    }

    /**
     *
     */
    public function Draw() {

        if (!empty($this->dataArray)) {

            $this->ConvertData();

            $options = "";
            $title = ($this->GetProperty("noChoose")) ? $this->GetProperty("noChooseTitle") : $this->dataArray[0]['title'];


            foreach ($this->dataArray as $val) {
                if (!isset($val['equal'])) {
                    $val['equal'] = $val['data'];
                }

                $select = "";
                if ($val['equal'] == $this->data || (!empty($this->properties['noChooseVal'])
                        && $val['data'] == $this->properties['noChooseVal'])
                ) {
                    $select = "selected='selected'";
                    $title = $val['title'];
                }

                $options .= "<option value='" . $val['data'] . "' " . $select . ">{$val['title']}</option>";
            }
        }

        $onchange = ($this->GetProperty('onchange') != "") ? "{$this->properties['onchange']}" : "";

        if (!isset($this->properties['titleDisable'])) {
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
            echo "<div class='controlSelectWrap' style='width:" . $this->properties['width'] . "px'>
				<label class='controlSelectLabel'>{$title}</label> 
			    <select id='" . $this->domId . "' class='controlSelect' style='width:" . $this->properties['width'] . "px' 
	            onchange=\"$(this).prev().html($('#" . $this->domId . " :selected').text()); {$onchange} \"			 
			   >";
        }

        if ($this->GetProperty("noChoose")) {
            $select = ($this->data == false) ? "selected='selected'" : "";
            echo "<option value='" . $this->properties['noChooseVal'] . "' " . $select . ">" . $this->properties['noChooseTitle'] . "</option>";
            $select = false;
        }
        echo $options;

        echo "</select></div>";

        if ($this->GetProperty('requirements') != "") {
            echo "<div class='formRequirements' style='margin: 5px 0  5px 0;'>{$this->properties['requirements']}</div>";
        }

        if (!isset($this->properties['titleDisable'])) {
            echo "</div>";
        }
    }
}
