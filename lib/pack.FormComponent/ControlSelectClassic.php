<?php

/**
 * Class ControlSelectClassic
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ControlSelectClassic extends ControlSelect
{

    /**
     *
     */
    public function setDefaults()
    {
        $this->setProperty("titleWidth", 160)
            ->setProperty("ignore", false)
            ->setProperty("class", "controlSelectClassic")
            ->setProperty("width", 200)
            ->setProperty("noChoose", true)
            ->setProperty("noChooseTitle", "Not selected")
            ->setProperty("noChooseVal", "");
    }

    /**
     * @param $arr
     */
    public function ManualAdd($arr)
    {
        $this->dataArray[key($arr)] = $arr[key($arr)];
    }

    /**
     * @param $arr
     * @param $arr2
     */
    public function Fill($arr, $arr2)
    {
        foreach ($arr as $key => &$val) {
            $this->dataArray[$val[$arr2[1]]] = $val[$arr2[0]];
        }
    }

    /**
     *
     */
    public function Draw()
    {

        if (!empty($this->dataArray)) {

            $this->convertData();

            $options = "";
            $title = ($this->getProperty("noChoose")) ? $this->getProperty("noChooseTitle") : $this->dataArray[0]['title'];


            foreach ($this->dataArray as $val) {
                if (!isset($val['equal'])) {
                    $val['equal'] = $val['data'];
                }

                $select = "";
                if ($val['equal'] == $this->data || (!empty($this->properties['noChooseVal'])
                        && $val['data'] == $this->properties['noChooseVal'])
                ) {
                    $select = "selected='selected'";
                    $title = $val['title'];
                }

                $options .= "<option value='" . $val['data'] . "' " . $select . ">{$val['title']}</option>";
            }
        }

        echo "<select id='" . $this->domId . "' name='{$this->key}'
		      class='{$this->properties['class']}'
		      style='width:" . $this->properties['width'] . "px;'";
        echo ($this->getProperty('onchange') != "") ? "onchange='{$this->properties['onchange']}'" : "";
        echo ">";


        if ($this->getProperty("noChoose")) {
            $select = ($this->data == false) ? "selected='selected'" : "";
            echo "<option value='" . $this->properties['noChooseVal'] . "' " . $select . ">" . $this->properties['noChooseTitle'] . "</option>";
            $select = false;
        }
        echo $options;

        echo "</select>";

    }

}
