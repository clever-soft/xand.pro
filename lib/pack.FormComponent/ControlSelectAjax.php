<?php

class ControlSelectAjax extends ControlSelect {

    //---
    public function Draw() {

        $options = "";
        $title = ($this->getProperty("noChoose")) ? $this->getProperty("noChooseTitle") : $this->dataArray[0]['title'];

        if (!empty($this->dataArray))
        {

            foreach ($this->dataArray as $val)
            {
                if (!isset($val['equal']))
                    $val['equal'] = $val['data'];

                $select = "";
                if ($val['equal'] == $this->data || (!empty($this->properties['noChooseVal'])
                            && $val['data'] == $this->properties['noChooseVal']))
                {
                    $select = "selected='selected'";
                    $title = $val['title'];
                }

                $options .= "<option value='" . $val['data'] . "' " . $select . ">{$val['title']}</option>";
            }
        }

        $onchange = ($this->getProperty('onchange') != "") ? "{$this->properties['onchange']}" : "";

        if (!isset($this->properties['titleDisable']))
            echo "<div class='formElement' style='padding-left:{$this->properties['titleWidth']}px'>	
			  <span style='margin-left:-{$this->properties['titleWidth']}px'>{$this->name}</span>";
        echo "

        <div class='controlSelectWrap' style='width:" . $this->properties['width'] . "px'>
				<div class='controlSelectAjaxLoader'></div>
				<label class='controlSelectLabel'>{$title}</label>
			    <select id='" . $this->domId . "' class='controlSelect' style='width:" . $this->properties['width'] . "px' 
	            onchange=\"$(this).prev().html($('#" . $this->domId . " :selected').text()); {$onchange} \"
			   >";

        if ($this->getProperty("noChoose")) {
            $select = ($this->data == false) ? "selected='selected'" : "";
            echo "<option value='" . $this->properties['noChooseVal'] . "' " . $select . ">".$this->properties['noChooseTitle']."</option>";
            $select = false;
        }
        echo $options;

        echo "</select></div>";

        if ($this->getProperty('requirements') != "")
            echo "<div class='formRequirements' style='margin: 5px 0  5px 0;'>{$this->properties['requirements']}</div>";

        if (!isset($this->properties['titleDisable']))
            echo "</div>";
    }

    //----
}