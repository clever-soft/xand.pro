<?php

class ValidateCaptcha implements IValidate
{
    public $name = "Captcha";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if(mb_strlen($value, "UTF-8") != 6)
            $errors = ($object->required === true) ? I18n::__("%Please fill field%") :  $object->required;

        if(! isset($_SESSION['frontedCaptcha']) || $_SESSION['frontedCaptcha'] != $value)
            $errors = I18n::__("%Security code is wrong%");

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}