<?php

class ValidateSelect implements IValidate
{
    public $name = "Select";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {

        $errors = (! isset($object->options[$value]) || $value == -1) ?
            (($object->required === true) ? I18n::__("%Please choose value%")  :  $object->required)
            : false;

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}