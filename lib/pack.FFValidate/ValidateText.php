<?php

class ValidateText implements IValidate
{
    public $name = "Text";

    public function Test($value = false, FFObjectBase $object, $slaves = array())
    {
        $errors = false;

        if(mb_strlen($value, "UTF-8") < 4 || mb_strlen($value, "UTF-8") >32 )
            $errors = ($object->required === true) ? I18n::__("%Please fill field%") :  $object->required;

        return array(
            "errors"    => $errors,
            "corrected" => $value
        );

    }

}