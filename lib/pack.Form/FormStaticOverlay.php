<?php
/**
 * Class FormStaticOverlay
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   1
 *
 *
 * 
 *
 **/

class FormStaticOverlay extends FormStatic
{
	public function DrawHead()
	{  
		echo "<div align='left' class='formOverlay'></div>";
		echo "<div align='left' class='formStatic formStaticOverlay' id='form{$this->domId}'>";
		echo '<div class="formStaticTitle">'.$this->name.'</div>';		
		echo '<div class="formStaticContent">';		
		return $this;
	}		
}
?>