<?php

/**
 * Class FormModal
 *
 * @тип     Class
 * @пакет    Form
 * @версия   1
 *
 *
 *
 *
 **/
class FormModal extends FormBase
{
    public function DrawHead() {
        echo '<a onClick="Wnd.Close(); return false;" class="formModalTitleClose"><i class="fa fa-times"></i></a>';
        echo '<div class="formModalTitle">' . $this->name . '</div>';
        echo "<div align='left' class='formModal'>";
        echo '<div class="formModalContent">';
        return $this;
    }

    //----
    public function DrawFooter() {
        echo "</div>";
        if (isset($this->buttons)) {
            echo "<div class='formBaseButtons'> ";
            $this->DrawButtons();
            echo "</div>";
        }
        echo "</div>";
        return $this;
    }

    //----
    public static function Widget($name, $module, $widget, $params = "") {
        $widgetName = ucfirst($widget) . "Widget";
        $params = ($params) ? "?{$params}" : "";

        include_once(PATH_REAL . DS . "mod/{$module}/widgets/{$widgetName}.php");

        $widget = new $widgetName("{$module}_{$widget}", array(
            "init" => true,
            "global" => false,
            "url" => "admin/widgets/{$module}/{$widget}{$params}"
        ));
        $widget->init();

        echo '<a onClick="Wnd.Close(); return false;" class="formModalTitleClose"><i class="fa fa-times"></i></a>';
        echo '<div class="formModalTitle">' . $name . '</div>';
        echo "<div align='left' class='formModal'>";
        echo "<div class='formModalContent' id='{$widget->uid}'>Loading...</div>";
        echo "</div>";
        ?>
        <script>
            <?=$widget->ajax->eid?>.eventComplete = function () {
                Wnd.Align();
            };
            <?=$widget->ajax->eid?>.Act("View");
        </script>
    <?php

    }

}