<?php

/**
 * Class FormBase
 *
 * @тип     Class
 * @пакет    Form
 * @версия   4
 *
 *
 * */
class FormBase extends ClassBase
{

    //----
    public $domId = "";
    public $dataArray = array();

    public $ajax = "";
    public $nav = "";
    public $inputs = array();
    public $buttons = array();
    public $tabs = array();


    //----
    public $disableTimer = false;
    public $disableCreateAjax = false;

    //----
    function __construct()
    {
        $arr = func_get_args();

        $this->name = (isset($arr[0])) ? $arr[0] : "";
        $this->dataArray = (isset($arr[1])) ? $arr[1] : NULL;
        if (count($this->dataArray) == count($this->dataArray, COUNT_RECURSIVE)) {
            //если бы передан одномерный массив
            $this->dataArray = array($this->dataArray);
        }
        $property = (isset($arr[2])) ? $arr[2] : NULL;
        $this->disableTimer = (isset($arr[2])) ? $arr[2] : false;

        $this->domId = $this->getUniqueName();


        $this->SetDefaults();

        if (!empty($property)) {
            foreach ($property as $key => $val)
                $this->properties[$key] = $val;
        }
    }

    //----
    public function SetDefaults()
    {
        $this->setProperty("formEnterKeyDispatch", true);
    }

    //----
    public function cloneAjax()
    {
        $this->disableCreateAjax = true;
        if ($this->parent->ajax)
            $this->ajax = $this->parent->ajax;
        return $this;
    }

    //----------------
    public function CloneNav()
    {
        if ($this->parent->nav)
            $this->nav = $this->parent->nav;
        return $this;
    }

    //-----
    public function Add()
    {
        $args = func_get_args();

        foreach ($args as $object) {

            if ($object instanceof InputBase) {
                $object->data = (isset($this->dataArray[0][$object->key]) && $object->data == "") ? $this->dataArray[0][$object->key] : $object->data;
                $object->assignParent($this);
                array_push($this->inputs, $object);
            } elseif ($object instanceof GrouperBase) {

                if ($object instanceof GrouperTab) {

                    if (!isset($this->properties["tabMode"])) {
                        $object->setProperty("show", true);
                        $gth = new GrouperTabHead();

                        $this->properties["tabMode"] = count($this->inputs);
                        array_push($this->inputs, $gth);
                    } else {
                        $object->setProperty("show", false);
                    }


                    if (isset($object->properties["disabled"])) {
                        $this->inputs[$this->properties["tabMode"]]
                            ->properties["disabled"][count($this->inputs[$this->properties["tabMode"]]->dataArray)] = true;
                    }

                    array_push($this->inputs[$this->properties["tabMode"]]->dataArray, $object->name);
                }

                $object->assignParent($this);
                array_push($this->inputs, $object);
            } elseif ($object instanceof ButtonBase) {
                $object->assignParent($this);
                array_push($this->buttons, $object);
            } elseif ($object instanceof Ajax) {
                $object->setExemplarId($this->domId);
                $this->ajax = $object;
            } elseif ($object instanceof Nav) {
                $this->nav = $object;
            } else {
                echo get_class($object) . " : Object not supported";
            }
        }
        return $this;
    }

    //----
    public function AjaxReady()
    {
        if (!isset($this->ajax->eid))
            Message::I()->Error("AJAX not found")->Draw()->E();
        else {
            echo "<script>function {$this->ajax->eid}GetData(){";

            if (!empty($this->inputs))
                foreach ($this->inputs as $key => $val) {
                    if (method_exists($val, "PrepareData"))
                        $val->PrepareData();
                }

            $result = "";
            echo "var forms_data = {";
            if (isset($this->inputs))
                foreach ($this->inputs as $key => $val)
                    $result .= $val->GetDataJson();
            echo substr($result, 2, strlen($result));
            echo "};";

            if (isset($this->properties["virtualCollector"]))
                echo "forms_data = FormBaseCollector('{$this->ajax->eid}', forms_data);";

            echo "return $.toJSON(forms_data); };";

            if ($this->properties["formEnterKeyDispatch"])
                echo "FormEnterKeyDispatch('form" . $this->domId . "');";

            echo "</script>";
        }
    }

    //----
    public function DrawContent()
    {
        if ($this->nav == false)
            $this->nav = new Nav();

        if ($this->ajax !== false && !$this->disableCreateAjax)
            $this->ajax->createJsExemplar();

        if (!empty($this->inputs))
            foreach ($this->inputs as $val) {
                $val->Draw();
            }

        $this->AjaxReady();

        return $this;
    }

    //----
    public function DrawButtons()
    {

        foreach ($this->buttons as $item) {

            $confirm = ($item->properties["dialogEnable"]) ? $item->properties["dialog"] : "";
            $dataForce = (isset($item->properties["dataForce"])) ? $item->properties["dataForce"] : "'false'";

            $href    = "";
            $onclick = "Button" . $item->properties['script'] . "(this, {$this->ajax->eid}, {$dataForce}, {$this->ajax->eid}GetData); return false;";

            if (isset($item->properties['adminMenu'])) {
                $onclick = "CAdminMenu.Click(this); return false;";
                $href = "href='" . $item->properties['adminMenu'] . "'";
            }

            echo "<button class=\"{$item->properties['cssClass']} btn\" {$href} title = \"{$item->title}\" data-confirm = \"{$confirm}\"
                          data-action = \"{$item->action}\"  onclick=\"{$onclick}\">{$item->title}</button>";
        }
    }

    //----
    public function Draw()
    {
        if (method_exists($this, "DrawHead")) {
            $this->DrawHead();
        }
        $this->DrawContent();
        if (method_exists($this, "DrawFooter")) {
            $this->DrawFooter();
        }
    }

}
