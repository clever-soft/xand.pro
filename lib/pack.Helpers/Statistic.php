<?php
class Statistic
{
	public static function Whois($ip) 
	{
	  if ($ip!="") 
	  {
		$str = "";
		$sock = fsockopen ("whois.ripe.net",43,$errno,$errstr);
		if ($sock) 
		{
		   fputs ($sock, $ip."\r\n");
		   while (!feof($sock)) 
		   {
				$str .= trim(fgets ($sock,128)." <br>");
		   }
		}
		else 
		{
			  $str.="$errno($errstr)";
			  return;
		}
		fclose ($sock);
	  }
	  return $str;
	}

	public static function DetectAgent($USER_AGENT)
	{
		$engines = array(
		array('Aport', 'Aport robot'),
		array('Google', 'Google robot'),
		array('msnbot', 'MSN robot'),
		array('Rambler', 'Rambler robot'),
		array('Yahoo', 'Yahoo robot'),
		array('AbachoBOT', 'AbachoBOT'),
		array('accoona', 'Accoona robot'),
		array('AcoiRobot', 'AcoiRobot'),
		array('ASPSeek', 'ASPSeek robot'),
		array('CrocCrawler', 'CrocCrawler robot'),
		array('Dumbot', 'Dumbot robot'),
		array('FAST-WebCrawler', 'FAST-WebCrawler'),
		array('GeonaBot', 'GeonaBot'),
		array('Gigabot', 'Gigabot'),
		array('Lycos', 'Lycos spider'),
		array('MSRBOT', 'MSRBOT robot'),
		array('Scooter', 'Altavista robot'),
		array('AltaVista', 'Altavista robot'),
		array('WebAlta', 'WebAlta robot'),
		array('IDBot', 'ID-Search Bot'),
		array('eStyle', 'eStyle Bot'),
		array('Mail.Ru', 'Mail.Ru Bot'),
		array('Scrubby', 'Scrubby robot'),
		array('Yandex', 'Yandex robot'),
		array('YaDirectBot', 'Yandex Direct'),
		array('Firefox','Firefox'),
		array('Netscape','Netscape'),
		array('Chrome','Chrome'),
		array('Safari','Safari'),
		array('Opera','Opera'),
		array('MSIE 6.0','IE6'),
		array('MSIE 7.0','IE7'),
		array('MSIE 8.0','IE8'),
		array('MSIE 9.0','IE9'),
		array('seamonkey','Seamonkey'), 
		array('konqueror','Konqueror'),
		array('netscape','Netscape'),                           
		array('gecko','Gecko'),
		array('navigator','Navigator'),
		array('mosaic','Mosaic'),
		array('lynx','Lynx'),
		array('amaya','Amaya'),	   
		array('omniweb','Omniweb'),
		array('avant','Avant'),
		array('camino','Camino '),
		array('flock','Flock'),
		array('aol','Aol')
			
		);
	
		foreach ($engines as $engine)
		{
			if (strstr($USER_AGENT, $engine[0]))
			{
			return($engine[1]);
			}
		}
		return "Unknown";
	}


	public static function TownBeta($IP)
	{
	
		  $url = "http://speed-tester.info/ip_location.php?ip=".$IP;
		  $unique_start = '12px;}';
		  $unique_end = '<div id="map"';			
		  
		  $code = file_get_contents($url);
		  $code = mb_convert_encoding($code, "utf8", "windows-1251");
		  preg_match('/'.preg_quote($unique_start,'/').'(.*)'.preg_quote($unique_end, '/').'/Us', $code, $match);
		  $temp = $match[1];
		  $temp = substr($temp, 10, strlen($temp));
		  
		  $temp = strip_tags($temp, "<table><td><tr>");
		  $temp = str_replace('check">', 'check" width="100%">', $temp);
			
		  return $temp;
	
	}

	public static function IpInt($ip) 
	{	   	   
	   $a=explode(".",$ip);
	   return $a[0]*256*256*256+$a[1]*256*256+$a[2]*256+$a[3];
	}

	public static function IntIp($i) 
	{	   
	   $d[0]=(int)($i/256/256/256);
	   $d[1]=(int)(($i-$d[0]*256*256*256)/256/256);
	   $d[2]=(int)(($i-$d[0]*256*256*256-$d[1]*256*256)/256);
	   $d[3]=$i-$d[0]*256*256*256-$d[1]*256*256-$d[2]*256;
	   return "$d[0].$d[1].$d[2].$d[3]";
	} 
	
	public static function Country($ip)
	{ 		
						
		  // $url  = "http://smart-ip.net/geoip-json/".$ip;					  		    		 
		 //  $data = @file_get_contents($url); 
		  // $code = (array) json_decode($data);
		  		    
		   return array(
		   					"ip"      => (float) Statistic::IpInt($ip),
							"code"    =>  $_SERVER['GEOIP_COUNTRY_CODE'], 						
							"country" =>  $_SERVER['GEOIP_COUNTRY_NAME'],
							"city"    =>  "",
							"region"  =>  "" 
						);
	}
	
	

}