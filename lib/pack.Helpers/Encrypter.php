<?php

/**
 * @author  <kubrey@gmail.com>
 */
class Encrypter {

    protected $_cryptKey = '7b128263a99eaf06358ae4350035f10f';
    private $_algorithm = MCRYPT_BLOWFISH;
    private $_mode = MCRYPT_MODE_ECB;
    private $_mcrypt, $_vector;

    public function __construct() {
        $this->_mcrypt = mcrypt_module_open($this->_algorithm, '', $this->_mode, '');
        $this->_vector = mcrypt_create_iv(mcrypt_enc_get_iv_size($this->_mcrypt), MCRYPT_RAND);
        mcrypt_generic_init($this->_mcrypt, $this->_cryptKey, $this->_vector);
    }

    /**
     * 
     * @param string $data
     * @return string
     */
    public function encrypt($data) {
        if(!$data){
            return "";
        }
        return base64_encode(mcrypt_generic($this->_mcrypt, $data));
    }

    /**
     * 
     * @param string $val
     * @return boolean
     */
    public static function isEncrypted($val) {
        if (!is_string($val)) {
            return false;
        }
        if (strpos($val, '=') !== false || strpos($val, '+') !== false || strpos($val, '/') !== false) {
            return true;
        }
        $arr = str_split(str_replace(' ', '', $val));
        foreach ($arr as $char) {
            if (is_numeric($char)) {
                continue;
            }
            if (ctype_lower($char) === true) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param string $data
     * @return string
     */
    public function decrypt($data) {

        if (empty($data)){
            return "";
        }
        return str_replace("\x0", '', trim(mdecrypt_generic($this->_mcrypt, base64_decode((string) $data))));
    }

    /**
     * 
     * @param string $data
     * @return string
     */
    public static function extraEncode($data) {
        $mode = MCRYPT_MODE_CBC;
        $cryptKey = '42b16283a9ae9f06358ea4730053f10f';
        $algor = MCRYPT_BLOWFISH;
        $mcrypt = mcrypt_module_open($algor, '', $mode, '');
        $vector = mcrypt_create_iv(mcrypt_enc_get_iv_size($mcrypt), MCRYPT_RAND);
        mcrypt_generic_init($mcrypt, $cryptKey, $vector);
        return base64_encode(mcrypt_generic($mcrypt, $data));
    }

    /**
     * 
     * @param string $data
     * @return string
     */
    public static function extraDecode($data) {
        $mode = MCRYPT_MODE_CBC;
        $cryptKey = '42b16283a9ae9f06358ea4730053f10f';
        $algor = MCRYPT_BLOWFISH;
        $mcrypt = mcrypt_module_open($algor, '', $mode, '');
        $vector = mcrypt_create_iv(mcrypt_enc_get_iv_size($mcrypt), MCRYPT_RAND);
        mcrypt_generic_init($mcrypt, $cryptKey, $vector);
        return str_replace("\x0", '', trim(mdecrypt_generic($this->_mcrypt, base64_decode((string) $data))));
    }

    /**
     * 
     * @param string $secret
     * @return \stdClass|boolean
     */
    public function extractSecret($secret) {
        if (!is_string($secret)) {
            return false;
        }
        $refact = explode('-', $secret);
        $ccnum = substr($refact[0], 0, strlen($refact[0]) - 1);
        $cvv = substr(substr(end($refact), 0, strlen(end($refact)) - 1), 1);
        $cc = new stdClass();
        $cc->ccnum = $ccnum;
        $cc->cvv = $cvv;
        return $cc;
    }

}
