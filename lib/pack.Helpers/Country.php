<?php

class Country
{

    public $list = array();
    public $listFilter = array();

    /**
     * Country constructor.
     */
    public function __construct()
    {
        $CC = array();
        $CC["AU"] = "Australia";
        $CC["AX"] = "Åland Islands";
        $CC["AT"] = "Austria";
        $CC["AZ"] = "Azerbaijan";
        $CC["AL"] = "Albania";
        $CC["DZ"] = "Algeria";
        $CC["AI"] = "Anguilla";
        $CC["AO"] = "Angola";
        $CC["AD"] = "Andorra";
        $CC["AQ"] = "Antarctic";
        $CC["AG"] = "Antigua and Barbuda";
        $CC["AN"] = "Netherlands' Antilles";
        $CC["AR"] = "Argentina";
        $CC["AM"] = "Armenia";
        $CC["AW"] = "Aruba";
        $CC["AF"] = "Afghanistan";
        $CC["BS"] = "Bahamas";
        $CC["BD"] = "Bangladesh";
        $CC["BB"] = "Barbados";
        $CC["BH"] = "Bahrain";
        $CC["BY"] = "Belarus";
        $CC["BZ"] = "Belize";
        $CC["BE"] = "Belgium";
        $CC["BJ"] = "Benin";
        $CC["BM"] = "Bermuda";
        $CC["BV"] = "Bouvet Island";
        $CC["BG"] = "Bulgaria";
        $CC["BO"] = "Bolivia";
        $CC["BA"] = "Bosnia and Herzegovina";
        $CC["BW"] = "Botswana";
        $CC["BR"] = "Brazil";
        $CC["BN"] = "Brunei";
        $CC["BF"] = "Burkina Faso";
        $CC["BI"] = "Burundi";
        $CC["BT"] = "Bhutan";
        $CC["VU"] = "Vanuatu";
        $CC["VA"] = "Vatican";
        $CC["GB"] = "United Kingdom";
        $CC["HU"] = "Hungary";
        $CC["VE"] = "Venezuela";
        $CC["VG"] = "British Virgin Islands";
        $CC["VI"] = "US Virgin Islands";
        $CC["AS"] = "American Samoa";
        $CC["TP"] = "East Timor";
        $CC["VN"] = "Vietnam";
        $CC["GA"] = "Gabon";
        $CC["HT"] = "Haiti";
        $CC["GY"] = "Guyana";
        $CC["GM"] = "Gambia";
        $CC["GH"] = "Ghana";
        $CC["GP"] = "Guadeloupe";
        $CC["GT"] = "Guatemala";
        $CC["GN"] = "Guinea";
        $CC["GW"] = "Guinea-Bissau";
        $CC["DE"] = "Germany";
        $CC["GI"] = "Gibraltar";
        $CC["HN"] = "Honduras";
        $CC["HK"] = "Hong Kong";
        $CC["GD"] = "Grenada";
        $CC["GL"] = "Greenland";
        $CC["GR"] = "Greece";
        $CC["GE"] = "Georgia";
        $CC["GU"] = "Guam";
        $CC["DK"] = "Denmark";
        $CC["CD"] = "Congo, Democratic Republic of the";
        $CC["DJ"] = "Djibouti";
        $CC["DM"] = "Dominica";
        $CC["DO"] = "Dominican Republic";
        $CC["EG"] = "Egypt";
        $CC["ZM"] = "Zambia";
        $CC["EH"] = "Western Sahara";
        $CC["ZW"] = "Zimbabwe";
        $CC["IL"] = "Israel";
        $CC["IN"] = "India";
        $CC["ID"] = "Indonesia";
        $CC["JO"] = "Jordan";
        $CC["IQ"] = "Iraq";
        $CC["IR"] = "Iran";
        $CC["IE"] = "Ireland";
        $CC["IS"] = "Iceland";
        $CC["ES"] = "Spain";
        $CC["IT"] = "Italy";
        $CC["YE"] = "Yemen";
        $CC["CV"] = "Cape Verde";
        $CC["KZ"] = "Kazakhstan";
        $CC["KY"] = "Cayman Islands";
        $CC["KH"] = "Cambodia";
        $CC["CM"] = "Cameroon";
        $CC["CA"] = "Canada";
        $CC["QA"] = "Qatar";
        $CC["KE"] = "Kenya";
        $CC["CY"] = "Cyprus";
        $CC["KG"] = "Kyrgyzstan";
        $CC["KI"] = "Kiribati";
        $CC["CN"] = "China";
        $CC["CC"] = "Cocos (Keeling) Islands";
        $CC["CO"] = "Colombia";
        $CC["KM"] = "Comoros";
        $CC["CG"] = "Congo [Republic]";
        $CC["CR"] = "Costa Rica";
        $CC["CI"] = "Côte d’Ivoire";
        $CC["CU"] = "Cuba";
        $CC["KW"] = "Kuwait";
        $CC["CK"] = "Cook Islands";
        $CC["LA"] = "Laos";
        $CC["LV"] = "Latvia";
        $CC["LS"] = "Lesotho";
        $CC["LR"] = "Liberia";
        $CC["LB"] = "Lebanon";
        $CC["LY"] = "Libya";
        $CC["GG"] = "Guernsey";
        $CC["IM"] = "Isle of Man";
        $CC["JE"] = "Jersey";
        $CC["RS"] = "Serbia";
        $CC["SO"] = "Somalia";
        $CC["LT"] = "Lithuania";
        $CC["LI"] = "Liechtenstein";
        $CC["LU"] = "Luxembourg";
        $CC["MU"] = "Mauritius";
        $CC["MR"] = "Mauritania";
        $CC["MG"] = "Madagascar";
        $CC["YT"] = "Mayotte";
        $CC["MO"] = "Macau";
        $CC["MK"] = "Macedonia [FYROM]";
        $CC["MW"] = "Malawi";
        $CC["MY"] = "Malaysia";
        $CC["ML"] = "Mali";
        $CC["MV"] = "Maldives";
        $CC["MT"] = "Malta";
        $CC["ME"] = "Montenegro";
        $CC["MA"] = "Morocco";
        $CC["MQ"] = "Martinique";
        $CC["MH"] = "Marshall Islands";
        $CC["MX"] = "Mexico";
        $CC["FM"] = "Federated States of Micronesia";
        $CC["MZ"] = "Mozambique";
        $CC["MD"] = "Moldova";
        $CC["MC"] = "Monaco";
        $CC["MN"] = "Mongolia";
        $CC["MS"] = "Montserrat";
        $CC["MM"] = "Myanmar [Burma]";
        $CC["NA"] = "Namibia";
        $CC["NR"] = "Nauru";
        $CC["NP"] = "Nepal";
        $CC["NE"] = "Niger";
        $CC["NG"] = "Nigeria";
        $CC["NL"] = "Netherlands";
        $CC["NI"] = "Nicaragua";
        $CC["NU"] = "Niue";
        $CC["NZ"] = "New Zealand";
        $CC["NC"] = "New Caledonia";
        $CC["AN"] = "Netherlands Antilles";
        $CC["NO"] = "Norway";
        $CC["NF"] = "Norfolk Island";
        $CC["AE"] = "United Arab Emirates";
        $CC["OM"] = "Oman";
        $CC["PK"] = "Pakistan";
        $CC["PW"] = "Palau";
        $CC["PS"] = "Palestinian Territories";
        $CC["PA"] = "Panama";
        $CC["PG"] = "Papua New Guinea";
        $CC["PY"] = "Paraguay";
        $CC["PE"] = "Peru";
        $CC["PN"] = "Pitcairn";
        $CC["PL"] = "Poland";
        $CC["PT"] = "Portugal";
        $CC["PR"] = "Puerto Rico";
        $CC["RE"] = "Réunion";
        $CC["CX"] = "Christmas Island";
        $CC["RU"] = "Russia";
        $CC["RW"] = "Rwanda";
        $CC["RO"] = "Romania";
        $CC["SV"] = "El Salvador";
        $CC["WS"] = "Samoa";
        $CC["SM"] = "San Marino";
        $CC["ST"] = "Sao Tomea and Principe";
        $CC["SA"] = "Saudi Arabia";
        $CC["SZ"] = "Swaziland";
        $CC["SJ"] = "Svalbard and Jan Mayen Islands";
        $CC["SH"] = "St Helena";
        $CC["KP"] = "Korea (North)";
        $CC["MP"] = "Northern Mariana Islands";
        $CC["SC"] = "Seychelles";
        $CC["VC"] = "Saint Vincent and the Grenadines";
        $CC["PM"] = "Saint Pierre and Miquelon";
        $CC["SN"] = "Senegal";
        $CC["KN"] = "Saint Kitts (Christopher) and Nevis";
        $CC["LC"] = "Saint Lucia";
        $CC["SG"] = "Singapore";
        $CC["SY"] = "Syria";
        $CC["SK"] = "Slovakia";
        $CC["SI"] = "Slovenia";
        $CC["US"] = "United States";
        $CC["SB"] = "Solomon Islands";
        $CC["SD"] = "Sudan";
        $CC["SR"] = "Suriname";
        $CC["SL"] = "Sierra Leone";
        $CC["TJ"] = "Tajikistan";
        $CC["TL"] = "Timor-Leste";
        $CC["TH"] = "Thailand";
        $CC["TW"] = "Taiwan";
        $CC["TZ"] = "Tanzania";
        $CC["TC"] = "Turks and Caicos Islands";
        $CC["TG"] = "Togo";
        $CC["TK"] = "Tokelau";
        $CC["TO"] = "Tonga";
        $CC["TT"] = "Trinidad and Tobago";
        $CC["TV"] = "Tuvalu";
        $CC["TN"] = "Tunisia";
        $CC["TM"] = "Turkmenistan";
        $CC["TR"] = "Turkey";
        $CC["UG"] = "Uganda";
        $CC["UZ"] = "Uzbekistan";
        $CC["UA"] = "Ukraine";
        $CC["WF"] = "Wallis and Futuna Islands";
        $CC["UY"] = "Uruguay";
        $CC["FO"] = "Faroe Islands";
        $CC["FJ"] = "Fiji";
        $CC["PH"] = "Philippines";
        $CC["FI"] = "Finland";
        $CC["FK"] = "Falkland (Malvinas) Islands";
        $CC["FR"] = "France";
        $CC["GF"] = "French Guiana";
        $CC["PF"] = "French Polynesia";
        $CC["HM"] = "Heard and McDonald Islands";
        $CC["HR"] = "Croatia";
        $CC["CF"] = "Central African Republic";
        $CC["TD"] = "Chad";
        $CC["CZ"] = "Czech Republic";
        $CC["CL"] = "Chile";
        $CC["CH"] = "Switzerland";
        $CC["SE"] = "Sweden";
        $CC["LK"] = "Sri Lanka";
        $CC["EC"] = "Ecuador";
        $CC["GQ"] = "Equatorial Guinea";
        $CC["ER"] = "Eritrea";
        $CC["EE"] = "Estonia";
        $CC["ET"] = "Ethiopia";
        $CC["YU"] = "Yugoslavia";
        $CC["ZA"] = "South Africa";
        $CC["GS"] = "South Georgia and the South Sandwich Islands";
        $CC["KR"] = "South Korea";
        $CC["JM"] = "Jamaica";
        $CC["JP"] = "Japan";
        $CC["TF"] = "French Southern Territories";
        $CC["IO"] = "British Indian Ocean Territory";
        $CC["UM"] = "United States Minor Outlying Islands";
        $CC["EU"] = "Europa";
        $CC["A1"] = "(not set)";
        $CC["ZZ"] = "(not set)";
        $CC["XX"] = "(not set)";

        asort($CC);

        $this->list = $CC;

        foreach ($CC as $k => $c) {
            $this->listFilter[] = array("data" => $k, "title" => $c, "equal" => $k);
        }
    }

    /**
     * @param $ip
     *
     * @return string
     */
    public function getHtmlBlock($ip)
    {
        $cc = "xx";

        if (function_exists("geoip_country_code_by_name")) {
            try {
                @ $test = geoip_country_code_by_name($ip);
                if (!empty($test))
                    $cc = $test;
                
            } catch (Exception $e) {
                $cc = "xx";
            }
        }

        $sTpl = new Stemplater();
        $html = $sTpl->render("tpl-admin/phtml/admin/plugins", "geo", ["cc" => $cc, "ip" => $ip]);
        return $html;
    }


    /**
     * @param $field
     *
     * @return string
     */
    public function __get($field)
    {
        return (isset($this->list[$field])) ? $this->list[$field] : "";
    }

}