<?php

/**
 * Class Debug
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Debug
{

    const LOG_LEVEL_INFO = 0;
    const LOG_LEVEL_WARNING = 1;
    const LOG_LEVEL_CRITICAL = 2;
    const LOG_LEVEL_ERROR = 3;

    public static $log = array();

    /**
     * @param $var
     * @param null $var_name
     * @param null $indent
     * @param null $reference
     */
    public static function Dump(&$var, $var_name = NULL, $indent = NULL, $reference = NULL)
    {

        $do_dump_indent = "<span style='color:#666666;'>|</span> &nbsp;&nbsp; ";
        $reference = $reference . $var_name;
        $keyVar = 'the_do_dump_recursion_protection_scheme';
        $keyName = 'referenced_object_name';

        // So this is always visible and always left justified and readable
        echo "<div style='text-align:left; background-color:white; font: 100% monospace; color:black;'>";

        if (is_array($var) && isset($var[$keyVar])) {
            $real_var = &$var[$keyVar];
            $real_name = &$var[$keyName];
            $type = ucfirst(gettype($real_var));
            echo "$indent$var_name <span style='color:#666666'>$type</span> = <span style='color:#e87800;'>&amp;$real_name</span><br>";
        } else {
            $var = array($keyVar => $var, $keyName => $reference);
            $avar = &$var[$keyVar];

            $type = ucfirst(gettype($avar));
            if ($type == "String")
                $type_color = "<span style='color:green'>";
            elseif ($type == "Integer")
                $type_color = "<span style='color:red'>";
            elseif ($type == "Double") {
                $type_color = "<span style='color:#0099c5'>";
                $type = "Float";
            } elseif ($type == "Boolean")
                $type_color = "<span style='color:#92008d'>";
            elseif ($type == "NULL")
                $type_color = "<span style='color:black'>";

            if (is_array($avar)) {
                $count = count($avar);
                echo "$indent" . ($var_name ? "$var_name => " : "") . "<span style='color:#666666'>$type ($count)</span><br>$indent(<br>";
                $keys = array_keys($avar);
                foreach ($keys as $name) {
                    $value = &$avar[$name];
                    self::Dump($value, "['$name']", $indent . $do_dump_indent, $reference);
                }
                echo "$indent)<br>";
            } elseif (is_object($avar)) {
                echo "$indent$var_name <span style='color:#666666'>$type</span><br>$indent(<br>";
                foreach ($avar as $name => $value)
                    self::Dump($value, "$name", $indent . $do_dump_indent, $reference);
                echo "$indent)<br>";
            } elseif (is_int($avar))
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . htmlentities($avar) . "</span><br>";
            elseif (is_string($avar))
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color\"" . htmlentities($avar) . "\"</span><br>";
            elseif (is_float($avar))
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . htmlentities($avar) . "</span><br>";
            elseif (is_bool($avar))
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . ($avar == 1 ? "TRUE" : "FALSE") . "</span><br>";
            elseif (is_null($avar))
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> {$type_color}NULL</span><br>";
            else
                echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> " . htmlentities($avar) . "<br>";

            $var = $var[$keyVar];
        }

        echo "</div>";
    }

    /**
     * @param bool $serialize
     *
     * @return array|string
     */
    public static function smartBacktrace($serialize = false)
    {
        $trace = debug_backtrace();
        $stack = array();
        if (is_array($trace)) {
            foreach ($trace as $route) {

                if (!isset($route['file']))
                    continue;

                $ft = explode('/', $route['file']);
                $f = end($ft);

                if (isset($route['file']) && $f == __FILE__) {
                    continue;
                }

                if (isset($route['file'])) {
                    $stack[] = ($serialize)
                        ? array('file' => $f, 'fun' => $route['function'])
                        : $f . " > " . $route['function'] . " [Line: " . $route['line'] . "]";
                }
            }

            $stack = ($serialize)
                ? serialize($stack)
                : implode(PHP_EOL, $stack);
        }
        return $stack;
    }


    /**
     * @param Exception $e
     * @param int $level
     * @param null $module
     * @param array $options
     * @param bool $callback
     *
     * @return bool
     */
    public static function logException(Exception $e, $level = self::LOG_LEVEL_INFO, $module = null, array $options = array(), $callback = false)
    {
        self::logMsg($e->getMessage() . "\n" . $e->getTraceAsString() . "\n on " . $e->getLine(), "exceptions.log");
        if (is_array($callback) && $callback) {
            $class = (string)$callback[0];
            $callable = "{$class}::{$callback[1]}";
            if (@is_callable($callable) === true) {
                call_user_func_array($callable, $callback[2]);
            }
        }
        return true;
    }

    /**
     *
     * @param string $msg
     * @param string $file
     */
    public static function logMsg($msg, $file = 'common.log')
    {
        Config::makeDirectory("system/logs");
        $path = PATH_REAL . DS . "system/logs/" . $file;
        if (strpos(DIRECTORY_SEPARATOR, $file) !== false) {
            //если указана подпапка - проверить ее на существование
            $subfolders = explode(DIRECTORY_SEPARATOR, $file);
            array_pop($subfolders);//убираем сам файл
            $subdir = "";
            foreach ($subfolders as $i => $dir) {
                $subdir .= $dir . "/";
                if (!is_dir(PATH_REAL . DS . "system/logs/" . $subdir)) {
                    mkdir(PATH_REAL . DS . "system/logs/" . $subdir);
                }
            }
        }
        $trace = debug_backtrace();
        $lastTrace = end($trace);
        if (is_file($path) && filesize($path) > 50 * 1024 * 1024) {
            $flag = "";
        } else {
            $flag = FILE_APPEND;
        }
        $lastTrace['file'] = isset($lastTrace['file']) ? $lastTrace['file'] : $lastTrace['class'];
        $lastTrace['line'] = isset($lastTrace['line']) ? $lastTrace['line'] : "not-set-line";
        $msg = date("Y-m-d H:i:s") . " " . date_default_timezone_get() . "\n" . $lastTrace['file'] . ": " . $lastTrace['line'] . "\n" . $msg . " \n\r";
        if ($flag) {
            @file_put_contents($path, $msg, $flag);
        } else {
            file_put_contents($path, $msg);
        }
    }

    /**
     * @return array
     */
    public static function backtrace()
    {
        $d = debug_backtrace();
        $back = [];
        foreach ($d as $row) {
            if (isset($row['file']) && isset($row['line'])) {
                $back[] = $row['file'] . " - " . $row['line'] . " - " . $row['function'];
            }
        }
        return $back;
    }

    /**
     * @param $data
     */
    public static function info($data)
    {
        if (isset($_SESSION[AUTH_KEY]['idUser']) && !in_array($_SESSION[AUTH_KEY]['idUser'], array(8, 15))) {
            return;
        }

        $perm = new ControlTable("Debug Info:", "1", array("show" => false), array(array('data' => $data)));
        $perm->Draw();
    }

    /**
     * @param $query
     * @param $time
     */
    public static function PushDbLog($query, $time)
    {
        if (!DEV_DB_LOG) {
            return;
        }

        if (DEV_DB_LOG_FILE) {
            Debug::logMsg($query, "mysqli.log");
        }

        $backtrace = debug_backtrace();
        $backtrace = array_reverse($backtrace);

        $backtraceCompact = array();
        foreach ($backtrace as $b) {
            $backtraceCompact[] = (isset($b["class"])) ? $b["class"] . $b["type"] . $b["function"] : $b["function"];
        }

        $memory = round(memory_get_peak_usage() / 1024, 2);

        self::$log[] = array(
            "time" => $time,
            "memory" => $memory,
            "backtrace" => implode("<br/>", $backtraceCompact),
            "query" => SqlFormatter::format($query)
        );
    }

    /**
     * @param bool $slowFirst
     */
    public static function Db($slowFirst = true)
    {
        if (!empty(self::$log)) {
            if ($slowFirst)
                Arrays::Sksort(self::$log, "time", false);

            Debug::Table(self::$log, array(

                "time" => "Time",
                "memory" => "Memory",
                "backtrace" => "Backtrace",
                "query" => "Query",
            ), true);
        }
    }

    /**
     * @param array $data
     * @param array $columns
     * @param bool $appendStyles
     */
    public static function Table($data = array(), $columns = array(), $appendStyles = false)
    {
        $out = "";

        if (!empty($data)) {
            $out .= "<table border='0' cellpadding='0' cellspacing='0' class='debug'>";
            $out .= "<tr>";
            $out .= "<th align='center'>#</th>";
            if (!empty($columns)) {
                foreach ($columns as $column) {
                    $out .= "<th align='left'>{$column}</th>";
                }
            } else {
                $out .= "<th align='left'>Value</th>";
            }
            $out .= "</tr>";


            foreach ($data as $k => $row) {

                $out .= "<tr>";
                $kk = $k + 1;
                $out .= "<td valign='top' align='center'>{$kk}</td>";

                if (!empty($columns)) {
                    foreach ($columns as $key => $name) {
                        $out .= "<td valign='top' align='left'>{$row[$key]}</td>";
                    }
                } else {
                    $out .= "<td valign='top' align='left'>{$row}</td>";
                }


                $out .= "</tr>";
            }

            $out .= "</table>";

            if ($appendStyles)
                $out .= "<style> table.debug {
                    margin: 10px 0;box-sizing: border-box; border: solid 1px gainsboro; padding: 0; font-size: 12px; 
                    font-family: Consolas, Courier, monospace;            }
                table.debug td {  border-top: solid 1px gainsboro;  padding: 5px 20px;font-family: Consolas, Courier, monospace;   }                
                table.debug td pre{ margin: 0;   }
                table.debug th {  background-color: #fcfcfc; padding: 10px 20px;font-family: Consolas, Courier, monospace; }
                </style> ";

        }

        echo $out;


    }

    /**
     * @deprecated
     */
    public static function init()
    {
        if (isset($_SESSION[AUTH_KEY]['isSu']) && $_SESSION[AUTH_KEY]['isSu'] == 1) {
            $_SESSION['qlog'] = array();
        }
    }

}
