<?php

class Parser
{
    /**
     * @param $txt
     */
    public static function DebugError($txt)
    {

        echo "<strong>";
        echo $txt . "<br />\n";
        echo "</strong>";
    }

    /**
     * @param $txt
     */
    public static function DebugData($txt)
    {

        echo "<hr>";
        echo "<textarea style='width: 100%;'>";
        echo $txt . "<br />\n";
        echo "</textarea>";
    }

    /**
     * @param $uri
     * @param string $post
     * @return mixed
     */
    public static function Curl($uri, $post = "")
    {
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $uri);
        curl_setopt($ch2, CURLOPT_USERAGENT, "Googlebot/2.1 (+http://www.google.com/bot.html)");
        curl_setopt($ch2, CURLOPT_REFERER, "www.google.com");
        curl_setopt($ch2, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array('X-Requested-With: XMLHttpRequest'));
        $data = curl_exec($ch2);
        curl_close($ch2);

        return $data;

    }

    /**
     * @param $uri
     * @return mixed
     */
    public static function CurlSimple($uri)
    {
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $uri);
        curl_setopt($ch2, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_REFERER, "www.google.com");
        curl_setopt($ch2, CURLOPT_USERAGENT, "Googlebot/2.1 (+http://www.google.com/bot.html)");
        curl_setopt($ch2, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);                

        $data = curl_exec($ch2);
        curl_close($ch2);

        return $data;

    }


}