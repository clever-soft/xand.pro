<?php

/**
 * Class MCache
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class MCache
{

    protected static $instance;

    /**
     * MCache constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return bool
     */
    public static function GetStats()
    {
        self::Connect();
        if (MC_ENABLE === false) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            $val = self::$instance->getStats();
            return $val;
        } else {
            return false;
        }
    }

    /**
     *
     * @return boolean|Memcache
     */
    public static function Connect()
    {
        if (MC_ENABLE === false) {
            return false;
        }
        if (!class_exists('Memcache')) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            return self::$instance;
        }

        self::$instance = new Memcache;
        self::$instance->addServer(MC_SERVER, MC_PORT);
        $stats = @self::$instance->getExtendedStats();
        if (!$stats || !isset($stats[MC_SERVER . ":" . MC_PORT])) {
            return false;
        }
        $try = @self::$instance->connect(MC_SERVER, MC_PORT);
        if ($try === false) {
            return false;
        }
        return self::$instance;
    }

    /**
     *
     * @param string $key
     *
     * @return boolean|mixed
     */
    public static function Get($key)
    {
        self::Connect();
        if (MC_ENABLE === false) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            $val = self::$instance->get($key);
            if (!is_resource($val)) {
                $val = unserialize($val);
            }
            return $val;
        } else {
            return false;
        }
    }

    /**
     *
     * @param string $key
     * @param mixed $val
     * @param int $time
     *
     * @return boolean
     */
    public static function Set($key, $val, $time = 86400)
    {
        self::connect();
        if (MC_ENABLE === false) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            if (!is_resource($val)) {
                $val = serialize($val);
            }
            $do = self::$instance->set($key, $val, MEMCACHE_COMPRESSED, $time);
            return $do;
        } else {
            return false;
        }
    }

    /**
     * Удаляет все записи
     * @return boolean
     */
    public static function Flush()
    {
        self::connect();
        if (MC_ENABLE === false) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            $do = self::$instance->flush();
            return $do;
        } else {
            return false;
        }
    }

    /**
     * Удаляет запись по ключу
     *
     * @param string $key
     *
     * @return boolean
     */
    public static function Delete($key)
    {
        self::connect();
        if (MC_ENABLE === false) {
            return false;
        }
        if (self::$instance instanceof Memcache) {
            $do = self::$instance->delete($key);
            return $do;
        } else {
            return false;
        }
    }

    /**
     *
     */
    public static function Close()
    {
        if (self::$instance instanceof Memcache) {
            self::$instance->Close();
        }
    }

    /**
     *
     */
    private function __clone()
    {
    }


}
