<?php

class Config
{
    const PATH_CACHE = "system/cache/_conf.php";
    const PATH_CACHE_DIR = "system/cache";
    const PATH_XML = "conf.xml";

    //---
    public static function RefreshCache()
    {
        if (!self::makeDirectory()) {
            //ToDo: Replace buety text
            exit("Can't create system directories. Please check permission");
        }
        $xml = simplexml_load_file(PATH_REAL . DS . self::PATH_XML);

        $variables = array();
        $constants = "";

        foreach ($xml->group as $group) {

            $groupPrefix = strtoupper((string)$group->prefix);
            $method = (isset($group->method)) ? (string)$group->method : "define";


            foreach ($group->setting[0] as $key => $option) {

                $type = (isset($option["type"])) ? (string)$option["type"] : "string";

                switch ($type) {
                    case "number":
                        $option = (int)$option;
                        break;

                    case "boolean":
                        $option = ($option == "1") ? 'true' : 'false';
                        break;

                    default:
                        $option = "'" . $option . "'";
                }

                switch ($method) {
                    case "ini_set":
                        $option = str_replace("'", "", $option);
                        $constants .= "ini_set('{$key}', {$option}); \r\n";
                        break;

                    case "db":
                        $groupPrefix = strtolower($groupPrefix);
                        if (!isset($variables[$groupPrefix]))
                            $variables[$groupPrefix] = array();

                        $variables[$groupPrefix][] = "'" . $key . "' => " . $option . "";
                        break;

                    default:
                        $key = strtoupper($key);
                        $constants .= "define('{$groupPrefix}_{$key}', {$option}); \r\n";
                }
            }

        }

        $constants .=
            'if (PATH_SITE == "") { define("PATH_DS", DS . PATH_SITE); } else { define("PATH_DS", DS . PATH_SITE . DS); } ' . "\r\n" .
            'date_default_timezone_set(SITE_TIMEZONE);';

        $fileRaw = '';
        foreach ($variables as $k => $v) {
            $fileRaw .= 'Xand::$db["' . $k . '"] =  array(' . implode(",", $v) . ');';
            $fileRaw .= "\r\n";
        }

        $data = "<?php \r\n{$constants}\r\n{$fileRaw}";

        file_put_contents(PATH_REAL . DS . self::PATH_CACHE, $data);

    }

    /**
     * @param string $makeDir some/directory/bla
     * @return bool
     *
     * Добавить защиту от дурака
     */
    public static function makeDirectory($makeDir = self::PATH_CACHE_DIR)
    {
        $makeDir = trim($makeDir, "/");
        $dir = explode("/", $makeDir);
        if (is_dir(PATH_REAL . DS . $makeDir)) {
            return true;
        } else {
            $dirName = '';
            for ($i = 0; $i < count($dir); $i++) {
                $dirName .= "/" . $dir[$i];
                if (is_dir(PATH_REAL . $dirName))
                    continue;
                else {
                    if (mkdir(PATH_REAL . $dirName))
                        continue;
                    else
                        return false;
                }
            }
        }

        return true;
    }

    //---
    public static function Save($data)
    {

        $xml = simplexml_load_file(PATH_REAL . DS . self::PATH_XML);
        $groupIndex = 0;

        foreach ($xml->group as $group) {

            $groupPrefix = strtolower((string)$group->prefix);

            foreach ($group->setting[0] as $key => $option) {

                $key = (string)$key;
                if ((isset($data[$groupPrefix][$key])))
                    $xml->group[$groupIndex]->setting[0]->$key = $data[$groupPrefix][$key];
            }
            $groupIndex++;
        }


        if (is_writable(PATH_REAL . DS . self::PATH_XML)) {
            $xml->saveXML(PATH_REAL . DS . self::PATH_XML);
            self::RefreshCache();
            return true;
        } else {
            return false;
        }

    }

}

