<?php

/**
 * Class Message
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Message
{

    const DEFAULT_MESSAGE = "Text message";

    protected static $_instance;
    protected $postProcess = [];
    protected $queue = array();

    /**
     * Message constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Message
     */
    public static function I()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param mixed $callback
     *
     * @return $this
     */
    public function setPostProcess($callback = [])
    {
        $this->postProcess = $callback;
        return $this;
    }

    /**
     * @return $this
     */
    public function setPostProcess18n()
    {
        $this->postProcess = array('I18n', '__');
        return $this;
    }

    /**
     *
     */
    public function resetPostProcess()
    {
        $this->postProcess = [];
        return $this;
    }

    /**
     * @param string $message
     * @param bool $container
     *
     * @return $this
     */
    public function Info($message = self::DEFAULT_MESSAGE, $container = false)
    {
        array_push($this->queue, array(
            "body" => $this->PostProcessing($message),
            "class" => "msgInfo msg",
            "icon" => "fa-info-circle",
            "container" => $container
        ));
        return $this;
    }

    /**
     * @param $message
     *
     * @return string
     */
    protected function PostProcessing($message)
    {
        if (is_callable($this->postProcess)) {
            $class = (string)$this->postProcess[0];
            $callable = "{$class}::{$this->postProcess[1]}";
            $params = isset($this->postProcess[2]) ? $this->postProcess[2] : [];
            $params[] = $message;
            if (@is_callable($callable) === true) {
                $msg = call_user_func_array($callable, $params);
                return $msg;
            }
        }
        return $message;
    }

    /**
     * @param string $message
     * @param bool $container
     *
     * @return $this
     */
    public function Error($message = self::DEFAULT_MESSAGE, $container = false)
    {
        array_push($this->queue, array(
            "body" => $this->PostProcessing($message),
            "class" => "msgError msg",
            "icon" => "fa-exclamation-circle",
            "container" => $container
        ));
        return $this;
    }

    /**
     * @param array $messages
     *
     * @return $this
     */
    public function Errors($messages = array())
    {
        if (!is_array($messages)) {
            return $this->Error($messages);
        }

        foreach ($messages as $name => $text) {
            if (is_array($text)) {
                list($key, $val) = each($text);
                array_push($this->queue, array(
                    "body" => $this->PostProcessing($val),
                    "container" => $key,
                    "icon" => "fa-exclamation-circle",
                    "class" => "msgError msg"
                ));
            } else {
                array_push($this->queue, array(
                    "body" => $this->PostProcessing($text),
                    "container" => $name,
                    "icon" => "fa-exclamation-circle",
                    "class" => "msgError msg"
                ));
            }
        }
        return $this;
    }

    /**
     * @param string $message
     * @param bool $container
     *
     * @return $this
     */
    public function Success($message = self::DEFAULT_MESSAGE, $container = false)
    {
        array_push($this->queue, array(
            "body" => $this->PostProcessing($message),
            "class" => "msgSuccess msg",
            "icon" => "fa-check-square-o",
            "container" => $container
        ));
        return $this;
    }

    /**
     * @param string $message
     * @param bool $container
     *
     * @return $this
     */
    public function Warning($message = self::DEFAULT_MESSAGE, $container = false)
    {
        array_push($this->queue, array(
            "body" => $this->PostProcessing($message),
            "class" => "msgWarning msg",
            "icon" => "fa-warning",
            "container" => $container
        ));
        return $this;
    }

    /**
     * @return $this
     */
    public function Draw()
    {
        echo $this->toString();
        return $this;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $string = "";
        foreach ($this->queue as $q) {
            $icon = (isset($q["icon"])) ? "<i class='fa {$q["icon"]}'></i>" : "";
            $string .= "<div class='{$q["class"]}'>{$icon} {$q["body"]}</div>";
        }

        $this->queue = array();

        return $string;
    }


    /**
     * @return $this
     */
    public function Reload()
    {
        echo "<script> setTimeout(function(){ window.location.reload(); }, 1000); </script>";
        return $this;
    }

    /**
     *
     */
    public function E()
    {
        exit();
    }

    /**
     *
     */
    private function __clone()
    {
    }

}