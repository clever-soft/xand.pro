<?php

/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 31.05.2017
 * Time: 10:09
 */
class CmslibClean extends Clean
{
    public function run()
    {
        $this->setPack("lib");
        $this->setPackName("pack.Button");
        $this->setVersion("1.0.6");
        $this->setFiles([
            "Test.php"
        ]);
        $this->setDirectory([
            'test'
        ]);
        $this->fileClean();
    }
}