<?php

/**
 * Class TemplateCms
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class TemplateCms extends BaseTemplate
{

    const TPL_FOLDER = "phtml";

    public $forms = array();
    public $sources = array();
    private $tplFile;
    private $dir;

    /**
     * TemplateCms constructor.
     *
     * @param string $tplFile
     * @param bool $dir
     */
    public function __construct($tplFile = "", $dir = false)
    {
        $this->tplFile = $tplFile . ".phtml";
        $this->dir = ($dir) ? PATH_PUBLIC . $dir : PATH_PUBLIC . PATH_TPL . DS . PATH_THEME . DS . self::TPL_FOLDER;
    }

    /**
     * @param $content
     *
     * @return bool
     */
    public function RenderCompiled($content)
    {
        return true;
    }


    /**
     * @param $matches
     *
     * @return mixed|string
     */
    public function TplDecode($matches)
    {
        $match = explode(":", $matches[1]);
        if (is_callable([$this, $match[0]], true)) {
            $call = array_shift($match);
            if (!isset($match[0]))
                $match[0] = false;
            if (!isset($match[1]))
                $match[1] = false;
            $output = $this->$call(...$match);
        } else
            $output = "This method {$match[0]}, not supports in the X-CMF";
        return $output;
    }


    /**
     *
     */
    public function Load()
    {
        include_once($this->Compile());
    }

    /**
     * @return string
     */
    private function Compile()
    {
        $pathSource = PATH_REAL . DS . $this->dir . DS . $this->tplFile;
        $this->tplFile = str_replace(DS, "-", $this->tplFile);
        if (!Config::makeDirectory(PATH_CTPL))
            exit("Can't create system directories. Please check permission");
        $pathCompiledTpl = PATH_REAL . DS . PATH_CTPL . DS . ((defined('SITE_CURRENT_LANG')) ? SITE_CURRENT_LANG : SITE_DEFAULT_LANG);

        if (!is_dir($pathCompiledTpl)) {
            mkdir($pathCompiledTpl);
        }

        if (!(file_exists($pathCompiledTpl . DS . $this->tplFile) && FC_COMPILE_TEMPLATES)) {

            if (!file_exists($pathSource)) {
                exit("This template {$pathSource}, not found.");
            }

            $content = file_get_contents($pathSource);
            $content = $this->Render($content, false);
            $content = I18n::__($content);

            $sources = "";
            if (!empty($this->sources)) {
                $sources = "<?php " . implode("\r\n", $this->sources) . "?>";
            }

            file_put_contents($pathCompiledTpl . DS . $this->tplFile, $sources . $content);

        }

        return $pathCompiledTpl . DS . $this->tplFile;
    }

    /**
     * @param $content
     * @param bool $innerRender
     *
     * @return mixed|string
     */
    public function Render($content, $innerRender = true)
    {
        while (true) {
            if (substr_count($content, "{{") == 0) break;
            $content = preg_replace_callback("|{{(.*)}}|U", array($this, "TplDecode"), $content);
        }

        if ($innerRender) {
            ob_start();
            eval(" ?> " . $content . " <?php ");
            $content = ob_get_contents();
            ob_end_clean();
        }

        return $content;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        ob_start();
        include_once($this->Compile());
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

    /**
     * @param $file
     *
     * @return string
     */
    public function Informer($file)
    {

        $informerObjectNameArr = explode(DS, $file);
        $informerObjectName = "";

        foreach ($informerObjectNameArr as $name)
            $informerObjectName .= ucfirst($name);

        if (class_exists($informerObjectName)) {

            $this->sources[] = '$this->assignArray((new ' . $informerObjectName . ')->getContent());';
            return $this->Inc($file);

        } else {
            return "Informer: '{$informerObjectName}' doesn't exists!";
        }
    }

    /**
     * @param $file
     *
     * @return string
     */
    public function Inc($file)
    {
        $path = ($file[0] == DS)

            ? PATH_REAL . DS . PATH_PUBLIC . $file . ".phtml"
            : PATH_REAL . DS . PATH_PUBLIC . PATH_TPL . DS . PATH_THEME . DS . "phtml" . DS . $file . ".phtml";

        return (file_exists($path)) ? file_get_contents($path) : "File does not exist: " . $path;
    }


    /**
     * Add menu by parent or All
     *
     * @param $file
     * @param $parent
     * @return bool|string
     */
    public function Menu($file, $parent = false)
    {
        $menuObjectNameArr = explode(DS, $file);
        $menuObjectName = "";

        foreach ($menuObjectNameArr as $name)
            $menuObjectName .= ucfirst($name);

        if (class_exists($menuObjectName)) {
            $this->sources[] = '$this->assignArray((new ' . $menuObjectName . ')->getMenu(' . $parent . '));';
            return $this->Inc($file);

        } else {
            return "Informer: '{
                $menuObjectName}' doesn't exists!";
        }
    }

    /**
     * @param $par
     * @param bool $call
     *
     * @return mixed|string
     */
    public function Data($par, $call = true)
    {
        if (isset($this->isMailer) && $this->isMailer === true) {
            $call = true;
        }
        if (substr_count($par, ".") == 1) {
            $parEx = explode(".", $par);
            if ($call) {
                return $this->data[$parEx[0]][$parEx[1]];
            } else {
                return (isset($this->data[$parEx[0]][$parEx[1]])) ? '<?php echo $this->Data("' . $par . '"); ?>' : "";
            }

        } else {
            if ($call) {
                return (isset($this->data[$par])) ? $this->data[$par] : "";
            } else {
                return (isset($this->data[$par])) ? '<?php echo $this->Data("' . $par . '"); ?>' : "";
            }
        }
    }

    /**
     * @param $par
     * @param bool $call
     *
     * @return string
     */
    public function Form($par, $call = true)
    {
        if ($call) {
            return $this->forms[$par]->Draw();
        } else {
            return (isset($this->forms[$par])) ? '<?php echo $this->Form("' . $par . '"); ?>' : "";
        }
    }

    /**
     * @return string
     */
    public function Dir()
    {
        return PATH_DS . PATH_TPL . DS . PATH_THEME;
    }

    /**
     * @return string
     */
    public function DirAdmin()
    {
        return PATH_DS . PATH_TPL . DS . "tpl-admin";
    }

    /**
     * @return string
     */
    public function DirLib()
    {
        return PATH_DS . PATH_TPL . DS . "tpl-lib";
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function Img($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='normal'/>";
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function ImgRight($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='right'/>";
    }

    /**
     * @param $par
     *
     * @return string
     */
    public function ImgLeft($par)
    {
        return "<img src='" . PATH_DS . PATH_TPL . DS . PATH_THEME . DS . "img" . DS . $par . "' class='left'/>";
    }

    /**
     * @return string
     */
    public function Lang()
    {
        return SITE_CURRENT_LANG;
    }

    /**
     * @return mixed
     */
    public function Store()
    {
        return SITE_CURRENT_STORE;
    }

    /**
     * @return string
     */
    public function PathDs()
    {
        return PATH_DS;
    }

    /**
     * @param $group
     * @param $obj
     *
     * @return $this
     */
    public function AssignForm($group, $obj)
    {
        $this->forms[$group] = $obj;
        return $this;
    }


}
