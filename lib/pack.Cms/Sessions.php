<?php

/**
 * Class Sessions
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Sessions
{
    private $ip;

    /**
     * @return SDbQuery
     */
    public static function model()
    {
        return SDbQuery::table("customers_sessions");
    }

    /**
     * Sessions constructor.
     */
    public function __construct()
    {

        $this->ip = $_SERVER["REMOTE_ADDR"];

        session_set_save_handler
        (
            array($this, 'open'), //
            array($this, 'close'), //
            array($this, 'read'), //
            array($this, 'write'), //
            array($this, 'destroy'), //
            array($this, 'gc')
        );
        return true;
    }

    /**
     * @param $save_path
     * @param $session_name
     *
     * @return bool
     */
    public function open($save_path, $session_name)
    {
        return true;
    }

    /**
     * @return bool
     */
    public function close()
    {
        return true;
    }

    /**
     * @param $sid
     *
     * @return null
     */
    public function read($sid)
    {
        $data = self::model()->where(["sid" => $sid])->selectRow();

        if ($data && $this->ip != $data['ip']) {
            setcookie("PHPSESSID", "", time() - 360000);
            // exit("Access denied");
        }

        return $data ? $data['data'] : null;
    }

    /**
     *
     * @param type $sid
     * @param type $sessionData
     *
     * @return boolean
     */
    public function write($sid, $sessionData)
    {

        $sidExists = self::model()->where(["sid" => $sid])->selectRow();




        if (!$sidExists) {

            self::model()->insert(array(
                'sid' => $sid,
                'dateCreate' => time(),
                'dateUpdate' => time(),
                'ip' => $this->ip,
                "data" => $sessionData
            ));

            return true;


        } else {
            self::model()
                ->where(array("sid" => $sid))
                ->update(array(
                    'sid' => $sid,
                    "dateUpdate" => time(),
                    "data" => $sessionData
                ));
            return true;
        }



    }

    /**
     *
     * @param int $id
     *
     * @return boolean
     */
    public function destroy($id)
    {
        return  self::model()->where(["sid" => $id])->delete();
    }

    /**
     *
     * @param int $maxLifeTime
     *
     * @return boolean
     */
    public function gc($maxLifeTime)
    {
        $lt = time() - $maxLifeTime;

        self::model()->whereRaw("dateUpdate < {$lt}")->delete();
    }

}