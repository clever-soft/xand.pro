<?php

/**
 * Class Date
 */
class Date
{
    /**
     * @param $string
     *
     * @return bool|int|string
     */
    public static function ToUnix($string)
    {
        if (!is_numeric($string))
            return date("U");

        $d = substr($string, 0, 2);
        $m = substr($string, 3, 2);
        $y = substr($string, 6, 4);
        return mktime('0', '0', '0', $m, $d, $y);
    }


    /**
     * @param $date_stamp
     *
     * @return string
     */
    public static function Format($date_stamp)
    {
        if (!is_numeric($date_stamp)) {
            $dt = DateTime::createFromFormat("Y-m-d H:i:s", $date_stamp);
            if (!is_object($dt)) {
                return date("d.m.Y", time()) . " <span style='color:#777; float: none; display: inline;'>" . date("H:i", time()) . "</span>";
            }
            $date_stamp = $dt->modify('+2 hour')
                ->format('U');
        }

        return date("d.m.Y", $date_stamp) . " <span style='color:#777; float: none; display: inline;'>" . date("H:i", $date_stamp) . "</span>";
    }


    /**
     * @param $date_stamp
     *
     * @return string
     */
    public static function FormatSec($date_stamp)
    {
        if (!is_numeric($date_stamp)) {
            $dt = DateTime::createFromFormat("Y-m-d H:i:s", $date_stamp);
            if (!is_object($dt)) {
                return date("d.m.Y", time()) . " <span style='color:#777; float: none; display: inline;'>" . date("H:i:s", time()) . "</span>";
            }
            $date_stamp = $dt->modify('+2 hour')
                ->format('U');
        }

        return date("d.m.Y", $date_stamp) . " <span style='color:#777; float: none; display: inline;'>" . date("H:i:s", $date_stamp) . "</span>";
    }


    /**
     * @param $date_stamp
     *
     * @return string
     */
    public static function FormatLong($date_stamp)
    {
        return date("d.m.Y", $date_stamp) . " <span style='color:#777; float: none; display: inline;'>" . date("H:i", $date_stamp) . "</span>";
    }

    /**
     * @param $seconds
     *
     * @return string
     */
    public static function GetDHM($seconds)
    {
        $days = floor($seconds / 86400);
        $seconds = $seconds - ($days * 86400);
        $hours = floor($seconds / 3600);
        $seconds = $seconds - ($hours * 3600);
        $minutes = floor($seconds / 60);

        $minutes = ($minutes < 10) ? "0" . $minutes : $minutes;
        $hours = ($hours < 10) ? "0" . $hours : $hours;

        if ($days > 0) {
            $days = ($days < 10) ? "0" . $days : $days;
            return $days . ":" . $hours . ":" . $minutes;
        }

        return $hours . ":" . $minutes;
    }

    /**
     * @param $seconds
     *
     * @return string
     */
    public static function GetDHMExtend($seconds)
    {
        $days = floor($seconds / 86400);
        $seconds = $seconds - ($days * 86400);
        $hours = floor($seconds / 3600);
        $seconds = $seconds - ($hours * 3600);
        $minutes = floor($seconds / 60);

        $result = "";

        if ($days > 0) $result .= $days . " " . I18n::__("days") . ", ";
        if ($hours > 0 && $hours < 24) $result .= $hours . " " . I18n::__("hours") . ", ";
        if ($minutes > 0 && $minutes < 60) $result .= $minutes . " " . I18n::__("minutes") . ", ";
        if ($seconds > 0 && $seconds < 60) $result .= $seconds . " " . I18n::__("seconds") . ", ";

        return trim($result, ", ");
    }

    /**
     * Возвращает первый и последний день указанной недели
     *
     * @param int $week
     * @param int $year
     *
     * @return array
     */
    public function getStartAndEndDate($week, $year)
    {
        $week--;
        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('d.m.Y', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('d.m.Y', $time);
        return $return;
    }

    /**
     * @param $date_stamp
     * @return string
     */
    public static function formatShort($date_stamp)
    {
        if (!is_numeric($date_stamp)) {
            $dt = DateTime::createFromFormat("Y-m-d H:i:s", $date_stamp);
            if (!is_object($dt)) {
                return date("d.m.Y", time());
            }
            $date_stamp = $dt->modify('+2 hour')
                ->format('U');
        }

        return date("d.m.Y", $date_stamp);
    }

}