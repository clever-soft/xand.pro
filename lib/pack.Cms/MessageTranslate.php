<?php

/**
 *
 * */
class MessageTranslate extends Message
{
    private function __construct() {
    }

    private function __clone() {
    }

    //---
    public static function I() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    //---
    protected function PostProcessing($message) {
        return I18n__("%{$message}%");
    }
}