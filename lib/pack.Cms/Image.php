<?php

/*
13.03.2012 - Добавлено новый метод обрезки картинки CroppedThumbnail

*/

class Image
{

    //---
    public static function GetImageDir($folder = "i")
    {
        $scan = scandir(PATH_REAL . DS . PATH_PUBLIC . "uploads/" . $folder);
        sort($scan);

        $last = (int)array_pop($scan);
        if (is_dir(PATH_REAL .  DS . PATH_PUBLIC . "uploads/{$folder}/" . $last)) {
            $scan = scandir(PATH_REAL .  DS . PATH_PUBLIC . "uploads/{$folder}/" . $last);
            if (count($scan) > 100) $last++;
        }

        $path = "uploads/{$folder}/" . $last;
        $pathReal = PATH_REAL .  DS . PATH_PUBLIC . $path;

        if (!is_dir($pathReal)) {
            mkdir($pathReal);
            exec("mkdir " . $pathReal);
            exec("chmod 0777 " . $pathReal);
        }


        return $path;
    }


   //---

    public static function CropImage($nw, $nh, $source, $dest, $stype, $quality)
    {
        $size = getimagesize($source);
        $w = $size[0];
        $h = $size[1];

        switch ($stype) {
            case 'image/gif':
                $simg = imagecreatefromgif($source);
                break;
            case 'image/jpeg':
                $simg = imagecreatefromjpeg($source);
                break;
            case 'image/png':
                $simg = imagecreatefrompng($source);
                self::image_change_alphaColor($simg, imagecolorallocatealpha($simg, 255, 255, 255, 0));
                imagesavealpha($simg, false);
                break;
        }

        $dimg = imagecreatetruecolor($nw, $nh);

        $wm = $w / $nw;
        $hm = $h / $nh;
        $h_height = $nh / 2;
        $w_height = $nw / 2;
        if ($w > $h) {
            $adjusted_width = $w / $hm;
            $half_width = $adjusted_width / 2;
            $int_width = $half_width - $w_height;
            imagecopyresampled($dimg, $simg, -$int_width, 0, 0, 0, $adjusted_width, $nh, $w, $h);
        } elseif (($w < $h) || ($w == $h)) {
            $adjusted_height = $h / $wm;
            $half_height = $adjusted_height / 2;
            $int_height = $half_height - $h_height;
            imagecopyresampled($dimg, $simg, 0, -$int_height, 0, 0, $nw, $adjusted_height, $w, $h);
        } else {
            imagecopyresampled($dimg, $simg, 0, 0, 0, 0, $nw, $nh, $w, $h);
        }


        imagejpeg($dimg, $dest, $quality);
    }

   //---
    public static function CroppedThumbnail($imgSrc, $dest, $stype, $quality, $thumbnail_width, $thumbnail_height)
    {

        list($width_orig, $height_orig) = getimagesize($imgSrc);

        switch ($stype) {
            case 'image/gif':
                $myImage = imagecreatefromgif($imgSrc);
                break;
            case 'image/jpeg':
                $myImage = imagecreatefromjpeg($imgSrc);
                break;
            case 'image/png':
                $myImage = imagecreatefrompng($imgSrc);
                self::image_change_alphaColor($myImage, imagecolorallocatealpha($myImage, 255, 255, 255, 0));
                imagesavealpha($myImage, false);
                break;
        }

        $ratio_orig = $width_orig / $height_orig;

        if ($thumbnail_width / $thumbnail_height > $ratio_orig) {
            $new_height = $thumbnail_width / $ratio_orig;
            $new_width = $thumbnail_width;
        } else {
            $new_width = $thumbnail_height * $ratio_orig;
            $new_height = $thumbnail_height;
        }

        $x_mid = $new_width / 2;  //horizontal middle
        $y_mid = $new_height / 2; //vertical middle

        $process = imagecreatetruecolor(round($new_width), round($new_height));

        imagecopyresampled($process, $myImage, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
        $thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);

        imagecopyresampled($thumb, $process, 0, 0, ($x_mid - ($thumbnail_width / 2)), ($y_mid - ($thumbnail_height / 2)),
            $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);

        imagedestroy($process);
        imagedestroy($myImage);

        imagejpeg($thumb, $dest, $quality);

    }

   //---
    public static function CreateNewImage()
    {
        $args = func_get_args();

        $S = $args[0];
        $T = $args[1];
        $stype = $args[2];

        $x_max = (isset($args[3])) ? $args[3] : IMG_WIDTH;
        $y_max = (isset($args[4])) ? $args[4] : IMG_HEIGHT;


        $tojpg = (isset($args[5])) ? $args[5] : true;
        $quality = (isset($args[6])) ? $args[6] : IMG_QUALITY;


        $size = getimagesize($S);
        if ($size === false) die ('Bad image file!');

        switch ($stype) {
            case 'image/gif':
                $source = imagecreatefromgif($S);
                break;
            case 'image/jpeg':
                $source = imagecreatefromjpeg($S);
                break;
            case 'image/png':
                $source = imagecreatefrompng($S);
                break;
        }

        $x_src = imagesx($source);
        $y_src = imagesy($source);

        $X = $x_src;
        $Y = $y_src;

        if ($x_src > $x_max || $y_src > $y_max) {
            if ($x_src > $y_src) {
                $X = $x_max;
                $Y = ceil(($y_src * $x_max) / $x_src);
            } elseif ($y_src > $x_src) {
                $Y = $y_max;
                $X = ceil(($x_src * $y_max) / $y_src);
            } else {
                $Y = $y_max;
                $X = ceil(($x_src * $y_max) / $y_src);
            }
        }
        $result_arr = array($X, $Y);

        $target = imagecreatetruecolor($X, $Y);

        $dest_x = $X;
        $dest_y = $Y;

        if ($tojpg) {

            if ($stype == 'image/png') {
                //
                self::image_change_alphaColor($source, imagecolorallocatealpha($target, 255, 255, 255, 0));
                imagesavealpha($source, false);
                imagecopyresampled($target, $source, 0, 0, 0, 0, $X, $Y, $size[0], $size[1]);
                imagejpeg($target, $T, $quality);

            } else {
                imagecopyresampled($target, $source, 0, 0, 0, 0, $X, $Y, $size[0], $size[1]);
                imagejpeg($target, $T, $quality);
            }
        } else {
            switch ($stype) {
                case 'image/gif':
                    imagecopyresampled($target, $source, 0, 0, 0, 0, $X, $Y, $size[0], $size[1]);
                    imagegif($target, $T);
                    break;
                case 'image/jpeg':
                    imagecopyresampled($target, $source, 0, 0, 0, 0, $X, $Y, $size[0], $size[1]);
                    imagejpeg($target, $T, $quality);
                    break;
                case 'image/png':
                    imagealphablending($target, false);
                    imagesavealpha($target, true);
                    imagecopyresampled($target, $source, 0, 0, 0, 0, $X, $Y, $size[0], $size[1]);
                    imagepng($target, $T, 9);
                    break;
            }
        }
        imagedestroy($target);
        imagedestroy($source);

        return $result_arr;
    }
}
