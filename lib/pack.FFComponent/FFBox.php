<?php

/**
 * Class FFBox
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFBox extends FFObjectBase
{
    public $html = "";

    /**
     * FFBox constructor.
     *
     * @param string $html
     * @param string $key
     * @param string $class
     */
    public function __construct($html = "empty", $key = "", $class = "")
    {
        $this->html = $html;
        $this->key = ($key) ? $key : md5($html);
        $this->class = $class;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        return "<div class='{$this->class}'>{$this->html}</div>";
    }

}