<?php

class FFInputDateGroup extends FFInput
{
    public $slaveName;

    private $yearsForm   = array();
    private $monthsForm  = array();

    //---
    public function SetDefaults($options = array())
    {
        $years = range(date('Y'), (date('Y') + 10));
        foreach ($years as $y) {
            $this->yearsForm[$y] = $y;
        }

        //$this->monthsForm[-1] = I18n::__("%Month%");
        for ($mn = 1; $mn <= 12; $mn++) {
            $mkey = ($mn < 10) ? "0" . $mn : $mn."";
            $this->monthsForm[$mkey] = $mkey;
        }

        $this->requiredRule = new ValidateDateGroup;
        $this->slaveKey     = $options["slaveKey"];
    }


    //---
    protected function RenderBody($data)
    {

        $this->slaveName    = ($this->key2)  ? $this->key2."[".$this->slaveKey."]"  : $this->slaveKey;

        $html = "
        <select name='{$this->name}'
                style='width: 80px;'                
                required-type='{$this->requiredRule->name}'
                class='{$this->class}'{$this->requiredTT}{$this->readonly}>";

        if (isset($this->options["allowEmpty"])) {
            $html .= "<option value='-1'>".I18n::__("%Year%")."</option>";
        }
        foreach ($this->yearsForm as $k => $v) {
            $selected = ($data === $k) ? "selected='selected'" : "";
            $html .= "<option value='{$k}' {$selected}>{$v}</option>";
        }

        $html .= "</select>";

        $html .= "
        <select name='{$this->slaveName}'
                style='width: 80px;'
                id='{$this->slaveKey}' required-type='{$this->requiredRule->name}'
                class='{$this->class}'
                {$this->requiredTT}{$this->readonly}>";

        if (isset($this->options["allowEmpty"])) {
            $html .= "<option value='-1'>".I18n::__("%Month%")."</option>";
        }
        foreach ($this->monthsForm as $k => $v) {
            $selected = ($data === $k) ? "selected='selected'" : "";
            $html .= "<option value='{$k}' {$selected}>{$v}</option>";
        }

        $html .= "</select>";


        return  "<li>{$html}</li>";
    }

}
