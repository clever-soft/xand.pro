<?php

class FFInputCreditCard extends FFInput
{
    public function SetDefaults($options = array())
    {
        $this->requiredRule  = new ValidateCreditCard;
        $this->slaveKey      = $options["slaveKey"];
    }

    //---
    protected function RenderBody($data)
    {
        return  "<li><input type='text'
                            name='{$this->name}'                            
                            value='{$data}'
                            required-type='{$this->requiredRule->name}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            {$this->requiredTT}{$this->readonly}></li>";
    }

}