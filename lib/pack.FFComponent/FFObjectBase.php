<?php

/**
 * Class FFObjectBase
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFObjectBase
{

    public $key = "";
    public $key2 = "";
    public $title = "";
    public $name = "";
    public $data;

    public $defaultValue = null;

    public $placeholder;
    public $class;
    public $readonly;
    public $required;
    public $requiredRule;
    public $requiredTT;
    public $slaveKey;
    public $titleWidth;
    public $arrayIndex = false;
    public $objects = array();

    protected $rawHtml = [];

    protected $internalCounter = 0;


    protected $model = array();

    /**
     * FFObjectBase constructor.
     *
     * @param string $title
     * @param bool $key
     * @param array $options
     */
    public function __construct($title = "Unnamed", $key = false, $options = array())
    {

        //--- Main
        $this->title = (!isset($options["autoTranslateDisable"])) ? I18n::__("%{$title}%") : $title;
        $this->key = ($key) ? $key : md5($this->title);
        $this->key2 = (isset($options["key2"])) ? $options["key2"] : $this->key2;
        $this->requiredRule = new ValidateExists;

        //--- Component defaults
        if (method_exists($this, "SetDefaults")) {
            $this->SetDefaults($options);
        }

        //--- Custom values
        $this->requiredRule = (isset($options["requiredRule"])) ? $options["requiredRule"] : $this->requiredRule;
        $this->defaultValue = (isset($options["defaultValue"])) ? $options["defaultValue"] : $this->defaultValue;
        $this->placeholder = (isset($options["placeholder"])) ? $options["placeholder"] : $this->placeholder;
        $this->class = (isset($options["class"])) ? $options["class"] : $this->class;
        $this->required = (isset($options["required"])) ? $options["required"] : $this->required;
        $this->titleWidth = (isset($options["titleWidth"])) ? $options["titleWidth"] : $this->titleWidth;
        $this->readonly = (isset($options["readonly"])) ? " disabled='disabled'" : $this->readonly;
        $this->data = (isset($options["data"])) ? $options["data"] : $this->data;
        $this->arrayIndex = (isset($options['arrayIndex']) ? (int)$options['arrayIndex'] : false);
        $this->name = ($this->key2) ? $this->key2 . "[" . $this->key . "]" : $this->key;
        if ($this->arrayIndex !== false) {
            $this->setMultidimensional(true);
            $this->key .= '_' . $this->arrayIndex;
        }

        $this->requiredTT = ($this->required)
            ? (($this->required !== true)
                ? "required='required' required-text='" . I18n::__("%{$this->required}%") . "'"
                : "required='required'")
            : "";


    }

    /**
     * @param $status
     *
     * @return $this
     */
    public function setMultidimensional($status)
    {
        $this->isMultimedinsional = (bool)$status;
        return $this;
    }

    /**
     * @return $this
     */
    public function Add()
    {
        $objects = func_get_args();
        if (!empty($objects)) {
            foreach ($objects as $o) {
                if ($o instanceof IFFActions) {
                    $this->actions[$o->key] = $o;
                } elseif (is_string($o)) {
                    $this->objects[] = $o;
                } else {
                    $this->internalCounter++;
                    $this->objects[$o->key] = $o;
                }
            }
        }

        return $this;
    }

    /**
     * @param $htmlBlock
     *
     * @return $this
     */
    public function addHtml($htmlBlock)
    {
        $this->rawHtml[] = $htmlBlock;
        return $this;
    }

    /**
     * @param array $model
     * @param array $aliases
     *
     * @return $this
     */
    public function Fill($model = array(), $aliases = array())
    {
        if (empty($model)) {
            return $this;
        }
        if ($aliases) {
            foreach ($aliases as $real => $alias) {
                if (isset($model[$real])) {
                    $model[$alias] = $model[$real];
                }
            }
        }

        $this->model = array_merge($this->model, $model);

        return $this;
    }

    /**
     * @return string
     */
    protected function RenderTitle()
    {
        $titleWidth = ($this->titleWidth) ? " style='width:{$this->titleWidth}px;' " : "";
        $requiredAster = ($this->required) ? " <b>*</b>" : "";

        return "<li{$titleWidth}><label class='title'>{$this->title}{$requiredAster}</label></li>";
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        return "";
    }

    /**
     * @return string
     */
    protected function RenderContainer()
    {
        $html = "";

        foreach ($this->objects as $key => $o) {
            if (is_string($o)) {
                $html .= $o;
                continue;
            }
            /**
             * @var FFObjectBase $o
             */
            if ($o instanceof IFFContainer) {
                $o->Fill($this->model);
            }

            if ($this->key2) {
                if ($o->arrayIndex !== false && is_numeric($o->arrayIndex)) {
                    $o->name = $this->key2 . "[" . $o->key . "]";

                } else {
                    $o->name = $this->key2 . "[" . $o->key . "]";
                }
                $o->key2 = $this->key2;
            }

            if (isset($this->active) && !$this->active) {
                $o->readonly = " disabled='disabled'";
            }

            if (isset($this->model[$this->key2]) && $this->key2) {
                $value = (isset($this->model[$this->key2][$key])) ? $this->model[$this->key2][$key] : (string)$this->defaultValue;
            } else {
                $value = (isset($this->model[$key])) ? $this->model[$key] : (string)$this->defaultValue;
            }

            $html .= $o->Render($value);
        }

        return $html;
    }

    /**
     * @return array
     */
    protected function getArrayIndex()
    {
        $ar = explode('[', $this->key2);
        $index = trim($ar[1], "]");
        return [$ar[0], $index];
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function Render($data)
    {
        $html = "<ul>";
        $html .= $this->RenderTitle();
        $html .= $this->RenderBody($data);
        $html .= "</ul>";

        return $html;

    }


}