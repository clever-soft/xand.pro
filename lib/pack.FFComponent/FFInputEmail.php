<?php

class FFInputEmail extends FFObjectBase
{
    public function SetDefaults($options = array())
    {
        $this->requiredRule  = new ValidateEmail;
    }

    //---
    protected function RenderBody($data)
    {
        return  "<li><input type='email'
                            name='{$this->name}'
                            required-type='{$this->requiredRule->name}'                            
                            value='{$data}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            {$this->requiredTT}{$this->readonly}></li>";
    }

}