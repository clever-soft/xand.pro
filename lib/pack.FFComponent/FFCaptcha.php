<?php

/**
 * Class FFCaptcha
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class FFCaptcha extends FFObjectBase
{

    /**
     *
     */
    public static function Draw()
    {
        $str = rand(100000, 999999) . '';
        $_SESSION['frontedCaptcha'] = $str;

        $fontPath = PATH_REAL . DS . PATH_PUBLIC . "tpl/tpl-lib/plugins/captcha";

        $x0 = 110;
        $y0 = 38;
        $ks = 1;

        $img = imagecreate($x0, $y0);


        $backgroundColor = ImageColorAllocate($img, 255, 255, 255);
        $borderColor = ImageColorAllocate($img, 0, 0, 0);
        $blackColor = ImageColorAllocate($img, 240, 240, 240);
        $fontColor = ImageColorAllocate($img, 0, 0, 0);
        $shiftX = 10;


        for ($k = 0; $k < 6; $k++) {
            $ugol = rand(-20, 20);

            imagettftext($img, 18, $ugol, $k * 12 + $shiftX, 28, $fontColor, "{$fontPath}/cap.ttf", $str[$k]);
        }

        header("Content-type: image/png");
        ImagePng($img);
        ImageDestroy($img);
    }

    /**
     * @param array $options
     */
    protected function SetDefaults($options = array())
    {
        $this->requiredRule = new ValidateCaptcha;
    }


    /**
     * @param $data
     *
     * @return string
     */
    protected function RenderBody($data)
    {
        $path = PATH_DS . SITE_DEFAULT_LANG . "/system/captcha/ff";

        return "<li class='ffCaptcha'>

                     <img src='{$path}'/>
                     <a href='' onclick='CaptchaRefresh(this); return false;'>
                                <i class='fa fa-refresh'></i>
                     </a>

                     <input type='text'
                            name='{$this->name}'                             
                            value='{$data}'
                            placeholder='{$this->placeholder}'
                            class='{$this->class}'
                            required-type='{$this->requiredRule->name}'
                            {$this->requiredTT}{$this->readonly}>

                 </li>";
    }

}