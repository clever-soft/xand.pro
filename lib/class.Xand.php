<?php

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");   // Date in the past

//=== configurations from cache file >>>  ===/
define("PATH_REAL", substr(dirname(__FILE__), 0, strlen(dirname(__FILE__)) - 4));
define("DS", "/");
define("BR", "<br />");
define("AUTH_KEY", "auth");

if (file_exists(PATH_REAL . "/conf.xml")) {
    if (!file_exists(PATH_REAL . "/system/cache/_conf.php")) {
        include_once("pack.Cms/Config.php");
        Config::RefreshCache();
    }
} else {
    header("Location: ./install/");
    exit();
}


include_once(PATH_REAL . "/system/cache/_conf.php");

/**
 * Class Xand
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class Xand
{

    // Globals

    public static $currentModule = "";

    public static $classes = array();
    public static $classesLog = array();
    public static $classesFolders = array();
    public static $db = array();
    public static $mongodb = array();
    public static $nav;

    //---
    public static $r = array();

    /**
     *
     */
    public static function Route()
    {

        self::$nav = new Nav();
        self::$nav->parse();

        $multiLangMode = true;

        if (isset(self::$nav->path[0]) && self::$nav->path[0] == "admin") {
            define("SITE_CURRENT_LANG", SITE_DEFAULT_LANG);
        } else {
            if ($multiLangMode) {
                if (isset(self::$nav->path[0]) && !in_array(self::$nav->path[0], explode(",", strtolower(SITE_LANGS)))) {
                    array_unshift(self::$nav->path, SITE_DEFAULT_LANG);
                    header("Location: http://" . XAND_DOMAIN . DS . self::$nav->toString());
                }
                $currLang = (isset(self::$nav->path[0]) && in_array(self::$nav->path[0], explode(",", strtolower(SITE_LANGS))))

                    ? self::$nav->path[0]
                    : strtolower(SITE_DEFAULT_LANG);

                define("SITE_CURRENT_LANG", $currLang);

                if (isset(self::$nav->path[0]) && strtolower(self::$nav->path[0]) == strtolower(SITE_CURRENT_LANG)) {
                    array_shift(self::$nav->path);
                }
            } else {
                define("SITE_CURRENT_LANG", SITE_DEFAULT_LANG);
            }
        }

        $module = (isset(self::$nav->path[0]) && !empty(self::$nav->path[0]) ? self::$nav->path[0] : XAND_DEFAULT_MODULE);
        $controller = (isset(self::$nav->path[1]) && !empty(self::$nav->path[1]) ? self::$nav->path[1] : "default");
        $action = (isset(self::$nav->path[2]) && !empty(self::$nav->path[2]) ? self::$nav->path[2] : "index");

        $baseController = new BaseController();
        if (!$baseController->run($module, $controller, $action)) {
            echo "System error occurred, route not found: $module/$controller/$action";
            debug_print_backtrace();
        }
        return;
    }

    /**
     * @param string $className
     */
    public static function AutoloadUniversal($className)
    {

        if (DEV_DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $backtrace = array_reverse($backtrace);

            $backtraceCompact = array();
            foreach ($backtrace as $b) {
                $backtraceCompact[] = (isset($b["class"])) ? $b["class"] . $b["type"] . $b["function"] : $b["function"];
            }

            $memory = round(memory_get_peak_usage() / 1024, 2);
            $memoryDiff = 0;
            if (!empty(self::$classesLog)) {
                $memoryDiff = round($memory - self::$classesLog[count(self::$classesLog) - 1]["memory"], 2);
            }

            self::$classesLog[] = array(
                "class" => $className,
                "memory" => $memory,
                "memoryDiff" => $memoryDiff,
                "backtrace" => implode("<br/>", $backtraceCompact)
            );
        }

        if (!self::autoloadFromCache($className)) {
            self::autoloadReal($className);
        }
    }

    /**
     * @param string $className
     *
     * @return bool
     */
    private static function autoloadFromCache($className)
    {
        if (isset(Xand::$classes[$className]) && isset(Xand::$classesFolders[Xand::$classes[$className]])) {
            if (!@include_once(PATH_REAL . Xand::$classesFolders[Xand::$classes[$className]] . DS . $className . ".php")) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $className
     */
    private static function autoloadReal($className)
    {
        self::ScanClassesList();

        if (isset(Xand::$classes[$className]) && isset(Xand::$classesFolders[Xand::$classes[$className]])) {
            include_once(PATH_REAL . Xand::$classesFolders[Xand::$classes[$className]] . DS . $className . ".php");
        } else {
        }
    }

    /**
     *
     */
    public static function ScanClassesList()
    {
        self::$classesFolders = array();
        $classes = array();
        $packages = glob(PATH_REAL . DS . PATH_CORE . "/*", GLOB_ONLYDIR);
        foreach ($packages as $package) {
            $paths = glob($package . DS . "*.php");
            foreach ($paths as $path)
                $classes[] = $path;
        }


        $modules = glob(PATH_REAL . DS . "mod/*", GLOB_ONLYDIR);
        foreach ($modules as $mod) {
            $paths = array_merge(
                glob($mod . "/models/*.php"),
                glob($mod . "/components/*.php"),
                glob($mod . "/informers/*.php"),
                glob($mod . "/forms/*.php")
            );

            foreach ($paths as $path)
                $classes[] = $path;

        }

        foreach ($classes as $class) {
            $baseName = str_replace(".php", "", basename($class));
            $folder = str_replace(PATH_REAL, "", str_replace($baseName . ".php", "", $class));

            if (!isset(self::$classesFolders[$folder])) {
                $index = count(self::$classesFolders);
                self::$classesFolders[$folder] = $index;
            } else {
                $index = self::$classesFolders[$folder];
            }
            self::$classes[$baseName] = $index;
        }

        self::$classesFolders = array_keys(self::$classesFolders);
        self::SaveClassesList();
    }

    /**
     *
     */
    public static function SaveClassesList()
    {
        $out = "<?php\r\n";

        $out .= 'Xand::$classesFolders = array(';
        foreach (self::$classesFolders as $val) {
            $out .= '"' . $val . '",';
        }
        $out = trim($out, ",");
        $out .= '); ';
        $out .= 'Xand::$classes = array(';
        foreach (self::$classes as $key => $val) {
            $out .= '"' . $key . '"=>' . $val . ',';
        }
        $out = trim($out, ",");
        $out .= '); ';

        file_put_contents(PATH_REAL . DS . "system/cache/_classes.php", $out);
    }


    /**
     *
     */
    public static function LoadClassesList()
    {
        if (!file_exists(PATH_REAL . DS . "system/cache/_classes.php")) {
            self::ScanClassesList();
        }

        include_once(PATH_REAL . DS . "system/cache/_classes.php");
    }

    /**
     * @param string $key
     * @param bool $data
     */
    public static function Register($key = "key", $data = false)
    {
        if (is_array($data)) {
            self::$r[$key] = array();
            foreach ($data as $dat)
                array_push(Xand::$r[$key], $dat);
        } else {
            self::$r[$key] = $data;
        }
    }

    /**
     *
     */
    public static function Debug()
    {
        if (!empty(Xand::$classesLog))
            Debug::Table(Xand::$classesLog, array(
                    "class" => "Class",
                    "memory" => "Memory",
                    "memoryDiff" => "Memory diff",
                    "backtrace" => "Backtrace"
                )
            );

    }
}

Xand::LoadClassesList();


try {
    spl_autoload_register(array('Xand', 'AutoloadUniversal'));
} catch (LogicException $e) {
    header("HTTP/1.0 500 Internal Server Error");
    echo $e->getMessage();
    exit();
}

$s = new Sessions();
session_start();

register_shutdown_function(function () {
    if (MC_ENABLE !== false)
        MCache::Close();
});
