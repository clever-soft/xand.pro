<?php 
/**
 * Class ButtonAppend
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/ 
class ButtonMailEdit extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Mail Edit";				
		$this->action 		= "MailEditReady";
		
		$this->setProperty("cssClass","buttons_ico_text")
			 ->setProperty("fontClass", "fa-credit-card")
			 ->setProperty("fontColor", "#A4A9FF") 
			 
		
		
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/email_edit.png);") 
			 
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/email_edit2.png);")  
			 
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIcoText");	
	}
}
?>