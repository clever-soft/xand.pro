<?php

/**
 * Class ButtonCancel
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonCancel extends ButtonBase
{
    public function SetDefaults()
    {
        $this->title = "Cancel";
        $this->action = "Cancel";

        $this->setProperty("script", "DataAct")
            ->setProperty("svg", "buttonCancel")
            ->setProperty("dialogEnable", false)
            ->setProperty("dialog", "Are you sure you want cancel ?")
            ->setProperty("cssClass", "bGray");
    }
}
