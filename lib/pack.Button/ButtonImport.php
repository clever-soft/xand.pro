<?php

//--------
class ButtonImport extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Импорт";				
		$this->action 		= "Import";
		
		$this->setProperty("cssClass", "buttons_ico")
			 ->setProperty("styles", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/table_import.png);")
			 ->setProperty("dialogEnable", false)
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");	
			
	}
}

?>