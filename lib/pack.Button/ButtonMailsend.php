<?php 
/**
 * Class ButtonMailsend
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonMailsend extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Sending  force";				
		$this->action 		= "SendForce";
		
		$this->setProperty("svg","buttonMail-blue")
 			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Are you sure you want to push a letter?")
			 ->setProperty("script","DataAct");
	}
}