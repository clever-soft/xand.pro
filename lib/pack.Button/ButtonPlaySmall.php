<?php

//--------
class ButtonPlaySmall extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Start";
        $this->action = "Play";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/tpl/svg/play.svg);")
                ->setProperty("stylesBig", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/big-reset.png);")
                ->setProperty("dialog", "Are you sure you want to make account active?")
                ->setProperty("dialogEnable", false)
                ->setProperty("script", "DataAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>