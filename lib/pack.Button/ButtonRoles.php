<?php
/**
 * Class ButtonView
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonRoles extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "User roles";				
		$this->action 		= "RolesReady";

		$this->setProperty("svg","buttonsRoles")
			 ->setProperty("script", "DataAct");
	}
}
