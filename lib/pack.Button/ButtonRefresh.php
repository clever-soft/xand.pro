<?php

//--------
class ButtonRefresh extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Refresh";				
		$this->action 		= "Refresh";
		
		$this->setProperty("fontClass", "fa-refresh")
			 ->setProperty("fontColor", "#8ed74d")
			 ->setProperty("svg",       "buttonRefresh")
			 ->setProperty("script",    "DataAct");
			
	}
}
