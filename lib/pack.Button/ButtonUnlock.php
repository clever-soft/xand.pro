<?php 
/**
 * Class ButtonUnlock
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonUnlock extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Закрыть";				
		$this->action 		= "Unlock";

		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/unlock.png);")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("dialog","Вы точно хотите открыть обсуждения?")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");	
	}
}
?>