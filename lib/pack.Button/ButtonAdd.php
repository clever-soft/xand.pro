<?php

/**
 * Class ButtonAdd
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class ButtonAdd extends ButtonBase
{
    public function setDefaults()
    {
        $this->title = "Add";
        $this->action = "AddFinish";

        $this->setProperty("styles", "buttonPlus")
            ->setProperty("dialog", "Do you really want to add!")
            ->setProperty("script", "JsonAct")
            ->setProperty("cssClass", "btn-primary")
            ->setProperty("drawType", "DrawForms");
    }
}
