<?php 
/**
 * Class ButtonEscalate
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonUnescalate extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Un escalate";				
		$this->action 		= "UnEscalate";	
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/tpl/svg/danger-on.svg);")
			 ->setProperty("drawType","DrawIco")
			 ->setProperty("script","DataAct");		
	}	
}
?>