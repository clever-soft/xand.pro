<?php 
/**
 * Class ButtonAppend
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/ 
class ButtonLogout extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Logout";				
		$this->action 		= "Logout";
		
		$this->setProperty("cssClass","buttons_ico_text")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/big-exit.png);")			 
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/big-exit.png);") 
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIcoText");	
	}
}
?>