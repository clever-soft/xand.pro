<?php

//--------
class ButtonTruncate extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Cleaning queue";				
		$this->action 		= "Truncate";
		
		$this->setProperty("cssClass", "buttons_ico_text")
		
		 	 ->setProperty("fontClass", "fa-minus-square-o")
			 ->setProperty("fontColor", "#FF3F2A") 
		
			 ->setProperty("styles", "background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/table_truncate.png);")
			 ->setProperty("stylesBig","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/truncate2.png);")
			 ->setProperty("dialog", "Do you really want to clear the table ?")
			 ->setProperty("dialogEnable", true)
			 ->setProperty("script", "DataAct")
			 ->setProperty("drawType","DrawIcoText");	
			
	}
}

?>