<?php
/**
 * Class ButtonInfo
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonInfo extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Информация";				
		$this->action 		= "Info";
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/information.png);")
			 ->setProperty("dialog","Вы точно хотите войти?")
			 ->setProperty("script","MODAL")
			 ->setProperty("drawType","DrawIco");			
	}
}
?>