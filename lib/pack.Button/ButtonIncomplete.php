<?php

//--------
class ButtonUncomplete extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Uncomplete";
        $this->action = "uncomplete";

        $this->setProperty("cssClass", "buttons_ico")
                ->setProperty("styles", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/discomplete.png);")
//                ->setProperty("stylesBig", "background-image:url(" . PATH_DS . PATH_CORE . DS . "pack.Button/styles/ico/big-reset.png);")
                ->setProperty("dialog", "Are you sure you want to complete?")
                ->setProperty("dialogEnable", false)
                ->setProperty("script", "DataAct")
                ->setProperty("drawType", "DrawIco");
    }

}

?>
