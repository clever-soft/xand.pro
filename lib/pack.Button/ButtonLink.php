<?php
/**
 * Created by PhpStorm.
 * User: kubrey
 * Date: 11.12.15
 * Time: 15:30
 */

class ButtonLink extends ButtonBase{
    public function SetDefaults() {
        $this->title = "Add";
        $this->action = "AddReady";

        $this->setProperty("fontClass", "fa-plus")
            ->setProperty("fontColor", "#8ed74d")
            ->setProperty("href", PATH_DS."admin/")
            ->setProperty("script", "DataAct")
            ->setProperty("drawType", "DrawIcoText");
    }
}