<?php 
/**
 * Class ButtonEdit
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 **/

class ButtonInvoice extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Generate & download Invoice";				
		$this->action 		= "InvoiceReady";

		$this->setProperty("svg","buttonPdf")
			 ->setProperty("script","Ajax");
	}
}
?>