<?php 
/**
 * Class ButtonStatistic
 *
 * @тип  	 Class
 * @пакет    Button
 * @версия   1
 *
 * 
 *
 *
 **/
class ButtonStatistic extends ButtonBase
{		
	public function SetDefaults()
	{
		$this->title 		= "Статистика";				
		$this->action 		= "Statistic";
		
		$this->setProperty("cssClass","buttons_ico")
			 ->setProperty("styles","background-image:url(".PATH_DS.PATH_CORE.DS."pack.Button/styles/ico/statistics.png);")
			 ->setProperty("script","DataAct")
			 ->setProperty("drawType","DrawIco");	
	}
}
?>