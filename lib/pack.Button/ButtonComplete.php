<?php

//--------
class ButtonComplete extends ButtonBase {

    public function SetDefaults() {
        $this->title = "Complete";
        $this->action = "complete";

        $this->setProperty("svg", "buttonComplete")
             ->setProperty("dialog", "Are you sure you want to complete?")
             ->setProperty("dialogEnable", false)
             ->setProperty("script", "DataAct");
    }

}

?>