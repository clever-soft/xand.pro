<?php
 

class Panel extends GrouperBase
{
	public function SetDefaults()
	{
	}	
	
	public function Draw()
	{
		 echo "<div class = 'formPanel'>
				 <h5>{$this->name}</h5>";

                 foreach($this->dataArray as $key => $val)
                 {
					 if(empty($val->data) && isset($this->parent->dataArray[0][$val->key]))
                         $val->data = $this->parent->dataArray[0][$val->key];

                     $val->Draw();
                 }

		 echo "</div>";
	}	
}

