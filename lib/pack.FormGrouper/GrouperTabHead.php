<?php

/**
 * Class GrouperTabHead
 *
 * @тип  	 Class
 * @пакет    Form
 * @версия   1
 *
 *	
 * 
 *
 **/

class GrouperTabHead extends InputBase
{ 
	public function SetDefaults()
	{		
	    $this->setProperty("width","100%")
			 ->setProperty("ignore", true);
	
		 $this->properties["disabled"] = array();			 
			 
	}	
	//---------------
	public function GetDataJson(){}		
	//---------------
	
	public function Draw()
	{ 	   
	
		echo "<div class='grouperTabTitleWrap'>";
		foreach($this->dataArray as $k => $title)
		{
			$cssClass = ($k == 0) ? "grouperTabTitleActive" : "grouperTabTitleNoactive";
			
			//Debug::Dump($this->properties["disabled"]);
			
			echo (isset($this->properties["disabled"][$k])) 
			? "<a href='' class='grouperTabTitle grouperTabTitleDisabled' onclick='return false;'>{$title}</a>"
			: "<a href='' class='grouperTabTitle {$cssClass}' onclick='GrouperTabHeadClick(this); return false;'>{$title}</a>";
			
		}
		echo "</div>";
	
	}	
}
?>


