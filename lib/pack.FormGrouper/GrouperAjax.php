<?php

/**
 * Class GrouperAjax
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class GrouperAjax extends InputBase
{
    public $ajax;

    /**
     *
     */
    public function SetDefaults()
    {
        $this->setProperty("class", "input_div")
            ->setProperty("titleEnable", false)
            ->setProperty("ajaxZoneId", $this->domId . "Ajax");


    }

    /**
     *
     */
    public function PrepareData()
    {
        echo "if({$this->domId}OnGetData != null) {$this->domId}OnGetData();";
    }

    /**
     * @return string
     */
    public function GetDataJson()
    {
        if ($this->properties['ignore'] == false) return ", '" . $this->key . "':$('#" . $this->domId . "').val()";
    }

    /**
     *
     */
    public function Draw()
    {
        echo "<script>" . $this->domId . "OnGetData = null;</script>";

        if (!$this->getProperty("path")) Message::Fast("EI|Not set path for ajax controller");

        $this->ajax = new Ajax($this->getProperty("path"), false, "html");
        $this->ajax->setExemplarId($this->domId);
        $this->ajax->setZone($this->getProperty("ajaxZoneId"));
        $this->ajax->createJsExemplar();


        echo "<div class='{$this->properties['class']}' id='" . $this->getProperty("ajaxZoneId") . "' ></div>";
        echo "<input id='{$this->domId}' type='hidden'  value='{$this->data}'/>";

    }
    
}
