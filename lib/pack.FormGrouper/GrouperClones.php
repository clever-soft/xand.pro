<?php


class GrouperClones extends GrouperBase
{
    public function SetDefaults()
    {
    }

    //-------
    public function GetDataJson()
    {
        $result =  $this->properties["baseObject"]->GetDataJson();
        return $result;
    }

    //-------
    public function Draw()
    {
        $iterator = 1;

        $defaultGroupName =  $this->properties["baseObject"]->name;
        $this->properties["baseObject"]->name = "<span class='itemCloneName'>{$defaultGroupName} {$iterator}</span>";
        foreach ($this->properties["baseObject"]->dataArray as $k => &$v) {
            $v->key .= "-" . $iterator;
            //Debug::Dump($v);
        }

        echo "<div class = 'formPanel grouperClones'>
				<h5>{$this->name}</h5>";

        ?>
        <div class="grouperClonesItems">
            <div class="grouperClonesItem" data-index="1">
                <button class='btn btn-mini grouperClonesRemove' disabled="disabled"  onclick='grouperClonesRemove(this); return false;'>Remove</button>
                <?php $this->properties["baseObject"]->Draw();  ?>
            </div>
        </div>

        <a class="grouperClonesAddNew" onclick="GrouperClonesClone(this, '<?=$defaultGroupName?>'); return false;">
            <i class="fa fa-plus"></i> Add more
        </a>

        <?php

        echo "</div>";
    }
}

