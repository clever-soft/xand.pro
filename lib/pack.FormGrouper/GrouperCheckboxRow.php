<?php
class GrouperCheckboxRow extends GrouperBase
{
    public function SetDefaults()
	{		
	    $this->setProperty("width","100%")	
			 ->setProperty("titleWidth",160) 		 
			 ->setProperty("active", false)
			 ->setProperty("data", array())
			 ->setProperty("show", false)	  
			 ->setProperty("defaultColumnWidth", 160);		
	}	
	
	//---------------
	public function GetDataJson()
	{			
		$result = ", '".$this->key."': $('#".$this->domId."').val()";
		foreach($this->dataArray as $key => $val)	$result .= $val->GetDataJson();			
		return $result;
	}
	
	//---------------
	public function Draw()
	{   
	   $active = ($this->getProperty("active")) ? true : false;	
				
	   if(isset($this->properties["data"][0][$this->key])) 
		   $checked = ($this->properties["data"][0][$this->key]) 
				  ? $this->properties["data"][0][$this->key] : false; 

	   $checked  = ($active) ? "checkboxSel" : "checkboxUnSel";
	   $disabled = ($active) ? "" 			 : "grouperCheckboxDisabled";	 
	 
		?>

        <div class="grouperCheckbox">
					<?php   
					 foreach($this->dataArray as $key => $val)
					 {	   				 
						  if($val->data == "" && isset($this->parent->dataArray[0][$val->key]))  
								$val->data = $this->parent->dataArray[0][$val->key];	  
													
						  echo "<div style='display:block;'>";
							  $val->Draw();									
						  echo "</div>";
									  
					 }	  
							 
                    ?>
            <div id="grouperCheckboxBlock<?php echo $this->domId; ?>" class="grouperCheckboxBlocks <?php  echo $disabled;  ?>"></div>

            <h5>
                <a href="" class="checkbox <?php echo $checked; ?>" id="<?php echo $this->domId; ?>"
                   onclick="ActivateContaner(this, '<?php echo $this->domId; ?>'); return false;">
                    <?php echo $this->name; ?></a>
            </h5>

        </div>

        <?php
	}	
}