<?php

/**
 * Class QuerySimple
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class QuerySimple extends QueryBase
{

    /**
     * @return string
     */
    public function execute()
    {
        $distinct = ($this->distinct) ? " DISTINCT " : "";

        $output = " SELECT " . $distinct . implode(" , ", $this->select);
        $output .= " FROM " . implode(" , ", $this->tables);
        $output .= (!empty($this->criteria)) ? " WHERE " . implode(' AND ', $this->criteria) : "";

        if ($this->order != false)
            $output .= ($this->order == false) ? "" : " ORDER BY " . $this->order;

        $output .= ($this->limit == false) ? "" : " LIMIT " . $this->limit;

        // echo SqlFormatter::format($output);
        $data = SDb::rows($output, $this->holders);

        return $data;
    }


    /**
     * @return int
     */
    public function countExecute()
    {
        $output = " SELECT COUNT(*) as cnt";
        $output .= " FROM " . implode(" , ", $this->tables);
        $output .= (!empty($this->criteria)) ? " WHERE " . implode(' AND ', $this->criteria) : "";
        $count = SDb::cell($output, $this->holders);

        return $count;
    }
}