<?php

/**
 * Class RuleLikeChildQuery
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleLikeChildQuery extends RuleBase
{
    public $queryTable;
    public $queryField;

    /**
     * RuleLikeChildQuery constructor.
     *
     * @param string $field
     * @param string $table
     * @param bool|callable $queryTable
     * @param $queryField
     */
    function __construct($field, $table, $queryTable, $queryField)
    {
        $this->field = $field;
        $this->table = $table;
        $this->queryTable = $queryTable;
        $this->queryField = $queryField;
    }

    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $path = SDbQuery::table($this->queryTable)->where([$this->field => $val])->selectCell($this->queryField);
        if ($this->provideHolders) {
            $val = $val . "%";
            return array($this->queryTable . "." . $this->queryField . " LIKE ?", array($val));
        }
        return $this->queryTable . "." . $this->queryField . " LIKE '" . $path . "%'";
    }
}