<?php

/**
 * Class RuleDummy
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleDummy extends RuleBase
{
    /**
     * @param $val
     *
     * @return string
     */
    public function GetRule($val)
    {

        return "1=1";
    }

}