<?php

/**
 * Class RuleDynamic
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleDynamic extends RuleBase
{
    /**
     * @param $val
     *
     * @return mixed
     */
    public function GetRule($val)
    {
        $callback = $this->callback;
        return $callback($val);
    }

}