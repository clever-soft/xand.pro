<?php

/**
 * Class RuleDate
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleDate extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $point = ($this->table == "") ? "" : ".";
        $val = str_replace("_", "", $val);
        if ($this->provideHolders) {
            return array($this->table . $point . $this->field . "=?", array($val));
        }
        return $this->table . $point . $this->field . " = " . $val;
    }
}