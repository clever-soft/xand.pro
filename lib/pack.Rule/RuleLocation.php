<?php

/**
 * Class RuleLocation
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleLocation extends RuleBase
{
    /**
     * @param $val
     *
     * @return array|string
     */
    public function GetRule($val)
    {
        $val = SDbQuery::table($this->table)->where([$this->field => $val])->selectCell("path");
        $point = "";
        if ($this->provideHolders) {
            $val = $val . "%";
            return array($this->table . $point . ".path LIKE ?", array($val));
        }
        return $this->table . $point . ".path LIKE '" . $val . "%'";

    }
}