<?php

/**
 * Description of class
 *
 * @author Kubrey <kubrey@gmail.com>
 */
class RuleCheckboxAsIs extends RuleBase {

    public function GetRule($val) {
//        $point = ($this->table == "") ? "" : ".";
//        return $this->table . $point . $this->field . " = " . $val;
        return $this->parseVal($val);
    }

    /**
     * Парсит значения чекбокса (1:3:8)
     * @param string $val
     * @return string
     */
    private function parseVal($val) {
        $val = str_replace('zero_value', '0', $val);
        $query = array();
        $criteria = '';
        $point = ($this->table == "") ? "" : ".";
        if (substr_count($val, ':') > 0) {
            $data = explode(':', $val);
        } else {
            $data = array($val);
        }
        foreach ($data as $dt) {
            if ($dt != '') {
                if (is_numeric($dt)) {
                    $query[] = $this->table . $point . $this->field . $dt;
                } else {
                    $query[] = $this->table . $point . $this->field . $dt;
                }
            }
        }
        $criteria.='(';
        foreach ($query as $q) {
            $criteria .= $q;
            if (count($query) > 1 && $q != end($query)) {
                $criteria.=' OR ';
            }
        }
        $criteria.=')';
        return $criteria;
    }

}

?>
