<?php

/**
 * Class RuleRangeUnix
 *
 * @author Alexander Wizard <alexanderwizard19@gmail.com>
 */
class RuleRangeUnix extends RuleBase
{

    /**
     * @param $val
     *
     * @return string
     */
    public function GetRule($val)
    {

        $point = ($this->table == "") ? "" : ".";

        $valExplode = explode("to", $val);

        $valExplode[0] = (isset($valExplode[0])) ? $valExplode[0] : "";
        $valExplode[1] = (isset($valExplode[1])) ? $valExplode[1] : "";


        $result = array();
        $valExplode[0] = (int)str_replace("_", "", $valExplode[0]);
        $valExplode[0]--;
        $valExplode[0] = ($valExplode[0] == "") ? "" : $this->table . $point . $this->field . " >= " . strtotime(str_replace("_", "", $valExplode[0]) . "235959");
        $valExplode[1] = ($valExplode[1] == "") ? "" : $this->table . $point . $this->field . " <  " . strtotime(str_replace("_", "", $valExplode[1]) . "235959");

        if ($valExplode[0] != "")
            $result[] = $valExplode[0];
        if ($valExplode[1] != "")
            $result[] = $valExplode[1];

        return implode(" AND ", $result);
    }

}