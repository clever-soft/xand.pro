<?php
define("MICROTIME", microtime(true));
if (strpos($_SERVER['REQUEST_URI'], ".css") !== false)    exit("file not found");
if (strpos($_SERVER['REQUEST_URI'], ".js") !== false)     exit("file not found");
if (strpos($_SERVER['REQUEST_URI'], ".jpeg") !== false)   exit("file not found");
if (strpos($_SERVER['REQUEST_URI'], ".jpg") !== false)    exit("file not found");
if (strpos($_SERVER['REQUEST_URI'], ".ico") !== false)    exit("file not found");
if (strpos($_SERVER['REQUEST_URI'], ".png") !== false)    exit("file not found");


include_once("../lib/class.Xand.php");
Xand::Route();

$t = round(microtime(true) - MICROTIME, 5);