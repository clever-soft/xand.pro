<?php
/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 20.04.2017
 * Time: 16:01
 */

//=== configurations from cache file >>>  ===/

define("PATH_REAL", substr(dirname(__FILE__), 0, strlen(dirname(__FILE__)) - 15));

require_once("Install.php");

if (!file_exists(PATH_REAL . "/conf.xml.example")) {
    echo
    "
    <div style='text-align:center; width: 100%; margin: auto'><h1>Something went wrong with your CMS</h1>
    <h2>Please make new install</h2><br><code>composer create-project xand/xand {path_to_project_folder}</code>
    </div>
    ";
    exit();
}
$install = new Install();
$xml = _getXml();
$xmlArr = [];
foreach ($xml as $gr) {
    foreach ($gr->setting[0] as $key => $setting) {
        $xmlArr[(string)$gr->prefix][$key] = $setting;
    }
}
?>
    <html>
    <head>
        <title>Install Xand</title>
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <style>
            #Administrator,
            #System,
            #File_manager,
            #Paths,
            #Images,
            #File_cache,
            #Memory_cache,
            #PHP,
            #Develop,
            #MySql,
            #Update_Server,
            .has-error {
                display: none;
            }

            .checkdb {
                margin: 0 80px;
            }

            fieldset {
                min-height: 600px;
            }

            fieldset > p {
                position: absolute;
                bottom: 0;
            }
        </style>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {


                $(".next").click(function () {
                    if ($('#Site').is(":visible")) {
                        current_fs = $('#Site');
                        next_fs = $('#Administrator');
                    } else if ($('#Administrator').is(":visible")) {
                        current_fs = $('#Administrator');
                        next_fs = $('#System');
                    }
                    else if ($('#System').is(":visible")) {
                        current_fs = $('#System');
                        next_fs = $('#File_manager');
                    }
                    else if ($('#File_manager').is(":visible")) {
                        current_fs = $('#File_manager');
                        next_fs = $('#Paths');
                    }
                    else if ($('#Paths').is(":visible")) {
                        current_fs = $('#Paths');
                        next_fs = $('#Images');
                    }
                    else if ($('#Images').is(":visible")) {
                        current_fs = $('#Images');
                        next_fs = $('#File_cache');
                    }
                    else if ($('#File_cache').is(":visible")) {
                        current_fs = $('#File_cache');
                        next_fs = $('#Memory_cache');
                    }
                    else if ($('#Memory_cache').is(":visible")) {
                        current_fs = $('#Memory_cache');
                        next_fs = $('#PHP');
                    }
                    else if ($('#PHP').is(":visible")) {
                        current_fs = $('#PHP');
                        next_fs = $('#Develop');
                    }
                    else if ($('#Develop').is(":visible")) {
                        current_fs = $('#Develop');
                        next_fs = $('#MySql');
                    }
                    else if ($('#MySql').is(":visible")) {
                        current_fs = $('#MySql');
                        next_fs = $('#Update_Server');
                    }

                    next_fs.show();
                    current_fs.hide();
                });

                $('.previous').click(function () {
                    if ($('#Administrator').is(":visible")) {
                        current_fs = $('#Administrator');
                        next_fs = $('#Site');
                    } else if ($('#System').is(":visible")) {
                        current_fs = $('#System');
                        next_fs = $('#Administrator');
                    }

                    else if ($('#File_manager').is(":visible")) {
                        current_fs = $('#File_manager');
                        next_fs = $('#System');
                    }
                    else if ($('#Paths').is(":visible")) {
                        current_fs = $('#Paths');
                        next_fs = $('#File_manager');
                    }
                    else if ($('#Images').is(":visible")) {
                        current_fs = $('#Images');
                        next_fs = $('#Paths');
                    }
                    else if ($('#File_cache').is(":visible")) {
                        current_fs = $('#File_cache');
                        next_fs = $('#Images');
                    }
                    else if ($('#Memory_cache').is(":visible")) {
                        current_fs = $('#Memory_cache');
                        next_fs = $('#File_cache');
                    }
                    else if ($('#PHP').is(":visible")) {
                        current_fs = $('#PHP');
                        next_fs = $('#Memory_cache');
                    }
                    else if ($('#Develop').is(":visible")) {
                        current_fs = $('#Develop');
                        next_fs = $('#PHP');
                    }
                    else if ($('#MySql').is(":visible")) {
                        current_fs = $('#MySql');
                        next_fs = $('#Develop');
                    }
                    else if ($('#Update_Server').is(":visible")) {
                        current_fs = $('#Update_Server');
                        next_fs = $('#MySql');
                    }
                    next_fs.show();
                    current_fs.hide();
                });
                $('.checkdb').click(function () {
                    var host = $("#host").val();
                    var user = $("#user").val();
                    var name = $("#nameDb").val();
                    var password = $("#password").val();


                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: "checkDb.php",
                        global: false,
                        data: "host=" + host + "&user=" + user + "&name=" + name + "&password=" + password,
                        dataType: "json",
                        success: function (json) {
                            console.log(json);
                           if (json.error === true) {
                               var div = $("div.has-error");
                               div.text(json.reason)
                               div.show();
                           } else {
                               var botton = $("a.next.disabled");
                               var div = $("div.has-error");
                               div.text("All OK")
                               botton.removeClass("disabled");
                           }
                        }
                    });
                });

            });
        </script>

    </head>
    <body>
    <div class="container">
        <div class="col-lg-7">
            <form action="/install/makeXml.php" method="post" name="makeXml" id="formMakeXml" class="form-horizontal">
                <fieldset id="Site">
                    <h2>Site</h2>
                    <label>Main Configuration</label>
                    <div class="form-group">
                        <label for="name" class="col-lg-4 control-label">Site Name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                   value="<?php echo $xmlArr['site']['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Timezone</label>
                        <div class="col-lg-8">
                            <select class="form-control" name="timezone[]" id="timezone">
                                <?php
                                foreach (Install::$timezonesShort as $tz) {
                                    $selected = "";
                                    if ($tz == $xmlArr['site']['timezone']) {
                                        $selected = "selected";
                                    }
                                    echo "<option value='$tz' $selected>$tz</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="langs" class="col-lg-4 control-label">Available Langs</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="langs" name="langs" placeholder="langs"
                                   value="<?php echo $xmlArr['site']['langs']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="default_lang" class="col-lg-4 control-label">Default Lang</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="default_lang" name="default_lang"
                                   placeholder="default_lang" value="<?php echo $xmlArr['site']['default_lang']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="learning_mode" class="col-lg-4 control-label">Learning Mode</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['site']['learning_mode'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="learning_mode" name="learning_mode" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="Administrator">
                    <h2>Administrator</h2>
                    <label>Administrator Configuration</label>
                    <div class="form-group">
                        <label for="email" class="col-lg-4 control-label">E-mail</label>
                        <div class="col-lg-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="info@info.com"
                                   value="<?php echo $xmlArr['admin']['email']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailhost" class="col-lg-4 control-label">E-mail host</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="emailhost" name="emailhost"
                                   placeholder="smtp.example.com" value="<?php echo $xmlArr['admin']['emailhost']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emaillogin" class="col-lg-4 control-label">E-mail Login</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="emaillogin" name="emaillogin"
                                   placeholder="info@info.com" value="<?php echo $xmlArr['admin']['emaillogin']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailpass" class="col-lg-4 control-label">E-mail Password</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="emailpass" name="emailpass" placeholder="qwerty"
                                   value="<?php echo $xmlArr['admin']['emailpass']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailsa" class="col-lg-4 control-label">SMTP Autentification</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['admin']['emailsa'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="emailsa" name="emailsa" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailsp" class="col-lg-4 control-label">SMTP Port</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="emailsp" name="emailsp" placeholder="25"
                                   value="<?php echo $xmlArr['admin']['emailsp']; ?>">
                        </div>
                    </div>
                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="System">
                    <h2>System</h2>
                    <label>System Configuration</label>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Version</label>
                        <div class="col-lg-8">
                            <label class="form-control"><?php echo $xmlArr['xand']['version']; ?></label>
                            <input type="hidden" class="form-control" id="version" name="version"
                                   value="<?php echo $xmlArr['xand']['version']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="domain" class="col-lg-4 control-label">Domain</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="domain" name="domain" placeholder="example.com"
                                   value="<?php echo $_SERVER['SERVER_NAME'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Default Module</label>
                        <div class="col-lg-8">
                            <label class="form-control"><?php echo $xmlArr['xand']['default_module']; ?></label>
                            <input type="hidden" class="form-control" id="default_module" name="default_module"
                                   value="<?php echo $xmlArr['xand']['default_module']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="salt" class="col-lg-4 control-label">Salt</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="salt" name="salt" placeholder="22GdfglmklkTT"
                                   value="<?php echo $xmlArr['xand']['salt']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="authlog" class="col-lg-4 control-label">Loging Auth</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['xand']['authlog'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="authlog" name="authlog" <?php echo $checked; ?>>
                        </div>
                    </div>

                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="File_manager">
                    <h2>File Manager</h2>
                    <label>File Manager</label>
                    <div class="form-group">
                        <label for="language" class="col-lg-4 control-label">Language</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="language" name="language" placeholder="en"
                                   value="<?php echo $xmlArr['fm']['language']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="root_dir" class="col-lg-4 control-label">TPL Root folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="root_dir" name="root_dir"
                                   placeholder="/public/tpl/tpl-main/" value="<?php echo $xmlArr['fm']['root_dir']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tmp_file_path" class="col-lg-4 control-label">Temp folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="tmp_file_path" name="tmp_file_path"
                                   placeholder="/system/temp" value="<?php echo $xmlArr['fm']['tmp_file_path']; ?>">
                        </div>
                    </div>


                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="Paths">
                    <h2>Paths</h2>
                    <label>Don't change if you dont now what is this</label>
                    <div class="form-group">
                        <label for="site" class="col-lg-4 control-label">Site folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="site" name="site" placeholder="/"
                                   value="<?php echo $xmlArr['path']['site']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="public" class="col-lg-4 control-label">Public folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="public" name="public" placeholder="public/"
                                   value="<?php echo $xmlArr['path']['public']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="theme" class="col-lg-4 control-label">Theme folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="theme" name="theme" placeholder="tpl-main"
                                   value="<?php echo $xmlArr['path']['theme']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="core" class="col-lg-4 control-label">Lib folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="core" name="core" placeholder="lib"
                                   value="<?php echo $xmlArr['path']['core']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cache" class="col-lg-4 control-label">Cache folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="cache" name="cache" placeholder="system/cache"
                                   value="<?php echo $xmlArr['path']['cache']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="upload" class="col-lg-4 control-label">Upload folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="upload" name="upload" placeholder="uploads"
                                   value="<?php echo $xmlArr['path']['upload']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ccore" class="col-lg-4 control-label">Ccore folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="ccore" name="ccore" placeholder="system/ccore"
                                   value="<?php echo $xmlArr['path']['ccore']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ctpl" class="col-lg-4 control-label">Ctpl folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="ctpl" name="ctpl" placeholder="system/ctpl"
                                   value="<?php echo $xmlArr['path']['ctpl']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tpl" class="col-lg-4 control-label">Tpl folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="tpl" name="tpl" placeholder="tpl"
                                   value="<?php echo $xmlArr['path']['tpl']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modules" class="col-lg-4 control-label">Modules folder</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="modules" name="modules" placeholder="mod"
                                   value="<?php echo $xmlArr['path']['modules']; ?>">
                        </div>
                    </div>


                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="Images">
                    <h2>Images</h2>
                    <label>Images Configuration</label>
                    <div class="form-group">
                        <label for="quality" class="col-lg-4 control-label">Quality</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="quality" name="quality" placeholder="90"
                                   value="<?php echo $xmlArr['img']['quality']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="width" class="col-lg-4 control-label">Width</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="width" name="width" placeholder="800"
                                   value="<?php echo $xmlArr['img']['width']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="height" class="col-lg-4 control-label">Height</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="height" name="height" placeholder="600"
                                   value="<?php echo $xmlArr['img']['height']; ?>">
                        </div>
                    </div>


                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="File_cache">
                    <h2>File Cache</h2>
                    <label>File Cache avalible</label>
                    <div class="form-group">
                        <label for="compile_templates" class="col-lg-4 control-label">Compile Templates</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['fc']['compile_templates'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="compile_templates"
                                   name="compile_templates" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="Memory_cache">
                    <h2>Memory Cache</h2>
                    <label>MemCache Configuration</label>
                    <div class="form-group">
                        <label for="enable" class="col-lg-4 control-label">Enable Memcache</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['mc']['enable'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="enable" name="enable" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="port" class="col-lg-4 control-label">Port</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="port" name="port" placeholder="11211"
                                   value="<?php echo $xmlArr['mc']['port']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="timeout" class="col-lg-4 control-label">Timeout</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="timeout" name="timeout" placeholder="86400"
                                   value="<?php echo $xmlArr['mc']['timeout']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="server" class="col-lg-4 control-label">Server</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="server" name="server" placeholder="127.0.0.1"
                                   value="<?php echo $xmlArr['mc']['server']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="queries" class="col-lg-4 control-label">Queries</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['mc']['queries'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="queries" name="queries" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="templates" class="col-lg-4 control-label">Templates</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['mc']['templates'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="templates" name="templates" <?php echo $checked; ?>>
                        </div>
                    </div>

                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="PHP">
                    <h2>PHP</h2>
                    <label>Additional PHP options</label>
                    <div class="form-group">
                        <label for="apc.cache_by_default" class="col-lg-4 control-label">apc.cache_by_default</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="apc.cache_by_default"
                                   name="apc.cache_by_default" placeholder="-1"
                                   value="<?php echo $xmlArr['php']['apc.cache_by_default']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="display_errors" class="col-lg-4 control-label">display_errors</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="display_errors" name="display_errors"
                                   placeholder="true" value="<?php echo $xmlArr['php']['display_errors']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="error_reporting" class="col-lg-4 control-label">error_reporting</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="error_reporting" name="error_reporting"
                                   placeholder="E_ALL" value="<?php echo $xmlArr['php']['error_reporting']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="session.name" class="col-lg-4 control-label">session.name</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="session.name" name="session.name"
                                   placeholder="'SID'" value="<?php echo $xmlArr['php']['session.name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="session.gc_maxlifetime"
                               class="col-lg-4 control-label">session.gc_maxlifetime</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="session.gc_maxlifetime"
                                   name="session.gc_maxlifetime" placeholder="21600"
                                   value="<?php echo $xmlArr['php']['session.gc_maxlifetime']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="session.cookie_lifetime"
                               class="col-lg-4 control-label">session.cookie_lifetime</label>
                        <div class="col-lg-8">
                            <input type="number" class="form-control" id="session.cookie_lifetime"
                                   name="session.cookie_lifetime" placeholder="21600"
                                   value="<?php echo $xmlArr['php']['session.cookie_lifetime']; ?>">
                        </div>
                    </div>


                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="Develop">
                    <h2>Develop</h2>
                    <label>For Develop</label>
                    <div class="form-group">
                        <label for="debug_mode" class="col-lg-4 control-label">Debug Mode</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['dev']['debug_mode'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="debug_mode" name="debug_mode" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="db_log" class="col-lg-4 control-label">DB Log</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['dev']['db_log'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="db_log" name="db_log" <?php echo $checked; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="db_log_file" class="col-lg-4 control-label">DB Log In file</label>
                        <div class="col-lg-8 checkbox">
                            <?php
                            $checked = "";
                            if ($xmlArr['dev']['db_log_file'] == "1")
                                $checked = "checked";
                            ?>
                            <input type="checkbox" id="db_log_file" name="db_log_file" <?php echo $checked; ?>>
                        </div>
                    </div>


                    <p><a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next">Next</a></p>
                </fieldset>
                <fieldset id="MySql">
                    <h2>MySql</h2>
                    <label>MySql Manager</label>
                    <div class="form-group">
                        <label for="host" class="col-lg-4 control-label">Host</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="host" name="host" placeholder="localhost"
                                   value="<?php echo $xmlArr['db']['host']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nameDb" class="col-lg-4 control-label">Name DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="nameDb" name="nameDb" placeholder="xand"
                                   value="<?php echo $xmlArr['db']['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-lg-4 control-label">User DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="user" name="user" placeholder="root"
                                   value="<?php echo $xmlArr['db']['user']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-lg-4 control-label">Password DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="password" name="password" placeholder=""
                                   value="<?php echo $xmlArr['db']['password']; ?>">
                        </div>
                    </div>
                    <div class="has-error"></div>
                    <p>
                        <a class="btn btn-primary previous">Previous</a>
                        <a class="btn btn-primary next disabled">Next</a>
                        <a class="btn btn-primary checkdb">Check Database</a></p>
                </fieldset>
                <fieldset id="Update_Server">
                    <h2>Update Server</h2>
                    <label>Don't change if you dont now what is this</label>
                    <div class="form-group">
                        <label for="host" class="col-lg-4 control-label">Host</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="host" name="update.host" placeholder="localhost"
                                   value="<?php echo $xmlArr['db_update']['host']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-lg-4 control-label">Name DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="name" name="update.name" placeholder="xand"
                                   value="<?php echo $xmlArr['db_update']['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-lg-4 control-label">User DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="user" name="update.user" placeholder="root"
                                   value="<?php echo $xmlArr['db_update']['user']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-lg-4 control-label">Password DB</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="password" name="update.password" placeholder=""
                                   value="<?php echo $xmlArr['db_update']['password']; ?>">
                        </div>
                    </div>

                    <p><a class="btn btn-primary previous">Previous</a>
                        <input class="btn btn-success" type="submit" value="submit"></p>
                </fieldset>

            </form>
        </div>
    </div>

    </body>
    </html>


<?
function _getXml()
{
    $fileXml = PATH_REAL . "/conf.xml.example";
    if (!file_exists($fileXml)) {
        exit("conf file not found");
    }
    $xml = simplexml_load_file($fileXml);
    return $xml;

}