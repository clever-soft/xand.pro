<?php
/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 21.04.2017
 * Time: 11:45
 */
define("INSTALL_PATH_REAL", substr(dirname(__FILE__), 0, strlen(dirname(__FILE__)) - 15));

if (empty($_REQUEST)) {
    exit("Nothing to do");
}
foreach ($_REQUEST as &$r) {
    if ($r == 'on') {
        $r = true;
    }
}


$doc = new DOMDocument();
$xml_site = $doc->createElement('site');
/**
 * Site
 */
$xml_group = $doc->createElement("group");

$xml_site_name = $doc->createElement("name");
$xml_site_name->nodeValue = "Site";
$xml_site_prefix = $doc->createElement("prefix");
$xml_site_prefix->nodeValue = "site";
$xml_site_setting = $doc->createElement("setting");

$xml_site_setting_name = $doc->createElement("name");
$xml_site_setting_name->nodeValue = $_REQUEST["name"];
$xml_site_setting_timezone = $doc->createElement("timezone");
$xml_site_setting_timezone->nodeValue = $_REQUEST["timezone"][0];
$xml_site_setting_langs = $doc->createElement("langs");
$xml_site_setting_langs->nodeValue = $_REQUEST["langs"];
$xml_site_setting_default_lang = $doc->createElement("default_lang");
$xml_site_setting_default_lang->nodeValue = $_REQUEST["default_lang"];
$xml_site_setting_learning_mode = $doc->createElement("learning_mode");
$xml_site_setting_learning_mode_attr = $doc->createAttribute('type');
$xml_site_setting_learning_mode_attr->value = 'boolean';
$xml_site_setting_learning_mode->appendChild($xml_site_setting_learning_mode_attr);
$xml_site_setting_learning_mode->nodeValue = $_REQUEST["learning_mode"];

$xml_site_setting->appendChild($xml_site_setting_name);
$xml_site_setting->appendChild($xml_site_setting_timezone);
$xml_site_setting->appendChild($xml_site_setting_langs);
$xml_site_setting->appendChild($xml_site_setting_default_lang);
$xml_site_setting->appendChild($xml_site_setting_learning_mode);

$xml_group->appendChild($xml_site_name);
$xml_group->appendChild($xml_site_prefix);
$xml_group->appendChild($xml_site_setting);

$xml_site->appendChild($xml_group);
/**
 * Admin
 */
$xml_group_admin = $doc->createElement("group");

$xml_admin_name = $doc->createElement("name");
$xml_admin_name->nodeValue = "Administrator";
$xml_admin_prefix = $doc->createElement("prefix");
$xml_admin_prefix->nodeValue = "admin";
$xml_admin_setting = $doc->createElement("setting");

$xml_admin_setting_email = $doc->createElement("email");
$xml_admin_setting_email->nodeValue = $_REQUEST["email"];
$xml_admin_setting_emailhost = $doc->createElement("emailhost");
$xml_admin_setting_emailhost->nodeValue = $_REQUEST["emailhost"];
$xml_admin_setting_emaillogin = $doc->createElement("emaillogin");
$xml_admin_setting_emaillogin->nodeValue = $_REQUEST["emaillogin"];
$xml_admin_setting_emailpass = $doc->createElement("emailpass");
$xml_admin_setting_emailpass->nodeValue = $_REQUEST["emailpass"];
$xml_admin_setting_emailsa = $doc->createElement("emailsa");
$xml_admin_setting_emailsa_attr = $doc->createAttribute('type');
$xml_admin_setting_emailsa_attr->value = 'number';
$xml_admin_setting_emailsa->appendChild($xml_admin_setting_emailsa_attr);
$xml_admin_setting_emailsa->nodeValue = (isset($_REQUEST["emailsa"])) ? 1 : 0;
$xml_admin_setting_emailsp = $doc->createElement("emailsp");
$xml_admin_setting_emailsp_attr = $doc->createAttribute('type');
$xml_admin_setting_emailsp_attr->value = 'number';
$xml_admin_setting_emailsp->appendChild($xml_admin_setting_emailsp_attr);
$xml_admin_setting_emailsp->nodeValue = $_REQUEST["emailsp"];

$xml_admin_setting->appendChild($xml_admin_setting_email);
$xml_admin_setting->appendChild($xml_admin_setting_emailhost);
$xml_admin_setting->appendChild($xml_admin_setting_emaillogin);
$xml_admin_setting->appendChild($xml_admin_setting_emailpass);
$xml_admin_setting->appendChild($xml_admin_setting_emailsa);
$xml_admin_setting->appendChild($xml_admin_setting_emailsp);

$xml_group_admin->appendChild($xml_admin_name);
$xml_group_admin->appendChild($xml_admin_prefix);
$xml_group_admin->appendChild($xml_admin_setting);

$xml_site->appendChild($xml_group_admin);
/**
 * System
 */
$xml_group_sys = $doc->createElement("group");

$xml_sys_name = $doc->createElement("name");
$xml_sys_name->nodeValue = "System";
$xml_sys_prefix = $doc->createElement("prefix");
$xml_sys_prefix->nodeValue = "xand";
$xml_sys_setting = $doc->createElement("setting");

$xml_sys_setting_version = $doc->createElement("version");
$xml_sys_setting_version->nodeValue = $_REQUEST["version"];
$xml_sys_setting_domain = $doc->createElement("domain");
$xml_sys_setting_domain->nodeValue = $_REQUEST["domain"];
$xml_sys_setting_default_module = $doc->createElement("default_module");
$xml_sys_setting_default_module->nodeValue = $_REQUEST["default_module"];
$xml_sys_setting_salt = $doc->createElement("salt");
$xml_sys_setting_salt->nodeValue = $_REQUEST["salt"];
$xml_sys_setting_authlog = $doc->createElement("authlog");
$xml_sys_setting_authlog_attr = $doc->createAttribute('type');
$xml_sys_setting_authlog_attr->value = 'boolean';
$xml_sys_setting_authlog->appendChild($xml_sys_setting_authlog_attr);
$xml_sys_setting_authlog->nodeValue = $_REQUEST["authlog"];

$xml_sys_setting->appendChild($xml_sys_setting_version);
$xml_sys_setting->appendChild($xml_sys_setting_domain);
$xml_sys_setting->appendChild($xml_sys_setting_default_module);
$xml_sys_setting->appendChild($xml_sys_setting_salt);
$xml_sys_setting->appendChild($xml_sys_setting_authlog);

$xml_group_sys->appendChild($xml_sys_name);
$xml_group_sys->appendChild($xml_sys_prefix);
$xml_group_sys->appendChild($xml_sys_setting);

$xml_site->appendChild($xml_group_sys);
/**
 * File manager
 */
$xml_group_fm = $doc->createElement("group");

$xml_fm_name = $doc->createElement("name");
$xml_fm_name->nodeValue = "File manager";
$xml_fm_prefix = $doc->createElement("prefix");
$xml_fm_prefix->nodeValue = "fm";
$xml_fm_setting = $doc->createElement("setting");

$xml_fm_setting_language = $doc->createElement("language");
$xml_fm_setting_language->nodeValue = $_REQUEST["language"];
$xml_fm_setting_root_dir = $doc->createElement("root_dir");
$xml_fm_setting_root_dir->nodeValue = $_REQUEST["root_dir"];
$xml_fm_setting_tmp_file_path = $doc->createElement("tmp_file_path");
$xml_fm_setting_tmp_file_path->nodeValue = $_REQUEST["tmp_file_path"];

$xml_fm_setting->appendChild($xml_fm_setting_language);
$xml_fm_setting->appendChild($xml_fm_setting_root_dir);
$xml_fm_setting->appendChild($xml_fm_setting_tmp_file_path);

$xml_group_fm->appendChild($xml_fm_name);
$xml_group_fm->appendChild($xml_fm_prefix);
$xml_group_fm->appendChild($xml_fm_setting);

$xml_site->appendChild($xml_group_fm);
/**
 * Paths
 */
$xml_group_path = $doc->createElement("group");

$xml_path_name = $doc->createElement("name");
$xml_path_name->nodeValue = "Paths";
$xml_path_prefix = $doc->createElement("prefix");
$xml_path_prefix->nodeValue = "path";
$xml_path_setting = $doc->createElement("setting");

$xml_path_setting_site = $doc->createElement("site");
$xml_path_setting_site->nodeValue = $_REQUEST["site"];
$xml_path_setting_public = $doc->createElement("public");
$xml_path_setting_public->nodeValue = $_REQUEST["public"];
$xml_path_setting_theme = $doc->createElement("theme");
$xml_path_setting_theme->nodeValue = $_REQUEST["theme"];
$xml_path_setting_core = $doc->createElement("core");
$xml_path_setting_core->nodeValue = $_REQUEST["core"];
$xml_path_setting_cache = $doc->createElement("cache");
$xml_path_setting_cache->nodeValue = $_REQUEST["cache"];
$xml_path_setting_upload = $doc->createElement("upload");
$xml_path_setting_upload->nodeValue = $_REQUEST["upload"];
$xml_path_setting_ccore = $doc->createElement("ccore");
$xml_path_setting_ccore->nodeValue = $_REQUEST["ccore"];
$xml_path_setting_ctpl = $doc->createElement("ctpl");
$xml_path_setting_ctpl->nodeValue = $_REQUEST["ctpl"];
$xml_path_setting_tpl = $doc->createElement("tpl");
$xml_path_setting_tpl->nodeValue = $_REQUEST["tpl"];
$xml_path_setting_modules = $doc->createElement("modules");
$xml_path_setting_modules->nodeValue = $_REQUEST["modules"];

$xml_path_setting->appendChild($xml_path_setting_site);
$xml_path_setting->appendChild($xml_path_setting_public);
$xml_path_setting->appendChild($xml_path_setting_theme);
$xml_path_setting->appendChild($xml_path_setting_core);
$xml_path_setting->appendChild($xml_path_setting_cache);
$xml_path_setting->appendChild($xml_path_setting_upload);
$xml_path_setting->appendChild($xml_path_setting_ccore);
$xml_path_setting->appendChild($xml_path_setting_ctpl);
$xml_path_setting->appendChild($xml_path_setting_tpl);
$xml_path_setting->appendChild($xml_path_setting_modules);

$xml_group_path->appendChild($xml_path_name);
$xml_group_path->appendChild($xml_path_prefix);
$xml_group_path->appendChild($xml_path_setting);

$xml_site->appendChild($xml_group_path);
/**
 * Images
 */
$xml_group_img = $doc->createElement("group");

$xml_img_name = $doc->createElement("name");
$xml_img_name->nodeValue = "Images";
$xml_img_prefix = $doc->createElement("prefix");
$xml_img_prefix->nodeValue = "img";
$xml_img_setting = $doc->createElement("setting");

$xml_img_setting_quality = $doc->createElement("quality");
$xml_img_setting_quality_attr = $doc->createAttribute('type');
$xml_img_setting_quality_attr->value = 'number';
$xml_img_setting_quality->appendChild($xml_img_setting_quality_attr);
$xml_img_setting_quality->nodeValue = $_REQUEST["quality"];
$xml_img_setting_width = $doc->createElement("width");
$xml_img_setting_width_attr = $doc->createAttribute('type');
$xml_img_setting_width_attr->value = 'number';
$xml_img_setting_width->appendChild($xml_img_setting_width_attr);
$xml_img_setting_width->nodeValue = $_REQUEST["width"];
$xml_img_setting_height = $doc->createElement("height");
$xml_img_setting_height_attr = $doc->createAttribute('type');
$xml_img_setting_height_attr->value = 'number';
$xml_img_setting_height->appendChild($xml_img_setting_height_attr);
$xml_img_setting_height->nodeValue = $_REQUEST["height"];

$xml_img_setting->appendChild($xml_img_setting_quality);
$xml_img_setting->appendChild($xml_img_setting_width);
$xml_img_setting->appendChild($xml_img_setting_height);

$xml_group_img->appendChild($xml_img_name);
$xml_group_img->appendChild($xml_img_prefix);
$xml_group_img->appendChild($xml_img_setting);

$xml_site->appendChild($xml_group_img);
/**
 * File cache
 */
$xml_group_fc = $doc->createElement("group");

$xml_fc_name = $doc->createElement("name");
$xml_fc_name->nodeValue = "File cache";
$xml_fc_prefix = $doc->createElement("prefix");
$xml_fc_prefix->nodeValue = "fc";
$xml_fc_setting = $doc->createElement("setting");

$xml_fc_setting_compile_templates = $doc->createElement("compile_templates");
$xml_fc_setting_compile_templates_attr = $doc->createAttribute('type');
$xml_fc_setting_compile_templates_attr->value = 'boolean';
$xml_fc_setting_compile_templates->appendChild($xml_fc_setting_compile_templates_attr);
$xml_fc_setting_compile_templates->nodeValue = (isset($_REQUEST["compile_templates"])) ? 1 : 0;

$xml_fc_setting->appendChild($xml_fc_setting_compile_templates);

$xml_group_fc->appendChild($xml_fc_name);
$xml_group_fc->appendChild($xml_fc_prefix);
$xml_group_fc->appendChild($xml_fc_setting);

$xml_site->appendChild($xml_group_fc);
/**
 * Memory cache
 */
$xml_group_mc = $doc->createElement("group");

$xml_mc_name = $doc->createElement("name");
$xml_mc_name->nodeValue = "Memory cache";
$xml_mc_prefix = $doc->createElement("prefix");
$xml_mc_prefix->nodeValue = "mc";
$xml_mc_setting = $doc->createElement("setting");

$xml_mc_setting_enable = $doc->createElement("enable");
$xml_mc_setting_enable_attr = $doc->createAttribute('type');
$xml_mc_setting_enable_attr->value = 'boolean';
$xml_mc_setting_enable->appendChild($xml_mc_setting_enable_attr);
$xml_mc_setting_enable->nodeValue = (isset($_REQUEST["enable"])) ? 1 : 0;
$xml_mc_setting_port = $doc->createElement("port");
$xml_mc_setting_port_attr = $doc->createAttribute('type');
$xml_mc_setting_port_attr->value = 'number';
$xml_mc_setting_port->appendChild($xml_mc_setting_port_attr);
$xml_mc_setting_port->nodeValue = $_REQUEST["port"];
$xml_mc_setting_timeout = $doc->createElement("timeout");
$xml_mc_setting_timeout_attr = $doc->createAttribute('type');
$xml_mc_setting_timeout_attr->value = 'number';
$xml_mc_setting_timeout->appendChild($xml_mc_setting_timeout_attr);
$xml_mc_setting_timeout->nodeValue = $_REQUEST["timeout"];
$xml_mc_setting_server = $doc->createElement("server");
$xml_mc_setting_server->nodeValue = $_REQUEST["server"];
$xml_mc_setting_queries = $doc->createElement("queries");
$xml_mc_setting_queries_attr = $doc->createAttribute('type');
$xml_mc_setting_queries_attr->value = 'boolean';
$xml_mc_setting_queries->appendChild($xml_mc_setting_queries_attr);
$xml_mc_setting_queries->nodeValue = $_REQUEST["queries"];
$xml_mc_setting_templates = $doc->createElement("templates");
$xml_mc_setting_templates_attr = $doc->createAttribute('type');
$xml_mc_setting_templates_attr->value = 'boolean';
$xml_mc_setting_templates->appendChild($xml_mc_setting_templates_attr);
$xml_mc_setting_templates->nodeValue = $_REQUEST["templates"];

$xml_mc_setting->appendChild($xml_mc_setting_enable);
$xml_mc_setting->appendChild($xml_mc_setting_port);
$xml_mc_setting->appendChild($xml_mc_setting_timeout);
$xml_mc_setting->appendChild($xml_mc_setting_server);
$xml_mc_setting->appendChild($xml_mc_setting_queries);
$xml_mc_setting->appendChild($xml_mc_setting_templates);

$xml_group_mc->appendChild($xml_mc_name);
$xml_group_mc->appendChild($xml_mc_prefix);
$xml_group_mc->appendChild($xml_mc_setting);

$xml_site->appendChild($xml_group_mc);
/**
 * PHP
 */
$xml_group_php = $doc->createElement("group");

$xml_php_name = $doc->createElement("name");
$xml_php_name->nodeValue = "PHP";
$xml_php_prefix = $doc->createElement("prefix");
$xml_php_prefix->nodeValue = "php";
$xml_php_method = $doc->createElement("method");
$xml_php_method->nodeValue = "ini_set";
$xml_php_setting = $doc->createElement("setting");

$xml_php_setting_apc = $doc->createElement("apc.cache_by_default");
$xml_php_setting_apc_attr = $doc->createAttribute('type');
$xml_php_setting_apc_attr->value = 'number';
$xml_php_setting_apc->appendChild($xml_php_setting_apc_attr);
$xml_php_setting_apc->nodeValue = $_REQUEST["apc_cache_by_default"];
$xml_php_setting_display_errors = $doc->createElement("display_errors");
$xml_php_setting_display_errors->nodeValue = $_REQUEST["display_errors"];
$xml_php_setting_error_reporting = $doc->createElement("error_reporting");
$xml_php_setting_error_reporting->nodeValue = $_REQUEST["error_reporting"];
$xml_php_setting_session_name = $doc->createElement("session.name");
$xml_php_setting_session_name->nodeValue = "\"" . $_REQUEST["session_name"] . "\"";
$xml_php_setting_session_gc_maxlifetime = $doc->createElement("session.gc_maxlifetime");
$xml_php_setting_session_gc_maxlifetime_attr = $doc->createAttribute('type');
$xml_php_setting_session_gc_maxlifetime_attr->value = 'number';
$xml_php_setting_session_gc_maxlifetime->appendChild($xml_php_setting_session_gc_maxlifetime_attr);
$xml_php_setting_session_gc_maxlifetime->nodeValue = $_REQUEST["session_gc_maxlifetime"];
$xml_php_setting_session_cookie_lifetime = $doc->createElement("session.cookie_lifetime");
$xml_php_setting_session_cookie_lifetime_attr = $doc->createAttribute('type');
$xml_php_setting_session_cookie_lifetime_attr->value = 'number';
$xml_php_setting_session_cookie_lifetime->appendChild($xml_php_setting_session_cookie_lifetime_attr);
$xml_php_setting_session_cookie_lifetime->nodeValue = $_REQUEST["session_cookie_lifetime"];

$xml_php_setting->appendChild($xml_php_setting_apc);
$xml_php_setting->appendChild($xml_php_setting_display_errors);
$xml_php_setting->appendChild($xml_php_setting_error_reporting);
$xml_php_setting->appendChild($xml_php_setting_session_name);
$xml_php_setting->appendChild($xml_php_setting_session_gc_maxlifetime);
$xml_php_setting->appendChild($xml_php_setting_session_cookie_lifetime);

$xml_group_php->appendChild($xml_php_name);
$xml_group_php->appendChild($xml_php_prefix);
$xml_group_php->appendChild($xml_php_method);
$xml_group_php->appendChild($xml_php_setting);

$xml_site->appendChild($xml_group_php);
/**
 * Develop
 */
$xml_group_dev = $doc->createElement("group");

$xml_dev_name = $doc->createElement("name");
$xml_dev_name->nodeValue = "Develop";
$xml_dev_prefix = $doc->createElement("prefix");
$xml_dev_prefix->nodeValue = "dev";
$xml_dev_setting = $doc->createElement("setting");

$xml_dev_setting_debug_mode = $doc->createElement("debug_mode");
$xml_dev_setting_debug_mode_attr = $doc->createAttribute('type');
$xml_dev_setting_debug_mode_attr->value = 'boolean';
$xml_dev_setting_debug_mode->appendChild($xml_dev_setting_debug_mode_attr);
$xml_dev_setting_debug_mode->nodeValue = (isset($_REQUEST["debug_mode"])) ? 1 : 0;
$xml_dev_setting_db_log = $doc->createElement("db_log");
$xml_dev_setting_db_log_attr = $doc->createAttribute('type');
$xml_dev_setting_db_log_attr->value = 'boolean';
$xml_dev_setting_db_log->appendChild($xml_dev_setting_db_log_attr);
$xml_dev_setting_db_log->nodeValue = (isset($_REQUEST["db_log"])) ? 1 : 0;
$xml_dev_setting_db_log_file = $doc->createElement("db_log_file");
$xml_dev_setting_db_log_file_attr = $doc->createAttribute('type');
$xml_dev_setting_db_log_file_attr->value = 'boolean';
$xml_dev_setting_db_log_file->appendChild($xml_dev_setting_db_log_file_attr);
$xml_dev_setting_db_log_file->nodeValue = (isset($_REQUEST["db_log_file"])) ? 1 : 0;

$xml_dev_setting->appendChild($xml_dev_setting_debug_mode);
$xml_dev_setting->appendChild($xml_dev_setting_db_log);
$xml_dev_setting->appendChild($xml_dev_setting_db_log_file);

$xml_group_dev->appendChild($xml_dev_name);
$xml_group_dev->appendChild($xml_dev_prefix);
$xml_group_dev->appendChild($xml_dev_setting);

$xml_site->appendChild($xml_group_dev);
/**
 * MySql local
 */

$xml_group_db = $doc->createElement("group");

$xml_db_name = $doc->createElement("name");
$xml_db_name->nodeValue = "MySql local";
$xml_db_prefix = $doc->createElement("prefix");
$xml_db_prefix->nodeValue = "db";
$xml_db_method = $doc->createElement("method");
$xml_db_method->nodeValue = "db";
$xml_db_setting = $doc->createElement("setting");

$xml_db_setting_host = $doc->createElement("host");
$xml_db_setting_host->nodeValue = $_REQUEST["host"];
$xml_db_setting_name = $doc->createElement("name");
$xml_db_setting_name->nodeValue = $_REQUEST["nameDb"];
$xml_db_setting_user = $doc->createElement("user");
$xml_db_setting_user->nodeValue = $_REQUEST["user"];
$xml_db_setting_password = $doc->createElement("password");
$xml_db_setting_password->nodeValue = $_REQUEST["password"];

$xml_db_setting->appendChild($xml_db_setting_host);
$xml_db_setting->appendChild($xml_db_setting_name);
$xml_db_setting->appendChild($xml_db_setting_user);
$xml_db_setting->appendChild($xml_db_setting_password);

$xml_group_db->appendChild($xml_db_name);
$xml_group_db->appendChild($xml_db_prefix);
$xml_group_db->appendChild($xml_db_method);
$xml_group_db->appendChild($xml_db_setting);

$xml_site->appendChild($xml_group_db);
/**
 * Update Server
 */
$xml_group_db_update = $doc->createElement("group");

$xml_db_update_name = $doc->createElement("name");
$xml_db_update_name->nodeValue = "Update Server";
$xml_db_update_prefix = $doc->createElement("prefix");
$xml_db_update_prefix->nodeValue = "db_update";
$xml_db_update_method = $doc->createElement("method");
$xml_db_update_method->nodeValue = "db";
$xml_db_update_setting = $doc->createElement("setting");

$xml_db_update_setting_host = $doc->createElement("host");
$xml_db_update_setting_host->nodeValue = $_REQUEST["update_host"];
$xml_db_update_setting_name = $doc->createElement("name");
$xml_db_update_setting_name->nodeValue = $_REQUEST["update_name"];
$xml_db_update_setting_user = $doc->createElement("user");
$xml_db_update_setting_user->nodeValue = $_REQUEST["update_user"];
$xml_db_update_setting_password = $doc->createElement("password");
$xml_db_update_setting_password->nodeValue = $_REQUEST["update_password"];

$xml_db_update_setting->appendChild($xml_db_update_setting_host);
$xml_db_update_setting->appendChild($xml_db_update_setting_name);
$xml_db_update_setting->appendChild($xml_db_update_setting_user);
$xml_db_update_setting->appendChild($xml_db_update_setting_password);

$xml_group_db_update->appendChild($xml_db_update_name);
$xml_group_db_update->appendChild($xml_db_update_prefix);
$xml_group_db_update->appendChild($xml_db_update_method);
$xml_group_db_update->appendChild($xml_db_update_setting);

$xml_site->appendChild($xml_group_db_update);

$doc->appendChild($xml_site);
$doc->save(INSTALL_PATH_REAL . "/conf.xml");

require_once(INSTALL_PATH_REAL . "/" . $_REQUEST["core"] . "/" . "class.Xand.php");


$install = new Update();
$install->actionUpdateMysql(false, true, $_REQUEST["update_name"]);
SDb::query("INSERT INTO `cms_textpages` (`idPage`, `idParent`, `npp`, `pathNpp`, `level`, `path`, `tpl`, `tplFolder`, `url`, `h1`, `h2`, `title`, `keywords`, `description`, `content`, `menuClass`, `menuIcon`, `menuTitle`, `menuDescription`, `javaScript`, `hashTag`, `isMenu`, `isActive`, `isSystem`, `isFooter`, `isFooterExtra`, `dateCreate`, `dateUpdate`) VALUES
	(4, 0, 1, NULL, 0, '/4/', 'intro', 'cms', 'home', 'Home', 'Home', 'Home', 'Home', 'Home', '<br>', '', '', 'Главная', 'Home', '', '0', 0, 1, 0, 1, 1, 1451339201, 1491761162);");
SDb::query("INSERT INTO `cms_textpages_i18n` (`id`, `idPage`, `lang`, `title`, `menuTitle`, `menuDescription`, `h1`, `h2`, `keywords`, `description`, `content`) VALUES
	(92, 4, 'ru', 'Home', 'Главная', 'Home', 'Home', 'Home', 'Home', 'Home', '<br>'),
	(93, 4, 'en', 'Home', 'Home', 'Home', 'Home', 'Home', 'Home', 'Home', '');");
SDb::query("INSERT INTO `users` (`idUser`, `firstname`, `lastname`, `email`, `username`, `password`, `attemptsLogin`, `isActive`, `isSu`, `settings`, `dateLogin`, `department`, `test3`) VALUES
	(1, '', '', '', 'admin', 'af4aaf2c3fd9ce6ef825c86d563f6f8c:d2', 0, 1, 1, NULL, NULL, NULL, NULL);");
header("Location: http://" . $_REQUEST["domain"] . "/");