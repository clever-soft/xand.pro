<?php
/**
 * Created by PhpStorm.
 * User: Bond
 * Date: 21.04.2017
 * Time: 11:15
 */


$error['error'] = false;
$link = mysqli_connect($_REQUEST['host'], $_REQUEST['user'], $_REQUEST['password']);
if (!$link) {
    $error['error'] = true;
    $error['reason'] = 'Not connected : ' . mysqli_error($link);
} else {

// make foo the current db
    $db_selected = mysqli_select_db($link, $_REQUEST['name']);
    if (!$db_selected) {
        $error['error'] = true;
        $error['reason'] = 'Cannot use foo : ' . mysqli_error($link);
    }
}

echo json_encode($error);