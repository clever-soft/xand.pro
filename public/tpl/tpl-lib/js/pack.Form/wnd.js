function CWnd() {
    this.Path = "";
    this.Controller = "";
    this.afterLoad = null;

    //---
    this.Message = function (text, type) {
        $("body").append("<div id='mGroup'></div>");
        $("#mGroup").html("<div id='mOverlay'></div>");
        switch (type) {
            case "info":
                $("body").append("<div class='mMessage mMessageInfo'><strong>Информация.</strong> " + text + "</div>");
                break;
            case "error":
                $("body").append("<div class='mMessage mMessageError'><strong>Произошла ошибка.</strong> " + text + "</div>");
                break;
            case "warning":
                $("body").append("<div class='mMessage mMessageWarning'><strong>Внимание.</strong> " + text + "</div>");
                break;
        }
    };

    //---
    this.Image = function (src) {
        $("#adminSlider").css("-webkit-filter", "blur(1px) grayscale(90%)");

        $("body").append("<div id='mGroup'></div>");
        $("#mGroup").html("<div id='mOverlay' onClick='Wnd.Close();'></div>").append("<div id='mWrapBorder' style='padding: 5px'></div>");
        $("#mWrapBorder").html("<div id='mWrap'></div>");
        $("#mWrap").html("<img style='max-width:900px' src='" + src + "' />");
        this.Align();

    };

    //---
    this.Create = function (data) {
        $("#adminSlider").css("-webkit-filter", "blur(1px) grayscale(90%)");

        $("body").append("<div id='mGroup'></div>");
        $("#mGroup").html("<div id='mOverlay' onClick='Wnd.Close();'></div>").append("<div id='mWrapBorder'></div>");
        $("#mWrapBorder").html("<div id='mWrap'></div>");
        $("#mWrapBorder").append("<div id='mWrap2'></div>");
        $("#mWrap").html(data);

        this.Align();
    };

    //---
    this.CreateSecond = function (data) {
        $("#mWrap").hide(0);
        $("#mWrap2").html(data).show(0);

        this.Align();
    };

    //---
    this.Loader = function (status) {
        if (status) {
            $("body").append("<div id='mGroup'></div>");
            $("#mGroup").append("<div id='mLoader'></div>");
        }
        else {
            Wnd.Close();
        }
    };

    //---
    this.Align = function () {
        var h = parseInt($("#mWrapBorder").css("height"));
        h = Math.ceil(h / 2);
        var w = parseInt($("#mWrapBorder").css("width"));
        w = Math.ceil(w / 2);

        var top = $(document).scrollTop() + (parseInt($(window).innerHeight()) / 2) - 20 - h;
        top = (top < 0) ? 0 : parseInt(top);

        $("#mWrapBorder").css("top", "" + top + "px")
            .css("margin-left", "-" + w + "px")
            .css("display", "block");
    };

    //---
    this.Load = function (Action, Data, Path, Controller) {

        if (!Path)
            Path = document.location.hash.replace(/#/g, "");

        if (!Controller)
            Controller = this.Controller;
        else
            this.Controller = Controller;

        this.Path = encodeURIComponent(Path);

        $("#mWrapBorder").css("display", "block");
        $.ajax({
            type: "POST",
            url: Controller,
            cache: true,
            data: "PATH=" + this.Path + "&ACT=" + Action + "&DATA=" + Data,
            beforeSend: function (html) {
                Wnd.Loader(true);
            },
            error: function () {
                console.log("Request Error!");
            },
            success: function (html) {
                Wnd.Loader(false);
                Wnd.Create(html);

                if (Wnd.afterLoad !== null) {
                    Wnd.afterLoad();
                    Wnd.afterLoad = null;
                }

                /* >>> callback on end >>> */

                if ($('.inputDate')) {
                    $('.inputDate').datetimepicker({
                        dateFormat: 'dd.mm.yy'
                    });
                }

            }
        });


    };

    //---
    this.LoadSecond = function (Action, Data) {

        $.ajax({
            type: "POST",
            url: this.Controller,
            cache: true,
            data: "PATH=" + this.Path + "&ACT=" + Action + "&DATA=" + Data,
            beforeSend: function (html) {
            },
            error: function () {
                console.log("Request Error!");
            },
            success: function (html) {
                Wnd.CreateSecond(html);
            }
        });


    };

    //---
    this.Close = function () {
        $("#adminSlider").css("-webkit-filter", "none");
        $("#mGroup").remove();
    };

    //---
    this.CloseSecond = function () {
        $("#mWrap2").hide(0);
        $("#mWrap").show(0);
        this.Align();
    }


}

Wnd = new CWnd;

$(document).keydown(function (e) {
    if (e.keyCode == 27) {
        Wnd.Close();
    }
});
//----
function MethodModal(id, type, z) {
    Wnd.Controller = '/mod/' + z;
    Wnd.Load(type, id);
}

//----