//----------------------------------------------------------------------
//- pack.Form
//----------------------------------------------------------------------
tabOnShow = [];

function GrouperTabHeadClick(dom) {
    var tab = $(dom);

    $(".grouperTabTitle").removeClass("grouperTabTitleActive").addClass("grouperTabTitleNoactive");
    tab.removeClass("grouperTabTitleNoactive").addClass("grouperTabTitleActive");
    $(".grouperTabContent").css("display", "none");
    $(".grouperTabContent").eq(tab.index()).css("display", "block");

    if (tabOnShow[tab.index()] != null) {
        tabOnShow[tab.index()]();
        tabOnShow[tab.index()] = null;
    }

}

function FormEnterKeyDispatch(domId) {
    $('#' + domId).find("input").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            $('#' + domId).find("button.btn-primary").click();

        }
    });
}


//-----
function CaptchaRefresh(dom) {
    var img = $(dom).parent().prev().find("img");
    img.attr('src', img.attr('src') + '?' + Math.floor(Math.random() * 10000));

}

//-----
function FileManager(dom) {
    window.document.open('/tools/filemanager', '', 'top=100, left=100, width=1000, height=700, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=0');
}

//-----
function CreateWindow(url) {
    window.document.open(url, url, 'top=100, left=100, width=1000, height=700, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=0');
}


//---
function CheckboxClick(dom) {
    if ($(dom).hasClass('checkboxDisabled')) return;

    if ($(dom).hasClass('checkboxSel')) {
        $(dom).removeClass('checkboxSel').addClass('checkboxUnSel');
    }
    else {
        $(dom).removeClass('checkboxUnSel').addClass('checkboxSel');
    }
}


//---
function CheckboxAjaxClick(dom, aEid, action) {
    if ($(dom).hasClass('checkboxDisabled')) return;

    $(dom).addClass("checkboxLoading");

    var data;
    if ($(dom).hasClass('checkboxSel')) {
        $(dom).removeClass('checkboxSel').addClass('checkboxUnSel');
        data = 0;
    }
    else {
        $(dom).removeClass('checkboxUnSel').addClass('checkboxSel');
        data = 1;
    }

    aEid.Push(false, action, data, function () {

    }, function (data) {
        $(dom).removeClass("checkboxLoading");
    });

}

//---
function CheckboxGetData(domId) {
    return ( $('#' + domId).hasClass('checkboxSel')) ? 1 : 0;
}

//---
function GetSelectMultipleById(domId) {
    var arr = $("#" + domId).val();
    var str = "";

    if ($.isArray(arr))
        var str = arr.join();

    return str;
}

//---
function getContentById(domId) {
    var str = $("#" + domId).val();
    return ContentPrepare(str);
}

//---
function ContentPrepare(str) {

    str = str.replace(/\r\n|\r|\n/g, "#BR#");
    str = str.replace(/&/g, '#AM#');
    str = str.replace(/'/g, '#OK#');
    str = str.replace(/"/g, '#DK#');
    str = str.replace(/\+/g, '#PL#');
    str = str.replace(/&/g, "#AM#");
    str = str.replace(/\\/g, "#SL#");
    str = str.replace(/\//g, "#SLO#");

    return str;
}

//---
function InputUploadProccess(el_uid, callbackSuccess) {

    $('#form_' + el_uid).ajaxSubmit({

        beforeSubmit: function () {
            $('#' + el_uid + '_text').css("display", "none");
            $('#' + el_uid + '_loading').css("display", "block");
            $('#' + el_uid + '_callback').html('Loading...');
        },

        success: function (responseText) {

            $('#' + el_uid + '_text').css("display", "block");
            $('#' + el_uid + '_loading').css("display", "none");
            $('#' + el_uid + '_callback').html(responseText);
            if (callbackSuccess != null) callbackSuccess();
        }
    });

}


//---
function JCropInputUploadProccess(el_uid, callbackSuccess) {
    domIdVar = el_uid;

    $('#form_' + el_uid).ajaxSubmit({
        dataType: 'json',
        beforeSubmit: function () {
            $('#' + domIdVar + '_text').css("display", "none");
            $('#' + domIdVar + '_loading').css("display", "block");
            $('#' + domIdVar + '_callback').html('Loading...');
        },

        success: function (data) {

            $('#' + domIdVar + '_text').css("display", "block");
            $('#' + domIdVar + '_loading').css("display", "none");

            if (data.error) {
                $('#' + domIdVar + '_callback').html("Error: " + data.error);
            }
            else {
                $('#' + domIdVar + '_value').val(data.data);

                var tmpImg = new Image();
                tmpImg.src = data.preview;

                tmpImg.onload = function () {

                    $('#' + domIdVar + '_callback').html(tmpImg);
                    JCropInit(this, domIdVar, [0, 0, 200, 200]);

                };


            }

            Wnd.Align();
        }
    });

}


//---
function JCropInit(dom, domId, coord) {
    var attr = $(this).attr('load');

    console.log("JCropInit");

    domIdVar = domId;
    coordVar = coord;

    $(dom).Jcrop({
        minSize: [200, 200],
        aspectRatio: 1,
        onChange: function (c, domId) {
            $('#' + domIdVar + '_data').val(c.x + ',' + c.y + ',' + c.w + ',' + c.h);
        }
    }, function () {

        this.animateTo(coordVar);
    });
}

//---
function InputUploadProccessMass(el_uid, callbackSuccess) {

    $('#form_' + el_uid).ajaxSubmit({

        dataType: 'json',
        beforeSubmit: function () {
            $('#' + el_uid + '_text').css("display", "none");
            $('#' + el_uid + '_loading').css("display", "block");
            $('#' + el_uid + '_callback').html('Loading...');
        },

        success: function (data) {

            $('#' + el_uid + '_text').css("display", "block");
            $('#' + el_uid + '_loading').css("display", "none");

            if (data.error) {
                $('#' + el_uid + '_callback').html("Error: " + data.error);
            }
            else {
                var html = "";
                for (var i in data.preview)
                    html += "<img width='25px' style='margin: 1px;' src='" + data.preview[i] + "' />";

                $('#' + el_uid + '_callback').html(html);
                $('#' + el_uid + '_value').val(data.data.join());
            }

            Wnd.Align();

        }
    });

}


//---
function InputNumericPasswordZeroCutter(val) {

    var fs = val.substring(0, 1);
    if (fs == "0") val = val.substring(1, val.length);

    var fs = val.substring(0, 1);
    if (fs == "0") val = val.substring(1, val.length);

    var fs = val.substring(0, 1);
    if (fs == "0") val = val.substring(1, val.length);

    var fs = val.substring(0, 1);
    if (fs == "0") val = val.substring(1, val.length);

    return val;
}

//---
function InputNumericPasswordKeyUp(input) {

    var val = input.value;
    val = val.replace(/[^\d,]/g, '');

    input.value = InputNumericPasswordZeroCutter(val);
}

//---
function SwitchAction(dom) {

    if ($(dom).find('p').eq(0).hasClass('switchOff')) {
        $(dom).find('p').eq(0).removeClass('switchOff').addClass('switchOnGray');
        $(dom).find('p').eq(1).removeClass('switchOn').addClass('switchOff');
    }
    else {
        $(dom).find('p').eq(0).removeClass('switchOnGray').addClass('switchOff');
        $(dom).find('p').eq(1).removeClass('switchOff').addClass('switchOn');
    }
}

//---
function SwitchGetData(domId) {
    return ( $('#' + domId).find('p').eq(0).hasClass('switchOff')) ? 1 : 0;
}


//---
function ClearTextarea(dom) {
    $(dom).parent().parent().find("textarea.inputTextarea:not([readonly])").val("");
}

//---
function AjaxFormSubmit(el_uid) {
    var options = {
        beforeSubmit: function () {
            $('#' + el_uid + '_text').css("opacity", "0");
            $('.btn-uploader').addClass("inputUploadLoadingBg");
        },

        success: function (responseText) {
            $('#' + el_uid + '_text').css("opacity", "1");
            $('.btn-uploader').removeClass("inputUploadLoadingBg");

            var target = $("#form_" + el_uid).parent().parent().parent().find("textarea.inputTextarea:not([readonly])");
            target.val(target.val() + "\n" + responseText + "\n");
        }
    };

    $('#form_' + el_uid).ajaxSubmit(options);

}

//---
function AjaxAttachmentsFormSubmit(el_uid) {
    var options = {
        beforeSubmit: function () {
            $('#' + el_uid + '_text').css("opacity", "0");
            $('.btn-uploader').addClass("inputUploadLoadingBg");
        },

        success: function (responseText) {
            $('#' + el_uid + '_text').css("opacity", "1");
            $('.btn-uploader').removeClass("inputUploadLoadingBg");

            var target = $("#" + el_uid);

            var targetVal = target.val();
            targetVal = targetVal.trim();
            responseText = responseText.trim();
            if (targetVal != "")
                targetVal = targetVal + "," + responseText;
            else
                targetVal = responseText;

            target.val(targetVal);

            var targetDom = $("#" + el_uid + "_list");

            var htmlBlock = "<div class='attachmentsBodyRow'>";
            htmlBlock += "<div class='attachmentsBodyColumn'>";
            htmlBlock += "<a class='attachmentsLink' target='_blank' href='" + responseText + "'>" + responseText + "</a>";
            htmlBlock += "</div>";

            htmlBlock += "<div class='attachmentsBodyColumn'>";
            htmlBlock += "<a href='' onclick='AjaxAttachmentsRemove(this); return false;' title='Remove' class='fa fa-trash-o'></a>";
            htmlBlock += "</div>";
            htmlBlock += "</div>";

            targetDom.append(htmlBlock);

        }
    };

    $('#form_' + el_uid).ajaxSubmit(options);
}

//---
function AjaxAttachmentsRemove(dom) {

    var container = $(dom).parent().parent().parent();
    var containerId = container.attr("id");
    containerId = containerId.replace("_list", "");

    $(dom).parent().parent().remove();

    var target = $("#" + containerId);
    var result = "";

    container.find(".attachmentsLink").each(function (i) {
        if (result == "")
            result += $(this).attr("href");
        else
            result += "," + $(this).attr("href");
    });
    target.val(result);
}

//---
function ControlCheckall(dom) {

    if (!$(dom).hasClass('checkboxSel')) {
        $(dom).parent().next().find(".checkbox:not(.checkboxDisabled)").each(function (i) {

            $(this).removeClass("checkboxUnSel").addClass("checkboxSel");

        });
    }
    else {
        $(dom).parent().next().find(".checkbox:not(.checkboxDisabled)").each(function (i) {

            $(this).removeClass("checkboxSel").addClass("checkboxUnSel");

        });
    }

    CheckboxClick(dom);

}


//----
function CSIChange(dom, eid, action) {
    var data = $(dom).attr('value');

    $.ajax({
        type: 'POST',
        url: eid.Controller,
        data: 'DATA=' + data + "&ACT=" + action,
        success: function (html) {
            $("#csi" + action).html(html);

        }
    });

}

//----
function CSIUpdateValue(domId, value) {
    $("#" + domId).val(value);
}


function FormBaseCollector(domId, data) {

    $("#form"+domId).find(".virtualFormComponent").each(function () {
        var name   = $(this).attr("name");
        data[name] = $(this).val();
    });

    return data;

}


//----
function grouperClonesRemove(dom) {
    $(dom).parent().remove();
}


//----
function GrouperClonesClone(dom, defaultName) {
    var grouperClonesItems = $(dom).parent().find(".grouperClonesItems");
    var baseClone = grouperClonesItems.find(".grouperClonesItem:first-child");
    var lastClone = grouperClonesItems.find(".grouperClonesItem:last-child");
    var indexNext = parseInt(lastClone.attr("data-index")) + 1;

    var baseCloneHtml = baseClone.html();
    baseCloneHtml += "";

    var wrap = "<div class='grouperClonesItem' data-index='" + indexNext + "'>" + baseCloneHtml + "</div>";
    grouperClonesItems.append(wrap);
    var lastChild = grouperClonesItems.find(".grouperClonesItem:last-child");

    lastChild.find(".itemCloneName").html(defaultName + " " + indexNext);
    lastChild.find(".grouperClonesRemove").removeAttr("disabled");

    lastChild.find("input,select").each(function () {
        $(this).removeAttr("id");
        $(this).addClass("virtualFormComponent");

        var oldName = $(this).attr("name");
        var oldNameArr = oldName.split("-");
        $(this).attr("name", oldNameArr[0] + "-" + indexNext);

    });


}

























