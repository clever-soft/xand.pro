(function($){
  $.fn.BbReady = function(options){
	    
    //  panel 
  

  
    var text = '<div class="inputBbPanel">';
  
      text = text + '<a href="#" id="id10" title="url">url</a>';
      text = text + '<a href="#" id="id1" title="b">b</a>';   
      text = text + '<a href="#" id="id2" title="i">i</a>';   
      text = text + '<a href="#" id="id3" title="u">u</a>';  
	  text = text + '<a href="#" id="id13" title="h2">h2</a>';	
	  text = text + '<a href="#" id="id4" title="h3">h3</a>';   	
	  text = text + '<a href="#" id="id5" title="h4">h4</a>';		  
	  text = text + '<a href="#" id="id6" title="php">php</a>'; 
	  text = text + '<a href="#" id="id7" title="js">js</a>'; 
	  text = text + '<a href="#" id="id8" title="html">html</a>'; 
	  text = text + '<a href="#" id="id9" title="css">css</a>';     
      text = text + '<a href="#" id="id11" title="img">img</a>';	  
     
	  
	      
    text = text + '</div>';
    
	
/*	*/
	
	
    $(this).wrap('<div class="inputBbBox"></div>');
    $(".inputBbBox").prepend(text);   
   
    var id = '#' + $(this).attr("id");
    var e = $(id).get(0);
    
    $('.inputBbPanel a').click(function() {
      var button_title = $(this).attr("title");
      var button_id    = $(this).attr("id");
      
	  var start 	= '['+button_title+']';
      var end 		= '[/'+button_title+']';	 
	
	  if(button_id == "id14")
	  {
		 start = button_title; 
		 end   = "";
	  }
	 
	  var param		= "";
	  if (button_title=='img')
	  {
	      param=prompt("Enter image URL","http://");
		  if (param)	start+=param;
	  }
	  else if (button_title=='url')
	  {
		  param=prompt("Enter article ID", "");		  		  
		  if (param) start = '[url=' + param + ']';
		  
		  param=prompt("Enter URL title","Title");
		  if (param) start += param;
		  
	  }
      insert(start, end, e);
      return false;
    });
	};

 function insert(start, end, element) {
    if (document.selection) {
       element.focus();
       sel = document.selection.createRange();
       sel.text = start + sel.text + end;
    } else if (element.selectionStart || element.selectionStart == '0') {
       element.focus();
       var startPos = element.selectionStart;
       var endPos = element.selectionEnd;
       element.value = element.value.substring(0, startPos) + start + element.value.substring(startPos, endPos) + end + element.value.substring(endPos, element.value.length);
    } else {
      element.value += start + end;
    }
  }
 
  
})(jQuery);

function BbEncoder(str)
{ 
     
   str = str.replace(/&lt;/g, 	   '<');     
   str = str.replace(/&gt;/g, 	   '>');  
   str = str.replace(/'/g,    	   '#OK#');   
   str = str.replace(/"/g,    	   '#DK#');    
   str = str.replace(/&/g, 	 	   "#AM#"); 
   str = str.replace(/\r\n|\r|\n/g,"#BR#");	         
   str = str.replace(/\t/g,	       "#TB#");     
   str = str.replace(/\+/g,	       "#PL#");        
   str = str.replace(/\\/g,	       "/");              
   
   return str;   
}