//---
//--- Widget filters
//---

function CFilters() {
    this.eid = false;

    this.sideFilters = "";
    this.optionsFilters = "";
    this.optionsTable = "";


    //---
    this.Init = function (eid, options) {
        this.eid = eid;
        this.Reset();

        //--
        $("#resetGlobal").attr("onclick", "FA.ResetGlobal(" + eid + "); return false;");

        //--
    };

    //---
    this.Reset = function () {
        this.sideFilters = "";
        this.optionsFilters = "";
        this.optionsTable = "";
    };

    //---
    this.Draw = function () {
        //---  fill data
        $("#sideFilters").html(this.sideFilters);
        $("#optionsFilters").html(this.optionsFilters);
        $("#optionsTable").html(this.optionsTable);

        //---  special postprcessing
        $(".datepicker").each(function () {

            $(this).datepicker({
                "dateFormat": "dd/mm/yy",
                "showButtonPanel": true,
                "changeMonth": true,
                "changeYear": true
            }).datepicker("setDate", $(this).val());
        });


        this.Reset();
    };


    //---
    this.PushSettings = function (key, name, filterOptions) {
        var active = (filterOptions["active"]) ? "checkboxActive" : "";
        var hide = (filterOptions["show"]) ? "cbMultiSelected " : "cbMultiUnSelected ";

        //GlobalFilters.Link({'_dataDisableUpdate' : 1}, {'_fShow_ATC' : 1}); return false

        var action = "FA.ShowHide(" + this.eid + ", this, \"" + key + "\"); return false;";
        var html = "<a id='optionF" + key + "' onclick='" + action + "' class='" + hide + active + "'>" + name + "</a>";

        if (key == "_ATC") {
            // --- хак ----
            if (GNav.param["_CA"]) {
                html += "<a onclick='" + this.eid + ".Link({}, {\"_CA\" : 1}); return false' class='cbMultiSelected'>Compact actions</a>";
            }
            else {
                html += "<a onclick='" + this.eid + ".Link({\"_CA\" : 1}); return false' class='cbMultiUnSelected'>Compact actions</a>";
            }
            // ----

            this.optionsTable += html;

        }
        else {
            this.optionsFilters += html;
        }
    };

    //---
    // { key, name, filterOptions, value, dataOptions }
    this.Wrap = function (object, body) {
        this.PushSettings(object["key"], object["name"], object["filterOptions"]);

        var slideClass = (object["filterOptions"]["slide"]) ? "actionSideHeadUp" : "actionSideHeadDown";
        var html = "<div class='actionSideHead " + slideClass + "'>";

        html += "<a onclick='FA.Hide(" + this.eid + ", this, \"" + object["key"] + "\"); return false;' class='filterHeadActions' title='Hide filter'><i class='fa fa-times'></i></a>";
        html += "<a onclick='FA.Reset(" + this.eid + ", this, \"" + object["key"] + "\"); return false;' class='filterHeadActions' title='Reset filter'><i class='fa fa-trash-o'></i></a>";

        //GlobalFilters.Link({}, {'sku':false, '_fShow_sku':false}, ajaxproducts); return false
        slideClass = (object["filterOptions"]["slide"]) ? "fa-chevron-up" : "fa-chevron-down";

        html += "<a class='actionSideTitle' onclick='FA.Slider(" + this.eid + ", this, \"" + object["key"] + "\"); return false;'>" +
            "<i class='fa " + slideClass + "'></i> " + object["name"] + "" +
            "</a>";

        html += "</div>";

        var hide = (object["filterOptions"]["slide"]) ? "style='display:none;' " : " ";
        html += "<div class='actionSideBody' " + hide + ">";
        html += body;
        html += "</div>";

        //console.log(object);

        hide = (object["filterOptions"]["show"]) ? "" : "style='display:none;' ";

        return "<div class='actionSide' id='filterBox" + object["key"] + "' " + hide + ">" + html + "</div>";


    };

    //------------------------
    //------------------------
    //------------------------

    this.PushActionInputFilter = function (object) {

        var body = "<div class='input-prepend filterOptionsBody' style='padding: 0; margin: 0;'>";
        body += "<span class='add-on' style='border-right: none;'><i class='fa fa-search'></i></span>";
        body += "<input class='span2' id='filter" + object["key"] + "' type='text' value='" + object["value"] + "' style='width:176px; text-indent:5px;  height:22px;'";
        body += " onkeydown='FA.InputFilterKeyDown(event, this, " + this.eid + ", \"" + object["key"] + "\");'></div>";

        this.sideFilters += this.Wrap(object, body);

    };

    //---
    this.PushActionInputDate = function (object) {

        var buttons =
            "<hr style='margin-top:4px;' />" +
            "<div style='text-align: right'>" +
            "<button type='button' class='btn btn-mini' onclick='FA.Reset(" + this.eid + ", this, \"" + object["key"] + "\"); return false;'>Reset</button>" +
            "<button type='button' class='btn btn-mini btn-primary' onclick='FA.InputDateKeyDown(this, " + this.eid + ", \"" + object["key"] + "\")'>OK</button>" +
            "</div>";

        var body = "<div class='input-prepend filterOptionsBody' style='padding: 0; margin: 0;'>";
        body += "<span class='add-on' style='border-right: none;'><i class='fa fa-calendar'></i></span>";
        body += "<input class='span2 datepicker' type='text' placeholder='dd/mm/yyyy' value='" + object["value"] + "' style='width:176px; text-indent:5px;  height:22px;'>" +
            "</div>";

        body = body + buttons;

        this.sideFilters += this.Wrap(object, body);
    };

    //---
    this.PushActionRangeFilter = function (object) {
        var valuesArr = object["value"].split("to");

        if (!valuesArr[0])  valuesArr[0] = "";
        if (!valuesArr[1])  valuesArr[1] = "";

        var buttons =
            "<hr style='margin-top:4px;' />" +
            "<div style='text-align: right'>" +
            "<button type='button' class='btn btn-mini' onclick='FA.Reset(" + this.eid + ", this, \"" + object["key"] + "\"); return false;'>Reset</button>" +
            "<button type='button' class='btn btn-mini btn-primary' onclick='FA.ActionRangeKeyDown(this, " + this.eid + ", \"" + object["key"] + "\")'>OK</button>" +
            "</div>";

        var body = "<div class='filterOptionsBody'>" +
            "<div class='input-prepend' style='padding: 0; margin: 0 0 5px;'>";
        body += "<span class='add-on' style='border-right: none;'><i class='fa fa-calendar'></i></span>";
        body += "<input class='span2 datepicker' type='text' placeholder='dd/mm/yyyy' value='" + valuesArr[0] + "' style='width:176px; text-indent:5px;  height:22px;'>" +
            "</div>";

        body += "<div class='input-prepend' style='padding: 0; margin: 0;'>";
        body += "<span class='add-on' style='border-right: none;'><i class='fa fa-calendar'></i></span>";
        body += "<input class='span2 datepicker' type='text' placeholder='dd/mm/yyyy' value='" + valuesArr[1] + "' style='width:176px; text-indent:5px;  height:22px;'>" +
            "</div>" +
            "</div> ";

        body = body + buttons;

        this.sideFilters += this.Wrap(object, body);
    };


    //---
    this.PushActionTableColumns = function (object) {
        this.PushActionCheckboxMultiple(object);
    };

    //---
    this.PushActionCheckboxMultiple = function (object) {
        var options = "";
        for (var i in object["options"]) {
            var active = (object["options"][i][2]) ? "cbMultiSelected" : "cbMultiUnSelected";
            var active = (object["options"][i][3]) ? "checkboxDisabled" : active;
            var js = (object["options"][i][3]) ? "" : " onclick='FA.RefreshCheckboxFilter(this); return false;' ";
            options += "<a " + js + "class='" + active + "' rel='" + object["options"][i][0] + "'>" + object["options"][i][1] + "</a>";
        }

        var buttons =
            "<hr style='margin-top:4px;' />" +
            "<div style='text-align: right'>" +
            "<button type='button' class='btn btn-mini' onclick='FA.Reset(" + this.eid + ", this, \"" + object["key"] + "\"); return false;'>Reset</button>" +
            "<button type='button' class='btn btn-mini btn-primary' onclick='FA.CheckboxFilterClick(this, " + this.eid + ", \"" + object["key"] + "\")'>OK</button>" +
            "</div>";


        var body =
            "<div class='chBoxMulti'>" +
            "<a onclick='FA.CheckboxFilterMassClick(this); return false' class='cbMultiUnSelected'>Select all / Deselect all</a>" +
            "<hr style='margin-top:4px;'/>" +
            "<div class='filterOptionsBody'>" +
            options +
            "</div>" +
            buttons +
            "</div>";

        this.sideFilters += this.Wrap(object, body);
    };

    //---
    this.PushActionMenuFilter = function (object) {

        var options = "";
        for (var i in object["options"]) {
            var active = (object["options"][i][2]) ? "actionMenuSelected" : "actionMenuUnSelected";
            var js = " onclick='FA.MenuClick(this, " + this.eid + ", \"" + object["key"] + "\"); return false;' ";

            options += "<a " + js + "class='" + active + "' rel='" + object["options"][i][0] + "'>" + object["options"][i][1] + "</a>";
        }

        var body = "<div class='actionMenu filterOptionsBody'>" + options + "</div>";

        this.sideFilters += this.Wrap(object, body);

    };


    //---
    this.PushActionBoolean = function (object) {
        this.PushActionMenuFilter(object);
    };

    //---
    this.PushActionSelectFilter = function (object) {

        var options = "";
        for (var i in object["options"]) {
            var selected = (object["options"][i][2]) ? " selected='selected' " : "";
            options += "<option value='" + object["options"][i][0] + "' " + selected + ">" + object["options"][i][1] + "</option>";
        }

        var body = "<div class='filterOptionsBody'>" +
            "<select class='action_select' onchange='FA.ActionSelectFilterChange(this, " + this.eid + ", \"" + object["key"] + "\");'>"
            + options + "</select>" +
            "</div>";

        this.sideFilters += this.Wrap(object, body);

    };


    //------------------------
    //------------------------
    //------------------------


}

GF = new CFilters();

//------------------------
// Actions components
//------------------------

function CFiltersActions() {
    //---
    this.Slider = function (eid, dom, key) {
        var elem = $(dom), include = {}, exclude = {};
        key = "_fSlide_" + key;

        if (elem.parent().hasClass("actionSideHeadDown")) {
            elem.parent().removeClass("actionSideHeadDown").addClass("actionSideHeadUp");
            elem.find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
            elem.parent().next().hide(0);
            include[key] = 1;
        }
        else {
            elem.parent().removeClass("actionSideHeadUp").addClass("actionSideHeadDown");
            elem.find("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
            elem.parent().next().show(0);
            exclude[key] = 1;
        }

        eid.LinkNa(include, exclude);

    };

    //---
    this.ShowHide = function (eid, dom, key) {
        if ($(dom).hasClass("cbMultiUnSelected")) {
            this.Show(eid, dom, key);
        }
        else {
            this.Hide(eid, dom, key);
        }

    };


    //_fShow_
    //_fOpen_

    //---
    this.Show = function (eid, dom, key) {
        $("#filterBox" + key).show();
        $("#optionF" + key).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");

        var include = {};
        include["_fShow_" + key] = 1;

        eid.Push(GNav.Prepare(include));

    };

    //---
    this.Hide = function (eid, dom, key) {
        $("#filterBox" + key).hide();
        $("#optionF" + key).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

        var exclude = {};
        exclude["_fShow_" + key] = 1;

        eid.Push(GNav.Prepare({}, exclude));

    };


    //---
    this.Reset = function (eid, dom, key) {
        $("#optionF" + key).removeClass("checkboxActive");

        var exclude = {};
        exclude[key] = 1;

        this.ResetBody($(dom).parent().parent().find(".filterOptionsBody"));
        eid.Link({}, exclude);

    };

    //---
    this.ResetBody = function (body) {

        //body.css("border", "solid 1px red");

        body.find("input").val("");
        body.find("select").find('option:selected').removeAttr("selected");

        body.find(".actionMenuSelected").removeClass("actionMenuSelected").addClass("actionMenuUnSelected");
        body.find(".actionMenuUnSelected").eq(0).removeClass("actionMenuUnSelected").addClass("actionMenuSelected");

        body.find(".cbMultiSelected").removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

    };

    //---
    this.ResetGlobal = function (eid) {
        eid.Act('filtersReset', false, GNav.Prepare());
        document.location.reload();
    };


    //---
    this.InputDateKeyDown = function (dom, eid, key) {
        var data = $(dom).parent().parent().find(".datepicker").val();
        var dataExp = data.split("/");
        data = dataExp[2] + "_" + dataExp[1] + "_" + dataExp[0];

        var include = {};
        include[key] = data;
        include["page"] = 1;

        $("#optionF" + key).addClass("checkboxActive");
        eid.Link(include);


    };

    //---
    this.ActionRangeKeyDown = function (dom, eid, key) {
        var data = $(dom).parent().parent().find(".datepicker").eq(0).val();
        var data2 = $(dom).parent().parent().find(".datepicker").eq(1).val();

        var dataExp = data.split("/");
        var dataExp2 = data2.split("/");

        data = (dataExp.length == 3) ? dataExp[2] + "_" + dataExp[1] + "_" + dataExp[0] : "";
        data2 = (dataExp2.length == 3) ? dataExp2[2] + "_" + dataExp2[1] + "_" + dataExp2[0] : "";

        var include = {};
        include[key] = data + "to" + data2;
        include["page"] = 1;

        $("#optionF" + key).addClass("checkboxActive");
        eid.Link(include);

    };


    //---
    this.InputFilterKeyDown = function (ev, dom, eid, key) {
        if (ev.which == 13) {
            var str = $(dom).val();
            var include = {};

            include[key] = str;
            include["page"] = 1;

            eid.Link(include);

            if ($(dom).hasClass("tableSmartFilterInput")) {
                $("#filter" + key).val(str);
            }

            if (str == "") {
                $("#optionF" + key).removeClass("checkboxActive");
            }
            else {
                $("#optionF" + key).addClass("checkboxActive");
            }
        }
    };

    //----
    this.MenuClick = function (dom, eid, key) {
        var value = $(dom).attr("rel");
        var include = {}, exclude = {};

        if (value == "") {
            exclude[key] = 1;
            $("#optionF" + key).removeClass("checkboxActive");
        }
        else {
            include[key] = value;
            include["page"] = 1;
            $("#optionF" + key).addClass("checkboxActive");
        }

        $(dom).parent().find("a").removeClass("actionMenuSelected").addClass("actionMenuUnSelected");
        $(dom).addClass("actionMenuSelected");


        eid.Link(include, exclude);

    };

    //----
    this.ActionSelectFilterChange = function (dom, eid, key) {
        var value = $(dom).find(":selected").attr('value');
        var include = {}, exclude = {};

        if (value == "") {
            exclude[key] = 1;
            $("#optionF" + key).removeClass("checkboxActive");
        }
        else {
            include[key] = value;
            include["page"] = 1;
            $("#optionF" + key).addClass("checkboxActive");
        }

        eid.Link(include, exclude);
    };

    //----
    this.CheckboxFilterMassClick = function (dom) {
        if ($(dom).hasClass("cbMultiSelected")) {
            $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
            $(dom).next().next()
                .find("a:not(.checkboxDisabled)")
                .removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

        }
        else {
            $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
            $(dom).next().next()
                .find("a:not(.checkboxDisabled)")
                .removeClass("cbMultiUnSelected").addClass("cbMultiSelected");

        }
    };


    //----
    this.CheckboxFilterGetSelected = function (dom) {
        var output = "";

        $(dom).parent().parent().find(".filterOptionsBody").find("a:not(.checkboxDisabled)").each(function (i) {
            if ($(this).hasClass("cbMultiSelected"))
                output += $(this).attr("rel") + ":";
        });

        return output.slice(0, -1);
    };


    //----
    this.CheckboxFilterClick = function (dom, eid, key) {
        replace = FA.CheckboxFilterGetSelected(dom);

        var include = {};
        include[key] = replace;
        include["page"] = 1;

        $("#optionF" + key).addClass("checkboxActive");
        eid.Link(include);
    };

    //----
    this.RefreshCheckboxFilter = function (dom) {
        if ($(dom).hasClass("checkboxDisabled"))
            return;

        if ($(dom).hasClass("cbMultiUnSelected")) {
            $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
        }
        else {
            $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

        }
    }


}
FA = new CFiltersActions();
