//----------------------------------------------------------------------
//- Buttons
//----------------------------------------------------------------------

function ButtonJsonModalCloseAct(dom, eid, data, method)
{
    var data = method();
    var action = $(dom).attr("data-action");

    Wnd.Controller = eid.Controller;
    Wnd.Close();
    eid.Act(action, data, GNav.Prepare());
}

//------
function ButtonJsonAct(dom, eid, data, method)
{
    var data = method();
    var action = $(dom).attr("data-action");
    eid.Act(action, data, GNav.Prepare());
}


//------
function ButtonAct(dom, eid, data, method) {
    ButtonJsonAct(dom, eid, data, method);
}


//------
function ButtonJsonModalCloseModalAct(dom, eid, data, method)
{
    var data = method();
    var action = $(dom).attr("data-action");

    Wnd.Controller = eid.Controller;
    Wnd.Close();
    Wnd.Load(action, data, GNav.Prepare());
}

//------
function ButtonModalClose(dom, eid, data)
{
    Wnd.Close();
}

//------
function ButtonTableAct(dom, eid, data)
{
    var action = $(dom).attr("data-action");
    var confirmHtml = $(dom).attr("data-confirm");
    var dataPack = TableMassGetSelected(data);

    if ($(dom).attr("data-confirm"))
    {
        if (confirm($(dom).attr("data-confirm"))) {
            eid.Act(action, dataPack, GNav.Prepare());
        }
    }
    else
    {
        eid.Act(action, dataPack, GNav.Prepare());
    }
}


//------
function ButtonTableModalAct(dom, eid, data)
{
    var action = $(dom).attr("data-action");
    var confirmHtml = $(dom).attr("data-confirm");
    var dataPack = TableMassGetSelected(data);

    if ($(dom).attr("data-confirm"))
    {
        if (confirm($(dom).attr("data-confirm"))) {

            Wnd.Controller = eid.Controller;
            Wnd.Close();
            Wnd.Load(action, dataPack, GNav.Prepare());
        }
    }
    else
    {
        Wnd.Controller = eid.Controller;
        Wnd.Close();
        Wnd.Load(action, dataPack, GNav.Prepare());
    }
}

//------
function ButtonDataAct(dom, eid, data)
{
    var action = $(dom).attr("data-action");

    if ($(dom).attr("data-confirm"))
    {
        if (confirm($(dom).attr("data-confirm"))) {
            eid.Act(action, data, GNav.Prepare());
        }
    }
    else
    {
        eid.Act(action, data, GNav.Prepare());
    }
}


//------
function ButtonPath(dom, eid, data)
{
    var path = $(dom).attr("data-action");

    if ($(dom).attr("data-confirm"))
    {
        eid.Link({'path': path});
    }
    else
    {
        eid.Link({'path': path});
    }
}

function ButtonDataPath(dom, eid, data) {
    ButtonDataAct(dom, eid, data);
}
;


//------
function ButtonModalAct(dom, eid, data)
{
    var action = $(dom).attr("data-action");
    Wnd.Controller = eid.Controller;
    Wnd.Load(action, data, GNav.Prepare());
}


//----------------------------------------------------------------------
//- Forms & Inputs & Controls
//----------------------------------------------------------------------

/**
 * @return {string}
 */
function PrepareContent(domId, val)
{
    var str = '';
    if (domId) str = $("#"+domId).val();
    if (val) str = val;

    str = str.replace(/\r\n|\r|\n/g, "#BR#");
    str = str.replace(/&/g, '#AM#');
    str = str.replace(/'/g, '#OK#');
    str = str.replace(/"/g, '#DK#');
    str = str.replace(/\+/g, '#PL#');
    str = str.replace(/&/g, "#AM#");
    str = str.replace(/\\/g, "#SL#");
    str = str.replace(/\//g, "#SLO#");

    return str;
}

//----
function CheckboxClick(dom)
{
    if ($(dom).hasClass('checkboxDisabled'))
        return;

    if ($(dom).hasClass('checkboxSel'))
    {
        $(dom).removeClass('checkboxSel').addClass('checkboxUnSel');
    }
    else
    {
        $(dom).removeClass('checkboxUnSel').addClass('checkboxSel');
    }
}
//----
function CheckboxGetData(domId)
{
    return ($('#' + domId).hasClass('checkboxSel')) ? 1 : 0;
}


//----------------------------------------------------------------------
//- Actions APP
//----------------------------------------------------------------------

function ActionFiltersReset(eid)
{
    eid.Act('filtersReset', false, GNav.Prepare());
}

function ActionSidebleEvent(dom, key)
{
    var elem = $(dom), include = {};
    key = "_fOpen_" + key;


    $(dom).parent().parent().css("opacity", "0.7");

    if (elem.parent().hasClass("actionSideHeadDown"))
    {

        include[key] = 1;
        GlobalFilters.LinkNa({'_dataDisableUpdate': 1}, include, null, function () {

            $(dom).parent().parent().css("opacity", "1");

        });

        elem.parent().removeClass("actionSideHeadDown").addClass("actionSideHeadUp");
        elem.find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        elem.parent().next().hide(0);

    }
    else
    {

        include[key] = 1;
        include['_dataDisableUpdate'] = 1;

        GlobalFilters.LinkNa(include, {}, null, function () {
            $(dom).parent().parent().css("opacity", "1");

        });

        elem.parent().removeClass("actionSideHeadUp").addClass("actionSideHeadDown");
        elem.find("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        elem.parent().next().show(0);
    }
}
;


function RefreshCheckboxFilter(dom)
{
    if ($(dom).hasClass("checkboxDisabled"))
        return;

    if ($(dom).hasClass("cbMultiUnSelected"))
    {
        $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
    }
    else
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

    }

}

function CheckboxFilterGetSelected(dom)
{
    var output = "";

    $(dom).parent().parent().find(".filterOptionsBody").find("a:not(.checkboxDisabled)").each(function (i)
    {
        if ($(this).hasClass("cbMultiSelected"))
            output += $(this).attr("rel") + ":";
    });

    return output.slice(0, -1);
}
;


function CheckboxFilterClick(dom, eid, key)
{
    replace = CheckboxFilterGetSelected(dom);

    var include = {};
    include[key] = replace;
    include["page"] = 1;

    eid.Link(include);
}



function CheckboxFilterMassClick(dom)
{
    if ($(dom).hasClass("cbMultiSelected"))
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).next().next()
                .find("a:not(.checkboxDisabled)")
                .removeClass("cbMultiSelected").addClass("cbMultiUnSelected");

    }
    else
    {
        $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
        $(dom).next().next()
                .find("a:not(.checkboxDisabled)")
                .removeClass("cbMultiUnSelected").addClass("cbMultiSelected");

    }
}



//----
function ActionInputFilterKeyDown(ev, dom, aeid, key)
{
    if (ev.which == 13)
    {
        var str = $(dom).val();

        var include = {};
        include[key] = str;
        include["page"] = 1;

        aeid.Link(include);
    }
}


function ActionInputFilterSearch(dom, aeid, key)
{
    var str = $(dom).parent().prev().find("input").val();

    var include = {};
    include[key] = str;
    include["page"] = 1;

    aeid.Link(include);
}


function ActionSelectFilterChange(dom, eid, key)
{
    var val = $(dom).attr('value');
    var include = {};
    var exclude = {};

    if (val == "")
    {
        exclude[key] = 1;
    }
    else
    {
        include[key] = val;
        include["page"] = 1;
    }

    eid.Link(include, exclude);
}


function ActivateContaner(dom, id)
{
    if ($(dom).hasClass("checkboxSel"))
    {
        $(dom).removeClass("checkboxSel").addClass("checkboxUnSel");
        $(dom).parent().parent().parent().find(".grouperCheckboxBlocks").addClass("grouperCheckboxDisabled");
        $("#" + id).val("0");

    }
    else
    {
        $(dom).removeClass("checkboxUnSel").addClass("checkboxSel");
        $(dom).parent().parent().parent().find(".grouperCheckboxBlocks").removeClass("grouperCheckboxDisabled");
        $("#" + id).val("1");
    }

}


function GrouperSpoiler(dom)
{
    var cont = $(dom).next();
    if (cont.hasClass("controlGrouperUp"))
    {
        cont.removeClass("controlGrouperUp").addClass("controlGrouperDown").slideDown(500);
        $(dom).find("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    }
    else
    {
        cont.removeClass("controlGrouperDown").addClass("controlGrouperUp").slideUp(500);
        $(dom).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    }

    if (Wnd.Align !== undefined)
        Wnd.Align();

}

//----------------------------------------------------------------------
//- pack.App.Table
//----------------------------------------------------------------------

/**
 * @return {string}
 */
function TableMassGetSelected(eid)
{
    var output = "",
        value;

    $("#table" + eid).find(".rowCheckboxes").each(function () {
        value = $(this).attr("data-value");

        if ($(this).hasClass("cbMultiSelected") && value) {
            output += value + ",";
        }
    });

    output = output.slice(0, -1);

    return output;
}

function TableRowsClick(dom)
{
    $(".tableRows").removeClass("tableRowsSelected");
    $(dom).addClass("tableRowsSelected");
}

function RefreshCheckbox(dom)
{
    if ($(dom).hasClass("checkboxDisabled"))
        return;

    if ($(dom).hasClass("cbMultiUnSelected"))
    {
        $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
        $(dom).parent().parent().addClass("tableRowsSelected");
    }
    else
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().removeClass("tableRowsSelected");
    }

}


function TableMassClick(dom)
{
    if ($(dom).hasClass("cbMultiSelected"))
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().parent().parent()
                .find(".rowCheckboxes:not(.checkboxDisabled)")
                .removeClass("cbMultiSelected").addClass("cbMultiUnSelected")
                .parent().parent().removeClass("tableRowsSelected");

    }
    else
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().parent().parent()
                .find(".rowCheckboxes:not(.checkboxDisabled)")
                .removeClass("cbMultiUnSelected").addClass("cbMultiSelected").parent().parent().addClass("tableRowsSelected");

        $(dom).parent().parent().removeClass("tableRowsSelected");
    }
}


//----------------------------------------------------------------------
//- pack.App.TIMER
//----------------------------------------------------------------------  

isActiveWnd = true;
confirmGlobal = false;
gloInt = false;

function makeTimeOffset(offset)
{
    var m = (offset / 60 | 0);
    var s = offset % 60;
    if (s < 10) {
        s = '0' + s;
    }
    return m + ':' + s;
}

function TimerStart(eid, secs)
{


    var dt = new Date();
    var dtsec = dt.getTime();


    if (gloInt !== undefined)
    {
        clearInterval(gloInt);
    }

    window.onfocus = function () {
        isActiveWnd = true;
    };
    window.onblur = function () {
        isActiveWnd = false;
    };

    gloInt = setInterval(function ()
    {
        if ($('.formBaseButtons').length != 0)
        {
            $('#zoneClockHelper').text('[pause]');
            return;
        }
        else
        {
            $('#zoneClockHelper').text('');
        }

        var currCounter = parseInt($('#zoneTimer').text());
        //$('title').html(currCounter); 			

        if (confirmGlobal)
            return;
        if (currCounter == 2)
        {
            if (!confirmGlobal && $('.formBaseButtons').length != 0)
            {
                confirmGlobal = true;

                if (confirm('Do you want reload this page ?'))
                {
                    eid.Refresh();
                }
                else
                {
                    $('#zoneTimer').text(secs);
                    confirmGlobal = false;
                }
            }
            else
            {
                eid.Refresh();
            }

        }

        var ndt = new Date();

        if (dtsec < ndt.getTime())
        {
            if (currCounter > 1)
            {
                $('#zoneTimer').text(parseInt($('#zoneTimer').text()) - 1);
                $('#zoneClock').text(makeTimeOffset(parseInt($('#zoneTimer').text()) - 1));
                dtsec++;
            }
        }

    }, 1000);
}


//----------------------------------------------------------------------
//- pack.Form
//----------------------------------------------------------------------  
tabOnShow = [];

function GrouperTabHeadClick(dom)
{
    var tab = $(dom);

    $(".grouperTabTitle").removeClass("grouperTabTitleActive").addClass("grouperTabTitleNoactive");
    tab.removeClass("grouperTabTitleNoactive").addClass("grouperTabTitleActive");
    $(".grouperTabContent").css("display", "none");
    $(".grouperTabContent").eq(tab.index()).css("display", "block");

    if (tabOnShow[tab.index()] != null)
    {
        tabOnShow[tab.index()]();
        tabOnShow[tab.index()] = null;
    }

}

function FormEnterKeyDispatch(domId)
{
    $('#' + domId).find("input").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            $('#' + domId).find("button.btn-primary").click();

        }
    });
}


//----------------------------------------------------------------------
//- common
//----------------------------------------------------------------------
function DropDownShow(dom)
{
    $(".dropDownBody").css("display", "none");
    if ($(dom).hasClass("dropDownHide"))
    {
        $(dom).removeClass("dropDownHide");
        $(dom).find(".dropDownBody").css("display", "block");
    }
    else
    {
        $(dom).addClass("dropDownHide");
        $(dom).find(".dropDownBody").css("display", "none");
    }
}



//-------------
//- GEO DD
//-------------

lastGeoDom = null;

function GeoIpDropDownShow(dom, ip)
{
    $(".dropDownBody").css("display", "none");

    lastGeoDom = dom;

    if ($(dom).hasClass("dropDownHide"))
    {
        $(dom).removeClass("dropDownHide");

        var qty = $(dom).find(".dropDownBody").length;

        if (qty == 0) {

            var html =
                    "<div class='dropDownBody' style='display: block' onclick='return false;' >" +
                    "<div class='dropDown'>" +
                    "<div class='dropDownContent'>" +
                    "Loading..." +
                    "</div>" +
                    "</div>" +
                    "</div>";

            $(dom).append(html);


            $.ajax({url: "/mod/crm/backends/backend.geoip.php?ip=" + ip,
                cache: true,
                dataType: "JSON",
                async: true,
                success: function (data)
                {
                    var ip = data.ip, html = "";

                    if (data.success)
                    {

                        for (var i in data) {
                            keyNormal = i;

                            if (keyNormal == "success")
                                continue;
                            if (keyNormal == "countryCode")
                                continue;

                            keyNormal = keyNormal.replace("countryName", "Country");
                            keyNormal = keyNormal.replace("regionName", "Region");
                            keyNormal = keyNormal.replace("zip", "ZIP");
                            keyNormal = keyNormal.replace("isp", "ISP");
                            keyNormal = keyNormal.replace("ip", "IP");
                            keyNormal = keyNormal.replace("city", "City");
                            keyNormal = keyNormal.replace("latitude", "Latitude");
                            keyNormal = keyNormal.replace("longitude", "Longitude");
                            keyNormal = keyNormal.replace("method", "Method");

                            html += "<div class='simpleMicroTC'><strong>" + keyNormal + "</strong><div>" + data[i] + "</div></div>";
                        }

                        $(lastGeoDom).find(".dropDownContent").html(html);
                    }
                    else
                    {
                        $(lastGeoDom).find(".dropDownContent").html("No data for this IP: " + ip);
                    }
                }
            });

        }
        else
        {
            var dropDownBody = $(dom).find(".dropDownBody");
            dropDownBody.css("display", "block");
        }



    }
    else
    {
        $(dom).addClass("dropDownHide");
        $(dom).find(".dropDownBody").css("display", "none");
    }
}

function CaptchaRefresh(dom)
{
    var img = $(dom).parent().prev().find("img");
    img.attr('src', img.attr('src') + '?' + Math.floor(Math.random() * 10000));

}

//-----
function FileManager(dom)
{
    window.open('/tools/filemanager', '', 'top=100, left=100, width=1000, height=700, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=0');
}

//-----
function BugsForm(dom)
{
    window.open('/mod/erp/bugs', '', 'top=100, left=100, width=1200, height=670, menubar=0, toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=0');
}

//-----
function CreateWindow(dom)
{
    window.open($(dom).attr("href"),
            $(dom).attr("href"), 'width=1100, height=600, location=no, toolbar=no, menubar=no, status=no, scrollbars=yes, resizable=yes');
}

