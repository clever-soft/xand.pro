//---
function Nav()
{
    this.param = {};
    this.path  = {};


    //--- Check PATH
    this.IsArray = function (inputArray)
    {
        return inputArray && !(inputArray.propertyIsEnumerable('length'))
            && typeof inputArray === 'object'
            && typeof inputArray.length === 'number';
    };

    //---
    this.Load = function (path, param)
    {
        this.path  = path;
        this.param = param;

        console.log(this.path);
        console.log(this.param);

    };

    //---
    this.Clean = function ()
    {
        this.path  = {};
        this.param = {};
    };

    //---
    this.Prepare = function (include, exclude)
    {

        var tmpParam = this.param;
        var path     = "/?";

        if (this.IsArray(this.path))
            path = this.path.join("/") + path;

        for (var i in include) {
            tmpParam[i] = include[i];
        }

        for (var i in exclude)
        {
            delete tmpParam[i];
        }

        for (var i in tmpParam) {

            if (tmpParam[i] == null)
                continue;
            if (tmpParam[i] == "")
                continue;

            var param = tmpParam[i];

            if (param == false)
                param = 0;

            path += i + "=" + param + "&";

        }

        console.log(path);

        return path;
    };

    //---
    this.Merge = function (include, include2)
    {
        for (var i in include2)
        {
            include[i] = include2[i];
        }

        return include;
    };

}

//---
GNav = new Nav();
ENav = new Nav();
