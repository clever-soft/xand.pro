//----------------------------------------------------------------------
//- pack.Widgets
//----------------------------------------------------------------------

function ActivateContaner(dom, id)
{
    if($(dom).hasClass("checkboxSel"))
    {
        $(dom).removeClass("checkboxSel").addClass("checkboxUnSel");
        $(dom).parent().parent().find(".grouperCheckboxBlocks").addClass("grouperCheckboxDisabled");
        $("#"+id).val("0");

    }
    else
    {
        $(dom).removeClass("checkboxUnSel").addClass("checkboxSel");
        $(dom).parent().parent().find(".grouperCheckboxBlocks").removeClass("grouperCheckboxDisabled");
        $("#"+id).val("1");
    }

}

//---
function ColumnSpoilerAction(dom)
{
    var cont =  $(dom).next();
    if($(dom).hasClass("fa-plus-square-o"))
    {
        $(dom).removeClass("fa-plus-square-o").addClass("fa-minus-square-o");

        var fullText  = cont.attr("title");
        var shortText = cont.html();
        cont.html(fullText);
        cont.attr("title", shortText);


    }
    else
    {
        $(dom).removeClass("fa-minus-square-o").addClass("fa-plus-square-o");

        var fullText  = cont.html();
        var shortText = cont.attr("title");
        cont.html(shortText);
        cont.attr("title", fullText);
    }

}

//---
function GrouperSpoiler(dom)
{
    var cont =  $(dom).next();
    if(cont.hasClass("controlGrouperUp"))
    {
        cont.removeClass("controlGrouperUp").addClass("controlGrouperDown").slideDown(500);
        $(dom).find("i").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    }
    else
    {
        cont.removeClass("controlGrouperDown").addClass("controlGrouperUp").slideUp(500);
        $(dom).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    }

    if(Wnd.Align !== undefined) Wnd.Align();

}



//----------------------------------------------------------------------
//- pack.Widget.Table
//----------------------------------------------------------------------

function TableMassGetSelected(eid)
{
    var output = "";

    $("#table"+eid).find(".rowCheckboxes").each(function (i)
    {
        if($(this).hasClass("cbMultiSelected"))
            output += $(this).attr("data-value") + ",";
    });

    return output.slice(0, -1);
};


function TableRowsClick(dom)
{
    $(".tableRows").removeClass("tableRowsSelected");
    $(dom).addClass("tableRowsSelected");
}

function RefreshCheckbox(dom)
{
    if($(dom).hasClass("checkboxDisabled"))
        return;

    if($(dom).hasClass("cbMultiUnSelected"))
    {
        $(dom).removeClass("cbMultiUnSelected").addClass("cbMultiSelected");
        $(dom).parent().parent().addClass("tableRowsSelected");
    }
    else
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().removeClass("tableRowsSelected");
    }

}


function TableMassClick(dom)
{
    if($(dom).hasClass("cbMultiSelected"))
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().parent().parent()
            .find(".rowCheckboxes:not(.checkboxDisabled)")
            .removeClass("cbMultiSelected").addClass("cbMultiUnSelected")
            .parent().parent().removeClass("tableRowsSelected");

    }
    else
    {
        $(dom).removeClass("cbMultiSelected").addClass("cbMultiUnSelected");
        $(dom).parent().parent().parent().parent()
            .find(".rowCheckboxes:not(.checkboxDisabled)")
            .removeClass("cbMultiUnSelected").addClass("cbMultiSelected").parent().parent().addClass("tableRowsSelected");

        $(dom).parent().parent().removeClass("tableRowsSelected");
    }
}


function PagerPageManualEvent(dom, azl)
{
    var pageNumber = $(dom).val();
    azl.Link({'page' : pageNumber});
}


//----------------------------------------------------------------------
//- pack.App.TIMER
//----------------------------------------------------------------------

isActiveWnd   = true;
confirmGlobal = false;
gloInt        = false;

function makeTimeOffset(offset)
{
    var m = (offset/60|0);
    var s = offset%60;
    if(s<10){
        s = '0'+s;
    }
    return m+':'+s;
}

function TimerStart(eid, secs)
{


    var dt        = new Date();
    var dtsec     = dt.getTime();


    if(gloInt!==undefined)
    {
        clearInterval(gloInt);
    }

    window.onfocus = function () { isActiveWnd = true;  };
    window.onblur  = function () { isActiveWnd = false; };

    gloInt = setInterval(function()
    {
        if($('.formBaseButtons').length != 0)
        {
            $('#zoneClockHelper').text('[pause]');
            return;
        }
        else
        {
            $('#zoneClockHelper').text('');
        }

        var currCounter = parseInt($('#zoneTimer').text());
        //$('title').html(currCounter);

        if(confirmGlobal) return;
        if(currCounter == 2)
        {
            if(! confirmGlobal && $('.formBaseButtons').length != 0)
            {
                confirmGlobal = true;

                if(confirm('Do you want reload this page ?'))
                {
                    eid.Refresh();
                }
                else
                {
                    $('#zoneTimer').text(secs);
                    confirmGlobal = false;
                }
            }
            else
            {
                eid.Refresh();
            }

        }

        var ndt = new Date();

        if(dtsec<ndt.getTime())
        {
            if(currCounter > 1)
            {
                $('#zoneTimer').text(parseInt($('#zoneTimer').text())-1);
                $('#zoneClock').text(makeTimeOffset(parseInt($('#zoneTimer').text())-1));
                dtsec++;
            }
        }

    },1000);
}
