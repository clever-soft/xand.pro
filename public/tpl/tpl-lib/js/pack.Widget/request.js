function Request(Controller, Zone, HashMode, Method, HistoryApiMode)
{
    this.Controller     = Controller;
    this.Zone           = Zone;
    this.HashMode       = HashMode;
    this.Method         = Method;
    this.HistoryApiMode = HistoryApiMode;
    this.Global         = true;
    this.Navigation     = "global";

    this.DefaultPage    = "";
    this.Hash           = "";
    this.eventStart     = null;
    this.eventComplete  = null;


    //---
    this.Link = function (include, exclude, refreshObject)
    {
        if(this.Navigation == "global")
        {
            path = GNav.Prepare(include, exclude);
        }
        else
        {
            path = ENav.Prepare(include, exclude);
        }

        this.Raw(path, 'View');

        if (refreshObject)
        {
            path = GNav.Prepare(include, exclude);
            refreshObject.Raw(path, 'View');
        }

    };

    //---
    this.LinkNa = function (include, exclude, eventStart, eventStop)
    {
        path = GNav.Prepare(include, exclude);
        this.Push(path, 'View', null, eventStart, eventStop);
    };


    //---  Navigation
    this.SetDefaultPage = function (page)
    {
        this.DefaultPage = page;
        return this;
    };

    //---
    this.Dispatch = function ()
    {

        if (document.location.hash != "" && document.location.hash != "#")
        {
            var temp_hash = document.location.hash;
            temp_hash = temp_hash.replace(/#/g, '');
            this.Go(temp_hash);
        }
        else
        {
            this.HashSet(this.DefaultPage);
            this.Go(this.DefaultPage);
        }
    };

    //---
    this.HashSet = function (Path)
    {
        if (this.HashMode == true)
        {
            document.location.hash = Path;
        }

        if (this.HistoryApiMode == true)
        {
            try
            {
                window.history.pushState({}, Path, Path);
            }
            catch (e)
            {
                this.HistoryApiMode = false;
            }
        }

        this.Hash = Path;
    };

    //---- Simple
    this.Refresh = function (ms)
    {
        if (!ms)
            ms = 500;

        Obj = this;
        setTimeout(function () {
            Obj.Raw(GNav.Prepare(), 'View');
        }, ms);

    };

    //---
    this.Go = function (Path, Method)
    {
        this.Raw(Path, 'View', null, this.HashMode, this.Zone, (!Method) ? this.Method : Method);
    };

    //---
    this.Act = function (Action, Data, Path, Method, Zone)
    {
        this.Raw(Path, Action, Data, false, Zone, (!Method) ? this.Method : Method);
    };



    //---   Push
    this.Push = function (Path, Action, Data, eventStart, eventStop)
    {
        if (!Path)
            Path = this.Hash;

        Path = encodeURIComponent(Path);

        $.ajax({
            type: "POST",
            cache: false,
            url: this.Controller,
            global: false,
            data: "PATH=" + Path + "&ACT=" + Action + "&DATA=" + Data,
            dataType: "html",
            beforeSend: function (html) {
                if (eventStart != null)
                    eventStart();
            },
            error: function () {
                console.log("Request Error!");
            },
            success: function (html) {
                if (eventStop != null)
                    eventStop(html);
            }
        });
    };



    //---   Raw
    this.Raw = function (Path, Action, Data, HashMode, Zone, Method)
    {
        if (!Path)
            Path = this.Hash;

        this.HashMode = HashMode;
        this.HashSet(Path);

        var Zone          = (!Zone)   ? this.Zone   : Zone;
        var currentMethod = (!Method) ? this.Method : Method;
        var eventStart = this.eventStart;
        var eventStop = this.eventComplete;
        var dataType = (currentMethod == "json") ? "json" : "html";

        Path = encodeURIComponent(Path);

        $.ajax({
            type: "POST",
            cache: false,
            url: this.Controller,
            global: this.Global,
            data: "PATH=" + Path + "&ACT=" + Action + "&DATA=" + Data,
            dataType: dataType,
            beforeSend: function (html)
            {
                if (eventStart == null)
                {
                }
                else
                {
                    eventStart();
                }
            },
            error: function ()
            {
                console.log("Request Error!");
            },
            success: function (html)
            {
                if (dataType == "json")
                {
                    if (eventStop != null)
                        eventStop(html);
                }
                else
                {
                    if (Zone != null)
                    {
                        switch (currentMethod)
                        {
                            case 'html':
                                $("#" + Zone).html(html);
                                break;
                            case 'htmlClass':
                                $("." + Zone).html(html);
                                break;
                            case 'append':
                                $("#" + Zone).append(html);
                                break;
                            case 'prepend':
                                $("#" + Zone).prepend(html);
                                break;
                            case 'nothing':
                                break;
                        }
                    }
                    if (eventStop != null)
                        eventStop(html);
                }
            }
        });

    }

}