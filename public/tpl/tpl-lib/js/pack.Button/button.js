/**
 *  @author Alexander Wizard <alexanderwizard19@gmail.com>
 */

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @param method
 * @constructor
 *
 */
function ButtonJsonModalCloseAct(dom, eid, data, method) {
    var data = method();
    var action = $(dom).attr("data-action");

    Wnd.Controller = eid.Controller;
    Wnd.Close();
    eid.Act(action, data, GNav.Prepare());
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @param method
 * @constructor
 */
function ButtonJsonAct(dom, eid, data, method) {
    var data = method();
    var action = $(dom).attr("data-action");
    eid.Act(action, data, GNav.Prepare());
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @param method
 * @constructor
 */
function ButtonJsonPushAct(dom, eid, data, method) {
    var data = method();
    var action = $(dom).attr("data-action");

    eid.Act(action, data, GNav.Prepare(), "htmlClass", "widgetMessages")
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @param method
 * @constructor
 */
function ButtonAct(dom, eid, data, method) {
    ButtonJsonAct(dom, eid, data, method);
}


/**
 *
 * @param dom
 * @param eid
 * @param data
 * @param method
 * @constructor
 */
function ButtonJsonModalCloseModalAct(dom, eid, data, method) {
    var data = method();
    var action = $(dom).attr("data-action");

    Wnd.Controller = eid.Controller;
    Wnd.Close();
    Wnd.Load(action, data, GNav.Prepare());
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonModalClose(dom, eid, data) {
    Wnd.Close();
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonModalSecondClose(dom, eid, data) {
    Wnd.CloseSecond();
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonTableAct(dom, eid, data) {
    var action = $(dom).attr("data-action");
    var confirmHtml = $(dom).attr("data-confirm");
    var dataPack = TableMassGetSelected(data);

    if ($(dom).attr("data-confirm")) {
        if (confirm($(dom).attr("data-confirm"))) {
            eid.Act(action, dataPack, GNav.Prepare());
        }
    }
    else {
        eid.Act(action, dataPack, GNav.Prepare());
    }
}


/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonTableModalAct(dom, eid, data) {
    var action = $(dom).attr("data-action");
    var confirmHtml = $(dom).attr("data-confirm");
    var dataPack = TableMassGetSelected(data);

    if ($(dom).attr("data-confirm")) {
        if (confirm($(dom).attr("data-confirm"))) {

            Wnd.Controller = eid.Controller;
            Wnd.Close();
            Wnd.Load(action, dataPack, GNav.Prepare());
        }
    }
    else {
        Wnd.Controller = eid.Controller;
        Wnd.Close();
        Wnd.Load(action, dataPack, GNav.Prepare());
    }
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonPath(dom, eid, data) {
    var path = $(dom).attr("data-action");

    if ($(dom).attr("data-confirm")) {
        eid.Link({'path': path});
    }
    else {
        eid.Link({'path': path});
    }
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonDataPath(dom, eid, data) {
    ButtonDataAct(dom, eid, data);
};


/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonDataAct(dom, eid, data) {
    var action = $(dom).attr("data-action");

    if ($(dom).attr("data-confirm")) {
        if (confirm($(dom).attr("data-confirm"))) {
            eid.Act(action, data, GNav.Prepare());
        }
    }
    else {
        eid.Act(action, data, GNav.Prepare());
    }
}

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonDataPath(dom, eid, data) {
    ButtonDataAct(dom, eid, data);
};

/**
 *
 * @param dom
 * @param eid
 * @param data
 * @constructor
 */
function ButtonModalAct(dom, eid, data) {

    if ($(dom).attr("data-confirm")) {
        if (confirm($(dom).attr("data-confirm"))) {
            var action = $(dom).attr("data-action");
            Wnd.Controller = eid.Controller;
            Wnd.Load(action, data, GNav.Prepare());
        }
    }
    else {
        var action = $(dom).attr("data-action");
        Wnd.Controller = eid.Controller;
        Wnd.Load(action, data, GNav.Prepare());
    }

}

/**
 *
 * @param dom
 * @param data
 * @constructor
 */
function ButtonModalSecondAct(dom, data) {
    var action = $(dom).attr("data-action");
    Wnd.LoadSecond(action, data);
}

