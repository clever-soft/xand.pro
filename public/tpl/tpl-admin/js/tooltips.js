//----------------------------------------------------------------------
//- Tooltips
//----------------------------------------------------------------------

function TooltipUpdateOk(dom) {
    var data = $(dom).prev().prev().val();
    var key = $(dom).parent().prev().attr("data-key");
    var packData = $.toJSON({"tipKey": key, "text": data, "de_DE": data});

    $.ajax({
        type: 'POST',
        cache: false,
        url: '/mod/crm/widgets/widget.tooltips.php',
        dataType: 'html',
        data: 'ACT=UpdateFinishByKey&DATA=' + packData,
        success: function (html) {
            $(dom).parent().prev().html(data);
            $(dom).parent().prev().show(0);
            $(dom).parent().parent().removeClass("editMode");
            $(dom).parent().remove();
        }
    });

}

//----
function TooltipUpdateCancel(dom) {
    $(dom).parent().prev().show(0);
    $(dom).parent().parent().removeClass("editMode");
    $(dom).parent().remove();
}

//----
function TooltipUpdateReady(dom) {

    var child = $(dom);

    if (!child.parent().hasClass("editMode")) {
        child.hide(0);
        child.parent().addClass("editMode");
        var data = child.html();
        var actions = "<div style='padding-top:5px'></div>";
        actions += "<button class='btn btn-small btn-primary' onclick='TooltipUpdateOk(this); return false;'>OK</button>";
        actions += "<button class='btn btn-small' onclick='TooltipUpdateCancel(this); return false;'>Cancel</button>";


        child.parent().append("<div class='ddBodyEdit'><textarea style='width: 250px; height: 150px;'>" + data + "</textarea>" + actions + "</div>");
    }

    /*
     {
     $(dom).removeClass("editMode");

     */


}

//----
function TooltipsDialog(dom) {
    var data;
    if ($(dom).hasClass("leftSubMenuExtSelect")) {
        data = "&DATA=0";
    }
    else {
        data = "&DATA=1";
    }

    $.ajax({
        type: 'POST',
        cache: false,
        url: '/mod/crm/widgets/widget.settings.php',
        dataType: 'html',
        data: 'ACT=Tooltips' + data,
        success: function (html) {
            location.reload();
        }
    });

}


//-------------
//- GEO DD
//-------------

TTCurrentGeoDom = null;

function TooltipGeoIp(dom, backend, ip) {

    TTCurrentGeoDom = $(dom).parent();

    var qty = TTCurrentGeoDom.find(".body").length;

    if (qty == 0) {

        var html =
            "<div class='body'>" +
            "<div class='content'>" +
            "<div class='contentData'>" +
            "Loading..." +
            "</div>" +
            "</div>";

        TTCurrentGeoDom.append(html);


        $.ajax({

            url: backend + "?ip=" + ip+"&city=1&countryName=1",
            cache: true,
            dataType: "JSON",
            async: true,
            success: function (data) {

                console.log(data);

                var ip = data.ip, html = "";

                if (data.status) {

                    for (var i in data) {
                        keyNormal = i;

                        if (keyNormal == "status")         continue;
                        if (keyNormal == "countryCode")    continue;

                        keyNormal = keyNormal.replace("countryName", "Country");
                        keyNormal = keyNormal.replace("regionName", "Region");
                        keyNormal = keyNormal.replace("zip", "ZIP");
                        keyNormal = keyNormal.replace("isp", "ISP");
                        keyNormal = keyNormal.replace("ip", "IP");
                        keyNormal = keyNormal.replace("city", "City");
                        keyNormal = keyNormal.replace("latitude", "Latitude");
                        keyNormal = keyNormal.replace("longitude", "Longitude");
                        keyNormal = keyNormal.replace("method", "Method");

                        html += "<div class='geoTC'><strong>" + keyNormal + "</strong><div>" + data[i] + "</div></div>";
                    }

                    TTCurrentGeoDom.find(".contentData").html(html);
                }
                else {
                    TTCurrentGeoDom.find(".contentData").html("No data for this IP: " + ip);
                }
            }
        });


    }
    else {
        TTCurrentGeoDom.find(".body").remove();
    }

}


//-------------
//- Deprecated
//-------------

function DropDownShow(dom) {
    $(".dropDownBody").css("display", "none");
    if ($(dom).hasClass("dropDownHide")) {
        $(dom).removeClass("dropDownHide");
        $(dom).find(".dropDownBody").css("display", "block");
    }
    else {
        $(dom).addClass("dropDownHide");
        $(dom).find(".dropDownBody").css("display", "none");
    }
}
