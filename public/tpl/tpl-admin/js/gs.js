//----------------------------------------------------------------------
//- global Search Api
//----------------------------------------------------------------------


blockFilled                 = false;
globalSearchEvent           = null;
globalSearchBackend 	    = "/mod/system/backends/backend.gs.php";
globalSearchStateBackend    = "/mod/system/backends/backend.gs_state.php";
slock 			            = false;
globalSearchNavMode         = false;
globalSearchNavIndex        = -1;

function globalSearchOnFocus()
{
    $(".searchRow").removeClass("searchRowFocus");
    globalSearchNavMode = false;
    $("#globalSearch").addClass("globalSearchWide");

}
/**
 *
 * @param e
 */
function globalSearchKeyDown(e)
{

    if(e.keyCode == 40 || e.keyCode == 38)
    {
        if(globalSearchNavMode == false)
        {
            globalSearchNavMode  = true;
            globalSearchNavIndex = -1;
        }

        $(".searchRow").removeClass("searchRowFocus");

        if(e.keyCode == 38) globalSearchNavIndex--;
        if(e.keyCode == 40) globalSearchNavIndex++;

        if(globalSearchNavIndex > -1 && globalSearchNavIndex < $(".searchRow").size())
        {
            $(".searchRow").eq(globalSearchNavIndex)
                .addClass("searchRowFocus")
                .focus();
        }
        else
        {
            globalSearchOnFocus();
            $("#globalSearch").focus();
        }

        e.preventDefault();
    }
}

function globalSearchKeyUp(e)
{

    $("#globalSearch").addClass("globalSearchWide");

    if(e.keyCode > 36 &&  e.keyCode < 41)
    {
        e.preventDefault();
        return;
    }

    if(globalSearchEvent != null) globalSearchEvent();
    else
    {
        if( $("#globalSearch").val() == "")
        {
            globalSearchClose();
            return;
        }

        if(slock) return;
        slock = true;

        $.ajax({
            type:	   "POST",
            data:     "PATH="+$("#globalSearch").val(),
            url: 	    globalSearchBackend,
            dataType:  "html",
            beforeSend: function(html)
            {
            },
            success: function(html)
            {
                $("#globalSearchOutWrap").css("display", "block");
                $("#globalSearchOutWrap").html(html);
                slock = false;
            }
        });
    }
    $("#globalSearch").removeClass("globalSearchEmpty");
    if($("#globalSearch").val() == "")	$("#globalSearchOutWrap").css("display", "none");

}

function globalSearchGo(idSearch)
{
    $.ajax({
        type:	    "POST",
        data:      "DATA="+idSearch,
        url: 	    globalSearchStateBackend,
        dataType:  "html",
        beforeSend: function(html)
        {
        },
        success: function(html)
        {
            $("#adminContent").append(html);
            html = html.replace('#', '');
            AdminAjax.Go(html);

            var hash_menu = html;
            var arr 	  = hash_menu.split(/\//);
            var idMenu    = "#m_"+arr[1];
            $("#adminMenuWrap").find("a").removeClass("adminMenuSelect").addClass("adminMenuUnSelect");
            $(idMenu).removeClass("adminMenuUnSelect").addClass("adminMenuSelect");

            globalSearchDisableClose();
        }
    });

}

function globalSearchClose()
{
    $("#globalSearchOutWrap").css("display", "none");
}

function globalSearchDisableClose()
{
    //.attr("disabled", "disabled")
    $("#globalSearch").val("");
    $("#globalSearchOutWrap").css("display", "none");
}

function globalSearchNoFind()
{
    $("#globalSearch").addClass("globalSearchEmpty");
    globalSearchClose();
}