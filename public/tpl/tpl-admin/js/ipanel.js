//----------------------------------------------------------------------
//- IPanel
//----------------------------------------------------------------------

ipCounterData = [];

//----
function LongPoolTimeout(){
    setTimeout(function(){
        LongPool();
    },  6000);
};

//----
function HighlightInfoPanelCounter(domId)
{

    var interval    = 300;
    var interval2   = 300;

    setTimeout(function(){
        $(domId).hide(0);
        setTimeout(function(){
            $(domId).show(0);

            var repeat = parseInt($(domId).attr("data-repeat"));
            repeat = repeat - 1;

            if(repeat > 0)
            {
                $(domId).attr("data-repeat", repeat);
                HighlightInfoPanelCounter(domId);
            }

        }, interval2);
    }, interval);
}

//----
function LongPool()
{
    $.ajax({ url: "/mod/crm/backends/backend.lp.php",
        global: false,
        cache: false,
        async: true,
        success: function(data)
        {

            if($(".interactivePanelLoading").length > 0)
                $(".interactivePanelLoading").remove();

            var allowDataTypes = ["testimonials", "calls", "tickets", "tasks", "dealers"];


            for(var i in data.counters)
            {
                $("#ip_"+i).html(data.counters[i]);

                if(ipCounterData[i] !== undefined)
                {
                    if(ipCounterData[i] != data.counters[i])
                    {
                        var diff = data.counters[i] - ipCounterData[i];
                        var sigh = (diff > 0) ? "+" : "";

                        if(allowDataTypes.indexOf(i)  >= 0 )
                        {
                            if(diff != 0) $("#ip_"+i+"_diff").html(sigh+diff);
                            else 	 	  $("#ip_"+i+"_diff").html("");

                            $("#ip_"+i+"_diff").attr("data-repeat", 2);
                            HighlightInfoPanelCounter("#ip_"+i+"_diff");


                        }
                    }
                    else
                    {
                        // ip_calls_diff
                    }
                }

                ipCounterData[i] = data.counters[i];

                if(allowDataTypes.indexOf(i)  >= 0 )
                {

                    if(data.counters[i] > 0)  $("#ip_"+i).css("color", "red");
                    else  				      $("#ip_"+i).css("color", "#444444");
                }

                if(data.counters.dealers_access == false)
                {
                    $("#ip_dealers").removeAttr("href").css("color", "#aaa").css("text-decoration", "none");
                    $("#ip_dealers_my").css("color", "#aaa");
                }
                else
                {
                    $("#ip_dealers").attr("href", "#mod/support/dealers").css("text-decoration", "underline");
                    $("#ip_dealers_my").css("color", "#333");
                }

                //console.log(ipCounterData[i]);

            }

            $("#wt_work").html(data.timing["work"]);
            $("#wt_plan").html(data.timing["plan"]);

            var balanceInt = parseInt(data.timing["balanceSec"]);
                   sigh    = (balanceInt > 0) ? "+" : "";
            var hlRed      = (balanceInt < 0) ? " style='color:red;'" : " style='color:#1E8EF1;'";

            $("#wt_balance").html("<span "+hlRed+">"+sigh + data.timing["balance"]+"</span>");


            for(var i in data.subtasks)
            {
                var type = data.subtasks[i]["type"];
                $("#ip_subtasks_"+type).html(data.subtasks[i]["qty"]);
            }

            for(var i in data.messages)
            {
                Popup.Push(data.messages[i][0], data.messages[i][1], data.messages[i][2]);
            }

            LongPoolTimeout();


        }, dataType: "json"});
}




