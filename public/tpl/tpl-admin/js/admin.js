/**
 *
 * @type {boolean}
 */
tabsClickBlock = false;

/**
 *
 * @param dom
 */
function tabsClick(dom) {
    globalSearchDisableClose();
    if (tabsClickBlock) return;
    var elem = $(dom);
    var href = elem.attr('href');
    href = href.replace('#', '');

    var arr = href.split(/\//);
    var idMenu = "#m_" + arr[1];

    $("#adminMenuWrap").find("a").removeClass("adminMenuSelect").addClass("adminMenuUnSelect");
    $(idMenu).removeClass("adminMenuUnSelect").addClass("adminMenuSelect");


    AdminAjax.Go(href);
};

/**
 *
 * @constructor
 */
function AdminMenu() {
    this.data = [];

    /**
     *
     * @param dom
     * @constructor
     */
    this.Click = function (dom) {
        //globalSearchDisableClose();
        var elem = $(dom);
        var href = elem.attr('href');
        href = href.replace('#', '');

        AdminAjax.Go(href);
        CAdminMenu.MenuColorize();

    };

    /**
     *
     * @param url
     * @constructor
     */
    this.Go = function (url) {

        AdminAjax.Go(url);
        CAdminMenu.MenuColorize();

    };


    /**
     *
     * @param menu
     * @constructor
     */
    this.Resize = function (menu)  //node1, node2, title, url
    {
        var winWidth = parseInt($(window).width());
        if ($("#adminSlider").hasClass("adminSliderActive"))
            winWidth = winWidth - 200;


        if (winWidth < 800) {

            $(".adminMenuWrapRight").hide();
            $("#adminMenuMessages").hide();
            $("#mobileMenu").show();
            this.MakeMenu("mobile");
        }
        else {
            $(".adminMenuWrapRight").show();
            $("#adminMenuMessages").show();
            $("#mobileMenu").hide();
            this.MakeMenu("default");
        }


    };

    /**
     *
     * @constructor
     */
    this.MenuColorize = function () {

        //globalSearchDisableClose();

        var hash_menu = AdminAjax.Hash;
        var path = hash_menu.split(/\//);
        var module = path.shift();

        $("#mainMenu").children().find("a").removeClass("adminMenuSelect").addClass("adminMenuUnSelect");
        $("#atm-" + module).removeClass("adminMenuUnSelect").addClass("adminMenuSelect");

        $("#mobileMenu").children().find("a").removeClass("leftSubMenuItemActive");
        $("#atmm-" + module).addClass("leftSubMenuItemActive");

        CAdminMenu.MakeSubMenu(module, path);

        if ($("#mobileMenu .leftSubMenuItemActive").length > 0) {
            $("#subMenuTitle").html($("#mobileMenu .leftSubMenuItemActive").html());
        }

        if ($(".adminMenuSelect").length > 0) {
            $("#subMenuTitle").html($(".adminMenuSelect").html());
        }


    };

    /**
     *
     * @param module
     * @param path
     * @constructor
     */
    this.MakeSubMenu = function (module, path) {

        var menuContent = "";

        for (var i in this.data) {
            if (this.data[i]["url"] == module) {


                for (var g in this.data[i]["access"]) {

                    var group = this.data[i]["access"][g];

                    if (g != "_common") {

                        menuContent += "<h4><i style='font-size: 11px;' class='fa fa-arrow-down'></i>" + group[0] + "</h4><hr/>";

                        for (var w in group[2]) {

                            var active = (w == path[1] && path[0] == group[1]) ? "leftSubMenuItemActive" : "";

                            menuContent += "<a href='#" + this.data[i]["url"] + "/" + group[1] + "/" + w + "' " +
                                "onclick='CAdminMenu.Click(this); return false;' class='leftSubMenuItem " + active + "'>"
                                + group[2][w] + "</a>";

                        }

                    }
                    else {

                        for (var w in group) {

                            var active = (w == path[0]) ? "leftSubMenuItemActive" : "";

                            menuContent += "<a href='#" + this.data[i]["url"] + "/" + w + "' " +
                                "onclick='CAdminMenu.Click(this); return false;' class='leftSubMenuItem " + active + "'>"
                                + group[w] + "</a>";

                        }

                    }
                }

            }
        }

        $("#leftSubMenu").html(menuContent);

        // if (noChooseLevel2)
        //  $("#leftSubMenu > .leftSubMenuItem:first").addClass("leftSubMenuItemActive");

    };

    /**
     *
     * @param menu
     * @returns {AdminMenu}
     * @constructor
     */
    this.Load = function (menu) {
        this.data = menu;
        return this;
    };

    /**
     *
     * @param type
     * @constructor
     */
    this.MakeMenu = function (type)  //node1, node2, title, url
    {

        $("#mainMenu > ul").html("");

        if (type == "mobile") {

            var menuContent = "";

            for (var i in this.data) {

                var firstUrl = "";
                for (var g in this.data[i]["access"]) {
                    var group = this.data[i]["access"][g];

                    if (g != "_common") {
                        for (var w in group[2]) {

                            if (!firstUrl)
                                firstUrl = this.data[i]["url"] + "/" + group[1] + "/" + w;
                        }
                    }
                    else {
                        for (var w in group) {
                            if (!firstUrl)
                                firstUrl = this.data[i]["url"] + "/" + w;

                        }
                    }
                }

                menuContent +=
                    "" +
                    "<a href='#" + firstUrl + "' id='atmm-" + this.data[i]["url"] + "' " +
                    "onclick='CAdminMenu.Click(this); return false;' class='leftSubMenuItem'>" + this.data[i]["name"] + "</a>" +
                    "";
            }

            $("#mobileMenuBody").html(menuContent);

        }
        else {

            for (var i in this.data) {

                var firstUrl = "";
                var menuContent = "";

                for (var g in this.data[i]["access"]) {

                    var group = this.data[i]["access"][g];

                    if (g != "_common") {
                        menuContent += "<li class='mainMenuSeparator'><i class='fa fa-arrow-down'></i> " + group[0] + "</li>";

                        for (var w in group[2]) {

                            menuContent += "<li>" +
                                "<a href='#" + this.data[i]["url"] + "/" + group[1] + "/" + w + "' onclick='CAdminMenu.Click(this); return false;'>"
                                + group[2][w] +
                                "</a></li>";

                            if (!firstUrl)
                                firstUrl = this.data[i]["url"] + "/" + group[1] + "/" + w;
                        }
                    }
                    else {
                        for (var w in group) {

                            menuContent += "<li>" +
                                "<a href='#" + this.data[i]["url"] + "/" + w + "' onclick='CAdminMenu.Click(this); return false;'>"
                                + group[w] +
                                "</a></li>";

                            if (!firstUrl)
                                firstUrl = this.data[i]["url"] + "/" + w;

                        }
                    }
                }

                menuContent = "<li class='adminTopMenu'>" +
                    "<a href='#" + firstUrl + "' id='atm-" + this.data[i]["url"] + "' onclick='CAdminMenu.Click(this); return false;'>" + this.data[i]["name"] + "</a>" +
                    "<ul>" +
                    menuContent +
                    "</ul>" +
                    "</li>";


                $("#mainMenu > ul").append(menuContent);
            }


        }
    }
}

/**
 *
 * @constructor
 */
function AdminSliderActive() {

    $("#adminSlider").css("padding-left", "230px").addClass("adminSliderActive");
    $(".leftMenuHidden").css("display", "block");
    $("#adminHead").css("margin-left", "-230px");
    $("#leftSubMenuWrap").show();
    $.cookie('sliderLeftView', "show");
}

/**
 *
 * @constructor
 */
function AdminSliderInActive() {
    $(".leftMenuHidden").css("display", "none");
    $("#adminHead").css("margin-left", "0px");
    $("#adminSlider").css("padding-left", "0px").removeClass("adminSliderActive");
    $("#leftSubMenuWrap").hide();
    $.cookie('sliderLeftView', "hide");
}

/**
 *
 * @constructor
 */
function AdminSlider(force, position) {

    if (force) {
        if (position == "show") {
            AdminSliderActive();
        }
        else {
            AdminSliderInActive();
        }
    }
    else {
        if ($("#adminSlider").hasClass("adminSliderActive"))
            AdminSliderInActive();
        else
            AdminSliderActive();
    }


}

/**
 *
 * @param dom
 * @constructor
 */
function AdminUserChange(dom) {
    //AdminAjax.Act("UserChange", $(dom).val());

    $.ajax({
        type: "GET",
        cache: false,
        url: $(dom).val(),
        global: true,
        success: function (html) {
            $("body").append(html);
            AdminAjax.HashSet('dashboard');
            setTimeout(function () {
                window.location.reload();
            }, 100);
        }
    });

};

/**
 *
 * @param e
 * @returns {boolean}
 * @constructor
 */
function CheckKeyDown(e) {
    if ($('.formBaseButtons').length == 0) {
        if (e.keyCode == 9) {
            AdminSlider();
            CAdminMenu.Resize();
            e.stopPropagation();
            return false;
        }
    }
}

function GoToUpdate(dom) {
    if (typeof location.origin === 'undefined')
        location.origin = location.protocol + '//' + location.host;
    var hreff = dom.data('href');
    hreff = location.origin + hreff;

    window.location.href = hreff;
    location.reload();
}



