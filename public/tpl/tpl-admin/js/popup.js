//----------------------------------------------------------------------
//- CPopup
//----------------------------------------------------------------------

function CPopup()
{
	this.cache  = {};
	this.height = 0;

	//---
	this.Remove = function(dom)
	{
		var pid			= $(dom).parent().parent().attr("id");
		this.cache[pid] = false;

        //console.log(pid);
        //console.log(this.cache);

		this.Refresh();
		//$(dom).remove();

	};

	//---
	this.Refresh = function()
	{
		 var tmp = this.cache;
		 this.cache  = {};
		 this.height = 0;

		 $(".popupBox").remove();

		 for(var t in tmp)
		 {
			if(tmp[t]){

                if(tmp[t][0] == 0){
                    this.Push(tmp[t][1], tmp[t][2], tmp[t][3], true);
                }

                if(tmp[t][0] == 1){
                    this.PushCall(tmp[t][1], tmp[t][2], tmp[t][3], true);
                }

            }
		 }

	};

    //---
    this.LoadUserInfo = function(idCustomer)
    {
        alert("Method is not ready...");
    };


    //---
    this.GetCallBody = function(data)
    {

        var content = "";
        data.phone                = (data.phone) ? data.phone : "Not found";
        data.idCustomer           = (data.idCustomer)   ? data.idCustomer  : 0;
        data.customerGroupClass   = (data.customerGroupClass)  ? data.customerGroupClass  : "popupBodyCustomerGuest";
        data.countryCode          = (data.countryCode)  ? data.countryCode  : "unknown";

        content += "<div class='popupBodyCL'>";
        content += "<i class='flag flag-16 flag-"+data.countryCode+" popupBodyFlag'></i>";
        content += "<a class='popupBodyCustomer "+data.customerGroupClass+"' href='' " +
                   "onclick='Popup.LoadUserInfo("+data.idCustomer+"); return false;'>";
        content += "<i class='fa fa-user'></i>";
        content += "</a>";
        content += " </div>";


        content += "<div class='popupBodyCR'>";

        content += "<p><b>Phone:</b>"+data.phone+"</p>";

        content += (data.name)    ? "<p><b>Name:</b>"    + data.name    + "</p>" : "";
        content += (data.email)   ? "<p><b>E-mail:</b>"  + data.email   + "</p>" : "";
        content += (data.address) ? "<p><b>Address:</b>" + data.address + "</p>" : "";

        content += "</p>";
        content += "</div>";

        return content;
    };


    //---
    this.SetCallData = function(uid, data)
    {
        if(! this.cache['pm'+uid])
            return;

        this.cache['pm'+uid][2] = data;
        var content = this.GetCallBody(data);
        $("#pm"+uid).find(".popupBody").html(content);
    };


    //---
    this.PushCall = function(title, data, uid, notAnimate)
    {
        pid = 'pm'+uid;

        if(this.cache[pid])
            return;

        this.cache[pid] = [1, title, data, uid];
        var subTitle    = (data.subTitle) ? "<span>"+data.subTitle+"</span>" : "";
        var contentBody = this.GetCallBody(data);

        var content   = "<div class='popupBox' id='"+pid+"'>";
            content  += "<i class='popupBoxType fa fa-phone'></i>" +
                        "<h3>"+title+subTitle+"<i class='fa fa-times' onclick='Popup.Remove(this);  return false;'></i></h3>";
            content   += "<div class='popupBody'>";
            content   += contentBody;
            content   += "</div>";
            content   += "</div>";

        $('body').append(content);

        this.Animate(pid, notAnimate);

        return uid;

    };

	//---
	this.Push = function(title, data, uid, notAnimate)
	{
		pid = 'pm'+uid;

		if(this.cache[pid])
			return;

		this.cache[pid] = [0, title, data, uid];

		var content  = "<div class='popupBox' id='"+pid+"'>";
			content += "<i class='popupBoxType fa fa-info-circle'></i>" +
                       "<h3>"+title+"<i class='fa fa-times' onclick='Popup.Remove(this); return false;'></i></h3>";

			content += data;
			content += "</div>";

		$('body').append(content);

        this.Animate(pid, notAnimate);

	};


    //---
    this.Animate = function(pid, notAnimate)
    {
        var offset = (this.height == 0) ? 10 : 35;
        var bottom = this.height + offset;
        var currH  = parseInt($("#"+pid).css("height"));
        currH += offset;

        this.height += currH;

        if(notAnimate)
        {
            $('#' + pid).css("bottom", bottom + "px");
        }
        else
        {
            $('#' + pid).animate({bottom: bottom + 'px'}, 1000, function () {});
        }
        //setTimeout(function(){ Popup.Remove($("#"+pid))}, 10000);

    };

}

Popup = new CPopup;



/* Extended functions */

//---
function MessageClick(dom) {
    tabsClick(dom);
    $(dom).remove();
}

//---
function PushMessage(mess, css, href) {
    if (!$("#adminMenuMessages").hasClass("." + css)) {
        css = "adminMenu adminMenuMessage " + css;
        $("#adminMenuMessages").append("<a href='#" + href + "' class='" + css + "' onclick='MessageClick(this); return false;'>" + mess + "</a>");
    }
}