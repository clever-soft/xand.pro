<?php
include_once("../../../../../lib/class.Xand.php");
if (!isset($_SESSION[AUTH_KEY])) exit("Access denied");

?>
<!DOCTYPE html>
<html>
<head>
    <title>File Manager</title>
</head>
<body class="fmBody">
<table border="0" style="width:100%; height:100%">
    <tr>
        <td align="center">
            <?php

            $FileManager = new FileManager();
            print $FileManager->create();

            ?>
        </td>
    </tr>
</table>
</body>
</html>