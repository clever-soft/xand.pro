<?php
$version = (isset($_POST['version'])) ? "v" . htmlspecialchars($_POST['version']) : "v4.0";
$error["error"] = "Service Unavalible";
$ret = json_encode($error);
$base_path = "/var/www/vip/data/www/framework.xand.pro/public/";


if ($version) {
    $version = explode(".", $version);
    $v = $base_path . $version[0] . "." . $version[1];

    if (is_dir($v)) {

        $arrPacksFiles = scandir($v);
        array_shift($arrPacksFiles);
        array_shift($arrPacksFiles);
        $endArr = [];
        foreach ($arrPacksFiles as $arr) {
            $tmp = explode("-", $arr);
            $namePack = array_shift($tmp);
            $arrWithoutName = implode("-", $tmp);
            $tmp = explode(".", $arrWithoutName);
            $ext = array_pop($tmp);
            if ($ext != "zip") continue;
            $tmpKey = array_search("v", $tmp);

            $tmpName = implode(".", array_slice($tmp, 0, $tmpKey));
            $tmpVersion = implode(".", array_slice($tmp, $tmpKey+1));
            if ($namePack == "full")
                $endArr[$namePack][] = $tmpVersion;
            else
                $endArr[$namePack][$tmpName][] = $tmpVersion;
        }
        $end = json_encode($endArr);
        exit($end);
    } else {
        exit($ret);
    }
} else {
    exit($ret);
}